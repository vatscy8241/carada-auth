﻿using CaradaAuthProvider.Api.Controllers.Results;
using CaradaAuthProvider.Api.Exceptions;
using CaradaAuthProvider.Api.Exceptions.Enums;
using CaradaAuthProvider.Api.Properties;
using Mti.Cpp.Logging;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;

namespace CaradaAuthProvider.Api.Filters
{
    /// <summary>
    /// エラーメッセージ作成用のフィルター。
    /// </summary>
    public class ExceptionMessageFilterAttribute : ExceptionFilterAttribute, IOrderedFilter
    {
        /// <summary>
        /// <see cref="IOrderedFilter.Order"/>
        /// </summary>
        public int order { get; set; }

        /// <summary>
        /// コンストラクタ。
        /// </summary>
        /// <param name="order">実行順序。デフォルトは999。</param>
        public ExceptionMessageFilterAttribute(int order = 999)
            : base()
        {
            this.order = order;
        }

        /// <summary>
        /// <see cref="ExceptionFilterAttribute.OnException(HttpActionExecutedContext)"/>
        /// <param name="actionExecutedContext">HTTP実行コンテキストのアクション</param>
        /// </summary>
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            actionExecutedContext.Response = CreateResponseErrorMessage(actionExecutedContext);

            base.OnException(actionExecutedContext);
        }

        /// <summary>
        /// HTTP応答エラーメッセージを作成する。
        /// </summary>
        /// <param name="actionExecutedContext">Action実行後のHTTPコンテキスト</param>
        /// <returns>Http応答メッセージ</returns>
        private HttpResponseMessage CreateResponseErrorMessage(HttpActionExecutedContext actionExecutedContext)
        {
            var applicationException = actionExecutedContext.Exception as AbstractApplicationException;

            if (applicationException != null)
            {
                try
                {
                    return CreateApplicationErrorResponse(actionExecutedContext, applicationException);

                }
                catch (AttributeNotGivenException)
                {
                    AppLog.Error(new NotImplementedException(string.Format("{0}のメッセージコードに例外アトリビュートが付与されていません。", applicationException.GetErrorCode())), "システム例外が発生しました。");
                    return CreateSystemErrorResponse(actionExecutedContext.Request);
                }
                catch (MessageNotDefinedException)
                {
                    AppLog.Error(new NotImplementedException(string.Format("{0}のメッセージコードがメッセージプロパティに定義されていません。", applicationException.GetErrorCode())), "システム例外が発生しました。");
                    return CreateSystemErrorResponse(actionExecutedContext.Request);
                }
            }

            AppLog.Error(actionExecutedContext.Exception, "システム例外が発生しました。");
            return CreateSystemErrorResponse(actionExecutedContext.Request);
        }

        /// <summary>
        /// 業務例外のエラーメッセージを作成する。
        /// </summary>
        /// <param name="actionExecutedContext">Action実行後のHTTPコンテキスト</param>
        /// <param name="applicationException">エラーコードを返す例外処理の抽象クラスを継承した例外クラス</param>
        /// <returns>HTTP応答メッセージ</returns>
        private HttpResponseMessage CreateApplicationErrorResponse(HttpActionExecutedContext actionExecutedContext, AbstractApplicationException applicationException)
        {
            var exceptionAttribute = GetExceptionOccuredAttribute(actionExecutedContext, applicationException);

            var errorMessage = applicationException.GetErrorMessage();

            // エラーメッセージを指定せずにスローしていた場合、メッセージコードに該当するメッセージをメッセージプロパティから取得する。
            if (string.IsNullOrEmpty(errorMessage))
            {
                errorMessage = GetErrorMessage(applicationException.GetErrorCode());

            }

            AppLog.Error(applicationException, "業務例外が発生しました。");

            var errorMessageResult = new ErrorMessageResult()
            {
                Error = applicationException.GetErrorCode(),
                ErrorDescription = errorMessage
            };

            return actionExecutedContext.Request.CreateResponse((HttpStatusCode)Enum.Parse(typeof(HttpStatusCode), exceptionAttribute.statusCode), errorMessageResult);
        }

        /// <summary>
        /// <see cref="ExceptionOccuredAttribute"/>を取得する。
        /// </summary>
        /// <param name="actionExecutedContext">Action実行後のHTTPコンテキスト</param>
        /// <param name="applicationException">エラーコードを返す例外処理の抽象クラスを継承した例外クラス</param>
        /// <exception cref="AttributeNotGivenException"></exception>
        /// <returns></returns>
        private ExceptionOccuredAttribute GetExceptionOccuredAttribute(HttpActionExecutedContext actionExecutedContext, AbstractApplicationException applicationException)
        {
            var exceptionAttributes = actionExecutedContext.ActionContext.ActionDescriptor.GetCustomAttributes<ExceptionOccuredAttribute>();

            var type = applicationException.GetType();
            if (type == typeof(CommitCaradaApplicationException))
            {
                type = typeof(CaradaApplicationException);
            }
            var exceptionAttribute = (from exception in exceptionAttributes
                                      where exception.exceptionType == type
                                      select exception).SingleOrDefault();

            if (null == exceptionAttribute)
            {
                throw new AttributeNotGivenException();
            }

            return exceptionAttribute;
        }

        /// <summary>
        /// エラーコードからエラーメッセージを取得する。
        /// </summary>
        /// <param name="errorCode">エラーコード</param>
        /// <exception cref="MessageNotDefinedException"></exception>
        /// <returns>エラーメッセージ</returns>
        private string GetErrorMessage(string errorCode)
        {
            var errorMessage = Message.ResourceManager.GetString(errorCode, Message.Culture);
            if (string.IsNullOrEmpty(errorMessage))
            {
                throw new MessageNotDefinedException();
            }
            return errorMessage;
        }

        /// <summary>
        /// システム例外のレスポンスを作成する。
        /// </summary>
        /// <param name="request">HTTP要求メッセージ</param>
        /// <returns>システム例外のレスポンス</returns>
        private HttpResponseMessage CreateSystemErrorResponse(HttpRequestMessage request)
        {
            var errorMessageResult = new ErrorMessageResult()
            {
                Error = ErrorKind.server_error.GetName(),
                ErrorDescription = Message.server_error
            };

            return request.CreateResponse(HttpStatusCode.InternalServerError, errorMessageResult);
        }

        /// <summary>
        /// ExceptionOccuredAttributeが指定されていない場合に投げられる例外。
        /// </summary>
        private class AttributeNotGivenException : Exception
        {
        }

        /// <summary>
        /// アプリケーション例外メッセージの未実装の場合に投げられる例外。
        /// </summary>
        private class MessageNotDefinedException : Exception
        {
        }
    }
}