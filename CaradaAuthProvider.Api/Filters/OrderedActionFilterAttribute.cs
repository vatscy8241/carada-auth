﻿using System.Web.Http.Filters;

namespace CaradaAuthProvider.Api.Filters
{
    /// <summary>
    /// WebAPI用のアクションフィルターの抽象クラス。
    /// </summary>
    public abstract class OrderedActionFilterAttribute : ActionFilterAttribute, IOrderedFilter
    {

        /// <summary>
        /// <see cref="IOrderedFilter.order"/>
        /// </summary>
        public int order { get; set; }

        /// <summary>
        /// 指定されたソート番号でコンストラクトする。
        /// </summary>
        /// <param name="order">ソート番号</param>
        protected OrderedActionFilterAttribute(int order = 100)
        {
            this.order = order;
        }
    }
}