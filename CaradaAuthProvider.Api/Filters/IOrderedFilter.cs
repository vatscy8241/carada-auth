﻿using System.Web.Http.Filters;

namespace CaradaAuthProvider.Api.Filters
{
    /// <summary>
    /// 実行順を制御するフィルターのインターフェース。
    /// </summary>
    public interface IOrderedFilter : IFilter
    {
        /// <summary>
        /// 実行順。デフォルト値100。
        /// 値の小さい順に実行される。
        /// </summary>
        int order { get; set; }
    }
}