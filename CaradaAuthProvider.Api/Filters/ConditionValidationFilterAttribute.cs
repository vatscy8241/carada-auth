﻿using CaradaAuthProvider.Api.Controllers.Results;
using CaradaAuthProvider.Api.Exceptions.Enums;
using CaradaAuthProvider.Api.Properties;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Http.Controllers;

namespace CaradaAuthProvider.Api.Filters
{
    /// <summary>
    /// コンディションの検証フィルター。
    /// </summary>
    public class ConditionValidationFilterAttribute : OrderedActionFilterAttribute
    {
        /// <summary>
        /// メッセージの区切り文字。
        /// </summary>
        private const char MESSAGE_DELIMITER = ',';

        /// <summary>
        /// コンストラクタ。
        /// </summary>
        /// <param name="order">実行順</param>
        public ConditionValidationFilterAttribute(int order)
            : base(order)
        {
        }

        /// <summary>
        /// <see cref="OnActionExecuting"/>
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            base.OnActionExecuting(SetInvalidParameterResponse(actionContext));
        }

        /// <summary>
        /// 無効なパラメーター時のレスポンスをセットする
        /// </summary>
        /// <param name="actionContext">実行するアクションに関する情報</param>
        private HttpActionContext SetInvalidParameterResponse(HttpActionContext actionContext)
        {
            var errorMessageResult = new ErrorMessageResult();

            foreach (var argument in actionContext.ActionArguments)
            {
                // パラメータが渡されていない時。
                // また、パラメータのkeyはあるのに値が指定されていない場合や区切り文字【,】が不正な場合もValueがnullになる。(最後に指定されたkeyに値が指定されていない場合はJsonExceptionになる)
                if (argument.Value == null)
                {
                    errorMessageResult.Error = ErrorKind.invalid_parameter.GetName();
                    errorMessageResult.ErrorDescription = Message.no_parameter;
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, errorMessageResult);
                    return actionContext;
                }
            }

            var modelState = actionContext.ModelState;

            if (modelState.IsValid)
            {
                return actionContext;
            }

            var errorMessageBuilder = new StringBuilder();

            foreach (var modelKey in modelState.Keys)
            {
                // 先頭の文字から【.】までの最短マッチを削除する。ex)condition.body => body
                var key = Regex.Replace(modelKey, @"^.*?\.", "");

                foreach (var error in modelState[modelKey].Errors)
                {
                    // それぞれのプロパティ値の検証の場合はExceptionはnullになる
                    if (error.Exception == null)
                    {
                        errorMessageResult.Error = ErrorKind.invalid_parameter.GetName();
                        errorMessageBuilder = CreateErrorMessage(key, error.ErrorMessage, errorMessageBuilder);
                        errorMessageBuilder.Append(MESSAGE_DELIMITER);
                    }
                    else if (error.Exception is JsonException)
                    {
                        errorMessageResult.Error = ErrorKind.invalid_parameter.GetName();
                        errorMessageResult.ErrorDescription = CreateErrorMessage(key, Message.invalid_json_format);

                        break;
                    }
                    else
                    {
                        // ここまで来ないはず。来た場合はとりあえずJSONフォーマットが不正のエラーメッセージをクライアントへ返す。
                        errorMessageResult.Error = ErrorKind.invalid_parameter.GetName();
                        errorMessageResult.ErrorDescription = CreateErrorMessage(key, Message.invalid_json_format);

                        break;
                    }

                }
            }

            if (errorMessageBuilder.Length > 0)
            {
                errorMessageResult.ErrorDescription = errorMessageBuilder.ToString().TrimEnd(new char[] { MESSAGE_DELIMITER });
            }

            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, errorMessageResult);

            return actionContext;
        }

        /// <summary>
        /// プロパティ名が埋め込まれたエラーメッセージのビルダーを作成する。
        /// </summary>
        /// <param name="key">プロパティ名</param>
        /// <param name="errorMessage">エラーメッセージ</param>
        /// <param name="strBuilder">エラーメッセージのビルダー</param>
        /// <returns>エラーメッセージのビルダー</returns>
        private StringBuilder CreateErrorMessage(string key, string errorMessage, StringBuilder strBuilder)
        {
            return strBuilder.Append(CreateErrorMessage(key, errorMessage));
        }

        /// <summary>
        /// プロパティ名が埋め込まれたエラーメッセージを作成する。
        /// ex)[Uid]値が指定されていません。
        /// </summary>
        /// <param name="key">プロパティ名</param>
        /// <param name="errorMessage">エラーメッセージ</param>
        /// <returns>エラーメッセージ</returns>
        private string CreateErrorMessage(string key, string errorMessage)
        {
            return string.Format(Message.invalid_parameter, key, errorMessage);
        }
    }
}