﻿using System;
using System.Net;

namespace CaradaAuthProvider.Api.Filters
{
    /// <summary>
    /// 例外が発生した場合のアトリビュート。
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
    public class ExceptionOccuredAttribute : Attribute
    {
        /// <summary>
        /// 発生した例外の型。
        /// </summary>
        public Type exceptionType { get; private set; }

        /// <summary>
        /// ステータスコード。
        /// </summary>
        public string statusCode { get; private set; }

        /// <summary>
        /// コンストラクタ。
        /// </summary>
        /// <param name="exceptionType">発生した例外の型</param>
        /// <param name="statusCode">ステータスコード</param>
        public ExceptionOccuredAttribute(Type exceptionType, HttpStatusCode statusCode)
        {
            this.exceptionType = exceptionType;
            this.statusCode = statusCode.ToString();
        }
    }
}