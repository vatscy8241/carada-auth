﻿using CaradaAuthProvider.Api.Properties;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Configuration;
using System.Web.Http.Controllers;

namespace CaradaAuthProvider.Api.Filters
{

    /// <summary>
    /// コントローラーの各アクション実行前後で共通的な処理を実行するフィルター
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class CustomActionFilterAttribute : OrderedActionFilterAttribute
    {

        // APIキーとAPIシークレットのマップ情報
        private static Dictionary<string, string> basicAuthMap;

        /// <summary>
        /// アプリケーションログ
        /// </summary>
        protected static readonly ILogger APP_LOG = LogManager.GetLogger(Settings.Default.LogTargetApiApplication);

        /// <summary>
        /// コンストラクタ。ソート番号はデフォルトで10になる。
        /// </summary>
        public CustomActionFilterAttribute()
            : base(10)
        {
        }

        /// <summary>
        /// 指定されたソート番号でコンストラクトする。
        /// </summary>
        /// <param name="order">ソート番号</param>
        public CustomActionFilterAttribute(int order)
            : base(order)
        {
        }

        /// <summary>
        /// メソッド実行前処理
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            // ヘッダの認証情報の検証
            ValidateResult result = validateAuthorization(actionContext);
            if (!result.Result)
            {
                APP_LOG.Debug(result.Message);
                var errorMessageResult = new
                {
                    Error = "unauthorized",
                    ErrorDescription = result.Message
                };
                HttpResponseMessage response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, errorMessageResult);
                actionContext.Response = response;
            }

        }

        /// <summary>
        /// ヘッダに埋め込まれているBasic認証情報の検証
        /// </summary>
        /// <param name="filterContext">コンテキスト</param>
        /// <returns>検証結果</returns>
        private ValidateResult validateAuthorization(HttpActionContext actionContext)
        {
            try
            {

                AuthenticationHeaderValue authorization = actionContext.Request.Headers.Authorization;

                //　認証情報が取得出来ない場合
                if (authorization == null)
                {
                    return ValidateResult.create(false, "認証情報が取得出来ません。");
                }
                // 認証形式がBasic認証でない場合
                if (authorization.Scheme != "Basic")
                {
                    return ValidateResult.create(false, string.Format("認証方法が不正です。(scheme={0})", authorization.Scheme));
                }
                // 認証情報のフォーマットが不正な場合
                string info = ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(authorization.Parameter));
                string[] infos = info.Split(':');
                if (infos.Length != 2)
                {
                    return ValidateResult.create(false, string.Format("認証情報が不正です。(authorization={0})", info));
                }

                string apiKey = infos[0];
                string apiSecret = infos[1];

                // APIキーが未設定の場合
                if (string.IsNullOrEmpty(apiKey))
                {
                    return ValidateResult.create(false, string.Format("認証情報にAPIキーが設定されていません。。(authorization={0})", info));
                }
                // APIシークレットが未設定の場合
                if (string.IsNullOrEmpty(apiSecret))
                {
                    return ValidateResult.create(false, string.Format("認証情報にAPIシークレットが設定されていません。。(authorization={0})", info));
                }
                // APIキーが存在しない場合
                Dictionary<string, string> basicAuthMap = GetBasicAuthMap();
                string secret = null;
                if (!basicAuthMap.TryGetValue(apiKey, out secret))
                {
                    return ValidateResult.create(false, string.Format("指定されたAPIキーは存在しません。(api_key={0}, apiSecret={1})", apiKey, apiSecret));
                }
                // APIシークレットが一致しない場合
                if (secret != apiSecret)
                {
                    return ValidateResult.create(false, string.Format("指定されたAPIキーとAPIシークレットの組み合わせは許可されていません。(api_key={0}, apiSecret={1})", apiKey, apiSecret));
                }
                return ValidateResult.create(true, null);
            }
            catch (Exception e)
            {
                return ValidateResult.create(false, string.Format("予期せぬエラーが発生しました。(error={0})", e.Message));
            }

        }

        /// <summary>
        ///  Basic認証情報マップを取得
        /// </summary>
        /// <returns>APIキーとAPIシークレットのMAP</returns>
        private Dictionary<string, string> GetBasicAuthMap()
        {
            if (basicAuthMap == null)
            {
                basicAuthMap = new Dictionary<string, string>();
                NameValueCollection basicAuthSettings = (NameValueCollection)WebConfigurationManager.GetSection("basicAuthSettings");
                foreach (string key in basicAuthSettings)
                {
                    string secret = basicAuthSettings[key];
                    basicAuthMap.Add(key, secret);
                }
            }
            return basicAuthMap;
        }

        /// <summary>
        /// 検証結果モデル
        /// </summary>
        private class ValidateResult
        {
            /// <summary>
            /// 検証結果
            /// </summary>
            public bool Result { get; set; }

            /// <summary>
            /// メッセージ
            /// </summary>
            public string Message { get; set; }

            /// <summary>
            /// 検証結果を生成
            /// </summary>
            /// <param name="result"></param>
            /// <param name="message"></param>
            /// <returns></returns>
            public static ValidateResult create(bool result, string message)
            {
                ValidateResult ret = new ValidateResult()
                {
                    Result = result,
                    Message = message
                };
                return ret;
            }
        }

    }
}