REM Script to installation and configuration of advanced logging
REM
REM 2015/02/23 Adding custom field 'X-Forwarded-For' by Shoji
REM 2015/03/02 Changing log-path by Shoji

REM Skip this script execution in the case of local emulator
if "%CONFIG%" == "Local" goto SkipScript

REM Set Variable Parameter --------------------------
REM Global Parameter
setx ROLEROOT %ROLEROOT% /M
setx ROLEDEPLOYMENTID %ROLEDEPLOYMENTID% /M
setx ROLENAME %ROLENAME% /M
setx ROLEINSTANCEID %ROLEINSTANCEID% /M
REM  advancedlogging_log_path
REM   Specify the directory path for the log file that AdvancedLogging outputs.
set advancedlogging_log_path="C:\Resources\Directory\IISAdvancedLogging"
REM -------------------------------------------------

REM Set Constant Parameter --------------------------
set advancedlogging_install_log="C:\Resources\Directory\IISAdvancedLogging\InstallLog.txt"
set advancedlogging_msiexec_log="C:\Resources\Directory\IISAdvancedLogging\MsiInstallLog.txt"
set advancedlogging_msifile_path="%ROLEROOT%\approot\bin\AdvancedLogging\AdvancedLogging64.msi"
SET appcmd_file_path="%windir%\system32\inetsrv\appcmd.exe"
REM -------------------------------------------------

echo Start of the script ---------------------------- >> %advancedlogging_install_log%
echo [START] %date%%time% Script to installation and configuration of Advanced Logging. >> %advancedlogging_install_log%

REM For Debug Information
echo [INFO] advancedlogging_log_path    : %advancedlogging_log_path% >> %advancedlogging_install_log%
echo [INFO] advancedlogging_install_log : %advancedlogging_install_log% >> %advancedlogging_install_log%
echo [INFO] advancedlogging_msiexec_log : %advancedlogging_msiexec_log% >> %advancedlogging_install_log%
echo [INFO] advancedlogging_msifile_path: %advancedlogging_msifile_path% >> %advancedlogging_install_log%
echo [INFO] appcmd_file_path            : %appcmd_file_path% >> %advancedlogging_install_log%
echo;>> %advancedlogging_install_log%

REM Installation of Advanced Logging
echo [INFO] The start of the installation of Advanced Logging. >> %advancedlogging_install_log%
msiexec /i %advancedlogging_msifile_path% /qn /l* %advancedlogging_msiexec_log%
IF ERRORLEVEL 1 (
  goto InstallationHandleError
) ELSE (
  echo [INFO] Successful installation of Advanced Logging. >> %advancedlogging_install_log%
)
echo;>> %advancedlogging_install_log%

REM Configuration of Advanced Logging
echo [INFO] The start of the configuration of Advanced Logging. >> %advancedlogging_install_log%
%appcmd_file_path% set config /section:system.webServer/advancedLogging/server /enabled:"True" /commit:apphost >> %advancedlogging_install_log%
IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
%appcmd_file_path% set config /section:system.applicationHost/sites /siteDefaults.advancedLogging.directory:"%advancedlogging_log_path%" /commit:apphost >> %advancedlogging_install_log%
IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
%appcmd_file_path% set config /section:system.applicationHost/advancedLogging /serverLogs.directory:"%advancedlogging_log_path%" /commit:apphost >> %advancedlogging_install_log%
IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
%appcmd_file_path% list config /section:system.webServer/advancedLogging/server | findstr baseFileName="""AdvancedLog"""
IF ERRORLEVEL 1 (
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog',enabled='true',logRollOption='Schedule',schedule='Daily',publishLogEvent='False']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /logDefinitions.[baseFileName='^%%COMPUTERNAME^%%-Server'].enabled:false /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"fields.[id='X-Forwarded-For',sourceName='X-Forwarded-For',sourceType='RequestHeader']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"fields.[id='X-Forwarded-For2',sourceName='X-Forwarded-For2',sourceType='RequestHeader']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog'].selectedFields.[id='Date-UTC']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog'].selectedFields.[id='Time-UTC']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog'].selectedFields.[id='Site Name']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog'].selectedFields.[id='Server Name']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog'].selectedFields.[id='Server-IP']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog'].selectedFields.[id='Method']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog'].selectedFields.[id='URI-Stem']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog'].selectedFields.[id='URI-Querystring']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog'].selectedFields.[id='Server Port']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog'].selectedFields.[id='UserName']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog'].selectedFields.[id='Client-IP']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog'].selectedFields.[id='Protocol Version']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog'].selectedFields.[id='User Agent']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog'].selectedFields.[id='Cookie']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog'].selectedFields.[id='Referer']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog'].selectedFields.[id='Host']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog'].selectedFields.[id='Status']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog'].selectedFields.[id='Substatus']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog'].selectedFields.[id='Win32Status']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog'].selectedFields.[id='Bytes Sent']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog'].selectedFields.[id='Bytes Received']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog'].selectedFields.[id='Time Taken']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog'].selectedFields.[id='X-Forwarded-For']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
  %appcmd_file_path% set config /section:system.webServer/advancedLogging/server /+"logDefinitions.[baseFileName='AdvancedLog'].selectedFields.[id='X-Forwarded-For2']" /commit:apphost >> %advancedlogging_install_log%
  IF ERRORLEVEL 1 ( goto ConfigurationHandleError )
) ELSE (
  echo [INFO] Already Configuration of Advanced Logging. >> %advancedlogging_install_log%
)
echo [INFO] Successful Configuration of Advanced Logging. >> %advancedlogging_install_log%
echo;>> %advancedlogging_install_log%

REM End: Script Successful Status
echo [COMPLETED] %date%%time% Script to installation and configuration of advanced logging script. >> %advancedlogging_install_log%
echo End of the script ------------------------------ >> %advancedlogging_install_log%
echo;>> %advancedlogging_install_log%
echo;>> %advancedlogging_install_log%
exit 0

REM End: installation of Advanced Logging Error Status
:InstallationHandleError
echo [ERROR] Failed installation of Advanced Logging. >> %advancedlogging_install_log%
goto HandleError

REM End: Configuration of Advanced Logging Error Status
:ConfigurationHandleError
echo [ERROR] Failed Configuration of Advanced Logging. >> %advancedlogging_install_log%
goto HandleError

REM End: Script Error Status
:HandleError
echo [FAILED] %date%%time% Script to installation and configuration of advanced logging script. >> %advancedlogging_install_log%
echo End of the script ------------------------------ >> %advancedlogging_install_log%
echo;>> %advancedlogging_install_log%
echo;>> %advancedlogging_install_log%
exit 0

REM End: Skip this script execution
:SkipScript
exit 0
