@echo off

setx ROLEROOT %ROLEROOT% /M
setx ROLEDEPLOYMENTID %ROLEDEPLOYMENTID% /M
setx ROLENAME %ROLENAME% /M
setx ROLEINSTANCEID %ROLEINSTANCEID% /M

REM create custom-log folder
MD C:\Resources\Directory\IISAdvancedLogging
ICACLS C:\Resources\Directory\IISAdvancedLogging /GRANT Administators:(CI)(OI)F
ICACLS C:\Resources\Directory\IISAdvancedLogging /GRANT Users:(CI)(OI)F

REM create custom-log folder
MD C:\Resources\Directory\ApplicationLog
ICACLS C:\Resources\Directory\ApplicationLog /GRANT Administators:(CI)(OI)F
ICACLS C:\Resources\Directory\ApplicationLog /GRANT Users:(CI)(OI)F

REM create custom-log folder
MD C:\Resources\Directory\AuditLog
ICACLS C:\Resources\Directory\AuditLog /GRANT Administators:(CI)(OI)F
ICACLS C:\Resources\Directory\AuditLog /GRANT Users:(CI)(OI)F


if "%CONFIG%" == "Release" goto exec
exit 0

:exec
UCSAzureStart.exe