﻿using CaradaAuthProvider.Api.Controllers.Conditions;
using CaradaAuthProvider.Api.Domain.Dtos;
using CaradaAuthProvider.Api.Domain.Logics;
using CaradaAuthProvider.Api.Domain.Logics.Impl;
using CaradaAuthProvider.Api.Exceptions;
using CaradaAuthProvider.Api.Exceptions.Enums;
using CaradaAuthProvider.Api.IdentityManagers;
using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Persistence.Storage.Entities;
using CaradaAuthProvider.Api.Properties;
using CaradaAuthProvider.Api.Utils;
using Mti.Cpp.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CaradaAuthProvider.Api.Services
{
    /// <summary>
    /// メールサービス。
    /// </summary>
    public class MailService : AbstractService
    {
        /// <summary>
        /// メールロジック。
        /// </summary>
        protected MailLogic mailLogic;

        /// <summary>
        /// チケットロジック
        /// </summary>
        protected TicketLogic ticketLogic;

        /// <summary>
        /// コンストラクタ。
        /// </summary>
        /// <param name="dbContext">DBコンテキスト</param>
        /// <param name="userManager">アプリケーションユーザーマネージャー</param>
        public MailService(CaradaAuthProviderDbContext dbContext, ApplicationUserManager userManager)
            : base(dbContext, userManager)
        {
            mailLogic = new MailLogicImpl(apiKey);
            ticketLogic = new TicketLogicImpl();
        }

        /// <summary>
        /// メールを送信する。
        /// </summary>
        /// <param name="condition"><see cref="SendMailCondition"/></param>
        public async virtual Task SendMail(SendMailCondition condition)
        {
            // emailを取得する。
            var entity = await userManager.FindByIdAsync(FindUid(condition));
            if (entity == null)
            {
                AppLog.Error($"該当のユーザーが見つかりません。[Uid={condition.Uid}]");
                throw new CaradaApplicationException(ErrorKind.data_mismatch, Message.data_mismatch);
            }
            if (string.IsNullOrEmpty(entity.Email))
            {
                AppLog.Error($"メールアドレスが登録されていません。[Uid={condition.Uid}]");
                throw new CaradaApplicationException(ErrorKind.has_not_email, Message.has_not_email);
            }

            // メールを送る
            mailLogic.SendMailAsync(ConvertDto(condition, entity.Email, MAIL_FROM_ADDRESS, MAIL_FROM_NAME));
        }

        #region private Methods

        /// <summary>
        /// UIDを取得する
        /// </summary>
        /// <param name="condition"><see cref="SendMailCondition"/></param>
        /// <returns>UID</returns>
        private string FindUid(SendMailCondition condition)
        {
            if (string.IsNullOrEmpty(condition.TicketId))
            {
                if (string.IsNullOrEmpty(condition.Uid))
                {
                    AppLog.Error($"UID、チケットIDが両方設定されていません。");
                    throw new CaradaApplicationException(ErrorKind.invalid_parameter, Message.required_uid_or_ticket);
                }
                else
                {
                    return condition.Uid;
                }
            }

            Ticket ticket = ticketLogic.FindTicket(condition.TicketId);
            if (ticket == null)
            {
                APP_LOG.Error("該当のチケットが見つかりません。");
                throw new CaradaApplicationException(ErrorKind.invalid_ticket, Message.invalid_ticket);
            }
            if (!ticket.ProcessedFlag)
            {
                APP_LOG.Error(string.Format("処理が完了していないチケットが指定されました。"));
                throw new CaradaApplicationException(ErrorKind.invalid_ticket, Message.invalid_ticket_not_processed);
            }
            if (ticket.ExpiresDateTime.CompareTo(DateUtilities.UTCNow) < 0)
            {
                APP_LOG.Error(string.Format("チケットの有効期限が切れています。[Uid={0}, Expires={1}]", ticket.Uid, ticket.ExpiresDateTime));
                throw new CaradaApplicationException(ErrorKind.invalid_ticket, Message.invalid_ticket_expired);
            }

            return ticket.Uid;
        }

        /// <summary>
        /// <see cref="MailSendInfoDto"/>に変換する。
        /// </summary>
        /// <param name="condition"><see cref="SendMailCondition"/></param>
        /// <param name="email">宛先のアドレス</param>
        /// <param name="from">送信元アドレス</param>
        /// <returns><see cref="MailSendInfoDto"/></returns>
        private MailSendInfoDto ConvertDto(SendMailCondition condition, string email, string from)
        {
            return ConvertDto(condition.Subject, condition.Body, new List<string> { email }, from);
        }


        /// <summary>
        /// <see cref="MailSendInfoDto"/>に変換する。
        /// </summary>
        /// <param name="condition"><see cref="SendMailCondition"/></param>
        /// <param name="email">宛先のアドレス</param>
        /// <param name="from">送信元アドレス</param>
        /// <param name="fromName">送信元表示名</param>
        /// <returns><see cref="MailSendInfoDto"/></returns>
        private MailSendInfoDto ConvertDto(SendMailCondition condition, string email, string from, string fromName)
        {
            return ConvertDto(condition.Subject, condition.Body, new List<string> { email }, from, fromName);
        }

        /// <summary>
        /// <see cref="MailSendInfoDto"/>に変換する。
        /// </summary>
        /// <param name="subject">件名</param>
        /// <param name="body">本文</param>
        /// <param name="toList">宛先のリスト</param>
        /// <param name="from">送信元アドレス</param>
        /// <returns><see cref="MailSendInfoDto"/></returns>
        private MailSendInfoDto ConvertDto(string subject, string body, List<string> toList, string from)
        {
            return ConvertDto(subject, body, toList, from, null);
        }

        /// <summary>
        /// <see cref="MailSendInfoDto"/>に変換する。
        /// </summary>
        /// <param name="subject">件名</param>
        /// <param name="body">本文</param>
        /// <param name="toList">宛先のリスト</param>
        /// <param name="from">送信元アドレス</param>
        /// <param name="fromName">送信元表示名</param>
        /// <returns><see cref="MailSendInfoDto"/></returns>
        private MailSendInfoDto ConvertDto(string subject, string body, List<string> toList, string from, string fromName)
        {
            return ConvertDto(subject, body, toList, null, null, from, fromName);
        }

        /// <summary>
        /// <see cref="MailSendInfoDto"/>に変換する。
        /// </summary>
        /// <param name="subject">件名</param>
        /// <param name="body">本文</param>
        /// <param name="toList">宛先のリスト</param>
        /// <param name="ccList">CCのリスト</param>
        /// <param name="bccList">BCCのリスト</param>
        /// <param name="from">送信元アドレス</param>
        /// <param name="fromName">送信元表示名</param>
        /// <returns><see cref="MailSendInfoDto"/></returns>
        private MailSendInfoDto ConvertDto(string subject, string body, List<string> toList, List<string> ccList, List<string> bccList, string from, string fromName)
        {
            var dto = new MailSendInfoDto();
            dto.From = from;
            dto.FromName = fromName;
            dto.ToList = toList;
            dto.CcList = ccList;
            dto.BccList = bccList;
            dto.Subject = subject;
            dto.Text = body;
            return dto;
        }

        #endregion
    }
}