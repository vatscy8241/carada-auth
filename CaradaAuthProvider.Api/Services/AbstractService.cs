﻿using CaradaAuthProvider.Api.IdentityManagers;
using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using CaradaAuthProvider.Api.Properties;
using Microsoft.AspNet.Identity.EntityFramework;
using NLog;
using System;
using System.Configuration;

namespace CaradaAuthProvider.Api.Services
{
    /// <summary>
    /// 基底サービス
    /// </summary>
    public abstract class AbstractService : MarshalByRefObject
    {
        /// <summary>
        /// アプリケーションログ
        /// </summary>
        protected static readonly ILogger APP_LOG = LogManager.GetLogger(Settings.Default.LogTargetApiApplication);

        /// <summary>
        /// 監査ログ
        /// </summary>
        protected static readonly ILogger AUDIT_LOG = LogManager.GetLogger(Settings.Default.LogTargetAudit);

        /// <summary>
        /// DBコンテキスト
        /// </summary>
        protected IdentityDbContext<CaradaIdUser> dbContext;

        /// <summary>
        /// Identityマネージャ
        /// </summary>
        protected ApplicationUserManager userManager;

        /// <summary>
        /// 認証コード置き換えキーワード
        /// </summary>
        protected static string VERIFYCODE_REPLACE_KEYWORD
        {
            get
            {
                return Settings.Default.VerifyCodeReplaceKeyword;
            }
        }

        /// <summary>
        /// CARADA ID置き換えキーワード
        /// </summary>
        protected static string CARADAID_REPLACE_KEYWORD
        {
            get
            {
                return Settings.Default.CaradaIdReplaceKeyword;
            }
        }

        /// <summary>
        /// メール送信元アドレス
        /// </summary>
        protected static string MAIL_FROM_ADDRESS
        {
            get
            {
                return Settings.Default.MailFromAddress;
            }
        }

        /// <summary>
        /// メール送信元名称
        /// </summary>
        protected static string MAIL_FROM_NAME
        {
            get
            {
                return Settings.Default.MailFromName;
            }
        }

        /// <summary>
        /// SendGridのAPIKey。
        /// </summary>
        protected readonly string apiKey;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="dbContext">DBコンテキスト</param>
        /// <param name="userManager">Identityマネージャ</param>
        public AbstractService(CaradaAuthProviderDbContext dbContext, ApplicationUserManager userManager)
        {
            this.dbContext = dbContext;
            this.userManager = userManager;
            apiKey = ConfigurationManager.AppSettings["ApiKey"];
        }
    }
}