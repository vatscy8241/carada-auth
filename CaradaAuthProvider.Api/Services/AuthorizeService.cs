﻿using CaradaAuthProvider.Api.Controllers.Conditions;
using CaradaAuthProvider.Api.Domain.Logics;
using CaradaAuthProvider.Api.Domain.Logics.Impl;
using CaradaAuthProvider.Api.Exceptions;
using CaradaAuthProvider.Api.Exceptions.Enums;
using CaradaAuthProvider.Api.IdentityManagers;
using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using CaradaAuthProvider.Api.Persistence.Storage.Entities;
using CaradaAuthProvider.Api.Properties;
using CaradaAuthProvider.Api.Services.Authlete;
using CaradaAuthProvider.Api.Services.Dtos;
using Microsoft.AspNet.Identity;
using Mti.Hc.Authlete.Common.Services;
using System.Collections.Generic;
using System.Linq;

namespace CaradaAuthProvider.Api.Services
{
    /// <summary>
    /// 認可サービス
    /// </summary>
    public class AuthorizeService : AbstractService
    {
        /// <summary>
        /// 代理アクセストークンロジック
        /// </summary>
        protected AgencyAccessTokenLogic agencyAccessTokenLogic;

        /// <summary>
        /// 認証認可サービス（MTI提供ライブラリ）
        /// </summary>
        protected AuthorizationServiceWrapper authorizationService;

        /// <summary>
        /// トークンサービス（MTI提供ライブラリ）
        /// </summary>
        protected TokenServiceWrapper tokenService;

        /// <summary>
        /// クライアントスコープサービス（MTI提供ライブラリ）
        /// </summary>
        protected ClientScopesServiceWrapper clientScopesService;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="dbContext">DBコンテキスト</param>
        /// <param name="userManager">Identityマネージャ</param>
        public AuthorizeService(CaradaAuthProviderDbContext dbContext, ApplicationUserManager userManager) : base(dbContext, userManager)
        {
            this.agencyAccessTokenLogic = new AgencyAccessTokenLogicImpl();
            this.authorizationService = new AuthorizationServiceWrapper();
            this.tokenService = new TokenServiceWrapper();
            this.clientScopesService = new ClientScopesServiceWrapper();
        }

        /// <summary>
        /// 認可
        /// </summary>
        /// <param name="condition">認可APIのリクエストパラメータ</param>
        /// <returns>アクセストークン情報</returns>
        public virtual AuthorizeResultDto Authorize(AuthorizeCondition condition)
        {

            AuthorizeResultDto dto = new AuthorizeResultDto();

            // --------------------------------------------------
            // パラメータの取得
            // --------------------------------------------------

            if (condition == null)
            {
                throw new CaradaApplicationException(ErrorKind.invalid_parameter, Message.no_parameter);
            }

            string clientId = condition.ClientId;
            string uid = condition.Uid;


            if (string.IsNullOrEmpty(clientId))
            {
                APP_LOG.Error("クライアントIDが設定されていません。");
                throw new CaradaApplicationException(ErrorKind.invalid_parameter, Message.invalid_parameter_not_client_id);
            }


            if (string.IsNullOrEmpty(uid))
            {
                APP_LOG.Error("ユーザー一意識別子が設定されていません。");
                throw new CaradaApplicationException(ErrorKind.invalid_parameter, Message.invalid_parameter_not_uid);
            }

            // --------------------------------------------------
            // ユーザー情報取得
            // --------------------------------------------------
            CaradaIdUser user = userManager.FindById(condition.Uid);

            // ユーザー情報が取得出来ない場合はエラー
            if (user == null)
            {
                APP_LOG.Error(string.Format("ユーザー一意識別子が設定されていません。[Uid={0}]", condition.Uid));
                throw new CaradaApplicationException(ErrorKind.data_mismatch, Message.data_mismatch);
            }

            // --------------------------------------------------
            // 認可コードの取得
            // --------------------------------------------------
            AuthorizationServiceResult authorizationServiceResult = authorizationService.GetAuthorizationCode(clientId, uid, FindScopes(clientId));

            // 認可コード取得に失敗した場合はエラー
            if (authorizationServiceResult == null || authorizationServiceResult.HasError())
            {
                if (authorizationServiceResult == null)
                {
                    APP_LOG.Error(string.Format("Authleteライブラリ.認可コード取得処理の結果がnullです。[ClientId={0}, Uid={1}]", clientId, uid));
                    throw new AuthleteException(Message.server_error_authorization_code_acquisition_failure);
                }

                APP_LOG.Error("Authleteライブラリ.認可コード取得処理でエラーが発生しました。[ClientId={0}, Uid={1}, Message={2}]", clientId, uid, authorizationServiceResult.ResultMessage);
                throw new AuthleteException(Message.server_error_authorization_code_acquisition_failure);
            }
            string code = authorizationServiceResult.Code;

            // --------------------------------------------------
            // アクセストークンの取得
            // --------------------------------------------------
            TokenServiceResult tokenResult = tokenService.GetToken(code);

            // アクセストークン取得取得に失敗した場合はエラー
            if (tokenResult == null || tokenResult.HasError())
            {
                if (tokenResult == null)
                {
                    APP_LOG.Error("Authleteライブラリ.アクセストークン取得処理の結果がnullです。[ClientId={0}, Code={1}]", clientId, code);
                    throw new AuthleteException(Message.server_error_access_token_acquisition_failure);
                }

                APP_LOG.Error("Authleteライブラリ.アクセストークン取得処理でエラーが発生しました。[ClientId={0}, Code={1}, Message={2}]", clientId, code, tokenResult.ResultMessage);
                throw new AuthleteException(Message.server_error_access_token_acquisition_failure);
            }

            return dto.Success(tokenResult.AccessToken, tokenResult.RefreshToken);
        }

        /// <summary>
        /// 代理アクセストークンを発行する
        /// </summary>
        /// <param name="condition">代理アクセストークン取得の入力情報クラス</param>
        /// <param name="jwtUser">API実行者識別子</param>
        /// <returns>代理アクセストークン発行結果DTO</returns>
        public virtual IssueAgencyAccessTokenResultDto IssueAgencyAccessToken(IssueAgencyAccessTokenCondition condition, string jwtUser)
        {
            // ユーザ取得
            CaradaIdUser user = userManager.FindById(condition.uid);
            if (user == null)
            {
                APP_LOG.Error(string.Format("該当のユーザーが見つかりませんでした。[Uid={0}]", condition.uid));
                throw new CaradaApplicationException(ErrorKind.data_mismatch, Message.user_not_exist);
            }

            // スコープ取得
            string[] scopes = ToArray(FindScopes(condition.client_id));

            // アクセストークン取得
            TokenServiceResult tokenResult = tokenService.CreateToken(condition.client_id, condition.uid, scopes, condition.expires_minute.Value, condition.expires_minute.Value);
            if (tokenResult == null)
            {
                APP_LOG.Error("Authleteライブラリ.アクセストークン取得処理の結果がnullです。[ClientId={0}, Code={1}]", condition.client_id, tokenResult.ResultCode);
                throw new AuthleteException(Message.server_error_access_token_acquisition_failure);
            }
            if (tokenResult.HasError())
            {
                APP_LOG.Error("Authleteライブラリ.アクセストークン取得処理でエラーが発生しました。[ClientId={0}, Code={1}, Message={2}]", condition.client_id, tokenResult.ResultCode, tokenResult.ResultMessage);
                throw new AuthleteException(Message.server_error_access_token_acquisition_failure);
            }

            // キャッシュ登録
            agencyAccessTokenLogic.RegisterAgencyAccessToken(tokenResult.AccessToken, new AgencyAccessToken
            {
                User = jwtUser
            }, tokenResult.ExpiresAt.Value);

            return new IssueAgencyAccessTokenResultDto
            {
                access_token = tokenResult.AccessToken,
                expires_at = tokenResult.ExpiresAt.Value
            };
        }

        /// <summary>
        /// アクセストークンの検証を行う
        /// </summary>
        /// <param name="condition">アクセストークン検証の入力情報クラス</param>
        /// <returns>アクセストークン検証結果DTO</returns>
        public virtual TokenIntrospectResultDto Introspect(TokenIntrospectCondition condition)
        {
            TokenIntrospectResultDto resultDto = new TokenIntrospectResultDto();

            // キャッシュ取得
            string agent = null;
            AgencyAccessToken agencyAccessToken = agencyAccessTokenLogic.FindAgencyAccessToken(condition.AccessToken);
            if (agencyAccessToken != null)
            {
                agent = agencyAccessToken.User;
            }

            IntrospectTokenServiceResult introspectTokenServiceResult = tokenService.Introspect(condition.AccessToken);
            if (introspectTokenServiceResult == null || introspectTokenServiceResult.HasError())
            {
                if (introspectTokenServiceResult == null)
                {
                    APP_LOG.Error(string.Format("Authleteライブラリ.アクセストークン検証処理の結果がnullです。[AccessToken={0}]", condition.AccessToken));
                    throw new AuthleteException(Message.server_error_access_token_verification_failure);
                }

                APP_LOG.Error("Authleteライブラリ.アクセストークン検証処理でエラーが発生しました。[AccessToken={0}, Message={1}]", condition.AccessToken, introspectTokenServiceResult.ResultMessage);
                throw new AuthleteException(Message.server_error_access_token_verification_failure);
            }

            TokenIntrospectDto dto = new TokenIntrospectDto
            {
                Usable = introspectTokenServiceResult.Usable,
                Uid = introspectTokenServiceResult.Uid,
                ClientId = introspectTokenServiceResult.ClientId,
                Scopes = ToArray(introspectTokenServiceResult.Scopes),
                Refreshable = introspectTokenServiceResult.Refreshable,
                Agent = agent
            };

            if (introspectTokenServiceResult.ExpiresAt.HasValue)
            {
                dto.ExpiresAt = introspectTokenServiceResult.ExpiresAt.Value;
            }
            else
            {
                dto.ExpiresAt = -1;

            }
            resultDto.TokenIntrospect = dto;
            return resultDto;
        }

        /// <summary>
        /// アクセストークンのリフレッシュを行う
        /// </summary>
        /// <param name="condition">トークンリフレッシュの入力情報クラス</param>
        /// <returns>トークンリフレッシュ結果DTO</returns>
        public virtual TokenRefreshResultDto Refresh(TokenRefreshCondition condition)
        {
            TokenRefreshResultDto resultDto = new TokenRefreshResultDto();

            TokenServiceResult tokenResult = tokenService.RefreshToken(condition.RefreshToken);
            if (tokenResult == null)
            {
                APP_LOG.Error(string.Format("Authleteライブラリ.アクセストークンリフレッシュ処理の結果がnullです。[RefreshToken={0}]", condition.RefreshToken));
                throw new AuthleteException(Message.server_error_access_token_refresh_failure);
            }
            if (tokenResult.HasError())
            {
                APP_LOG.Error(string.Format("Authleteライブラリ.アクセストークンリフレッシュ処理でエラーが発生しました。[RefreshToken={0}, Message={1}]", condition.RefreshToken, tokenResult.ResultMessage));
                throw new AuthleteException(Message.server_error_access_token_refresh_failure);
            }

            TokenRefreshDto dto = new TokenRefreshDto
            {
                AccessToken = tokenResult.AccessToken,
                RefreshToken = tokenResult.RefreshToken
            };

            resultDto.TokenRefresh = dto;
            return resultDto;
        }

        /// <summary>
        /// スコープを取得する
        /// </summary>
        /// <param name="clientId">クライアントID</param>
        /// <returns>スコープ</returns>
        private IEnumerable<string> FindScopes(string clientId)
        {
            var result = clientScopesService.GetClientScopes(clientId);
            if (result == null)
            {
                return new string[0];
            }
            return result.RequestableScopes;
        }

        /// <summary>
        /// IEnumerable値を配列に変換する
        /// </summary>
        /// <param name="enumValue">IEnumerable値</param>
        /// <returns>スコープ</returns>
        private T[] ToArray<T>(IEnumerable<T> enumValue)
        {
            if (enumValue == null)
            {
                return new T[0];
            }
            return Enumerable.ToArray(enumValue);
        }
    }
}