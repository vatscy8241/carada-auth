﻿using Mti.Hc.Authlete.Common.Services;
using System;
using System.Collections.Generic;

namespace CaradaAuthProvider.Api.Services.Authlete
{
    /// <summary>
    /// AuthorizationService(AuthleteLib)のラッパークラス
    /// </summary>
    public class AuthorizationServiceWrapper
    {
        /// <summary>
        /// 認証認可サービス（MTI提供ライブラリ）
        /// </summary>
        private AuthorizationService authorizationService;

        /// <summary>
        /// 認可コード取得関数
        /// </summary>
        public Func<string, string, IEnumerable<string>, AuthorizationServiceResult> GetAuthorizationCode;

        /// <summary>
        /// 認可コード取得前チケット取得関数
        /// </summary>
        public Func<string, IEnumerable<string>, AuthorizationServiceResult> ExecuteBeforeProcess;

        /// <summary>
        /// チケット対応認可コード取得関数
        /// </summary>
        public Func<string, string, AuthorizationServiceResult> ExecuteIssueProcess;

        /// <summary>
        /// エラー情報取得関数
        /// </summary>
        public Func<string, string, AuthorizationServiceResult> ExecuteFailProcess;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public AuthorizationServiceWrapper()
        {
            authorizationService = new AuthorizationService();

            GetAuthorizationCode = (clientId, uid, scopes) =>
            {
                return authorizationService.GetAuthorizationCode(clientId, uid, scopes);
            };

            ExecuteBeforeProcess = (clientId, scopes) =>
            {
                return authorizationService.ExecuteBeforeProcess(clientId, scopes);
            };

            ExecuteIssueProcess = (clientId, authelteTicket) =>
            {
                return authorizationService.ExecuteIssueProcess(clientId, authelteTicket);
            };

            ExecuteFailProcess = (authelteTicket, reason) =>
            {
                return authorizationService.ExecuteFailProcess(authelteTicket, reason);
            };
        }
    }
}