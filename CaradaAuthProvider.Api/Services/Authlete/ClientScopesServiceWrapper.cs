﻿using Mti.Hc.Authlete.Common.Dto;
using Mti.Hc.Authlete.Common.Services;
using System;

namespace CaradaAuthProvider.Api.Services.Authlete
{
    /// <summary>
    /// ClientScopesService(AuthleteLib)のラッパークラス
    /// </summary>
    public class ClientScopesServiceWrapper
    {
        /// <summary>
        /// クライアントスコープサービス（MTI提供ライブラリ）
        /// </summary>
        private ClientScopesService clientScopesService;

        /// <summary>
        /// クライアント別要求可能スコープ群取得関数
        /// </summary>
        public Func<string, ClientScopesServiceResult> GetClientScopes;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ClientScopesServiceWrapper()
        {
            clientScopesService = new ClientScopesService();

            GetClientScopes = (clientId) =>
            {
                return clientScopesService.GetClientScopes(clientId);
            };
        }
    }
}