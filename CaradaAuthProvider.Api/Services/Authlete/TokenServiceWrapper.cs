﻿using Mti.Hc.Authlete.Common.Services;
using System;

namespace CaradaAuthProvider.Api.Services.Authlete
{
    /// <summary>
    /// TokenService(AuthleteLib)のラッパークラス
    /// </summary>
    public class TokenServiceWrapper
    {
        /// <summary>
        /// トークンサービス（MTI提供ライブラリ）
        /// </summary>
        protected TokenService tokenService;

        /// <summary>
        /// アクセストークン取得関数
        /// </summary>
        public Func<string, TokenServiceResult> GetToken;

        /// <summary>
        /// アクセストークンリフレッシュ関数
        /// </summary>
        public Func<string, TokenServiceResult> RefreshToken;

        /// <summary>
        /// アクセストークン発行関数
        /// </summary>
        public Func<string, string, string[], long, long, TokenServiceResult> CreateToken;

        /// <summary>
        /// アクセストークン検証関数
        /// </summary>
        public Func<string, IntrospectTokenServiceResult> Introspect;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public TokenServiceWrapper()
        {
            tokenService = new TokenService();

            GetToken = (code) =>
            {
                return tokenService.GetToken(code);
            };

            RefreshToken = (refreshToken) =>
            {
                return tokenService.RefreshToken(refreshToken);
            };

            CreateToken = (clientId, uid, scopes, accessTokenDuration, refreshTokenDuration) =>
            {
                return tokenService.CreateToken(clientId, uid, scopes, accessTokenDuration, refreshTokenDuration);
            };

            Introspect = (accessToken) =>
            {
                return tokenService.Introspect(accessToken);
            };
        }
    }
}