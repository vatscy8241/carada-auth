﻿using CaradaAuthProvider.Api.Controllers.Conditions;
using CaradaAuthProvider.Api.Domain.Dtos;
using CaradaAuthProvider.Api.Domain.Logics;
using CaradaAuthProvider.Api.Domain.Logics.Impl;
using CaradaAuthProvider.Api.Exceptions;
using CaradaAuthProvider.Api.Exceptions.Enums;
using CaradaAuthProvider.Api.IdentityManagers;
using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using CaradaAuthProvider.Api.Persistence.Storage.Entities;
using CaradaAuthProvider.Api.Properties;
using CaradaAuthProvider.Api.Services.Dtos;
using CaradaAuthProvider.Api.Utils;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;

namespace CaradaAuthProvider.Api.Services
{
    /// <summary>
    /// 認証サービス
    /// </summary>
    public class AuthenticateService : AbstractService
    {
        /// <summary>
        /// チケットロジック
        /// </summary>
        protected TicketLogic ticketLogic;

        /// <summary>
        /// 秘密の質問ロジック
        /// </summary>
        protected SecurityQuestionLogic securityQuestionLogic;

        /// <summary>
        /// メールロジック
        /// </summary>
        protected MailLogic mailLogic;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="dbContext">DBコンテキスト</param>
        /// <param name="userManager">Identityマネージャ</param>
        public AuthenticateService(CaradaAuthProviderDbContext dbContext, ApplicationUserManager userManager) : base(dbContext, userManager)
        {
            this.ticketLogic = new TicketLogicImpl();
            this.mailLogic = new MailLogicImpl(apiKey);
            this.securityQuestionLogic = new SecurityQuestionLogicImpl(dbContext);
        }

        /// <summary>
        /// 認証処理を行う
        /// </summary>
        /// <param name="condition">認証APIリクエストパラメータ</param>
        /// <returns>認証処理結果DTO</returns>
        public virtual AuthenticateResultDto Authenticate(AuthenticateCondition condition)
        {
            AuthenticateResultDto resultDto = new AuthenticateResultDto();

            // ユーザ取得
            CaradaIdUser user = userManager.FindByName(condition.CaradaId);
            if (user == null)
            {
                APP_LOG.Error(string.Format("該当のユーザーが見つかりませんでした。[Uid={0}]", condition.CaradaId));
                throw new CaradaApplicationException(ErrorKind.user_not_exist, Message.user_not_exist);
            }

            // パスワード検証
            PasswordValidationStatus passwordValidationStatus = userManager.ValidatePassword(user, condition.Password);
            if (passwordValidationStatus == PasswordValidationStatus.AlreadyLockedOut)
            {
                APP_LOG.Error(string.Format("アカウントロック中です。[Uid={0}]", condition.CaradaId));
                throw new CommitCaradaApplicationException(ErrorKind.account_lockout, Message.account_lockout);
            }
            if (passwordValidationStatus == PasswordValidationStatus.LockedOut)
            {
                APP_LOG.Error(string.Format("アカウントがロックされました。[Uid={0}]", condition.CaradaId));
                throw new CommitCaradaApplicationException(ErrorKind.account_lockout, Message.account_lockout_start);
            }
            if (passwordValidationStatus == PasswordValidationStatus.Failure)
            {
                APP_LOG.Error(string.Format("パスワードが一致しません。[Uid={0}]", condition.CaradaId));
                throw new CommitCaradaApplicationException(ErrorKind.password_mismatch, Message.password_mismatch);
            }

            userManager.ResetAccessFailedCount(user.Id);

            if (!user.EmailConfirmed && condition.ConfirmedFlag)
            {
                IssuedTicketDto issuedTicketDto = ticketLogic.IssueTicket(TicketKind.UseStart, user, condition.TicketExpiresMinute.Value);
                resultDto.Ticket = new TicketDto()
                {
                    Id = issuedTicketDto.TicketId,
                    ExpiresAt = DateUtilities.GetEpochTime(issuedTicketDto.Ticket.ExpiresDateTime)
                };
            }

            resultDto.Uid = user.Id;

            return resultDto;
        }

        /// <summary>
        /// 利用開始用認証コードメールの送信を行う
        /// </summary>
        /// <param name="condition">利用開始用認証コードメール送信APIリクエストパラメータ</param>
        /// <returns>メール送信結果DTO</returns>
        public virtual SendMailResultDto SendUseStartVerifyCode(SendUseStartVerifyCodeCondition condition)
        {
            SendMailResultDto resultDto = new SendMailResultDto();

            Ticket ticket = ticketLogic.FindTicket(condition.TicketId);
            ValidateUseStartTicket(ticket, false);

            CaradaIdUser user = userManager.FindById(ticket.Uid);
            if (user == null)
            {
                APP_LOG.Error(string.Format("該当のユーザーが見つかりませんでした。[Uid={0}]", ticket.Uid));
                throw new CaradaApplicationException(ErrorKind.data_mismatch, Message.data_mismatch);
            }
            if (user.EmailConfirmed)
            {
                APP_LOG.Error(string.Format("既にメールアドレス登録が完了しています。[Uid={0}]", ticket.Uid));
                throw new CaradaApplicationException(ErrorKind.already_completed, Message.already_completed);
            }

            CaradaIdUser userByMailAddress = userManager.FindByEmail(condition.Email);
            if (userByMailAddress != null)
            {
                APP_LOG.Error(string.Format("メールアドレスが既に登録されています。[Uid={0}]", ticket.Uid));
                throw new CaradaApplicationException(ErrorKind.registered_email, Message.registered_email);
            }

            string verifyCode = IdGenerator.GenerateVerifyCode();

            ticket.MailAddress = condition.Email;
            ticket.VerifyCode = verifyCode;
            ticket.InvalidCount = 0;

            MailSendInfoDto mailSendInfoDto = new MailSendInfoDto()
            {
                ToList = new List<string>() { condition.Email },
                Subject = condition.Subject,
                Text = condition.Body.Replace(VERIFYCODE_REPLACE_KEYWORD, verifyCode),
                FromName = MAIL_FROM_NAME,
                From = MAIL_FROM_ADDRESS
            };

            mailLogic.SendMailAsync(mailSendInfoDto);

            ticketLogic.RegisterTicket(condition.TicketId, ticket);

            return resultDto;
        }

        /// <summary>
        /// 認証チケットの検証を行う
        /// </summary>
        /// <param name="condition">認証コード検証APIリクエストパラメータ</param>
        /// <returns>認証処理結果DTO</returns>
        public virtual VerifyResultDto VerifyTicket(AuthCodeVerifyCondition condition)
        {
            VerifyResultDto resultDto = new VerifyResultDto();

            VerifyResult verifyResult = ticketLogic.VerifyUseStartTicket(condition.TicketId, condition.VerifyCode);
            switch (verifyResult)
            {
                case VerifyResult.SUCCESS:
                    break;
                case VerifyResult.TICKET_NOT_FOUND:
                    APP_LOG.Error(string.Format("該当のチケットが見つかりませんでした。[TicketId={0}, VerifyCode={1}]", condition.TicketId, condition.VerifyCode));
                    throw new CaradaApplicationException(ErrorKind.invalid_ticket, Message.invalid_ticket);
                case VerifyResult.EXPIRED:
                    APP_LOG.Error(string.Format("チケットの有効期限が切れています。[TicketId={0}, VerifyCode={1}]", condition.TicketId, condition.VerifyCode));
                    throw new CaradaApplicationException(ErrorKind.invalid_ticket, Message.invalid_ticket_expired);
                case VerifyResult.INVALID:
                    APP_LOG.Error(string.Format("無効なチケットです。[TicketId={0}, VerifyCode={1}]", condition.TicketId, condition.VerifyCode));
                    throw new CaradaApplicationException(ErrorKind.invalid_code, Message.invalid_code);
                case VerifyResult.INVALIDED:
                    APP_LOG.Error(string.Format("チケットが無効となりました。[TicketId={0}, VerifyCode={1}]", condition.TicketId, condition.VerifyCode));
                    throw new CaradaApplicationException(ErrorKind.invalid_code, Message.invalid_code_disabled);
                case VerifyResult.CODE_MISMATCH:
                    APP_LOG.Error(string.Format("認証コードが一致しません。[TicketId={0}, VerifyCode={1}]", condition.TicketId, condition.VerifyCode));
                    throw new CaradaApplicationException(ErrorKind.code_mismatch, Message.code_mismatch);
                default:
                    throw new Exception(Message.server_error);
            }

            return resultDto;
        }

        /// <summary>
        /// 利用開始登録を行う
        /// </summary>
        /// <param name="condition">認証コード検証APIリクエストパラメータ</param>
        /// <returns>利用開始登録結果DTO</returns>
        public virtual RegisterUseStartResultDto RegisterUseStart(UseStartRegisterCondition condition)
        {
            RegisterUseStartResultDto resultDto = new RegisterUseStartResultDto();

            Ticket ticket = ticketLogic.FindTicket(condition.TicketId);
            ValidateUseStartTicket(ticket, true);

            CaradaIdUser user = userManager.FindById(ticket.Uid);
            if (user == null)
            {
                APP_LOG.Error(string.Format("ユーザー情報が取得出来ません。[Uid={0}]", ticket.Uid));
                throw new CaradaApplicationException(ErrorKind.data_mismatch, Message.data_mismatch);
            }
            if (user.EmailConfirmed)
            {
                APP_LOG.Error(string.Format("既にメールアドレス登録が完了しています。[Uid={0}", ticket.Uid));
                throw new CaradaApplicationException(ErrorKind.already_completed, Message.already_completed);
            }

            CaradaIdUser userByMailAddress = userManager.FindByEmail(ticket.MailAddress);
            if (userByMailAddress != null)
            {
                APP_LOG.Error(string.Format("メールアドレスが既に登録されています。[Uid={0}]", ticket.Uid));
                throw new CaradaApplicationException(ErrorKind.registered_email, Message.registered_email);
            }

            securityQuestionLogic.RegisterAnswer(ticket.Uid, condition.SecretQuestionId, condition.SecretQuestionAnswer);

            user.Email = ticket.MailAddress;
            user.EmailConfirmed = true;
            user.UseStartDateUtc = DateUtilities.UTCNow;
            IdentityResult result = userManager.Update(user);
            if (!result.Succeeded)
            {
                APP_LOG.Error(string.Format("ユーザー情報の更新に失敗しました。[Uid={0}]", ticket.Uid));
                throw new CaradaApplicationException(ErrorKind.server_error, Message.server_error_user_update_failure);
            }

            resultDto.Uid = ticket.Uid;
            ticketLogic.RemoveTicket(condition.TicketId);

            return resultDto;
        }

        /// <summary>
        /// 二段階認証用認証コードメールの送信を行う
        /// </summary>
        /// <param name="condition">二段階認証用認証コードメール送信APIリクエストパラメータ</param>
        /// <returns>二段階認証用メール送信結果DTO</returns>
        public virtual TwoFactorSendMailResultDto SendTwoFactorVerifyCode(SendTwoFactorVerifyCodeCondition condition)
        {
            TwoFactorSendMailResultDto resultDto = new TwoFactorSendMailResultDto();

            CaradaIdUser user = userManager.FindById(condition.Uid);
            if (user == null)
            {
                APP_LOG.Error(string.Format("ユーザー情報が取得出来ません。[Uid={0}]", condition.Uid));
                throw new CaradaApplicationException(ErrorKind.data_mismatch, Message.data_mismatch);
            }
            if (!user.EmailConfirmed || string.IsNullOrEmpty(user.Email))
            {
                APP_LOG.Error(string.Format("メールアドレスが登録されてません。[Uid={0}]", condition.Uid));
                throw new CaradaApplicationException(ErrorKind.data_mismatch, Message.has_not_email);
            }

            string verifyCode = IdGenerator.GenerateVerifyCode();

            MailSendInfoDto mailSendInfoDto = new MailSendInfoDto()
            {
                ToList = new List<string>() { user.Email },
                Subject = condition.Subject,
                Text = condition.Body.Replace(VERIFYCODE_REPLACE_KEYWORD, verifyCode),
                FromName = MAIL_FROM_NAME,
                From = MAIL_FROM_ADDRESS
            };

            mailLogic.SendMailAsync(mailSendInfoDto);

            IssuedTicketDto issuedTicketDto = ticketLogic.IssueTicket(TicketKind.TwoFactor, user, condition.TicketExpiresMinute.Value, verifyCode, user.Email);
            resultDto.Ticket = new TicketDto()
            {
                Id = issuedTicketDto.TicketId,
                ExpiresAt = DateUtilities.GetEpochTime(issuedTicketDto.Ticket.ExpiresDateTime)
            };

            return resultDto;
        }

        /// <summary>
        /// 秘密の質問リストを取得
        /// </summary>
        /// <returns>秘密の質問リスト</returns>
        public virtual List<SecurityQuestionResultDto> GetSecurityQuestionList()
        {
            // 秘密の質問マスタリストを取得
            IEnumerable<SecurityQuestionMasters> securityQuestionMasterList = securityQuestionLogic.GetSecurityQuestionList();

            // 結果リストを作成
            List<SecurityQuestionResultDto> ret = new List<SecurityQuestionResultDto>();
            foreach (SecurityQuestionMasters e in securityQuestionMasterList)
            {
                ret.Add(new SecurityQuestionResultDto { SecretQuestionId = e.DisplayOrder, SecretQuestionSentence = e.Question });
            }
            return ret;
        }

        /// <summary>
        /// チケットが有効か検証を行う。
        /// </summary>
        /// <param name="ticket">チケット</param>
        /// <param name="verifiedFlag">true:チケットが検証済み/false:チケットが未検証</param>
        private void ValidateUseStartTicket(Ticket ticket, bool verifiedFlag)
        {
            if (ticket == null)
            {
                APP_LOG.Error("該当のチケットが見つかりません。");
                throw new CaradaApplicationException(ErrorKind.invalid_ticket, Message.invalid_ticket);
            }
            if (ticket.Kind != TicketKind.UseStart)
            {
                APP_LOG.Error(string.Format("種別の異なるチケットが指定されました。[Uid={0}, Kind={1}]", ticket.Uid, ticket.Kind));
                throw new CaradaApplicationException(ErrorKind.invalid_ticket, Message.invalid_ticket_different_kinds);
            }
            if (ticket.ExpiresDateTime.CompareTo(DateUtilities.UTCNow) < 0)
            {
                APP_LOG.Error(string.Format("チケットの有効期限が切れています。[Uid={0}, Expires={1}]", ticket.Uid, ticket.ExpiresDateTime));
                throw new CaradaApplicationException(ErrorKind.invalid_ticket, Message.invalid_ticket_expired);
            }

            if (!verifiedFlag)
            {
                // チケットが未検証がチェックする。検証済みだった場合はエラー。
                if (ticket.VerifiedFlag)
                {
                    APP_LOG.Error(string.Format("認証コード検証が既に完了しています。[Uid={0}]", ticket.Uid));
                    throw new CaradaApplicationException(ErrorKind.invalid_ticket, Message.invalid_ticket_verified);
                }
                return;
            }

            // チケットが検証済みかチェックする。検証完了していない場合はエラー。
            if (!ticket.VerifiedFlag)
            {
                APP_LOG.Error(string.Format("認証コード検証が完了していません。[Uid={0}]", ticket.Uid));
                throw new CaradaApplicationException(ErrorKind.invalid_ticket, Message.invalid_ticket_unverified);
            }
            if (string.IsNullOrEmpty(ticket.MailAddress))
            {
                APP_LOG.Error(string.Format("メールアドレスが登録されてません。[Uid={0}]", ticket.Uid));
                throw new CaradaApplicationException(ErrorKind.invalid_ticket, Message.invalid_ticket_not_email);
            }
        }
    }
}