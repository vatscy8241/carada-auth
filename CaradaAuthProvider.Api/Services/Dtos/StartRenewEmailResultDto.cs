﻿namespace CaradaAuthProvider.Api.Services.Dtos
{
    /// <summary>
    /// メールアドレス変更開始結果DTO
    /// </summary>
    public class StartRenewEmailResultDto
    {
        /// <summary>
        /// チケットDTO
        /// </summary>
        public TicketDto Ticket { get; set; }
    }
}