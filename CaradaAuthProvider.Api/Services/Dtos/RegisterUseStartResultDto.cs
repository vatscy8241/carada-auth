﻿namespace CaradaAuthProvider.Api.Services.Dtos
{
    /// <summary>
    /// 利用開始登録結果DTO
    /// </summary>
    public class RegisterUseStartResultDto
    {
        /// <summary>
        /// ユーザID
        /// </summary>
        public string Uid { get; set; }
    }
}