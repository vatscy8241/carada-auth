﻿namespace CaradaAuthProvider.Api.Services.Dtos
{
    /// <summary>
    /// アクセストークン検証結果DTO
    /// </summary>
    public class TokenIntrospectResultDto
    {
        /// <summary>
        /// アクセストークン検証DTO
        /// </summary>
        public TokenIntrospectDto TokenIntrospect { get; set; }
    }
}