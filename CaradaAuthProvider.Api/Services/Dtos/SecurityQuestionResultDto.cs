﻿namespace CaradaAuthProvider.Api.Services.Dtos
{
    /// <summary>
    /// 秘密の質問リスト取得の出力情報
    /// </summary>
    public class SecurityQuestionResultDto
    {
        /// <summary>
        /// 表示順
        /// </summary>
        public int SecretQuestionId { get; set; }

        /// <summary>
        /// 秘密の質問
        /// </summary>
        public string SecretQuestionSentence { get; set; }
    }
}