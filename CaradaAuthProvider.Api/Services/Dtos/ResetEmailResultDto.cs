﻿namespace CaradaAuthProvider.Api.Services.Dtos
{
    /// <summary>
    /// メールアドレス再設定結果DTO
    /// </summary>
    public class ResetEmailResultDto
    {
        /// <summary>
        /// チケットDTO
        /// </summary>
        public TicketDto Ticket { get; set; }
    }
}