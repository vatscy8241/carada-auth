﻿namespace CaradaAuthProvider.Api.Services.Dtos
{
    /// <summary>
    /// 認証処理結果DTO
    /// </summary>
    public class AuthenticateResultDto
    {
        /// <summary>
        /// ユーザー一意識別子
        /// </summary>
        public string Uid { get; set; }

        /// <summary>
        /// チケットDTO
        /// </summary>
        public TicketDto Ticket { get; set; }
    }
}