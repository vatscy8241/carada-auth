﻿namespace CaradaAuthProvider.Api.Services.Dtos
{
    /// <summary>
    /// ユーザリセット結果DTO
    /// </summary>
    public class ResetUserResultDto
    {
        /// <summary>
        /// リセットユーザ
        /// </summary>
        public ResetUserDto User { get; set; }
    }
}