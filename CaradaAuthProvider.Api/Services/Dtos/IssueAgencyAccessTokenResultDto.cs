﻿namespace CaradaAuthProvider.Api.Services.Dtos
{
    /// <summary>
    /// 代理アクセストークン発行結果DTO
    /// </summary>
    public class IssueAgencyAccessTokenResultDto
    {
        /// <summary>
        /// アクセストークン
        /// </summary>
        public string access_token { get; set; }

        /// <summary>
        /// 有効期限エポック秒
        /// </summary>
        public long expires_at { get; set; }
    }
}