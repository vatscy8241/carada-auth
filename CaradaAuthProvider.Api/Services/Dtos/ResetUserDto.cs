﻿namespace CaradaAuthProvider.Api.Services.Dtos
{
    /// <summary>
    /// リセットユーザDTO
    /// </summary>
    public class ResetUserDto
    {
        /// <summary>
        /// CARADA ID
        /// </summary>
        public string carada_id { get; set; }

        /// <summary>
        /// パスワード
        /// </summary>
        public string password { get; set; }

        /// <summary>
        /// 初期化フラグ
        /// </summary>
        public string initialization { get; set; }
    }
}