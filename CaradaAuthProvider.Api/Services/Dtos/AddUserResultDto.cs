﻿namespace CaradaAuthProvider.Api.Services.Dtos
{
    /// <summary>
    /// ユーザ登録結果DTO
    /// </summary>
    public class AddUserResultDto
    {
        /// <summary>
        /// 登録ユーザ
        /// </summary>
        public AddUserDto User { get; set; }
    }
}