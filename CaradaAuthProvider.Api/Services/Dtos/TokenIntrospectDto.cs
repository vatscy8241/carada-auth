﻿namespace CaradaAuthProvider.Api.Services.Dtos
{
    /// <summary>
    /// アクセストークン検証DTO
    /// </summary>
    public class TokenIntrospectDto
    {
        /// <summary>
        /// アクセストークンが使用可能であるか示すフラグ
        /// </summary>
        public bool Usable { get; set; }

        /// <summary>
        /// UID
        /// </summary>
        public string Uid { get; set; }

        /// <summary>
        /// クライアントID
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// アクセストークンの有効期限日(エポックミリ秒).
        /// </summary>
        public long ExpiresAt { get; set; }

        /// <summary>
        /// アクセストークンに含まれる scopes.
        /// </summary>
        public string[] Scopes { get; set; }

        /// <summary>
        /// アクセストークンがリフレッシュ可能かどうかを示すフラグ
        /// </summary>
        public bool Refreshable { get; set; }

        /// <summary>
        /// 代理アクセストークンを使用した場合の代理実行者識別子
        /// </summary>
        public string Agent { get; set; }
    }
}