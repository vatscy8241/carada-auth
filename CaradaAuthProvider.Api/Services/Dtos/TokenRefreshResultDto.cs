﻿namespace CaradaAuthProvider.Api.Services.Dtos
{
    /// <summary>
    /// トークンリフレッシュ結果DTO
    /// </summary>
    public class TokenRefreshResultDto
    {
        /// <summary>
        /// トークンリフレッシュDTO
        /// </summary>
        public TokenRefreshDto TokenRefresh { get; set; }
    }
}