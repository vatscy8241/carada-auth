﻿namespace CaradaAuthProvider.Api.Services.Dtos
{
    /// <summary>
    /// 二段階認証メール送信結果DTO
    /// </summary>
    public class TwoFactorSendMailResultDto
    {
        /// <summary>
        /// チケットDTO
        /// </summary>
        public TicketDto Ticket { get; set; }
    }
}