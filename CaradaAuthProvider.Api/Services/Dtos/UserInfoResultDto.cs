﻿namespace CaradaAuthProvider.Api.Services.Dtos
{
    /// <summary>
    /// ユーザー情報取得結果DTO
    /// </summary>
    public class UserInfoResultDto
    {
        /// <summary>
        /// CARADA ID
        /// </summary>
        public string CaradaId { get; set; }

        /// <summary>
        /// メールアドレス
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// メールアドレス確認済フラグ
        /// </summary>
        public bool EmailConfirmed { get; set; }

        /// <summary>
        /// 秘密の質問DTO
        /// </summary>
        public SecurityQuestionResultDto SecretQuestion { get; set; }
    }
}
