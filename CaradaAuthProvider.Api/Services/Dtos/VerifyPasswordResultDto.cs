﻿namespace CaradaAuthProvider.Api.Services.Dtos
{
    /// <summary>
    /// パスワード検証結果DTO
    /// </summary>
    public class VerifyPasswordResultDto
    {
        /// <summary>
        /// チケットDTO
        /// </summary>
        public TicketDto Ticket { get; set; }
    }
}