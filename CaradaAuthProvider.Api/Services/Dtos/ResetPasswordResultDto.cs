﻿namespace CaradaAuthProvider.Api.Services.Dtos
{
    /// <summary>
    /// パスワード再設定結果DTO
    /// </summary>
    public class ResetPasswordResultDto
    {
        /// <summary>
        /// チケットDTO
        /// </summary>
        public TicketDto Ticket { get; set; }
    }
}