﻿namespace CaradaAuthProvider.Api.Services.Dtos
{
    /// <summary>
    /// 登録ユーザDTO
    /// </summary>
    public class AddUserDto
    {
        /// <summary>
        /// CARADA ID
        /// </summary>
        public string carada_id { get; set; }

        /// <summary>
        /// パスワード
        /// </summary>
        public string password { get; set; }

        /// <summary>
        /// UID
        /// </summary>
        public string uid { get; set; }
    }
}