﻿namespace CaradaAuthProvider.Api.Services.Dtos
{
    /// <summary>
    /// トークンリフレッシュDTO
    /// </summary>
    public class TokenRefreshDto
    {
        /// <summary>
        /// アクセストークン
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// リフレッシュトークン
        /// </summary>
        public string RefreshToken { get; set; }
    }
}
