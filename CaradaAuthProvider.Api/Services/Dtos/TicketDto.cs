﻿namespace CaradaAuthProvider.Api.Services.Dtos
{
    /// <summary>
    /// チケットDTO
    /// </summary>
    public class TicketDto
    {
        /// <summary>
        /// チケットID
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// チケット有効期限のエポック秒
        /// </summary>
        public long ExpiresAt { get; set; }
    }
}