﻿namespace CaradaAuthProvider.Api.Services.Dtos
{
    /// <summary>
    /// 認可の出力情報
    /// </summary>
    public class AuthorizeResultDto
    {
        /// <summary>
        /// アクセストークン
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// リフレッシュトークン
        /// </summary>
        public string RefreshToken { get; set; }

        /// <summary>
        /// 成功時にトークンを詰めて返却する処理
        /// </summary>
        /// <param name="accessToken">アクセストークン</param>
        /// <param name="refreshToken">リフレッシュトークン</param>
        public AuthorizeResultDto Success(string accessToken, string refreshToken)
        {
            this.AccessToken = accessToken;
            this.RefreshToken = refreshToken;
            return this;
        }
    }
}