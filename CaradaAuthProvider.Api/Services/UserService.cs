﻿using CaradaAuthProvider.Api.Controllers.Conditions;
using CaradaAuthProvider.Api.Domain.Dtos;
using CaradaAuthProvider.Api.Domain.Logics;
using CaradaAuthProvider.Api.Domain.Logics.Impl;
using CaradaAuthProvider.Api.Exceptions;
using CaradaAuthProvider.Api.Exceptions.Enums;
using CaradaAuthProvider.Api.IdentityManagers;
using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using CaradaAuthProvider.Api.Persistence.Storage.Entities;
using CaradaAuthProvider.Api.Properties;
using CaradaAuthProvider.Api.Services.Dtos;
using CaradaAuthProvider.Api.Utils;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;

namespace CaradaAuthProvider.Api.Services
{
    /// <summary>
    /// ユーザ情報に関するサービス
    /// </summary>
    public class UserService : AbstractService
    {
        /// <summary>
        /// チケットロジック
        /// </summary>
        protected TicketLogic ticketLogic;

        /// <summary>
        /// 秘密の質問ロジック
        /// </summary>
        protected SecurityQuestionLogic securityQuestionLogic;

        /// <summary>
        /// メールロジック
        /// </summary>
        protected MailLogic mailLogic;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="dbContext">DBコンテキスト</param>
        /// <param name="userManager">Identityマネージャ</param>
        public UserService(CaradaAuthProviderDbContext dbContext, ApplicationUserManager userManager) : base(dbContext, userManager)
        {
            ticketLogic = new TicketLogicImpl();
            securityQuestionLogic = new SecurityQuestionLogicImpl(dbContext);
            mailLogic = new MailLogicImpl(apiKey);
        }

        /// <summary>
        /// ユーザ情報を取得する
        /// </summary>
        /// <param name="condition">ユーザー情報取得APIリクエストパラメータ</param>
        /// <returns>ユーザー情報取得結果DTO</returns>
        public virtual UserInfoResultDto UserInfo(UserInfoCondition condition)
        {
            // ユーザ取得
            CaradaIdUser user = userManager.FindById(condition.Uid);
            if (user == null)
            {
                APP_LOG.Error(string.Format("該当のユーザーが見つかりませんでした。[Uid={0}]", condition.Uid));
                throw new CaradaApplicationException(ErrorKind.data_mismatch, Message.user_not_exist);
            }

            SecurityQuestionMasters securityQuestionMasters = securityQuestionLogic.FindSecurityQuestion(condition.Uid);
            /*
            if (securityQuestionMasters == null)
            {
                APP_LOG.Error(string.Format("秘密の質問が見つかりませんでした。[Uid={0}]", condition.Uid));
                throw new CaradaApplicationException(ErrorKind.data_mismatch, Message.security_question_not_exist);
            }
            */

            UserInfoResultDto result = new UserInfoResultDto
            {
                CaradaId = user.UserName,
                Email = user.Email,
                EmailConfirmed = user.EmailConfirmed
            };

            if (securityQuestionMasters != null)
            {
                result.SecretQuestion = new SecurityQuestionResultDto
                {
                    SecretQuestionId = securityQuestionMasters.SecurityQuestionId,
                    SecretQuestionSentence = securityQuestionMasters.Question
                };
            }

            return result;
        }

        /// <summary>
        /// パスワードを検証する
        /// </summary>
        /// <param name="condition">パスワード検証APIリクエストパラメータ</param>
        /// <returns>パスワード検証結果DTO</returns>
        public virtual VerifyPasswordResultDto VerifyPassword(VerifyPasswordCondition condition)
        {
            // ユーザ取得
            CaradaIdUser user = userManager.FindById(condition.Uid);
            if (user == null)
            {
                APP_LOG.Error(string.Format("該当のユーザーが見つかりませんでした。[Uid={0}]", condition.Uid));
                throw new CaradaApplicationException(ErrorKind.data_mismatch, Message.user_not_exist);
            }

            VerifyPassword(user, condition.Password);

            return new VerifyPasswordResultDto
            {
                Ticket = IssueTicket(TicketKind.PasswordValid, user, condition.TicketExpiresMinute.Value)
            };
        }

        /// <summary>
        /// パスワードを更新する
        /// </summary>
        /// <param name="condition">パスワード更新APIリクエストパラメータ</param>
        /// <returns>パスワード更新結果DTO</returns>
        public virtual ResultDto RenewPassword(RenewPasswordCondition condition)
        {
            Ticket ticket = ticketLogic.FindTicket(condition.TicketId);
            if (ticket == null)
            {
                APP_LOG.Error("該当のチケットが見つかりません。");
                throw new CaradaApplicationException(ErrorKind.invalid_ticket, Message.invalid_ticket);
            }
            if (ticket.Kind != TicketKind.PasswordValid && ticket.Kind != TicketKind.ResetPasseord)
            {
                APP_LOG.Error(string.Format("種別の異なるチケットが指定されました。[Uid={0}, Kind={1}]", ticket.Uid, ticket.Kind));
                throw new CaradaApplicationException(ErrorKind.invalid_ticket, Message.invalid_ticket_different_kinds);
            }
            if (ticket.ExpiresDateTime.CompareTo(DateUtilities.UTCNow) < 0)
            {
                APP_LOG.Error(string.Format("チケットの有効期限が切れています。[Uid={0}, Expires={1}]", ticket.Uid, ticket.ExpiresDateTime));
                throw new CaradaApplicationException(ErrorKind.invalid_ticket, Message.invalid_ticket_expired);
            }

            string uid = ticket.Uid;

            // ユーザ取得
            CaradaIdUser user = userManager.FindById(uid);
            if (user == null)
            {
                APP_LOG.Error(string.Format("該当のユーザーが見つかりませんでした。[Uid={0}]", uid));
                throw new CaradaApplicationException(ErrorKind.data_mismatch, Message.user_not_exist);
            }

            // ログイン失敗回数とロックアウト期間の初期化
            user.AccessFailedCount = 0;
            user.LockoutEndDateUtc = null;
            // 更新日時
            user.UpdateDateUtc = DateUtilities.UTCNow;
            userManager.Update(user);
            // パスワードリセットのAPIには専用のtokenの生成を必要とする
            var token = userManager.GeneratePasswordResetToken(uid);
            userManager.ResetPassword(uid, token, condition.Password);
            // ユーザー情報を更新したのでセキュリティスタンプを更新する
            userManager.UpdateSecurityStamp(uid);

            //チケット更新
            ticket.ExpiresDateTime = ticket.ExpiresDateTime.AddMinutes(1);
            ticket.ProcessedFlag = true;
            ticketLogic.RegisterTicket(condition.TicketId, ticket);

            return new ResultDto();
        }

        /// <summary>
        /// パスワードの再設定を開始する
        /// </summary>
        /// <param name="condition">パスワード再設定APIリクエストパラメータ</param>
        /// <returns>パスワード再設定結果DTO</returns>
        public virtual ResetPasswordResultDto ResetPassword(ResetPasswordCondition condition)
        {
            // ユーザ取得
            CaradaIdUser user = userManager.FindByEmail(condition.Email);
            if (user == null)
            {
                APP_LOG.Error(string.Format("該当のユーザーが見つかりませんでした。[Email={0}]", condition.Email));
                throw new CaradaApplicationException(ErrorKind.user_not_exist, Message.user_not_exist);
            }

            user.AccessFailedCount = 0;
            user.UpdateDateUtc = DateUtilities.UTCNow;
            IdentityResult result = userManager.Update(user);
            if (!result.Succeeded)
            {
                APP_LOG.Error(string.Format("ユーザー情報の更新に失敗しました。[Uid={0}]", user.Id));
                throw new CaradaApplicationException(ErrorKind.server_error, Message.server_error_user_update_failure);
            }

            string verifyCode = IdGenerator.GenerateVerifyCode();

            TicketDto ticketDto = IssueTicket(TicketKind.ResetPasseord, user, condition.TicketExpiresMinute.Value, verifyCode, condition.Email);

            MailSendInfoDto mailSendInfoDto = new MailSendInfoDto()
            {
                ToList = new List<string>() { condition.Email },
                Subject = condition.Subject,
                Text = condition.Body.Replace(VERIFYCODE_REPLACE_KEYWORD, verifyCode),
                FromName = MAIL_FROM_NAME,
                From = MAIL_FROM_ADDRESS
            };

            mailLogic.SendMailAsync(mailSendInfoDto);

            return new ResetPasswordResultDto
            {
                Ticket = ticketDto
            };
        }


        /// <summary>
        /// メールアドレス変更を開始する
        /// </summary>
        /// <param name="condition">メールアドレス変更開始APIリクエストパラメーター</param>
        /// <returns>メールアドレス変更開始結果DTO</returns>
        public virtual StartRenewEmailResultDto StartRenewEmail(StartRenewEmailCondition condition)
        {
            // ユーザ取得
            CaradaIdUser user = userManager.FindById(condition.Uid);
            if (user == null)
            {
                APP_LOG.Error(string.Format("該当のユーザーが見つかりませんでした。[Uid={0}]", condition.Uid));
                throw new CaradaApplicationException(ErrorKind.data_mismatch, Message.user_not_exist);
            }
            if (!string.IsNullOrEmpty(user.Email) && user.Email == condition.Email)
            {
                APP_LOG.Error(string.Format("現在のメールアドレスと同じものが指定されています。[Email={0}]", condition.Email));
                throw new CaradaApplicationException(ErrorKind.same_email, Message.same_email);
            }

            VerifyPassword(user, condition.Password);

            CaradaIdUser userByMailAddress = userManager.FindByEmail(condition.Email);
            if (userByMailAddress != null)
            {
                APP_LOG.Error(string.Format("メールアドレスが既に登録されています。[Email={0}]", condition.Email));
                throw new CaradaApplicationException(ErrorKind.registered_email, Message.registered_email);
            }

            return new StartRenewEmailResultDto
            {
                Ticket = IssueTicket(TicketKind.RenewEmail, user, condition.TicketExpiresMinute.Value, null, condition.Email)
            };
        }

        /// <summary>
        /// メールアドレス変更用認証コードメールを送信する
        /// </summary>
        /// <param name="condition">メールアドレス変更用認証コードメール送信APIリクエストパラメータ</param>
        /// <returns>結果DTO</returns>
        public virtual ResultDto SendRenewEmailVerifyCode(SendRenewEmailVerifyCodeCondition condition)
        {
            Ticket ticket = ticketLogic.FindTicket(condition.TicketId);
            ValidateValidTicket(ticket, TicketKind.RenewEmail);

            CaradaIdUser userByMailAddress = userManager.FindByEmail(ticket.MailAddress);
            if (userByMailAddress != null)
            {
                APP_LOG.Error(string.Format("メールアドレスが既に登録されています。[Email={0}]", ticket.MailAddress));
                throw new CaradaApplicationException(ErrorKind.registered_email, Message.registered_email);
            }

            string verifyCode = IdGenerator.GenerateVerifyCode();

            ticket.VerifyCode = verifyCode;
            ticket.InvalidCount = 0;

            MailSendInfoDto mailSendInfoDto = new MailSendInfoDto()
            {
                ToList = new List<string>() { ticket.MailAddress },
                Subject = condition.Subject,
                Text = condition.Body.Replace(VERIFYCODE_REPLACE_KEYWORD, verifyCode),
                FromName = MAIL_FROM_NAME,
                From = MAIL_FROM_ADDRESS
            };

            mailLogic.SendMailAsync(mailSendInfoDto);

            ticketLogic.RegisterTicket(condition.TicketId, ticket);

            return new ResultDto();
        }

        /// <summary>
        /// メールアドレスを更新する
        /// </summary>
        /// <param name="condition">メールアドレス更新APIリクエストパラメータ</param>
        /// <returns>結果DTO</returns>
        public virtual ResultDto RenewEmail(RenewEmailCondition condition)
        {
            Ticket ticket = ticketLogic.FindTicket(condition.TicketId);
            if (ticket == null)
            {
                APP_LOG.Error("該当のチケットが見つかりません。");
                throw new CaradaApplicationException(ErrorKind.invalid_ticket, Message.invalid_ticket);
            }
            if (ticket.Kind != TicketKind.RenewEmail && ticket.Kind != TicketKind.ResetEmail)
            {
                APP_LOG.Error(string.Format("種別の異なるチケットが指定されました。[Uid={0}, Kind={1}]", ticket.Uid, ticket.Kind));
                throw new CaradaApplicationException(ErrorKind.invalid_ticket, Message.invalid_ticket_different_kinds);
            }
            if (ticket.ExpiresDateTime.CompareTo(DateUtilities.UTCNow) < 0)
            {
                APP_LOG.Error(string.Format("チケットの有効期限が切れています。[Uid={0}, Expires={1}]", ticket.Uid, ticket.ExpiresDateTime));
                throw new CaradaApplicationException(ErrorKind.invalid_ticket, Message.invalid_ticket_expired);
            }

            // ユーザ取得
            CaradaIdUser user = userManager.FindById(ticket.Uid);
            if (user == null)
            {
                APP_LOG.Error(string.Format("該当のユーザーが見つかりませんでした。[Uid={0}]", ticket.Uid));
                throw new CaradaApplicationException(ErrorKind.data_mismatch, Message.user_not_exist);
            }

            CaradaIdUser userByMailAddress = userManager.FindByEmail(ticket.MailAddress);
            if (userByMailAddress != null)
            {
                APP_LOG.Error(string.Format("メールアドレスが既に登録されています。[Email={0}]", ticket.MailAddress));
                throw new CaradaApplicationException(ErrorKind.registered_email, Message.registered_email);
            }

            string oldMailAddress = user.Email;

            user.Email = ticket.MailAddress;
            user.UpdateDateUtc = DateUtilities.UTCNow;
            IdentityResult result = userManager.Update(user);
            if (!result.Succeeded)
            {
                APP_LOG.Error(string.Format("ユーザー情報の更新に失敗しました。[Uid={0}]", ticket.Uid));
                throw new CaradaApplicationException(ErrorKind.server_error, Message.server_error_user_update_failure);
            }

            MailSendInfoDto mailSendInfoDto = new MailSendInfoDto()
            {
                ToList = new List<string>() { ticket.MailAddress },
                Subject = condition.Subject,
                Text = condition.Body,
                FromName = MAIL_FROM_NAME,
                From = MAIL_FROM_ADDRESS
            };
            mailLogic.SendMailAsync(mailSendInfoDto);

            mailSendInfoDto.ToList = new List<string>() { oldMailAddress };
            mailLogic.SendMailAsync(mailSendInfoDto);

            try
            {
                ticketLogic.RemoveTicket(condition.TicketId);
            }
            catch
            {
            }

            return new ResultDto();
        }

        /// <summary>
        /// メールアドレスの再設定を開始する
        /// </summary>
        /// <param name="condition">メールアドレス再設定APIリクエストパラメータ</param>
        /// <returns>メールアドレス再設定結果DTO</returns>
        public virtual ResetEmailResultDto ResetEmail(ResetEmailCondition condition)
        {
            // ユーザ取得
            CaradaIdUser user = userManager.FindByName(condition.CaradaId);
            if (user == null)
            {
                APP_LOG.Error(string.Format("該当のユーザーが見つかりませんでした。[Uid={0}]", condition.CaradaId));
                throw new CaradaApplicationException(ErrorKind.user_not_exist, Message.user_not_exist);
            }

            VerifyPassword(user, condition.Password);

            CheckSecurityQuestionAnswer(user.Id, condition.SecretQuestionId, condition.SecretQuestionAnswer);

            CaradaIdUser userByMailAddress = userManager.FindByEmail(condition.Email);
            if (userByMailAddress != null)
            {
                if (userByMailAddress.Id == user.Id)
                {
                    APP_LOG.Error(string.Format("現在のメールアドレスと同じものが指定されています。[Email={0}]", condition.Email));
                    throw new CaradaApplicationException(ErrorKind.same_email, Message.same_email);
                }
                else
                {
                    APP_LOG.Error(string.Format("メールアドレスが既に登録されています。[Email={0}]", condition.Email));
                    throw new CaradaApplicationException(ErrorKind.registered_email, Message.registered_email);
                }
            }

            user.AccessFailedCount = 0;
            user.UpdateDateUtc = DateUtilities.UTCNow;
            IdentityResult result = userManager.Update(user);
            if (!result.Succeeded)
            {
                APP_LOG.Error(string.Format("ユーザー情報の更新に失敗しました。[Uid={0}]", user.Id));
                throw new CaradaApplicationException(ErrorKind.server_error, Message.server_error_user_update_failure);
            }

            string verifyCode = IdGenerator.GenerateVerifyCode();

            TicketDto ticketDto = IssueTicket(TicketKind.ResetEmail, user, condition.TicketExpiresMinute.Value, verifyCode, condition.Email);

            MailSendInfoDto mailSendInfoDto = new MailSendInfoDto()
            {
                ToList = new List<string>() { condition.Email },
                Subject = condition.Subject,
                Text = condition.Body.Replace(VERIFYCODE_REPLACE_KEYWORD, verifyCode),
                FromName = MAIL_FROM_NAME,
                From = MAIL_FROM_ADDRESS
            };

            mailLogic.SendMailAsync(mailSendInfoDto);

            return new ResetEmailResultDto
            {
                Ticket = ticketDto
            };
        }

        /// <summary>
        /// 秘密の質問を更新する
        /// </summary>
        /// <param name="condition">秘密の質問更新APIリクエストパラメータ</param>
        /// <returns>秘密の質問更新結果DTO</returns>
        public virtual void RenewSecretQuestion(RenewSecretQuestionCondition condition)
        {
            Ticket ticket = ticketLogic.FindTicket(condition.TicketId);
            ValidateValidTicket(ticket, TicketKind.PasswordValid);

            if (securityQuestionLogic.CountSecurityQuestion(ticket.Uid) > 1)
            {
                APP_LOG.Error(string.Format("複数件の秘密の質問回答が存在します。[UID={0}]", ticket.Uid));
                throw new CaradaApplicationException(ErrorKind.data_mismatch, Message.answer_multiple);
            }
            securityQuestionLogic.RegisterAnswer(ticket.Uid, condition.SecretQuestionId, condition.SecretQuestionAnswer);
        }

        /// <summary>
        /// CARADA ID通知メールを送信する
        /// </summary>
        /// <param name="condition">ID通知APIリクエストパラメータ</param>
        /// <returns>結果DTO</returns>
        public virtual ResultDto SendCaradaId(SendCaradaIdCondition condition)
        {
            CaradaIdUser user = userManager.FindByEmail(condition.Email);
            if (user == null)
            {
                APP_LOG.Error(string.Format("該当のユーザーが見つかりませんでした。[Email={0}]", condition.Email));
                throw new CaradaApplicationException(ErrorKind.user_not_exist, Message.user_not_exist);
            }

            MailSendInfoDto mailSendInfoDto = new MailSendInfoDto()
            {
                ToList = new List<string>() { condition.Email },
                Subject = condition.Subject,
                Text = condition.Body.Replace(CARADAID_REPLACE_KEYWORD, user.UserName),
                FromName = MAIL_FROM_NAME,
                From = MAIL_FROM_ADDRESS
            };

            mailLogic.SendMailAsync(mailSendInfoDto);

            return new ResultDto();
        }

        /// <summary>
        /// パスワードチェックを行う
        /// </summary>
        /// <param name="user">ユーザー</param>
        /// <param name="password">パスワード</param>
        private void VerifyPassword(CaradaIdUser user, string password)
        {
            PasswordValidationStatus passwordValidationStatus = userManager.ValidatePassword(user, password);

            if (passwordValidationStatus == PasswordValidationStatus.AlreadyLockedOut)
            {
                APP_LOG.Error(string.Format("アカウントロック中です。[Uid={0}]", user.Id));
                throw new CommitCaradaApplicationException(ErrorKind.account_lockout, Message.account_lockout);
            }
            if (passwordValidationStatus == PasswordValidationStatus.LockedOut)
            {
                APP_LOG.Error(string.Format("アカウントがロックされました。[Uid={0}]", user.Id));
                throw new CommitCaradaApplicationException(ErrorKind.account_lockout, Message.account_lockout_start);
            }
            if (passwordValidationStatus == PasswordValidationStatus.Failure)
            {
                APP_LOG.Error(string.Format("パスワードが一致しません。[Uid={0}]", user.Id));
                throw new CommitCaradaApplicationException(ErrorKind.password_mismatch, Message.password_mismatch);
            }

            userManager.ResetAccessFailedCount(user.Id);
        }

        /// <summary>
        /// チケットが有効か検証を行う。
        /// </summary>
        /// <param name="ticket">チケット</param>
        /// <param name="ticketKind">チケット種別</param>
        private void ValidateValidTicket(Ticket ticket, TicketKind ticketKind)
        {
            if (ticket == null)
            {
                APP_LOG.Error("該当のチケットが見つかりません。");
                throw new CaradaApplicationException(ErrorKind.invalid_ticket, Message.invalid_ticket);
            }
            if (ticket.Kind != ticketKind)
            {
                APP_LOG.Error(string.Format("種別の異なるチケットが指定されました。[Uid={0}, Kind={1}]", ticket.Uid, ticket.Kind));
                throw new CaradaApplicationException(ErrorKind.invalid_ticket, Message.invalid_ticket_different_kinds);
            }
            if (ticket.ExpiresDateTime.CompareTo(DateUtilities.UTCNow) < 0)
            {
                APP_LOG.Error(string.Format("チケットの有効期限が切れています。[Uid={0}, Expires={1}]", ticket.Uid, ticket.ExpiresDateTime));
                throw new CaradaApplicationException(ErrorKind.invalid_ticket, Message.invalid_ticket_expired);
            }
        }

        /// <summary>
        /// チケットを発行する
        /// <param name="ticketKind">チケット種別</param>
        /// <param name="user">ユーザ</param>
        /// <param name="ticketExpiresMinute">チケット有効期限</param>
        /// <param name="verifyCode">認証コード</param>
        /// <param name="mailAddress">メールアドレス</param>
        /// <returns>チケットDTO</returns>
        private TicketDto IssueTicket(TicketKind ticketKind, CaradaIdUser user, long ticketExpiresMinute, string verifyCode = null, string mailAddress = null)
        {
            IssuedTicketDto issuedTicketDto = ticketLogic.IssueTicket(ticketKind, user, ticketExpiresMinute, verifyCode, mailAddress);
            return new TicketDto
            {
                Id = issuedTicketDto.TicketId,
                ExpiresAt = DateUtilities.GetEpochTime(issuedTicketDto.Ticket.ExpiresDateTime)
            };
        }

        /// <summary>
        /// 秘密の質問の回答をチェックする
        /// </summary>
        /// <param name="userId">ユーザID</param>
        /// <param name="securityQuestionId">秘密の質問ID</param>
        /// <param name="answer">回答</param>
        /// <returns>チェック結果(Task用に返してるだけで、trueしか返ることはない)</returns>
        private bool CheckSecurityQuestionAnswer(string userId, int securityQuestionId, string answer)
        {
            if (securityQuestionLogic.CheckAnswer(userId, securityQuestionId, answer))
            {
                return true;
            }

            // 失敗回数のカウントアップをする
            userManager.AccessFailed(userId);
            if (userManager.IsLockedOut(userId))
            {
                APP_LOG.Error(string.Format("秘密の質問の回答不一致によりアカウントがロックされました。[Uid={0}]", userId));
                throw new CommitCaradaApplicationException(ErrorKind.account_lockout, Message.account_lockout_start_answer);
            }
            else
            {
                APP_LOG.Error(string.Format("秘密の質問の回答が一致しません。[Uid={0}]", userId));
                throw new CommitCaradaApplicationException(ErrorKind.answer_mismatch, Message.answer_mismatch);
            }
        }
    }
}