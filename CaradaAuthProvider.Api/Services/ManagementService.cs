﻿using CaradaAuthProvider.Api.Controllers.Conditions;
using CaradaAuthProvider.Api.Domain.Logics;
using CaradaAuthProvider.Api.Domain.Logics.Impl;
using CaradaAuthProvider.Api.Exceptions;
using CaradaAuthProvider.Api.Exceptions.Enums;
using CaradaAuthProvider.Api.IdentityManagers;
using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using CaradaAuthProvider.Api.Properties;
using CaradaAuthProvider.Api.Services.Dtos;
using CaradaAuthProvider.Api.Utils;
using System.Threading.Tasks;

namespace CaradaAuthProvider.Api.Services
{
    /// <summary>
    /// 管理ツール用サービス
    /// </summary>
    public class ManagementService : AbstractService
    {
        /// <summary>
        /// 秘密の質問ロジック
        /// </summary>
        protected SecurityQuestionLogic securityQuestionLogic;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="dbContext">DBコンテキスト</param>
        /// <param name="userManager">Identityマネージャ</param>
        public ManagementService(CaradaAuthProviderDbContext dbContext, ApplicationUserManager userManager) : base(dbContext, userManager)
        {
            this.securityQuestionLogic = new SecurityQuestionLogicImpl(dbContext);
        }

        /// <summary>
        /// 利用開始前ユーザー登録を行う
        /// </summary>
        /// <param name="condition">ユーザー登録情報</param>
        /// <returns>HttpResponseMessage</returns>
        public virtual async Task<AddUserResultDto> AddUser(RegistUserAddCondition condition)
        {
            AddUserResultDto dto = new AddUserResultDto();

            // CARADA IDの登録済みチェック
            CaradaIdUser user = await userManager.FindByNameAsync(condition.CaradaId);
            if (user != null)
            {
                APP_LOG.Error(string.Format("登録済みのCARADA IDです。[CaradaId={0}]", condition.CaradaId));
                throw new CaradaApplicationException(ErrorKind.registered_carada_id, Message.registered_carada_id);
            }

            // ユーザー情報登録処理
            CaradaIdUser newUser = new CaradaIdUser
            {
                UserName = condition.CaradaId,
                EmailConfirmed = false,
                TwoFactorEnabled = true,
                CreateDateUtc = DateUtilities.UTCNow,
                LockoutEnabled = true,
                EmployeeFlag = true,
                AccessFailedCount = 0
            };

            // パスワード生成
            var password = IdGenerator.GenerateRandomPassword();

            // ユーザー登録
            await userManager.CreateAsync(newUser, password);

            // CARADA IDでユーザー情報を検索
            user = await userManager.FindByNameAsync(condition.CaradaId);

            // ユーザーロールを登録
            await userManager.AddToRoleAsync(user.Id, Settings.Default.UserRole);

            // 結果を返す
            dto.User = new AddUserDto
            {
                carada_id = condition.CaradaId,
                password = password,
                uid = user.Id
            };

            return dto;
        }

        /// <summary>
        /// ユーザー情報をリセットする
        /// </summary>
        /// <param name="condition">リセットユーザー情報</param>
        /// <returns>HttpResponseMessage</returns>
        public virtual async Task<ResetUserResultDto> ResetUserInfo(ResetUserCondition condition)
        {
            ResetUserResultDto dto = new ResetUserResultDto();

            // CARADA IDの登録済みチェック
            var user = await userManager.FindByNameAsync(condition.CaradaId);

            if (user == null)
            {
                APP_LOG.Error(string.Format("該当のユーザーが見つかりませんでした。[CaradaId={0}]", condition.CaradaId));
                throw new CaradaApplicationException(ErrorKind.invalid_carada_id, Message.user_not_exist);
            }

            // ログイン失敗回数・ロックアウト期間初期化
            user.AccessFailedCount = 0;
            user.LockoutEndDateUtc = null;
            // 更新日時
            user.UpdateDateUtc = DateUtilities.UTCNow;

            // params:initializationが"1"の場合、利用開始前に初期化
            if (condition.Initialization == "1")
            {
                user.Email = null;
                user.EmailConfirmed = false;
            }

            // 秘密の回答削除
            if (condition.Initialization == "1")
            {
                securityQuestionLogic.RemoveSecurityQuestion(user.Id);

            }

            // ユーザー情報更新
            await userManager.UpdateAsync(user);

            // パスワード生成
            var generatePassword = IdGenerator.GenerateRandomPassword();

            // パスワードリセットのAPIには専用のtokenの生成を必要とする
            var token = await userManager.GeneratePasswordResetTokenAsync(user.Id);

            // パスワード再設定
            await userManager.ResetPasswordAsync(user.Id, token, generatePassword);

            // ユーザー情報を更新したのでセキュリティスタンプを更新する
            await userManager.UpdateSecurityStampAsync(user.Id);

            // 結果を返す
            dto.User = new ResetUserDto
            {
                carada_id = condition.CaradaId,
                password = generatePassword,
                initialization = (condition.Initialization == "1" ? "1" : "0")
            };

            return dto;
        }
    }
}