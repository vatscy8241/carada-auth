﻿using CaradaAuthProvider.Api.Exceptions;
using CaradaAuthProvider.Api.IdentityManagers;
using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Services;
using System;
using System.Data.Common;
using System.Data.Entity;
using System.Reflection;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Activation;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Remoting.Proxies;
using System.Runtime.Remoting.Services;

namespace CaradaAuthProvider.Api.Proxy
{
    /// <summary>
    /// サービス層のインターセプタ
    /// </summary>
    public class ServiceProxy : RealProxy
    {

        // サービスの基底クラス
        protected AbstractService service;

        // DBコンテキスト
        protected CaradaAuthProviderDbContext dbContext;

        // ユーザー翰林
        protected ApplicationUserManager userManager;


        /// <summary>
        /// サービス層のインターセプタ
        /// </summary>
        /// <param name="t">型</param>
        /// <param name="_dbContext">DBコンテキスト</param>
        /// <param name="_userManager">ユーザー管理</param>
        public ServiceProxy(Type t, CaradaAuthProviderDbContext _dbContext, ApplicationUserManager _userManager) : base(t)
        {
            dbContext = _dbContext;
            userManager = _userManager;
            service = (AbstractService)Activator.CreateInstance(t, _dbContext, _userManager);

        }

        /// <summary>
        /// Invoke
        /// </summary>
        /// <param name="request">リクエスト</param>
        /// <returns></returns>
        public override IMessage Invoke(IMessage request)
        {

            IMessage response = null;
            IMethodCallMessage call = (IMethodCallMessage)request;
            IConstructionCallMessage ctor = call as IConstructionCallMessage;

            if (ctor != null)
            {
                RealProxy rp = RemotingServices.GetRealProxy(service);
                rp.InitializeServerObject(ctor);
                MarshalByRefObject tp = this.GetTransparentProxy() as MarshalByRefObject;
                response = EnterpriseServicesHelper.CreateConstructionReturnMessage(ctor, tp);
            }
            else
            {
                response = Invoke(call);
            }

            return response;
        }


        /// <summary>
        /// メソッド呼び出し処理
        /// </summary>
        /// <param name="call"></param>
        /// <returns></returns>
        protected virtual IMessage Invoke(IMethodCallMessage call)
        {
            ReturnMessage response = null;


            // 自動トランザクション管理の場合
            if (IsImplicit(call.MethodBase))
            {
                // 接続文字列のチェック
                if (IsEmptyConnnectionString())
                {
                    dbContext.Database.Connection.ConnectionString = GetConnectionString();
                }

                using (DbConnection connection = dbContext.Database.Connection as DbConnection)
                {
                    using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                    {
                        // 対象メソッド実行
                        response = RemotingServices.ExecuteMessage(service, call) as ReturnMessage;

                        // 例外が発生しなければコミット（※コミットが無ければ自動的にロールバックされる）
                        if (response.Exception != null && !(response.Exception is CommitCaradaApplicationException))
                        {
                            transaction.Rollback();
                        }
                        else
                        {
                            transaction.Commit();
                        }
                    }
                }
            }
            else
            {
                // 対象メソッド実行
                response = RemotingServices.ExecuteMessage(service, call) as ReturnMessage;
            }

            return response;
        }

        /// <summary>
        /// 実行メソッドに独自トランザクション管理の属性が付いているかどうかを判定
        /// </summary>
        /// <param name="m">メソッド情報</param>
        /// <returns>判定結果</returns>
        private bool IsImplicit(MethodBase m)
        {
            object[] atts = m.GetCustomAttributes(true);
            if (atts.Length > 0)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 接続文字列の空チェック
        /// ※例外発生時に接続文字列が消えるのでそれをチェック
        /// </summary>
        /// <returns>判定結果</returns>
        protected virtual bool IsEmptyConnnectionString()
        {
            return string.IsNullOrEmpty(dbContext.Database.Connection.ConnectionString);
        }

        /// <summary>
        /// 接続文字列を取得
        /// </summary>
        /// <returns>接続文字列</returns>
        protected virtual string GetConnectionString()
        {
            return (string)System.Configuration.ConfigurationManager.ConnectionStrings["CaradaAuthProviderDbContext"].ConnectionString;
        }
    }


    /// <summary>
    /// 独自トランザクション管理属性
    /// RealProxyによる自動トランザクション管理でなく自分でトランザクション管理をする場合はこの属性をサービスのメソッドにつける
    /// </summary>
    public class ExplicitTransactionsAttribute : System.Attribute { }
}