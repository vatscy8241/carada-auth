using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.Migrations;

namespace CaradaAuthProvider.Api.Migrations
{

    /// <summary>
    /// 本番環境用
    /// </summary>
    internal sealed class ConfigurationRelease : BaseConfiguration
    {
        /// <summary>
        /// コンテキストにデータを追加してシードする
        /// </summary>
        /// <param name="context"></param>
        protected override void Seed(CaradaAuthProviderDbContext context)
        {
            // ロール
            context.Roles.AddOrUpdate(new IdentityRole()
            {
                Id = "cbd06013-b835-4f5d-ba80-88c55d41788b", //ロールID。ASP.NET Identityが自動生成するものを固定値にした。
                Name = Properties.Settings.Default.UserRole
            });

            // 秘密の質問マスタ
            AddSecurityQuestionMasters(context);

            // JWT発行者情報
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdmin01",
                SecretKey = "[BXd=JNB}{LOr4YS7)h^Tmewnf1;i{jfaW2_u.|VTG!49f18lvikRD_%Y3hW3np5",
                Remark = "本番環境用の発行者情報01"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdmin02",
                SecretKey = ":Ia+8RLj?x@DOSz@_mKv8Kp^*y^nFOnuBeoXh=%vWyrL_!r0f}pkbPS%H^>6EhP0",
                Remark = "本番環境用の発行者情報02"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdmin03",
                SecretKey = "0]]MjtWlCg#FC{mPY8Qdf7[+{s*7XSF6+i{%_1dy8G!rQ7@/rcOI){uNzRRwUtzj",
                Remark = "本番環境用の発行者情報03"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdmin04",
                SecretKey = "0=Jf3Zz*dLJf*Tu-W4wqYF%rE3s)=GzG#+exm7:sWa^]$I[Jna{fWk(^yc2QZz(8",
                Remark = "本番環境用の発行者情報04"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdmin05",
                SecretKey = "6rW^:2A)Xh?MlmXARe/guw!pn.8U6YruU3vM+?_zN$&ks8BeEXB0SgZci:buT_e%",
                Remark = "本番環境用の発行者情報05"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaCommonConsumer",
                SecretKey = "qq$][4{9OK4U!Y#4#gL-26rW=seL&madyQP9akdKkO;O:%Y#7?.&UAXYE_[6#8>j",
                Remark = "管理ツール用の発行者情報"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaBackyardSystem",
                SecretKey = "PP.8/HY|d)iHwYRTj|b!itKd-JRNsJNM!wvQb.&v.JLFmsD-Ezh99P!pGJ.~,nCG",
                Remark = "健康支援登録フローシステム"
            });
        }
    }
}
