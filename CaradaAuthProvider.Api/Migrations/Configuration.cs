using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.Migrations;

namespace CaradaAuthProvider.Api.Migrations
{

    /// <summary>
    /// ローカル・CI環境用
    /// </summary>
    internal sealed class Configuration : BaseConfiguration
    {
        /// <summary>
        /// コンテキストにデータを追加してシードする
        /// </summary>
        /// <param name="context"></param>
        protected override void Seed(CaradaAuthProviderDbContext context)
        {
            // ロール
            context.Roles.AddOrUpdate(new IdentityRole()
            {
                Id = "36e5abde-2d5c-4b9f-8d4b-adc5d9af9fad", //ロールID。ASP.NET Identityが自動生成するものを固定値にした。
                Name = Properties.Settings.Default.UserRole
            });

            // 秘密の質問マスタ
            AddSecurityQuestionMasters(context);

            // JWT発行者情報
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaIdPTest",
                SecretKey = "FZlXb!U2d[ydXtTuKp1Q7ncM@}9{?Iit}ta}w;n7Mx{fHa+PoYGr=}((B@V*uAt-",
                Remark = "開発テスト用の発行者情報(ローカル・CI)"
            });
        }
    }
}
