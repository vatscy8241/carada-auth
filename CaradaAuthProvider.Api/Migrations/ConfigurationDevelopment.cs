﻿using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.Migrations;

namespace CaradaAuthProvider.Api.Migrations
{
    /// <summary>
    /// 開発環境用
    /// </summary>
    internal sealed class ConfigurationDevelopment : BaseConfiguration
    {
        /// <summary>
        /// コンテキストにデータを追加してシードする
        /// </summary>
        /// <param name="context"></param>
        protected override void Seed(CaradaAuthProviderDbContext context)
        {
            // ロール
            context.Roles.AddOrUpdate(new IdentityRole()
            {
                Id = "1782a059-0151-4135-85df-e9bcf3a52551", //ロールID。ASP.NET Identityが自動生成するものを固定値にした。
                Name = Properties.Settings.Default.UserRole
            });

            // 秘密の質問マスタ
            AddSecurityQuestionMasters(context);

            // JWT発行者情報
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdminDev01",
                SecretKey = "BLG;*A4a*HZ6kErbe7Hg=)UbxEFUP6]o[@nU}1yHt:REN-qJ1x5s-Zk:Vt=bxFX=",
                Remark = "開発環境用の発行者情報01"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdminDev02",
                SecretKey = "Uc=Ocmk%K&>M=FttpfjDJEOMt&Qm]2!1RUU:A%=KWj:77@whVg&5F(H$$MKgRV)O",
                Remark = "開発環境用の発行者情報02"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdminDev03",
                SecretKey = "}*?Zil|{$Y-TJt5G2M{]#Zc%74q@CLpa{e@lrrN}anbsfDfI$;{(ZJh*qifK.tHY",
                Remark = "開発環境用の発行者情報03"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdminDev04",
                SecretKey = "$SWexLpp5PO&KZk{(u9JDpU>7PHqf;x}.eL(*n$O-w7J-a=-O]Ls5^*b.f+P8l]Z",
                Remark = "開発環境用の発行者情報04"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdminDev05",
                SecretKey = "!BnPq0N3(^E#5DoQ[qIc_A3Ywgo2VKSl!lX1&uh?iUkiByUl%;:f032b>VO;k(q8",
                Remark = "開発環境用の発行者情報05"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaIdPTestDev",
                SecretKey = "QL^RgpPo40|3?UD!#mqcLXUmVF]0#0J0Fv=7(c]Qud9jah>=Mj-%EZ}Kj*mj#vRn",
                Remark = "開発テスト用の発行者情報(開発環境)"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaCommonConsumer",
                SecretKey = "qq$][4{9OK4U!Y#4#gL-26rW=seL&madyQP9akdKkO;O:%Y#7?.&UAXYE_[6#8>j",
                Remark = "管理ツール用の発行者情報(開発環境)"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaBackyardSystem",
                SecretKey = "PP.8/HY|d)iHwYRTj|b!itKd-JRNsJNM!wvQb.&v.JLFmsD-Ezh99P!pGJ.~,nCG",
                Remark = "健康支援登録フローシステム"
            });
        }
    }
}