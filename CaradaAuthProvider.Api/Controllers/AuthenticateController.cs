﻿using CaradaAuthProvider.Api.Controllers.Conditions;
using CaradaAuthProvider.Api.Exceptions;
using CaradaAuthProvider.Api.Filters;
using CaradaAuthProvider.Api.Proxy;
using CaradaAuthProvider.Api.Services;
using CaradaAuthProvider.Api.Services.Dtos;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CaradaAuthProvider.Api.Controllers
{
    /// <summary>
    /// 認証処理を行うコントローラー
    /// </summary>
    [CustomActionFilter(1)]
    public class AuthenticateController : AbstractApiController
    {
        /// <summary>
        /// 認証API
        /// </summary>
        /// <param name="condition">認証情報</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        [ExceptionOccured(typeof(CaradaApplicationException), HttpStatusCode.BadRequest)]
        public HttpResponseMessage Authenticate([FromBody]AuthenticateCondition condition)
        {
            AuthenticateResultDto dto = CreateAuthenticateService().Authenticate(condition);

            if (dto.Ticket != null)
            {
                return CreateJsonResponse(new { Ticket = dto.Ticket, Uid = dto.Uid });
            }
            else
            {
                return CreateJsonResponse(new { Uid = dto.Uid });
            }
        }

        /// <summary>
        /// 利用開始用認証コードメール送信API
        /// </summary>
        /// <param name="condition">利用開始用認証コードメール送信情報</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        [ExceptionOccured(typeof(CaradaApplicationException), HttpStatusCode.BadRequest)]
        public HttpResponseMessage SendUseStartVerifyCode([FromBody]SendUseStartVerifyCodeCondition condition)
        {
            SendMailResultDto dto = CreateAuthenticateService().SendUseStartVerifyCode(condition);

            return Success;
        }

        /// <summary>
        /// 二段階認証用認証コードメール送信API
        /// </summary>
        /// <param name="condition">二段階認証用認証コードメール送信情報</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        [ExceptionOccured(typeof(CaradaApplicationException), HttpStatusCode.BadRequest)]
        public HttpResponseMessage SendTwoFactorVerifyCode([FromBody]SendTwoFactorVerifyCodeCondition condition)
        {
            TwoFactorSendMailResultDto dto = CreateAuthenticateService().SendTwoFactorVerifyCode(condition);

            return CreateJsonResponse(dto);
        }

        /// <summary>
        /// 認証コード検証API
        /// </summary>
        /// <param name="condition">コード検証情報</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        [ExceptionOccured(typeof(CaradaApplicationException), HttpStatusCode.BadRequest)]
        public HttpResponseMessage Verify([FromBody]AuthCodeVerifyCondition condition)
        {
            VerifyResultDto dto = CreateAuthenticateService().VerifyTicket(condition);

            return Success;
        }

        /// <summary>
        /// 利用開始登録API
        /// </summary>
        /// <param name="condition">利用開始登録情報</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        [ExceptionOccured(typeof(CaradaApplicationException), HttpStatusCode.BadRequest)]
        public HttpResponseMessage Register([FromBody]UseStartRegisterCondition condition)
        {
            RegisterUseStartResultDto dto = CreateAuthenticateService().RegisterUseStart(condition);

            return CreateJsonResponse(dto);
        }

        /// <summary>
        /// 秘密の質問リストを取得する
        /// </summary>
        /// <returns>秘密の質問リスト</returns>
        /// 
        [HttpGet]
        public HttpResponseMessage GetSecurityQuestion()
        {
            List<SecurityQuestionResultDto> secretQuestions = CreateAuthenticateService().GetSecurityQuestionList();

            return CreateJsonResponse(new { SecretQuestions = secretQuestions });
        }

        /// <summary>
        /// 認証サービスを生成する
        /// </summary>
        /// <returns>認証サービス</returns>
        protected virtual AuthenticateService CreateAuthenticateService()
        {
            ServiceProxy sp = new ServiceProxy(typeof(AuthenticateService), DbContext, UserManager);
            return (AuthenticateService)sp.GetTransparentProxy();

        }
    }
}