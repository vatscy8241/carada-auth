﻿using CaradaAuthProvider.Api.Controllers.Conditions;
using CaradaAuthProvider.Api.Controllers.Jwt;
using CaradaAuthProvider.Api.Exceptions;
using CaradaAuthProvider.Api.Filters;
using CaradaAuthProvider.Api.Proxy;
using CaradaAuthProvider.Api.Services;
using CaradaAuthProvider.Api.Services.Dtos;
using Mti.Cpp.Logging;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace CaradaAuthProvider.Api.Controllers
{
    /// <summary>
    /// 管理ツール用APIコントローラー
    /// </summary>
    [JwtValidationFilter]
    public class ManagementController : AbstractApiController
    {
        /// <summary>
        /// 利用開始前ユーザー登録API
        /// </summary>
        /// <param name="condition">JWT形式のユーザー登録情報</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        public async Task<HttpResponseMessage> AddUser([FromBody]Jwt<RegistUserAddCondition> condition)
        {
            try
            {
                AddUserResultDto dto = await CreateManagementService().AddUser(condition.Parameter);

                return CreateJsonResponse(dto.User);
            }
            catch (AbstractApplicationException e)
            {
                return CreateErrorResponse(condition.Parameter.CaradaId, e);
            }
        }

        /// <summary>
        /// ユーザー情報リセットAPI
        /// </summary>
        /// <param name="condition">JWT形式のユーザー登録情報</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        public async Task<HttpResponseMessage> ResetUserInfo([FromBody]Jwt<ResetUserCondition> condition)
        {
            try
            {
                ResetUserResultDto dto = await CreateManagementService().ResetUserInfo(condition.Parameter);

                return CreateJsonResponse(dto.User);
            }
            catch (AbstractApplicationException e)
            {
                return CreateErrorResponse(condition.Parameter.CaradaId, e);
            }
        }

        /// <summary>
        /// エラーレスポンスを生成する
        /// </summary>
        /// <param name="caradaId">CARADA ID</param>
        /// <param name="e">例外</param>
        /// <returns>エラーレスポンス</returns>
        private HttpResponseMessage CreateErrorResponse(string caradaId, AbstractApplicationException e)
        {
            AppLog.Error(e, "業務例外が発生しました。");
            HttpStatusCode code = HttpStatusCode.BadRequest;
            if (e.GetErrorCode() == "server_error")
            {
                code = HttpStatusCode.InternalServerError;
            }
            return CreateJsonResponse(new
            {
                carada_id = caradaId,
                error = e.GetErrorCode(),
                error_description = e.GetErrorMessage()
            }, code);
        }

        /// <summary>
        /// 管理ツール用サービスを生成する
        /// </summary>
        /// <returns>管理ツール用サービス</returns>
        protected virtual ManagementService CreateManagementService()
        {
            ServiceProxy sp = new ServiceProxy(typeof(ManagementService), DbContext, UserManager);
            return (ManagementService)sp.GetTransparentProxy();
        }
    }
}