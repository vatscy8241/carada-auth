﻿namespace CaradaAuthProvider.Api.Controllers.Results
{
    /// <summary>
    /// API実行結果のエラーメッセージ。
    /// </summary>
    public class ErrorMessageResult
    {
        /// <summary>
        /// エラー。
        /// </summary>
        public string Error;

        /// <summary>
        /// エラー内容。
        /// </summary>
        public string ErrorDescription;
    }
}