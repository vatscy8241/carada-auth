﻿namespace CaradaAuthProvider.Api.Controllers.Results
{
    /// <summary>
    /// アクセストークン取得の出力情報
    /// </summary>
    public class AccessTokenResult : AbstractResult
    {

        public string access_token { get; set; }
        public string token_type { get; set; }
        public string scope { get; set; }

        private AccessTokenResult()
        {
        }

        /// <summary>
        /// SSO用のレスポンスボディを生成します。
        /// 
        ///   scope == "authentication"の場合（認証のみ、アクセストークンを発行しない）
        ///   {
        ///     "access_token" : "authentication [UID] [IdProvider]",
        ///     "token_type" : "bearer",
        ///     "scope" : "[scope]"
        ///   }
        ///   
        ///   scope != "authentication"の場合
        ///   {
        ///     "access_token" : "[AccessToken] [UID] [IdProvider]",
        ///     "token_type" : "bearer",
        ///     "scope" : "[scope]"
        ///   }
        /// </summary>
        /// <param name="scope"></param>
        /// <param name="accessToken"></param>
        /// <param name="uid"></param>
        /// <param name="idProvider"></param>
        /// <returns></returns>
        public static AccessTokenResult CreateForSso(string scope, string accessToken, string uid, string idProvider)
        {
            return new AccessTokenResult()
            {
                access_token = string.Format("{0} {1} {2}", "authentication" == scope ? "authentication" : accessToken, uid, idProvider),
                token_type = "bearer",
                scope = scope,
            };
        }

        /// <summary>
        /// OAuth(非SSO)用のレスポンスボディを生成します。
        /// 
        ///   {
        ///     "access_token" : "[AccessToken]",
        ///     "token_type" : "bearer",
        ///     "scope" : "[scope]"
        ///   }
        /// </summary>
        /// <param name="scope"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static AccessTokenResult CreateForNotSso(string scope, string accessToken)
        {
            return new AccessTokenResult()
            {
                access_token = accessToken,
                token_type = "bearer",
                scope = scope
            };
        }
    }
}