﻿using CaradaAuthProvider.Api.Controllers.Conditions;
using CaradaAuthProvider.Api.Controllers.Jwt;
using CaradaAuthProvider.Api.Exceptions;
using CaradaAuthProvider.Api.Filters;
using CaradaAuthProvider.Api.Proxy;
using CaradaAuthProvider.Api.Services;
using CaradaAuthProvider.Api.Services.Dtos;
using Mti.Cpp.Logging;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CaradaAuthProvider.Api.Controllers
{
    /// <summary>
    /// 特権ユーザー用APIコントローラー
    /// </summary>
    [JwtValidationFilter]
    public class PrivilegeController : AbstractApiController
    {
        /// <summary>
        /// 代理アクセストークン取得API
        /// </summary>
        /// <param name="condition">JWT形式の代理アクセストークン取得情報</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        public HttpResponseMessage IssueAccessToken([FromBody]Jwt<IssueAgencyAccessTokenCondition> condition)
        {
            try
            {
                IssueAgencyAccessTokenResultDto dto = CreateAuthorizeService().IssueAgencyAccessToken(condition.Parameter, condition.User);

                return CreateJsonResponse(dto);
            }
            catch (AbstractApplicationException e)
            {
                return CreateErrorResponse(e);
            }
        }

        /// <summary>
        /// エラーレスポンスを生成する
        /// </summary>
        /// <param name="e">例外</param>
        /// <returns>エラーレスポンス</returns>
        private HttpResponseMessage CreateErrorResponse(AbstractApplicationException e)
        {
            AppLog.Error(e, "業務例外が発生しました。");
            HttpStatusCode code = HttpStatusCode.BadRequest;
            if (e.GetErrorCode() == "server_error")
            {
                code = HttpStatusCode.InternalServerError;
            }
            return CreateJsonResponse(new
            {
                error = e.GetErrorCode(),
                error_description = e.GetErrorMessage()
            }, code);
        }

        /// <summary>
        /// 認可サービスを生成する
        /// </summary>
        /// <returns>認可サービス</returns>
        protected virtual AuthorizeService CreateAuthorizeService()
        {
            ServiceProxy sp = new ServiceProxy(typeof(AuthorizeService), DbContext, UserManager);
            return (AuthorizeService)sp.GetTransparentProxy();
        }
    }
}