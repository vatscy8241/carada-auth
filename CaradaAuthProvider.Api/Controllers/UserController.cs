﻿using CaradaAuthProvider.Api.Controllers.Conditions;
using CaradaAuthProvider.Api.Exceptions;
using CaradaAuthProvider.Api.Filters;
using CaradaAuthProvider.Api.Proxy;
using CaradaAuthProvider.Api.Services;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CaradaAuthProvider.Api.Controllers
{
    /// <summary>
    /// ユーザー処理を行うコントローラー。
    /// </summary>
    [CustomActionFilter(1)]
    public class UserController : AbstractApiController
    {
        /// <summary>
        /// ユーザー情報取得API
        /// </summary>
        /// <param name="condition">ユーザー情報取得情報</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpGet]
        [ExceptionOccured(typeof(CaradaApplicationException), HttpStatusCode.BadRequest)]
        public HttpResponseMessage UserInfo([FromUri]UserInfoCondition condition)
        {
            return CreateJsonResponse(CreateUserService().UserInfo(condition));
        }

        /// <summary>
        /// パスワード検証API
        /// </summary>
        /// <param name="condition">パスワード検証情報</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        [ExceptionOccured(typeof(CaradaApplicationException), HttpStatusCode.BadRequest)]
        public HttpResponseMessage VerifyPassword([FromBody]VerifyPasswordCondition condition)
        {
            return CreateJsonResponse(CreateUserService().VerifyPassword(condition));
        }

        /// <summary>
        /// パスワード更新API
        /// </summary>
        /// <param name="condition">パスワード検証情報</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        [ExceptionOccured(typeof(CaradaApplicationException), HttpStatusCode.BadRequest)]
        public HttpResponseMessage RenewPassword([FromBody]RenewPasswordCondition condition)
        {
            CreateUserService().RenewPassword(condition);

            return Success;
        }

        /// <summary>
        /// パスワード再設定API
        /// </summary>
        /// <param name="condition">パスワード再設定情報</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        [ExceptionOccured(typeof(CaradaApplicationException), HttpStatusCode.BadRequest)]
        public HttpResponseMessage ResetPassword([FromBody]ResetPasswordCondition condition)
        {
            return CreateJsonResponse(CreateUserService().ResetPassword(condition));
        }

        /// <summary>
        /// メールアドレス変更開始API
        /// </summary>
        /// <param name="condition">メールアドレス変更開始情報</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        [ExceptionOccured(typeof(CaradaApplicationException), HttpStatusCode.BadRequest)]
        public HttpResponseMessage StartRenewEmail([FromBody]StartRenewEmailCondition condition)
        {
            return CreateJsonResponse(CreateUserService().StartRenewEmail(condition));
        }

        /// <summary>
        /// メールアドレス変更用認証コードメール送信API
        /// </summary>
        /// <param name="condition">メールアドレス変更用認証コードメール送信情報</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        [ExceptionOccured(typeof(CaradaApplicationException), HttpStatusCode.BadRequest)]
        public HttpResponseMessage SendRenewEmailVerifyCode([FromBody]SendRenewEmailVerifyCodeCondition condition)
        {
            CreateUserService().SendRenewEmailVerifyCode(condition);

            return Success;
        }

        /// <summary>
        /// メールアドレス更新API
        /// </summary>
        /// <param name="condition">メールアドレス更新情報</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        [ExceptionOccured(typeof(CaradaApplicationException), HttpStatusCode.BadRequest)]
        public HttpResponseMessage RenewEmail([FromBody]RenewEmailCondition condition)
        {
            CreateUserService().RenewEmail(condition);

            return Success;
        }

        /// <summary>
        /// メールアドレス再設定API
        /// </summary>
        /// <param name="condition">メールアドレス再設定情報</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        [ExceptionOccured(typeof(CaradaApplicationException), HttpStatusCode.BadRequest)]
        public HttpResponseMessage ResetEmail([FromBody]ResetEmailCondition condition)
        {
            return CreateJsonResponse(CreateUserService().ResetEmail(condition));
        }

        /// <summary>
        /// 秘密の質問更新API
        /// </summary>
        /// <param name="condition">秘密の質問更新情報</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        [ExceptionOccured(typeof(CaradaApplicationException), HttpStatusCode.BadRequest)]
        public HttpResponseMessage RenewSecretQuestion([FromBody]RenewSecretQuestionCondition condition)
        {
            CreateUserService().RenewSecretQuestion(condition);

            return Success;
        }

        /// <summary>
        /// ID通知API
        /// </summary>
        /// <param name="condition">ID通知情報</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        [ExceptionOccured(typeof(CaradaApplicationException), HttpStatusCode.BadRequest)]
        public HttpResponseMessage SendCaradaId([FromBody]SendCaradaIdCondition condition)
        {
            CreateUserService().SendCaradaId(condition);

            return Success;
        }

        /// <summary>
        /// ユーザーサービスを生成する
        /// </summary>
        /// <returns>ユーザーサービス</returns>
        protected virtual UserService CreateUserService()
        {
            ServiceProxy sp = new ServiceProxy(typeof(UserService), DbContext, UserManager);
            return (UserService)sp.GetTransparentProxy();

        }
    }
}