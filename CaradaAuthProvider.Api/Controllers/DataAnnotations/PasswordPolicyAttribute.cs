﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace CaradaAuthProvider.Api.Controllers.DataAnnotations
{
    /// <summary>
    /// パスワードのバリデータ。
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class PasswordPolicyAttribute : ValidationAttribute
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public PasswordPolicyAttribute()
        {
            ErrorMessageResourceType = typeof(Properties.Message);
            ErrorMessageResourceName = "property_value_password_policy";
        }

        /// <summary>
        /// <see cref="ValidationAttribute.IsValid(object)"/>
        /// </summary>
        /// <param name="value">検証するオブジェクトの値</param>
        /// <returns>true:有効/false:無効</returns>
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            if (!(typeof(string) == value.GetType()))
            {
                return false;
            }

            var targetValue = value as string;
            if (string.IsNullOrEmpty(targetValue))
            {
                return true;
            }

            // パスワードが8～128文字以内か判定
            if (targetValue.Length < 8 || targetValue.Length > 128)
            {
                return false;
            }

            // 半角英数記号かどうか判定する
            if (!Regex.IsMatch(targetValue, @"^[0-9a-zA-Z!-/:-@\[-`{-~]+$"))
            {
                return false;
            }

            // 半角英字と半角数字が両方使用されているかどうか判定する
            if (!Regex.IsMatch(targetValue, @"^(?=.*?[a-zA-Z])(?=.*?\d)(.*){1,}$"))
            {
                return false;
            }
            return true;
        }
    }
}