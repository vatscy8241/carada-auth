﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace CaradaAuthProvider.Api.Controllers.DataAnnotations
{
    /// <summary>
    /// ハイフン無しのGUIDのバリデータ。
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class OriginalGuidAttribute : ValidationAttribute
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public OriginalGuidAttribute()
            : base()
        {
            ErrorMessageResourceType = typeof(Properties.Message);
            ErrorMessageResourceName = "property_value_original_guid_type";
        }

        /// <summary>
        /// <see cref="ValidationAttribute.IsValid(object)"/>
        /// </summary>
        /// <param name="value">検証するオブジェクトの値</param>
        /// <returns>true:有効/false:無効</returns>
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            if (!(typeof(string) == value.GetType()))
            {
                return false;
            }

            var targetValue = value as string;
            if (string.IsNullOrEmpty(targetValue))
            {
                return true;
            }

            // 半角英数32文字かどうか判定する
            if (!Regex.IsMatch(targetValue, "^[0-9a-zA-Z]{32}$"))
            {
                return false;
            }
            return true;
        }
    }
}