﻿using CaradaAuthProvider.Api.Properties;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace CaradaAuthProvider.Api.Controllers.DataAnnotations
{
    /// <summary>
    /// 秘密の質問の答えのAttributeです。
    /// </summary>
    public class SecurityAnswerAttribute : ValidationAttribute
    {
        /// <summary>
        /// コンストラクタ。
        /// </summary>
        public SecurityAnswerAttribute()
        {
            ErrorMessageResourceType = typeof(Properties.Message);
            ErrorMessageResourceName = "invalid_security_answer";
        }

        /// <summary>
        /// 許容する文字セットかどうかを確認します。
        /// </summary>
        /// <param name="value">パラメータvalue</param>
        /// <returns>判定結果</returns>
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            var str = value as string;
            return str != null && str.All(a => Settings.Default.AllowSecurityAnswer.Contains(a));
        }
    }
}