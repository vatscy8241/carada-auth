﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CaradaAuthProvider.Api.Controllers.DataAnnotations
{
    /// <summary>
    /// <see cref="RangeAttribute"/>の拡張クラス。
    /// nullが指定された場合も有効な値となるようにした。
    /// </summary>
    public class RangeExtendAttribute : RangeAttribute
    {
        /// <summary>
        /// <see cref="RangeAttribute.RangeAttribute(int, int)"/>
        /// </summary>
        /// <param name="minimum">データフィールド値の最小許容値。</param>
        /// <param name="maximum">データフィールド値の最大許容値。</param>
        public RangeExtendAttribute(int minimum, int maximum)
            : base(minimum, maximum)
        {
        }

        /// <summary>
        /// <see cref="RangeAttribute.RangeAttribute(double, double)"/>
        /// </summary>
        /// <param name="minimum">データフィールド値の最小許容値。</param>
        /// <param name="maximum">データフィールド値の最大許容値。</param>
        public RangeExtendAttribute(double minimum, double maximum)
            : base(minimum, maximum)
        {
        }

        /// <summary>
        /// <see cref="RangeAttribute.RangeAttribute(Type, string, string)"/>
        /// </summary>
        /// <param name="type">テストするオブジェクトの型。</param>
        /// <param name="minimum">データフィールド値の最小許容値。</param>
        /// <param name="maximum">データフィールド値の最大許容値。</param>
        public RangeExtendAttribute(Type type, string minimum, string maximum)
            : base(type, minimum, maximum)
        {
        }

        /// <summary>
        /// <see cref="RangeAttribute.IsValid(object)"/>
        /// </summary>
        /// <param name="value">検証するデータフィールド</param>
        /// <returns>true:指定した値が範囲に入っている/false:それ以外</returns>
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }
            return base.IsValid(value);
        }
    }
}