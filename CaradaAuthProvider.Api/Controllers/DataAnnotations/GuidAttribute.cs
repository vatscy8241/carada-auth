﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CaradaAuthProvider.Api.Controllers.DataAnnotations
{
    /// <summary>
    /// Guidのバリデータ。
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class GuidAttribute : ValidationAttribute
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public GuidAttribute()
            : base()
        {
            ErrorMessageResourceType = typeof(Properties.Message);
            ErrorMessageResourceName = "property_value_guid_type";
        }

        /// <summary>
        /// <see cref="ValidationAttribute.IsValid(object)"/>
        /// </summary>
        /// <param name="value">検証するオブジェクトの値</param>
        /// <returns>true:有効/false:無効</returns>
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            if (typeof(Guid) == value.GetType())
            {
                return true;
            }

            if (!(typeof(string) == value.GetType()))
            {
                return false;
            }

            var targetValue = value as string;
            if (string.IsNullOrEmpty(targetValue))
            {
                return true;
            }

            Guid newGuid;
            return Guid.TryParse(targetValue, out newGuid);
        }
    }
}