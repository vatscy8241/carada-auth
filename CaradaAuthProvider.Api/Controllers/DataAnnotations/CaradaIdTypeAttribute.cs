﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace CaradaAuthProvider.Api.Controllers.DataAnnotations
{
    /// <summary>
    /// CaradaIdのバリデータ。
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class CaradaIdTypeAttribute : ValidationAttribute
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CaradaIdTypeAttribute()
        {
            ErrorMessageResourceType = typeof(Properties.Message);
            ErrorMessageResourceName = "property_value_carada_id_type";
        }

        /// <summary>
        /// <see cref="ValidationAttribute.IsValid(object)"/>
        /// </summary>
        /// <param name="value">検証するオブジェクトの値</param>
        /// <returns>true:有効/false:無効</returns>
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            if (!(typeof(string) == value.GetType()))
            {
                return false;
            }

            var targetValue = value as string;
            if (string.IsNullOrEmpty(targetValue))
            {
                return true;
            }

            // CaradaIdが128文字以内か判定
            if (targetValue.Length > 128)
            {
                return false;
            }

            // 半角英数記号(「-」「_」「.」)かどうか判定する
            if (!Regex.IsMatch(targetValue, "^(?=[0-9a-z])([-_.]?[a-z0-9])*[-_.]?$"))
            {
                return false;
            }
            return true;
        }
    }
}