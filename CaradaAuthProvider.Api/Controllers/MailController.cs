﻿using CaradaAuthProvider.Api.Controllers.Conditions;
using CaradaAuthProvider.Api.Exceptions;
using CaradaAuthProvider.Api.Filters;
using CaradaAuthProvider.Api.Proxy;
using CaradaAuthProvider.Api.Services;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace CaradaAuthProvider.Api.Controllers
{
    /// <summary>
    /// メール処理を行うコントローラー。
    /// </summary>
    [CustomActionFilter(1)]
    public class MailController : AbstractApiController
    {
        /// <summary>
        /// 指定ユーザーのメールアドレスへメールを送信するAPI。
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("send")]
        [ExceptionOccured(typeof(CaradaApplicationException), HttpStatusCode.BadRequest)]
        public async Task<HttpResponseMessage> Send([FromBody]SendMailCondition condition)
        {
            var mailService = CreateMailService();
            await mailService.SendMail(condition);

            return Success;
        }

        /// <summary>
        /// メールサービスを生成する。
        /// </summary>
        /// <returns>メールサービス</returns>
        protected virtual MailService CreateMailService()
        {
            ServiceProxy sp = new ServiceProxy(typeof(MailService), DbContext, UserManager);
            return (MailService)sp.GetTransparentProxy();
        }
    }
}