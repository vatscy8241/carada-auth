﻿using CaradaAuthProvider.Api.Filters;
using CaradaAuthProvider.Api.IdentityManagers;
using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Properties;
using Microsoft.AspNet.Identity.Owin;
using NLog;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;

namespace CaradaAuthProvider.Api.Controllers
{
    /// <summary>
    /// APIコントローラ基底クラス。
    /// </summary>
    [ConditionValidationFilter(1)]
    [ExceptionMessageFilter(999)]
    public abstract class AbstractApiController : ApiController
    {
        /// <summary>
        /// CaradaAuthProviderDbContext
        /// </summary>
        private CaradaAuthProviderDbContext _caradaIdPDbContext;


        /// <summary>
        /// ApplicationUserManager
        /// </summary>
        private ApplicationUserManager _userManager;

        /// <summary>
        /// アプリケーションログ
        /// </summary>
        protected static readonly ILogger APP_LOG = LogManager.GetLogger(Settings.Default.LogTargetApiApplication);

        /// <summary>
        /// 監査ログ
        /// </summary>
        protected static readonly ILogger AUDIT_LOG = LogManager.GetLogger(Settings.Default.LogTargetAudit);

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public AbstractApiController()
        {
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="userManager">ApplicationUserManager</param>
        public AbstractApiController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        /// <summary>
        /// CaradaAuthProviderDbContext
        /// </summary>
        public CaradaAuthProviderDbContext CaradaIdPDb
        {
            get
            {
                return _caradaIdPDbContext ?? HttpContext.Current.GetOwinContext().Get<CaradaAuthProviderDbContext>();
            }
            private set
            {
                _caradaIdPDbContext = value;
            }
        }

        /// <summary>
        /// UserManager
        /// </summary>
        protected virtual ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        /// <summary>
        /// DBコンテキスト
        /// </summary>
        protected virtual CaradaAuthProviderDbContext DbContext
        {
            get
            {
                return HttpContext.Current.GetOwinContext().Get<CaradaAuthProviderDbContext>();
            }
        }

        /// <summary>
        /// 成功を示す汎用レスポンス
        /// </summary>
        protected HttpResponseMessage Success
        {
            get
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { Result = "SUCCESS" });
            }
        }

        /// <summary>
        /// オブジェクトからレスポンスを生成する
        /// </summary>
        /// <typeparam name="T">オブジェクトの型</typeparam>
        /// <param name="obj">オブジェクト</param>
        /// <param name="statusCode">ステータスコード</param>
        /// <returns>レスポンス</returns>
        protected HttpResponseMessage CreateJsonResponse<T>(T obj, HttpStatusCode statusCode = HttpStatusCode.OK)
        {
            HttpResponseMessage response = new HttpResponseMessage(statusCode);
            response.Content = new ObjectContent<T>(obj, new JsonMediaTypeFormatter());
            return response;
        }
    }
}