﻿using CaradaAuthProvider.Api.Controllers.Conditions;
using CaradaAuthProvider.Api.Exceptions;
using CaradaAuthProvider.Api.Filters;
using CaradaAuthProvider.Api.Proxy;
using CaradaAuthProvider.Api.Services;
using CaradaAuthProvider.Api.Services.Dtos;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CaradaAuthProvider.Api.Controllers
{
    /// <summary>
    /// 認可処理を行うコントローラー
    /// </summary>
    [CustomActionFilter(1)]
    public class AuthorizeController : AbstractApiController
    {
        /// <summary>
        /// 認可API
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        [HttpPost]
        [ExceptionOccured(typeof(CaradaApplicationException), HttpStatusCode.BadRequest)]
        [ExceptionOccured(typeof(AuthleteException), HttpStatusCode.InternalServerError)]
        public HttpResponseMessage Authorize([FromBody]AuthorizeCondition condition)
        {
            AuthorizeResultDto ret = CreateAuthorizeService().Authorize(condition);

            return CreateJsonResponse(new
            {
                AccessToken = ret.AccessToken,
                RefreshToken = ret.RefreshToken
            });
        }

        /// <summary>
        /// アクセストークン検証API
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        [HttpGet]
        [ExceptionOccured(typeof(AuthleteException), HttpStatusCode.InternalServerError)]
        public HttpResponseMessage Introspect([FromUri]TokenIntrospectCondition condition)
        {
            TokenIntrospectResultDto dto = CreateAuthorizeService().Introspect(condition);

            return CreateJsonResponse(dto.TokenIntrospect);
        }

        /// <summary>
        /// トークンリフレッシュAPI
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        [HttpPost]
        [ExceptionOccured(typeof(AuthleteException), HttpStatusCode.InternalServerError)]
        public HttpResponseMessage Refresh([FromBody]TokenRefreshCondition condition)
        {
            TokenRefreshResultDto dto = CreateAuthorizeService().Refresh(condition);

            return CreateJsonResponse(dto.TokenRefresh);
        }

        /// <summary>
        /// 認可サービスを生成する
        /// </summary>
        /// <returns>認可サービス</returns>
        protected virtual AuthorizeService CreateAuthorizeService()
        {
            ServiceProxy sp = new ServiceProxy(typeof(AuthorizeService), DbContext, UserManager);
            return (AuthorizeService)sp.GetTransparentProxy();

        }

    }
}