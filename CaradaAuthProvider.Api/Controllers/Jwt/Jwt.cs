﻿using CaradaAuthProvider.Api.Controllers.Conditions;
using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using CaradaAuthProvider.Api.Properties;
using NLog;
using System.Linq;

namespace CaradaAuthProvider.Api.Controllers.Jwt
{
    /// <summary>
    /// JWTの検証と固有パラメータのバインド・検証を行います。
    /// </summary>
    public class Jwt<TParameter> : JwtBase<TParameter>
        where TParameter : AbstractCondition, new()
    {
        private CaradaAuthProviderDbContext _context = new CaradaAuthProviderDbContext();
        private static readonly ILogger APP_LOG = LogManager.GetLogger(Settings.Default.LogTargetApiApplication);

        /// <summary>
        /// 発行者IDに対するシークレットキーを取得します。
        /// 見つからない場合にはnullを返します。
        /// </summary>
        /// <param name="issuer">発行者ID</param>
        /// <returns>シークレットキー</returns>
        protected override string GetSecretKey(string issuer)
        {
            Issuers tokenIssuer = _context.Issuers.SingleOrDefault(ti => ti.IssuerId == issuer);
            if (tokenIssuer == null)
            {
                APP_LOG.Error($"[JWT_Invalid] DBには未登録のIssuerです。(issuer={issuer})");
            }
            return tokenIssuer != null ? tokenIssuer.SecretKey : null;
        }
    }
}