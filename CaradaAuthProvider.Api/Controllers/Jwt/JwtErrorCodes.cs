﻿
namespace CaradaAuthProvider.Api.Controllers.Jwt
{
    /// <summary>
    /// JWT検証時のエラーコードです。
    /// </summary>
    public static class JwtErrorCodes
    {
        public const string JwtInvalidRequest = "jwt_invalid_request";
        public const string JwtInvalidIssuer = "jwt_invalid_issuer";
        public const string JwtInvalidUser = "jwt_invalid_user";
        public const string JwtInvalidLifeTime = "jwt_invalid_life_time";
    }
}