﻿using CaradaAuthProvider.Api.Controllers.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace CaradaAuthProvider.Api.Controllers.Conditions
{
    /// <summary>
    /// メールアドレス更新APIリクエストパラメータ
    /// </summary>
    public class RenewEmailCondition : AbstractCondition
    {
        /// <summary>
        /// チケットID
        /// </summary>
        [Required]
        [OriginalGuid]
        public string TicketId { get; set; }

        /// <summary>
        /// 件名
        /// </summary>
        [Required]
        public string Subject { get; set; }

        /// <summary>
        /// 本文
        /// </summary>
        [Required]
        public string Body { get; set; }
    }
}