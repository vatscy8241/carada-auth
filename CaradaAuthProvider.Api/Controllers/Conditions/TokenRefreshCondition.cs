﻿using System.ComponentModel.DataAnnotations;

namespace CaradaAuthProvider.Api.Controllers.Conditions
{
    /// <summary>
    /// トークンリフレッシュの入力情報クラス
    /// </summary>
    public class TokenRefreshCondition : AbstractCondition
    {
        /// <summary>
        /// リフレッシュトークン
        /// </summary>
        [Required]
        public string RefreshToken { get; set; }
    }
}
