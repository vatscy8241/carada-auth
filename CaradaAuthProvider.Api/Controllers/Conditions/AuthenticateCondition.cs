﻿using CaradaAuthProvider.Api.Controllers.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace CaradaAuthProvider.Api.Controllers.Conditions
{
    /// <summary>
    /// 認証APIリクエストパラメータ
    /// </summary>
    public class AuthenticateCondition : AbstractCondition
    {
        /// <summary>
        /// CARADA ID
        /// </summary>
        [Required]
        [CaradaIdType]
        public string CaradaId { get; set; }

        /// <summary>
        /// パスワード
        /// </summary>
        [Required]
        [PasswordPolicy]
        public string Password { get; set; }

        /// <summary>
        /// 本人確認必須フラグ
        /// </summary>
        [Required]
        public bool ConfirmedFlag { get; set; }

        /// <summary>
        /// チケットの有効期限（分） 1-120で指定可
        /// ※指定なしのデフォルト60分
        /// </summary>
        [RangeExtend(1, 120)]
        public int? TicketExpiresMinute
        {
            get
            {
                return ticketExpiresMinute;
            }
            set
            {
                if (!value.HasValue)
                {
                    return;
                }
                ticketExpiresMinute = (int)value;
            }
        }

        /// <summary>
        /// <see cref="TicketExpiresMinute"/>デフォルト60。
        /// </summary>
        private int ticketExpiresMinute = 60;
    }
}
