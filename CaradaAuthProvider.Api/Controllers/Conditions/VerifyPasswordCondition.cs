﻿using CaradaAuthProvider.Api.Controllers.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace CaradaAuthProvider.Api.Controllers.Conditions
{
    /// <summary>
    /// パスワード検証の入力情報クラス
    /// </summary>
    public class VerifyPasswordCondition : AbstractCondition
    {
        /// <summary>
        /// UID
        /// </summary>
        [Required]
        [Guid]
        public string Uid { get; set; }

        /// <summary>
        /// パスワード
        /// </summary>
        [Required]
        [PasswordPolicy]
        public string Password { get; set; }

        /// <summary>
        /// チケットの有効期限（分） 1-120で指定可
        /// ※指定なしのデフォルト60分
        /// </summary>
        [RangeExtend(1, 120)]
        public int? TicketExpiresMinute
        {
            get
            {
                return ticketExpiresMinute;
            }
            set
            {
                if (!value.HasValue)
                {
                    return;
                }
                ticketExpiresMinute = (int)value;
            }
        }

        /// <summary>
        /// <see cref="TicketExpiresMinute"/>デフォルト60。
        /// </summary>
        private int ticketExpiresMinute = 60;
    }
}