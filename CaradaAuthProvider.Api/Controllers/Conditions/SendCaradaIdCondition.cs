﻿using CaradaAuthProvider.Api.Controllers.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace CaradaAuthProvider.Api.Controllers.Conditions
{
    /// <summary>
    /// ID通知APIリクエストパラメータ
    /// </summary>
    public class SendCaradaIdCondition : AbstractCondition
    {
        /// <summary>
        /// メールアドレス
        /// </summary>
        [Required]
        [EmailAddressJP]
        public string Email { get; set; }

        /// <summary>
        /// 件名
        /// </summary>
        [Required]
        public string Subject { get; set; }

        /// <summary>
        /// 本文
        /// </summary>
        [Required]
        public string Body { get; set; }
    }
}