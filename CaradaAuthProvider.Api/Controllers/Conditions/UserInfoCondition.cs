﻿using CaradaAuthProvider.Api.Controllers.DataAnnotations;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace CaradaAuthProvider.Api.Controllers.Conditions
{
    /// <summary>
    /// ユーザー情報取得の入力情報クラス
    /// </summary>
    public class UserInfoCondition : AbstractCondition
    {
        /// <summary>
        /// UID
        /// </summary>
        [Required]
        [Guid]
        public string Uid { get; set; }
    }
}