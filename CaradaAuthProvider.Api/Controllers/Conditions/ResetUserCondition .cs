﻿using CaradaAuthProvider.Api.Controllers.DataAnnotations;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace CaradaAuthProvider.Api.Controllers.Conditions
{
    /// <summary>
    /// ユーザー情報リセットAPI用Condition
    /// </summary>
    public class ResetUserCondition : AbstractCondition
    {
        /// <summary>
        /// CARADA ID
        /// </summary>
        [Required]
        [JsonProperty("carada_id")]
        [CaradaIdType]
        public string CaradaId { get; set; }

        /// <summary>
        /// 初期化フラグ
        /// </summary>
        [JsonProperty("initialization")]
        public string Initialization { get; set; }
    }
}