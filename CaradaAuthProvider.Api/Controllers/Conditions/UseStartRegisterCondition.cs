﻿using CaradaAuthProvider.Api.Controllers.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace CaradaAuthProvider.Api.Controllers.Conditions
{
    /// <summary>
    /// 利用開始登録リクエストパラメーター
    /// </summary>
    public class UseStartRegisterCondition : AbstractCondition
    {
        /// <summary>
        /// チケットID
        /// </summary>
        [Required]
        [OriginalGuid]
        public string TicketId { get; set; }

        /// <summary>
        /// 秘密の質問ID
        /// </summary>
        [Required]
        public int SecretQuestionId { get; set; }

        /// <summary>
        /// 秘密の質問の回答
        /// </summary>
        [Required]
        [SecurityAnswer]
        public string SecretQuestionAnswer { get; set; }
    }
}