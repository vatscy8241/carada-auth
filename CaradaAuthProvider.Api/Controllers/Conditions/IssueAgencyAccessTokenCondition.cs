﻿using CaradaAuthProvider.Api.Controllers.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace CaradaAuthProvider.Api.Controllers.Conditions
{
    /// <summary>
    /// 代理アクセストークン取得APIリクエストパラメーター
    /// </summary>
    public class IssueAgencyAccessTokenCondition : AbstractCondition
    {
        /// <summary>
        /// クライアントID
        /// </summary>
        [Required]
        public string client_id { get; set; }

        /// <summary>
        /// UID
        /// </summary>
        [Required]
        [Guid]
        public string uid { get; set; }

        /// <summary>
        /// チケットの有効期限（分） 1-120で指定可
        /// ※指定なしのデフォルト15分
        /// </summary>
        [RangeExtend(1, 120)]
        public int? expires_minute
        {
            get
            {
                return expiresMinute;
            }
            set
            {
                if (!value.HasValue)
                {
                    return;
                }
                expiresMinute = (int)value;
            }
        }

        /// <summary>
        /// <see cref="expires_minute"/>デフォルト15。
        /// </summary>
        private int expiresMinute = 15;
    }
}