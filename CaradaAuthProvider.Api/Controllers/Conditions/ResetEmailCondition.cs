﻿using CaradaAuthProvider.Api.Controllers.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace CaradaAuthProvider.Api.Controllers.Conditions
{
    /// <summary>
    /// メールアドレス再設定APIリクエストパラメータ
    /// </summary>
    public class ResetEmailCondition : AbstractCondition
    {
        /// <summary>
        /// CARADA ID
        /// </summary>
        [Required]
        [CaradaIdType]
        public string CaradaId { get; set; }

        /// <summary>
        /// パスワード
        /// </summary>
        [Required]
        [PasswordPolicy]
        public string Password { get; set; }

        /// <summary>
        /// 秘密の質問ID
        /// </summary>
        [Required]
        public int SecretQuestionId { get; set; }

        /// <summary>
        /// 秘密の質問の回答
        /// </summary>
        [Required]
        [SecurityAnswer]
        public string SecretQuestionAnswer { get; set; }

        /// <summary>
        /// メールアドレス
        /// </summary>
        [Required]
        [EmailAddressJP]
        public string Email { get; set; }

        /// <summary>
        /// 件名
        /// </summary>
        [Required]
        public string Subject { get; set; }

        /// <summary>
        /// 本文
        /// </summary>
        [Required]
        public string Body { get; set; }

        /// <summary>
        /// チケットの有効期限（分） 1-120で指定可
        /// ※指定なしのデフォルト60分
        /// </summary>
        [RangeExtend(1, 120)]
        public int? TicketExpiresMinute
        {
            get
            {
                return ticketExpiresMinute;
            }
            set
            {
                if (!value.HasValue)
                {
                    return;
                }
                ticketExpiresMinute = (int)value;
            }
        }

        /// <summary>
        /// <see cref="TicketExpiresMinute"/>デフォルト60。
        /// </summary>
        private int ticketExpiresMinute = 60;
    }
}