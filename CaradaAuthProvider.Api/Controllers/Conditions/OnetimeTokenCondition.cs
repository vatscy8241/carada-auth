﻿using CaradaAuthProvider.Api.Controllers.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace CaradaAuthProvider.Api.Controllers.Conditions
{
    // TODO 使用していない？
    /// <summary>
    /// ワンタイムトークン発行の入力情報クラス
    /// </summary>
    public class OnetimeTokentCondition : AbstractCondition
    {

        /// <summary>
        /// 統合認可プロバイダUID
        /// </summary>
        [Required]
        [Guid]
        public string Uid { get; set; }
        /// <summary>
        /// 送信元のクライアントID
        /// </summary>
        [Required]
        public string ClientId { get; set; }
        /// <summary>
        /// トークン種別
        /// </summary>
        [Required]
        public string TokenType { get; set; }
    }
}