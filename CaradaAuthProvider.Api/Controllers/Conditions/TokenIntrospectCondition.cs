﻿using System.ComponentModel.DataAnnotations;

namespace CaradaAuthProvider.Api.Controllers.Conditions
{
    /// <summary>
    /// アクセストークン検証の入力情報クラス
    /// </summary>
    public class TokenIntrospectCondition : AbstractCondition
    {
        /// <summary>
        /// アクセストークン
        /// </summary>
        [Required]
        public string AccessToken { get; set; }
    }
}