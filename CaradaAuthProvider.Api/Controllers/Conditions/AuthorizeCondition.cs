﻿using CaradaAuthProvider.Api.Controllers.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace CaradaAuthProvider.Api.Controllers.Conditions
{
    /// <summary>
    /// 認可の入力情報クラス
    /// </summary>
    public class AuthorizeCondition : AbstractCondition
    {

        /// <summary>
        /// クライアントID
        /// </summary>
        [Required]
        public string ClientId { get; set; }

        /// <summary>
        /// UID
        /// </summary>
        [Required]
        [Guid]
        public string Uid { get; set; }
    }
}