﻿using CaradaAuthProvider.Api.Controllers.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace CaradaAuthProvider.Api.Controllers.Conditions
{
    /// <summary>
    /// 二段階認証用認証コードメール送信APIリクエストパラメータ
    /// </summary>
    public class SendTwoFactorVerifyCodeCondition : AbstractCondition
    {
        /// <summary>
        /// UID
        /// </summary>
        [Required]
        [Guid]
        public string Uid { get; set; }

        /// <summary>
        /// 件名
        /// </summary>
        [Required]
        public string Subject { get; set; }

        /// <summary>
        /// 本文
        /// </summary>
        [Required]
        public string Body { get; set; }

        /// <summary>
        /// チケットの有効期限（分） 1-120で指定可
        /// ※指定なしのデフォルト60分
        /// </summary>
        [RangeExtend(1, 120)]
        public int? TicketExpiresMinute
        {
            get
            {
                return ticketExpiresMinute;
            }
            set
            {
                if (!value.HasValue)
                {
                    return;
                }
                ticketExpiresMinute = (int)value;
            }
        }

        /// <summary>
        /// <see cref="TicketExpiresMinute"/>デフォルト60。
        /// </summary>
        private int ticketExpiresMinute = 60;
    }
}