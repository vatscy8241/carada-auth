﻿using CaradaAuthProvider.Api.Controllers.DataAnnotations;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace CaradaAuthProvider.Api.Controllers.Conditions
{
    /// <summary>
    /// 利用開始前ユーザー登録API用Condition
    /// </summary>
    public class RegistUserAddCondition : AbstractCondition
    {
        /// <summary>
        /// CARADA ID
        /// </summary>
        [Required]
        [JsonProperty("carada_id")]
        [CaradaIdType]
        public string CaradaId { get; set; }
    }
}