﻿using CaradaAuthProvider.Api.Controllers.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace CaradaAuthProvider.Api.Controllers.Conditions
{
    /// <summary>
    /// パスワード更新の入力情報クラス
    /// </summary>
    public class RenewPasswordCondition : AbstractCondition
    {
        /// <summary>
        /// チケットID
        /// </summary>
        [Required]
        [OriginalGuid]
        public string TicketId { get; set; }

        /// <summary>
        /// パスワード
        /// </summary>
        [Required]
        [PasswordPolicy]
        public string Password { get; set; }
    }
}