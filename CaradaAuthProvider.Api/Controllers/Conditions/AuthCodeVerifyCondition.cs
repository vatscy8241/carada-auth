﻿using CaradaAuthProvider.Api.Controllers.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace CaradaAuthProvider.Api.Controllers.Conditions
{
    /// <summary>
    /// 認証コード検証APIリクエストパラメータ
    /// </summary>
    public class AuthCodeVerifyCondition : AbstractCondition
    {
        /// <summary>
        /// チケットID
        /// </summary>
        [Required]
        [OriginalGuid]
        public string TicketId { get; set; }

        /// <summary>
        /// 認証コード
        /// </summary>
        [Required]
        [OriginalGuid]
        public string VerifyCode { get; set; }
    }
}