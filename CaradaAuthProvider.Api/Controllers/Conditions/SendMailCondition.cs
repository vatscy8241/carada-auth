﻿using CaradaAuthProvider.Api.Controllers.DataAnnotations;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace CaradaAuthProvider.Api.Controllers.Conditions
{
    /// <summary>
    /// メール送信の入力情報クラス。
    /// </summary>
    public class SendMailCondition : AbstractCondition
    {
        /// <summary>
        /// ユーザー一意識別子。
        /// </summary>
        [Guid]
        public string Uid { get; set; }

        /// <summary>
        /// 件名 URF-8。
        /// </summary>
        [Required]
        public string Subject { get; set; }

        /// <summary>
        /// 本文 UTF-9。
        /// </summary>
        [Required]
        public string Body { get; set; }

        /// <summary>
        /// チケットID
        /// </summary>
        [OriginalGuid]
        public string TicketId { get; set; }
    }
}