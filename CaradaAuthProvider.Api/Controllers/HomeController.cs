﻿using System.Web.Mvc;

namespace CaradaAuthProvider.Api.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "CARADA認証認可APIサーバー";

            return View();
        }

    }
}
