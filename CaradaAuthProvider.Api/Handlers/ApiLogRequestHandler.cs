﻿using CaradaAuthProvider.Api.Utils;
using Mti.Cpp.Logging;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace CaradaAuthProvider.Api.Handlers
{
    /// <summary>
    /// Api への リクエストパラメータをログに書き出す
    /// </summary>
    public class ApiLogRequestHandler : DelegatingHandler
    {
        /// <summary>
        /// Api へのリクエストパラメータをログ出力する
        /// </summary>
        /// <param name="request">HttpRequestMessage</param>
        /// <param name="cancellationToken">CancellationToken</param>
        /// <returns></returns>
        protected override async Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request, CancellationToken cancellationToken)
        {
            // log request body
            var rawRequestString = await request.Content.ReadAsStringAsync();
            WriteRequestResponseLog("REQUEST", rawRequestString);

            // let other handlers process the request
            var result = await base.SendAsync(request, cancellationToken);

            // log response body
            var rawResponseString = await result.Content.ReadAsStringAsync();
            WriteRequestResponseLog("RESPONSE", rawResponseString);

            return result;
        }

        private void WriteRequestResponseLog(string parameterType, string queryStrings)
        {
            var maskedString = LogUtilities.GetParameterString(queryStrings);

            if (!string.IsNullOrEmpty(maskedString))
            {
                AppLog.Audit(null, parameterType, $"parameters:[{maskedString}]");
            }
            else
            {
                AppLog.Audit(null, parameterType, "");
            }
        }
    }
}