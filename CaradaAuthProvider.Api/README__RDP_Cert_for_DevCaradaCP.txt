
;;; ===========================================================================

;;; 本番環境以外の環境で共通のRDP証明書です。（本番のみ異なります）

;;; ＜インストール方法＞
;;; pfxファイルをダブルクリックしてウィザードから
;;; RDP-Dev-CaradaCP.pfxを自分のマシンにインポートする。
;;; または、
;;; 下記コマンドをVisualStudioの開発者コマンドプロンプトから実行する。
;;; certutil -user -p fmpAim4N -importpfx "RDP-Dev-CaradaCP.pfx"

;;; ■まとめ
;;; 
;;; 種別：リモートデスクトップ用証明書
;;; インストール時パスワード：fmpAim4N
;;; サムプリント：‎7f f4 56 c1 75 14 d1 cb 73 8d 9f 12 8e 98 46 7b dd ca 6b 53

;;; RDP設定時に下記を設定して下さい（各プロジェクト共通、本番用は別途展開）。
;;; RDP-Account: ccp_admin
;;; RDP-Password: fmpAim4N

;;; ===========================================================================

;;; ＜リモートデスクトップ用証明書の作成手順（再作成する場合）＞

;;;
;;; ＜注意事項＞
;;; 　SSL証明書の手順と同じです。
;;; 　各環境で共通で使用するRDP証明書です。

;;; ＜初回の生成時のみ＞
;;; 自己証明書を作成 - 最後に２回パスワード入力あり（同じものを入力する）

makecert -sky exchange -r -n "CN=RDP Dev-CaradaCP" -pe -a sha1 -len 2048 "RDP-Dev-CaradaCP.cer" -sv "RDP-Dev-CaradaCP.pvk"


;;; ＜初回の生成時のみ＞
;;; 証明書と秘密鍵を同梱しパスワードで暗号化する

pvk2pfx -pvk "RDP-Dev-CaradaCP.pvk" -spc "RDP-Dev-CaradaCP.cer" -pfx "RDP-Dev-CaradaCP.pfx" -pi fmpAim4N


