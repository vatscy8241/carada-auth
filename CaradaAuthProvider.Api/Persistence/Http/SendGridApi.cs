﻿using CaradaAuthProvider.Api.Persistence.Http.Entities;
using CaradaAuthProvider.Api.Persistence.Http.Manager;
using Mti.Cpp.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Text;
using System.Web;

namespace CaradaAuthProvider.Api.Persistence.Http
{
    /// <summary>
    /// SendGridApiV3を使用するクラス。
    /// SendGridのライブラリに必要な機能が実装されたら廃止する。
    /// </summary>
    public class SendGridApi
    {
        /// <summary>
        /// Authorizationのキー。
        /// </summary>
        private const string REQUEST_AUTHORIZATION_KEY = "Authorization";

        /// <summary>
        /// Bearer。
        /// </summary>
        private const string REQUEST_BEARER = "Bearer";

        /// <summary>
        /// 半角スペース。
        /// </summary>
        private const string SPACE = " ";

        /// <summary>
        /// スラッシュ。
        /// </summary>
        private const string SLASH = "/";

        /// <summary>
        /// コネクションタイムアウト値(ms)
        /// </summary>
        private const int ConnectionTimeout = 10000;

        /// <summary>
        /// Read/Writeタイムアウト値(ms)
        /// </summary>
        private const int ReadWriteTimeout = 10000;

        /// <summary>
        /// API Key。
        /// </summary>
        private readonly string API_KEY;

        /// <summary>
        /// バウンスAPIのURL
        /// </summary>
        private readonly string BOUNCE_API_URL = ConfigurationManager.AppSettings["SendGridBouceApiUri"];

        /// <summary>
        /// HTTP通信マネージャー
        /// </summary>
        private readonly HttpConnectionManager Http = new HttpConnectionManager(ConnectionTimeout, ReadWriteTimeout);


        /// <summary>
        /// コンストラクタ。
        /// </summary>
        /// <param name="apiKey">API KEY</param>
        public SendGridApi(string apiKey)
        {
            API_KEY = apiKey;
        }

        #region Bounce Mail

        /// <summary>
        /// 対象のメールアドレスのバウンス情報を取得する。
        /// </summary>
        /// <param name="email">メールアドレス。</param>
        /// <returns><see cref="List{Bounce}<"/></returns>
        public List<Bounce> GetBounce(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                AppLog.Error("パラメーター：emailにNullまたは空文字が渡されました。");
                throw new ArgumentNullException("パラメーター：emailにNullまたは空文字が渡されました。");
            }

            var urlBuilder = new StringBuilder();
            urlBuilder.Append(BOUNCE_API_URL);
            urlBuilder.Append(SLASH);
            urlBuilder.Append(HttpUtility.UrlEncode(email));

            var header = CreateHeader();

            var response = Http.DoGetRequest(urlBuilder.ToString(), null, header);

            if (response == null || response.StatusCode != HttpStatusCode.OK)
            {
                AppLog.Error(string.Format("SendGrid バウンス情報取得エラー(Email={0})", email));
                return null;
            }

            return response.ResponseBody.Deserialize<List<Bounce>>();

        }

        /// <summary>
        /// 対象のメールアドレスのバウンス情報を削除する。
        /// </summary>
        /// <param name="email">メールアドレス</param>
        /// <returns>true:削除成功/false:削除失敗</returns>
        public bool DeleteBounce(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                AppLog.Error("パラメーター：emailにNullまたは空文字が渡されました。");
                throw new ArgumentNullException("パラメーター：emailにNullまたは空文字が渡されました。");
            }

            var urlBuilder = new StringBuilder();
            urlBuilder.Append(BOUNCE_API_URL);
            urlBuilder.Append(SLASH);
            urlBuilder.Append(HttpUtility.UrlEncode(email));

            var header = CreateHeader();

            var response = Http.DoDeleteRequest(urlBuilder.ToString(), header);

            if (response == null || response.StatusCode != HttpStatusCode.NoContent)
            {
                AppLog.Error(string.Format("SendGrid バウンス情報削除エラー(Email={0}", email));
                return false;
            }
            AppLog.Info(string.Format("SendGrid バウンス情報削除完了(Email={0}", email));
            return true;
        }

        /// <summary>
        /// 対象のメールアドレスのリストのバウンス情報を削除する。
        /// </summary>
        /// <param name="emails"><see cref="DeleteBouncesParameters"/></param>
        /// <returns>true:削除成功/false:削除失敗</returns>
        public bool DeleteBounces(DeleteBouncesParameters emails)
        {
            if (emails == null || emails.Emails == null || emails.Emails.Count == 0)
            {
                AppLog.Error("パラメーター：emailsにNullが渡されました。");
                throw new ArgumentNullException("パラメーター：emailsにNullが渡されました。");
            }

            var header = CreateHeader();

            var response = Http.DoDeleteRequest(BOUNCE_API_URL, JsonConvert.SerializeObject(emails), header);

            var emailsStr = string.Empty;
            foreach (var email in emails.Emails)
            {
                emailsStr += email;
                emailsStr += ",";
            }
            emailsStr.TrimEnd();

            if (response == null || response.StatusCode != HttpStatusCode.NoContent)
            {
                AppLog.Error(string.Format("SendGrid バウンス情報削除エラー(Email={0}", emailsStr));
                return false;
            }
            AppLog.Info(string.Format("SendGrid バウンス情報削除完了(Email={0}", emailsStr));
            return true;
        }

        #endregion

        #region private Methods

        /// <summary>
        /// ヘッダー情報を作成する。
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> CreateHeader()
        {
            var authorizationValue = REQUEST_BEARER + SPACE + API_KEY;

            var parameters = new Dictionary<string, string>();
            parameters.Add(REQUEST_AUTHORIZATION_KEY, authorizationValue);

            return parameters;
        }

        #endregion

        #region parameters

        /// <summary>
        /// emailのリスト。
        /// </summary>
        public class DeleteBouncesParameters
        {
            /// <summary>
            /// email
            /// </summary>
            [JsonProperty("emails")]
            public List<string> Emails;
        }

        #endregion
    }
}