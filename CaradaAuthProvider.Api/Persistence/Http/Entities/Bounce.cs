﻿namespace CaradaAuthProvider.Api.Persistence.Http.Entities
{
    /// <summary>
    /// バウンス情報。
    /// </summary>
    public class Bounce
    {
        /// <summary>
        /// created
        /// </summary>
        public int created { get; set; }

        /// <summary>
        /// email
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// reason
        /// </summary>
        public string reason { get; set; }

        /// <summary>
        /// status
        /// </summary>
        public string status { get; set; }
    }
}