﻿using System;

namespace CaradaAuthProvider.Api.Persistence.Storage.Entities
{
    /// <summary>
    /// チケット種別
    /// </summary>
    public enum TicketKind
    {
        General,                // 汎用
        UseStart,               // 利用開始
        TwoFactor,              // 二段階認証
        PasswordValid,          // パスワード検証
        ResetPasseord,          // パスワード再設定
        RenewEmail,             // メールアドレス変更
        ResetEmail              // メールアドレス再設定
    }

    public class Ticket
    {
        /// <summary>
        /// ユーザID
        /// </summary>
        public string Uid { get; set; }

        /// <summary>
        /// チケット種別
        /// </summary>
        public TicketKind Kind { get; set; }

        /// <summary>
        /// 有効期限
        /// </summary>
        public DateTime ExpiresDateTime { get; set; }

        /// <summary>
        /// メールアドレス
        /// </summary>
        public string MailAddress { get; set; }

        /// <summary>
        /// 認証コード
        /// </summary>
        public string VerifyCode { get; set; }

        /// <summary>
        /// 検証NG回数
        /// </summary>
        public int InvalidCount { get; set; }

        /// <summary>
        /// 検証済みフラグ
        /// </summary>
        public bool VerifiedFlag { get; set; }

        /// <summary>
        /// 処理済みフラグ
        /// </summary>
        public bool ProcessedFlag { get; set; } = false;
    }
}