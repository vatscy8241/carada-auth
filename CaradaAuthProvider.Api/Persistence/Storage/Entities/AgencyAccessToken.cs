﻿namespace CaradaAuthProvider.Api.Persistence.Storage.Entities
{
    /// <summary>
    /// 代理アクセストークン
    /// </summary>
    public class AgencyAccessToken
    {
        /// <summary>
        /// API実行者識別子
        /// </summary>
        public string User
        { get; set; }
    }
}