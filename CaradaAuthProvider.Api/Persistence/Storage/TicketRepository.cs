﻿using CaradaAuthProvider.Api.Persistence.Storage.Entities;
using System;

namespace CaradaAuthProvider.Api.Persistence.Storage
{
    public class TicketRepository : AbstractStorageRepository
    {
        /// <summary>
        /// キャッシュキー接尾子
        /// </summary>
        private const string KEY_SUFFIX = ":TOKEN";

        /// <summary>
        /// キャッシュ有効期限(分)
        /// キャッシュDB上の有効期限で、チケットの持つ有効期限とは異なります。
        /// 必ずチケットの有効期限の上限値以上の値とすること。
        /// </summary>
        private const long CACHE_EXPIRATION_MINUTES = 150;

        /// <summary>
        /// チケットを取得する
        /// </summary>
        /// <param name="ticketId">チケットID</param>
        /// <returns></returns>
        public virtual Ticket Find(string ticketId)
        {
            return Get<Ticket>(CreateKey(ticketId));
        }

        /// <summary>
        /// チケットを登録する
        /// </summary>
        /// <param name="ticketId">チケットID</param>
        /// <param name="entity">エンティティ</param>
        public virtual void Register(string ticketId, Ticket entity)
        {
            if (!Put(CreateKey(ticketId), entity, CACHE_EXPIRATION_MINUTES))
            {
                throw new Exception("Redis store failed.");
            }
        }

        /// <summary>
        /// チケットを削除する
        /// </summary>
        /// <param name="ticketId">チケットID</param>
        public virtual void Remove(string ticketId)
        {
            if (!Remove<Ticket>(CreateKey(ticketId)))
            {
                throw new Exception("Redis remove failed.");
            }
        }

        /// <summary>
        /// キャッシュキーを生成する
        /// </summary>
        /// <param name="accessToken">アクセストークン</param>
        /// <returns>キャッシュキー</returns>
        private string CreateKey(string accessToken)
        {
            return accessToken + KEY_SUFFIX;
        }
    }
}