﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Configuration;

namespace CaradaAuthProvider.Api.Persistence.Storage
{
    public abstract class AbstractStorageRepository
    {
        /// <summary>
        /// 接続文字列
        /// </summary>
        private static readonly string RedisCacheConnectionString = ConfigurationManager.AppSettings["RedisCacheConnectionString"];

        /// <summary>
        /// コネクション
        /// </summary>
        private static Lazy<ConnectionMultiplexer> lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
        {
            return ConnectionMultiplexer.Connect(RedisCacheConnectionString);
        });

        /// <summary>
        /// キャッシュを取得する
        /// </summary>
        /// <param name="key">キャッシュキー</param>
        /// <returns>キャッシュ（無い場合はnull）</returns>
        protected virtual T Get<T>(string key)
        {
            var cache = CreateCache();
            var cachedValue = cache.StringGet(key);
            if (cachedValue.IsNullOrEmpty)
            {
                return default(T);
            }
            return JsonConvert.DeserializeObject<T>(cachedValue.ToString());
        }

        /// <summary>
        /// キャッシュを登録する
        /// </summary>
        /// <param name="key">キャッシュキー</param>
        /// <param name="dto">データAPI読込結果DTO</param>
        /// <param name="cacheExpirationMinutes">キャッシュ有効期限</param>
        /// <returns>処理結果</returns>
        protected virtual bool Put<T>(string key, T value, long cacheExpirationMinutes)
        {
            if (value == null || cacheExpirationMinutes <= 0)
            {
                return false;
            }
            return CreateCache().StringSet(key, JsonConvert.SerializeObject(value), TimeSpan.FromMinutes(cacheExpirationMinutes));
        }

        /// <summary>
        /// キャッシュを削除する
        /// </summary>
        /// <param name="key">キャッシュキー</param>
        /// <returns>処理結果</returns>
        protected virtual bool Remove<T>(string key)
        {
            var cache = CreateCache();
            if (cache.StringGet(key).IsNullOrEmpty)
            {
                return true;
            }
            return cache.KeyDelete(key);
        }

        /// <summary>
        /// キャッシュDBオブジェクトの生成
        /// </summary>
        /// <returns>キャッシュDBオブジェクト</returns>
        protected virtual IDatabase CreateCache()
        {
            return lazyConnection.Value.GetDatabase();
        }
    }
}