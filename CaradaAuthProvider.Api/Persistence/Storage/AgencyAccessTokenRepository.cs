﻿using CaradaAuthProvider.Api.Persistence.Storage.Entities;
using System;

namespace CaradaAuthProvider.Api.Persistence.Storage
{
    /// <summary>
    /// 代理アクセストークンリポジトリ
    /// </summary>
    public class AgencyAccessTokenRepository : AbstractStorageRepository
    {
        /// <summary>
        /// キャッシュキー接尾子
        /// </summary>
        private const string KEY_SUFFIX = ":ACCESSTOKEN";

        /// <summary>
        /// アクセストークンを取得する
        /// </summary>
        /// <param name="accessToken">アクセストークン</param>
        /// <returns></returns>
        public virtual AgencyAccessToken Find(string accessToken)
        {
            return Get<AgencyAccessToken>(CreateKey(accessToken));
        }

        /// <summary>
        /// アクセストークンを登録する
        /// </summary>
        /// <param name="accessToken">アクセストークン</param>
        /// <param name="entity">エンティティ</param>
        /// <param name="cacheExpirationMinutes">キャッシュ有効期限</param>
        public virtual void Register(string accessToken, AgencyAccessToken entity, long cacheExpirationMinutes)
        {
            if (!Put(CreateKey(accessToken), entity, cacheExpirationMinutes))
            {
                throw new Exception("Redis store failed.");
            }
        }

        /// <summary>
        /// アクセストークンを削除する
        /// </summary>
        /// <param name="accessToken">アクセストークン</param>
        public virtual void Remove(string accessToken)
        {
            if (!Remove<Ticket>(CreateKey(accessToken)))
            {
                throw new Exception("Redis remove failed.");
            }
        }

        /// <summary>
        /// キャッシュキーを生成する
        /// </summary>
        /// <param name="accessToken">アクセストークン</param>
        /// <returns>キャッシュキー</returns>
        private string CreateKey(string accessToken)
        {
            return accessToken + KEY_SUFFIX;
        }
    }
}