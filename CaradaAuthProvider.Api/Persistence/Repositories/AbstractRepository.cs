﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace CaradaAuthProvider.Api.Persistence.Repositories
{
    /// <summary>
    /// すべてのリポジトリの基底クラス。
    /// </summary>
    /// <typeparam name="TEntity">エンティティの型</typeparam>
    /// <typeparam name="TDbContext">データベースコンテキストの型</typeparam>
    public abstract class AbstractRepository<TEntity, TDbContext>
        where TEntity : class
        where TDbContext : DbContext
    {
        /// <summary>
        /// データベースコンテキスト
        /// </summary>
        protected TDbContext DefaultDbContext { get; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        protected AbstractRepository(TDbContext dbContext)
        {
            DefaultDbContext = dbContext;
        }

        /// <summary>
        /// 単一キーからエンティティを取得する。
        /// </summary>
        /// <param name="id">単一ID</param>
        /// <returns>エンティティ</returns>
        public virtual TEntity FindById(object id)
        {
            return DefaultDbContext.Set<TEntity>().Find(id);
        }

        /// <summary>
        /// 複合キーからエンティティを取得する。
        /// </summary>
        /// <param name="keys">複合キーの値(Orderで指定した順序で指定)</param>
        /// <returns>エンティティ</returns>
        public virtual TEntity FindByKeys(params object[] keys)
        {
            return DefaultDbContext.Set<TEntity>().Find(keys);
        }

        /// <summary>
        /// すべてのエンティティを取得する。
        /// </summary>
        /// <returns>すべてのエンティティ</returns>
        public virtual IEnumerable<TEntity> FindAll()
        {
            return from entity in DefaultDbContext.Set<TEntity>()
                   select entity;
        }

        /// <summary>
        /// エンティティの登録を行う。
        /// </summary>
        /// <param name="entity">エンティティ</param>
        /// <returns>登録件数</returns>
        public virtual int Insert(TEntity entity)
        {
            DefaultDbContext.Set<TEntity>().Add(entity);

            return DefaultDbContext.SaveChanges();
        }

        /// <summary>
        /// エンティティのバッチ登録を行う。
        /// </summary>
        /// <param name="entities">エンティティリスト</param>
        /// <returns>登録件数</returns>
        public virtual int InsertBatch(IEnumerable<TEntity> entities)
        {
            DefaultDbContext.Configuration.AutoDetectChangesEnabled = false;
            DefaultDbContext.Configuration.ValidateOnSaveEnabled = false;

            foreach (var entity in entities)
            {
                DefaultDbContext.Set<TEntity>().Add(entity);
            }

            return DefaultDbContext.SaveChanges();
        }

        /// <summary>
        /// エンティティの更新を行う。
        /// </summary>
        /// <param name="entity">エンティティ</param>
        /// <returns>更新件数</returns>
        public virtual int Update(TEntity entity)
        {
            if (DefaultDbContext.Entry(entity).State != EntityState.Modified)
            {
                DefaultDbContext.Set<TEntity>().Attach(entity);
                DefaultDbContext.Entry(entity).State = EntityState.Modified;
            }

            return DefaultDbContext.SaveChanges();
        }

        /// <summary>
        /// エンティティのバッチ更新を行う。
        /// </summary>
        /// <param name="entities">エンティティリスト</param>
        /// <returns>更新件数</returns>
        public virtual int UpdateBatch(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                if (DefaultDbContext.Entry(entity).State != EntityState.Modified)
                {
                    DefaultDbContext.Set<TEntity>().Attach(entity);
                    DefaultDbContext.Entry(entity).State = EntityState.Modified;
                }
            }

            return DefaultDbContext.SaveChanges();
        }

        /// <summary>
        /// エンティティの削除を行う。
        /// </summary>
        /// <param name="entity">エンティティ</param>
        /// <returns>削除件数</returns>
        public virtual int Delete(TEntity entity)
        {
            DefaultDbContext.Set<TEntity>().Attach(entity);
            DefaultDbContext.Entry(entity).State = EntityState.Deleted;

            return DefaultDbContext.SaveChanges();
        }

        /// <summary>
        /// エンティティのバッチ削除を行う。
        /// </summary>
        /// <param name="entities">エンティティリスト</param>
        /// <returns>削除件数</returns>
        public virtual int DeleteBatch(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                DefaultDbContext.Set<TEntity>().Remove(entity);
                DefaultDbContext.Entry(entity).State = EntityState.Deleted;
            }

            return DefaultDbContext.SaveChanges();
        }
    }
}