﻿using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using System.Linq;

namespace CaradaAuthProvider.Api.Persistence.Repositories
{
    /// <summary>
    /// 秘密の質問回答情報リポジトリ。
    /// </summary>
    public class SecurityQuestionAnswersRepository : AbstractRepository<SecurityQuestionAnswers, CaradaAuthProviderDbContext>
    {
        /// <summary>
        /// コンストラクタ。
        /// </summary>
        public SecurityQuestionAnswersRepository(CaradaAuthProviderDbContext dbContext)
            : base(dbContext)
        {
        }

        /// <summary>
        /// ユーザーIDからエンティティを取得する。
        /// 見かけ上のPRIMARY KEYとしてるため、名前をFindByIdとしている。
        /// </summary>
        /// <param name="userId">ユーザーID</param>
        /// <returns>エンティティ</returns>
        public virtual SecurityQuestionAnswers FindById(string userId)
        {
            return (from entity in DefaultDbContext.SecurityQuestionAnswers where entity.UserId == userId select entity).SingleOrDefault();
        }

        /// <summary>
        /// ユーザーIDからエンティティ件数を取得する。
        /// 見かけ上のPRIMARY KEYとしてるため、名前をFindByIdとしている。
        /// </summary>
        /// <param name="userId">ユーザーID</param>
        /// <returns>件数</returns>
        public virtual int CountById(string userId)
        {
            return (from entity in DefaultDbContext.SecurityQuestionAnswers where entity.UserId == userId select entity).Count();
        }

        /// <summary>
        /// ユーザーIDからエンティティを取得する。
        /// 見かけ上のPRIMARY KEYとしてるため、名前をFindByIdとしている。
        /// </summary>
        /// <param name="userId">ユーザーID</param>
        /// <returns>エンティティ</returns>
        public virtual void RemoveById(string userId)
        {
            var query = from entity in DefaultDbContext.SecurityQuestionAnswers where entity.UserId == userId select entity;
            DefaultDbContext.SecurityQuestionAnswers.RemoveRange(query);
        }
    }
}