﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CaradaAuthProvider.Api.Persistence.Repositories.Entities
{
    /// <summary>
    /// 秘密の質問回答情報テーブル
    /// </summary>
    public class SecurityQuestionAnswers
    {
        /// <summary>
        /// ユーザーID
        /// </summary>
        [Key]
        [StringLength(128)]
        [Column(Order = 1)]
        [ForeignKey("CaradaIdUser")]
        public string UserId { get; set; }

        /// <summary>
        /// 秘密の質問ID
        /// </summary>
        [Key]
        [Column(Order = 2)]
        [ForeignKey("SecurityQuestionMasters")]
        public int SecurityQuestionId { get; set; }

        /// <summary>
        /// 回答
        /// </summary>
        [Required]
        [StringLength(64)]
        public string Answer { get; set; }

        /// <summary>
        /// CARADA ID
        /// </summary>
        public CaradaIdUser CaradaIdUser { get; set; }

        /// <summary>
        /// SecurityQuestionId
        /// </summary>
        public SecurityQuestionMasters SecurityQuestionMasters { get; set; }
    }
}