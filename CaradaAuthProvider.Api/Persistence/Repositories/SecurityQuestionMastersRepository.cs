﻿using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using System.Collections.Generic;
using System.Linq;

namespace CaradaAuthProvider.Api.Persistence.Repositories
{
    /// <summary>
    /// 秘密の質問マスタリポジトリ。
    /// </summary>
    public class SecurityQuestionMastersRepository : AbstractRepository<SecurityQuestionMasters, CaradaAuthProviderDbContext>
    {
        /// <summary>
        /// コンストラクタ。
        /// </summary>
        public SecurityQuestionMastersRepository(CaradaAuthProviderDbContext dbContext)
            : base(dbContext)
        {
        }

        /// <summary>
        /// 表示順にソートされた秘密の質問リストを取得
        /// </summary>
        /// <returns>すべてのエンティティ</returns>
        public virtual IEnumerable<SecurityQuestionMasters> FindAllOrderByDisplayOrder()
        {
            return from entity in DefaultDbContext.Set<SecurityQuestionMasters>()
                   orderby entity.DisplayOrder
                   select entity;
        }
    }
}