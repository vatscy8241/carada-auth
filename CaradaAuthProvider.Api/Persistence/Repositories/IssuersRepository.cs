﻿using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;

namespace CaradaAuthProvider.Api.Persistence.Repositories
{
    /// <summary>
    /// JWTでのAPI使用の為の発行者情報リポジトリ。
    /// </summary>
    public class IssuersRepository : AbstractRepository<Issuers, CaradaAuthProviderDbContext>
    {
        /// <summary>
        /// コンストラクタ。
        /// </summary>
        public IssuersRepository(CaradaAuthProviderDbContext dbContext)
            : base(dbContext)
        {
        }
    }
}