﻿using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using CaradaAuthProvider.Api.Properties;
using Microsoft.AspNet.Identity.EntityFramework;
using NLog;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Text;

namespace CaradaAuthProvider.Api.Persistence.Repositories.Context
{
    /// <summary>
    /// CaradaAuthProvider用のデータベースコンテキスト
    /// </summary>
    [DbConfigurationType(typeof(GlobalDbConfiguration))]
    public class CaradaAuthProviderDbContext : IdentityDbContext<CaradaIdUser>
    {
        /// <summary>
        /// Debugログ用
        /// </summary>
        private static readonly ILogger log = LogManager.GetLogger(Settings.Default.LogTargetApi);

        /// <summary>
        /// 読み取り専用モードかどうか
        /// </summary>
        private bool IsReadOnlyMode = false;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CaradaAuthProviderDbContext()
            : base("CaradaAuthProviderDbContext", throwIfV1Schema: false)
        {
            this.Database.Log = s => log.Debug(s);
        }

        #region エンティティ

        /// <summary>
        /// 秘密の質問マスタ
        /// </summary>
        public virtual DbSet<SecurityQuestionMasters> SecurityQuestionMasters { get; set; }

        /// <summary>
        /// 秘密の質問回答情報
        /// </summary>
        public virtual DbSet<SecurityQuestionAnswers> SecurityQuestionAnswers { get; set; }

        /// <summary>
        /// JWTでのAPI使用の為の発行者情報
        /// </summary>
        public virtual DbSet<Issuers> Issuers { get; set; }

        #endregion

        /// <summary>
        /// 読み取り専用モードに変更する
        /// </summary>
        public void ReadOnlyMode()
        {
            IsReadOnlyMode = true;
        }

        /// <summary>
        /// 読み書きができるように変更する
        /// </summary>
        public void ReadWriteMode()
        {
            IsReadOnlyMode = false;
        }

        /// <summary>
        /// <see cref="SaveChanges"/>
        /// </summary>
        public override int SaveChanges()
        {
            if (IsReadOnlyMode)
            {
                log.Error("読み取り専用接続で更新系のSQLが発行されました。");
                throw new System.InvalidOperationException("読み取り専用接続で更新系のSQLが発行されました。");
            }

            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException exception)
            {
                var message = new StringBuilder();
                foreach (var validationErrors in exception.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        message.Append(string.Format("Property: {0} Error: {1}\n", validationError.PropertyName, validationError.ErrorMessage));
                    }
                }
                // TODO とりあえずログ出してthrowしとく => カスタムエラーにする？
                log.Debug(message.ToString());
                throw new DbEntityValidationException(message.ToString(), exception);
            }
        }
    }
}