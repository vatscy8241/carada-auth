﻿using CaradaAuthProvider.Api.Properties;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServer;
using System.Runtime.Remoting.Messaging;

namespace CaradaAuthProvider.Api.Persistence.Repositories.Context
{
    /// <summary>
    /// アプリケーションのDbConfigurationクラス
    /// </summary>
    public class GlobalDbConfiguration : DbConfiguration
    {
        /// <summary>
        /// 設定情報から取得したリトライ数とディレイ秒数を取得してSqlAzureExecutionStrategyにセットするコンストラクタ。
        /// </summary>
        public GlobalDbConfiguration()
        {
            int retryCount = Settings.Default.DbMaxRetryCount;
            TimeSpan delay = TimeSpan.FromSeconds(Settings.Default.DbMaxDelaySeconds);

            SetExecutionStrategy("System.Data.SqlClient",
                () => SuspendExecutionStrategy ? (IDbExecutionStrategy)new DefaultExecutionStrategy()
                    : new SqlAzureExecutionStrategy(retryCount, delay));
        }

        /// <summary>
        /// SqlAzureExecutionStrategyの実行を停止するかどうかを設定する
        /// </summary>
        public static bool SuspendExecutionStrategy
        {
            get
            {
                // TODO: 自前でトランザクション管理をするにはこの値をtrueにしないと例外になる
                // 影響範囲について要確認
                return (bool?)CallContext.LogicalGetData("SuspendExecutionStrategy") ?? true;
            }
            set
            {
                CallContext.LogicalSetData("SuspendExecutionStrategy", value);
            }
        }
    }
}