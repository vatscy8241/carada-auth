﻿namespace CaradaAuthProvider.Api.Persistence.Repositories.Context
{
    /// <summary>
    /// メソッド途中でDbContextを生成する際にコンストラクタの代替となるFactoryです。
    /// </summary>
    public class DbContextFactory
    {
        /// <summary>
        /// 新しいCaradaAuthProviderDbContextを返却します。
        /// </summary>
        public virtual CaradaAuthProviderDbContext NewCaradaAuthProviderDbContext() { return new CaradaAuthProviderDbContext(); }
    }
}