﻿using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;

namespace CaradaAuthProvider.Api.Persistence.Repositories
{
    /// <summary>
    /// カスタムAspNetUsersリポジトリ。
    /// </summary>
    public class CaradaIdUserRepository : AbstractRepository<CaradaIdUser, CaradaAuthProviderDbContext>
    {
        /// <summary>
        /// コンストラクタ。
        /// </summary>
        public CaradaIdUserRepository(CaradaAuthProviderDbContext dbContext)
            : base(dbContext)
        {
        }
    }
}