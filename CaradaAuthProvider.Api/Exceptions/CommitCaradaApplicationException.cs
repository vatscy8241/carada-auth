﻿using CaradaAuthProvider.Api.Exceptions.Enums;

namespace CaradaAuthProvider.Api.Exceptions
{
    /// <summary>
    /// トランザクション終了時にロールバックしない例外
    /// </summary>
    public class CommitCaradaApplicationException : CaradaApplicationException
    {
        /// <summary>
        /// <see cref="AbstractApplicationException(ErrorKind)"/>
        /// </summary>
        /// <param name="errorKind">エラー種別</param>
        public CommitCaradaApplicationException(ErrorKind errorKind)
            : base(errorKind)
        {
        }

        /// <summary>
        /// <see cref="AbstractApplicationException(ErrorKind, string)"/>
        /// </summary>
        /// <param name="errorKind">エラー種別</param>
        /// <param name="errorMessage">エラーメッセージ</param>
        public CommitCaradaApplicationException(ErrorKind errorKind, string errorMessage)
            : base(errorKind, errorMessage)
        {
        }
    }
}