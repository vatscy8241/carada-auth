﻿using System;

namespace CaradaAuthProvider.Api.Exceptions.Enums
{
    /// <summary>
    /// エラー種別enum。
    /// </summary>
    public enum ErrorKind
    {
        account_lockout,
        already_completed,
        code_mismatch,
        data_mismatch,
        has_not_email,
        invalid_carada_id,
        invalid_client,
        invalid_code,
        invalid_parameter,
        invalid_ticket,
        password_mismatch,
        answer_mismatch,
        refresh_failed,
        registered_carada_id,
        registered_email,
        unauthorized,
        user_not_exist,
        same_email,
        server_error
    }

    /// <summary>
    /// エラー種別enumの拡張クラス。
    /// </summary>
    public static partial class Extensions
    {
        /// <summary>
        /// 定数名を返す。
        /// </summary>
        /// <param name="errorKind">エラー種別</param>
        /// <returns>エラー種別の定数名</returns>
        public static string GetName(this ErrorKind errorKind)
        {
            return Enum.GetName(typeof(ErrorKind), errorKind);
        }
    }
}