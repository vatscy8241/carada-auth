﻿using CaradaAuthProvider.Api.Exceptions.Enums;
using System;

namespace CaradaAuthProvider.Api.Exceptions
{
    /// <summary>
    /// エラーコードを返す例外処理の抽象クラス。
    /// </summary>
    public abstract class AbstractApplicationException : Exception
    {
        /// <summary>
        /// エラー種別。
        /// </summary>
        protected ErrorKind? errorKind { get; set; }

        /// <summary>
        /// エラーメッセージ。
        /// </summary>
        protected string errorMessage { get; set; }

        /// <summary>
        /// コンストラクタ。
        /// </summary>
        public AbstractApplicationException()
        {
        }

        /// <summary>
        /// エラー種別を指定してコンストラクトする。
        /// </summary>
        /// <param name="errorKind">エラー種別</param>
        public AbstractApplicationException(ErrorKind errorKind)
        {
            this.errorKind = errorKind;
        }

        /// <summary>
        /// エラーメッセージを指定してコンストラクトする。
        /// </summary>
        /// <param name="errorMessage">エラーメッセージ</param>
        public AbstractApplicationException(string errorMessage)
        {
            this.errorMessage = errorMessage;
        }

        /// <summary>
        /// エラー種別・エラーメッセージを指定してコンストラクトする。
        /// </summary>
        /// <param name="errorKind">エラー種別</param>
        /// <param name="errorMessage">エラーメッセージ</param>
        public AbstractApplicationException(ErrorKind errorKind, string errorMessage)
        {
            this.errorKind = errorKind;
            this.errorMessage = errorMessage;
        }

        /// <summary>
        /// エラーコード(エラー種別の定数名)を返す。
        /// エラー種別が設定されていない場合は【server_error】を返す。
        /// </summary>
        /// <returns>エラーコード</returns>
        public virtual string GetErrorCode()
        {
            if (!errorKind.HasValue)
            {
                return ErrorKind.server_error.GetName();
            }
            return errorKind.Value.GetName();
        }

        /// <summary>
        /// エラーメッセージを返す。
        /// </summary>
        /// <returns>エラーメッセージ/null</returns>
        public virtual string GetErrorMessage()
        {
            return errorMessage;
        }
    }
}