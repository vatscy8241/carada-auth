﻿using CaradaAuthProvider.Api.Exceptions.Enums;

namespace CaradaAuthProvider.Api.Exceptions
{
    /// <summary>
    /// Authlete用例外処理クラス。
    /// </summary>
    public class AuthleteException : AbstractApplicationException
    {
        /// <summary>
        /// <see cref="AbstractApplicationException(ErrorKind)"/>
        /// </summary>
        public AuthleteException()
            : base(ErrorKind.server_error)
        {
        }

        /// <summary>
        /// <see cref="AbstractApplicationException(ErrorKind, string)"/>
        /// </summary>
        /// <param name="errorMessage">エラーメッセージ</param>
        public AuthleteException(string errorMessage)
            : base(ErrorKind.server_error, errorMessage)
        {
        }

        /// <summary>
        /// <see cref="AbstractApplicationException(ErrorKind, string)"/>
        /// 基本的に使用しない。
        /// </summary>
        /// <param name="errorKind">エラー種別</param>
        /// <param name="errorMessage">エラーメッセージ</param>
        public AuthleteException(ErrorKind errorKind, string errorMessage)
            : base(errorKind, errorMessage)
        {
        }
    }
}