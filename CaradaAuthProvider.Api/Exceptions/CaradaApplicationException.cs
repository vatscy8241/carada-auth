﻿using CaradaAuthProvider.Api.Exceptions.Enums;

namespace CaradaAuthProvider.Api.Exceptions
{
    /// <summary>
    /// Carada用業務例外処理クラス。
    /// </summary>
    public class CaradaApplicationException : AbstractApplicationException
    {
        /// <summary>
        /// <see cref="AbstractApplicationException(ErrorKind)"/>
        /// </summary>
        /// <param name="errorKind">エラー種別</param>
        public CaradaApplicationException(ErrorKind errorKind)
            : base(errorKind)
        {
        }

        /// <summary>
        /// <see cref="AbstractApplicationException(ErrorKind, string)"/>
        /// </summary>
        /// <param name="errorKind">エラー種別</param>
        /// <param name="errorMessage">エラーメッセージ</param>
        public CaradaApplicationException(ErrorKind errorKind, string errorMessage)
            : base(errorKind, errorMessage)
        {
        }
    }
}