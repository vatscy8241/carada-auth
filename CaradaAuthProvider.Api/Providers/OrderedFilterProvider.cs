﻿using CaradaAuthProvider.Api.Filters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace CaradaAuthProvider.Api.Providers
{
    /// <summary>
    /// 実行順を制御してフィルター情報を提供するクラス。
    /// </summary>
    public class OrderedFilterProvider : IFilterProvider
    {
        /// <summary>
        /// コンストラクタ。
        /// </summary>
        public OrderedFilterProvider()
        {
        }

        /// <summary>
        /// <see cref="IFilterProvider.GetFilters(HttpConfiguration, HttpActionDescriptor)"/>
        /// 実行順を制御したフィルター情報の列挙子を返す。
        /// </summary>
        /// <param name="configuration">HTTPサーバーインスタンスの構成情報</param>
        /// <param name="actionDescriptor">アクションメソッドに関する情報</param>
        /// <returns></returns>
        public IEnumerable<FilterInfo> GetFilters(HttpConfiguration configuration, HttpActionDescriptor actionDescriptor)
        {
            // コントローラーのフィルター情報
            var controllerFilters = OrderFilters(actionDescriptor.ControllerDescriptor.GetFilters(), FilterScope.Controller);

            // アクションのフィルター情報
            var actionFilters = OrderFilters(actionDescriptor.GetFilters(), FilterScope.Action);

            return controllerFilters.Concat(actionFilters);
        }

        /// <summary>
        /// 実行順にフィルター情報をソートして返す。
        /// </summary>
        /// <param name="filters">フィルター情報の列挙子</param>
        /// <param name="scope">フィルターのスコープ</param>
        /// <returns>実行順にソートしたフィルター情報の列挙子</returns>
        private IEnumerable<FilterInfo> OrderFilters(IEnumerable<IFilter> filters, FilterScope scope)
        {
            // IOrderdFilterを実装していないフィルターを全て取得し、実行順を100として保持する。
            var notOrderableFilter = filters.Where(filter => !(filter is IOrderedFilter))
                .Select(filter => new KeyValuePair<int, FilterInfo>(100, new FilterInfo(filter, scope)));

            // IOrderdFilterを実装してるフィルターを全て取得し、指定された実行順で保持する。
            var orderableFilter = filters.OfType<IOrderedFilter>()
                .OrderBy(filter => filter.order)
                .Select(filter => new KeyValuePair<int, FilterInfo>(filter.order, new FilterInfo(filter, scope)));

            // IOrderdFilterを実装しているものとしていないフィルターを連結し、実行順で並び替える。
            return notOrderableFilter.Concat(orderableFilter).OrderBy(filterCollections => filterCollections.Key).Select(filterCollections => filterCollections.Value);
        }
    }
}