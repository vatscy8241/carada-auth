﻿using System;

namespace CaradaAuthProvider.Api.Utils
{
    public class DateUtilities
    {
        /// <summary>
        /// UNIXエポックを表すDateTimeオブジェクト
        /// </summary>
        private static DateTime UNIX_EPOCH = new DateTime(1970, 1, 1, 0, 0, 0, 0);

        /// <summary>
        /// 現在時刻を取得する
        /// </summary>
        /// <returns></returns>
        public static DateTime Now
        {
            get
            {
                return DateTime.Now;
            }
        }

        /// <summary>
        /// UTC現在時刻を取得する
        /// </summary>
        /// <returns></returns>
        public static DateTime UTCNow
        {
            get
            {
                return DateTime.Now.ToUniversalTime();
            }
        }

        /// <summary>
        /// エポック秒に変換する
        /// </summary>
        /// <param name="targetTime">時刻</param>
        /// <returns>エポック秒</returns>
        public static long GetEpochTime(DateTime targetTime)
        {
            targetTime = targetTime.ToUniversalTime();
            TimeSpan elapsedTime = targetTime - UNIX_EPOCH;
            return (long)elapsedTime.TotalSeconds;
        }
    }
}