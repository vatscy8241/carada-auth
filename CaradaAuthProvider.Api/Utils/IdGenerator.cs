﻿using System;
using System.Text.RegularExpressions;

namespace CaradaAuthProvider.Api.Utils
{
    /// <summary>
    /// ID生成クラス。
    /// </summary>
    public class IdGenerator
    {
        /// <summary>
        /// パスワード使用文字
        /// </summary>
        private static readonly string STR_PASSWORD_USER_STRING = "0123456789" + "abcdefghijklmnopqrstuvwxyz";

        /// <summary>
        /// パスワード正規表現
        /// </summary>
        private static readonly Regex REGEX = new Regex(@"^(?=.*?[a-z])(?=.*?[0-9])[a-z0-9]{1,}$", RegexOptions.Compiled | RegexOptions.ExplicitCapture);

        /// <summary>
        /// パスワードの桁数
        /// </summary>
        private static readonly int LENGTH = 8;

        /// <summary>
        /// チケットIDを生成する。
        /// </summary>
        /// <returns>32文字ハイフンなしのUUID</returns>
        public static string GenerateTicketId()
        {
            return Generate32DigitsGUID();
        }

        /// <summary>
        /// 認証コードを生成する。
        /// </summary>
        /// <returns>認証コード</returns>
        public static string GenerateVerifyCode()
        {
            return Generate32DigitsGUID();
        }

        /// <summary>
        /// 二段階認証のキーを生成する。
        /// Cookieの値とAzure Storageのキーとして利用する。
        /// </summary>
        /// <returns>二段階認証のキー</returns>
        public static string GenerateTwoFactorLoginKey()
        {
            return Generate32DigitsGUID();
        }

        /// <summary>
        /// 32文字ハイフンなしのUUIDを生成する。
        /// </summary>
        /// <returns>32文字ハイフンなしのUUID</returns>
        private static string Generate32DigitsGUID()
        {
            return Guid.NewGuid().ToString("N");
        }

        /// <summary>
        /// ランダムパスワード生成
        /// </summary>
        /// <returns>ランダムなパスワード文字列</returns>
        public static string GenerateRandomPassword()
        {
            var _r = new Random();
            var password = "";

            // 正規表現にマッチしたパスワードが生成されるまでリトライ
            do
            {
                // パスワード使用文字列からランダムに値を取得
                char[] list = new char[LENGTH];
                for (int i = 0; i < LENGTH; i++)
                {
                    int num = _r.Next(0, STR_PASSWORD_USER_STRING.Length);
                    list[i] = STR_PASSWORD_USER_STRING[num];
                }

                // パスワードを生成
                password = new string(list);
            }
            while (!REGEX.IsMatch(password));

            return password;
        }
    }
}