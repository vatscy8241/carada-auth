﻿using Microsoft.WindowsAzure.ServiceRuntime;
using NLog;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace CaradaAuthProvider.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // NLogのログ出力先をローカルストレージに設定
            var localResource = RoleEnvironment.GetLocalResource("ApplicationLog");
            GlobalDiagnosticsContext.Set(@"ApplicationLogPath", localResource.RootPath);
            GlobalDiagnosticsContext.Set(@"ApplicationLogPrefix", RoleEnvironment.CurrentRoleInstance.Id);
            LogManager.ReconfigExistingLoggers();


            //var dmc = DiagnosticMonitor.GetDefaultInitialConfiguration();
            //// 30秒単位で転送(実際はもっと長くする)
            //dmc.Directories.ScheduledTransferPeriod = TimeSpan.FromSeconds(30);

            //// カスタムログのコンテナをQuotaを設定
            //var dconfig = new DirectoryConfiguration()
            //{
            //    Container = "user-logs",
            //    DirectoryQuotaInMB = 100,
            //};

            //// LocalStorageのRootPathをログの基点フォルダとして設定
            //var res = RoleEnvironment.GetLocalResource("ls");
            //dconfig.Path = res.RootPath;

            //// データソースに追加
            //dmc.Directories.DataSources.Add(dconfig);
            //DiagnosticMonitor.Start("DiagnosticsConnectionString", dmc);
        }
    }
}
