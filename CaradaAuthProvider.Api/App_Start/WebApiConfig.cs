﻿using CaradaAuthProvider.Api.Handlers;
using CaradaAuthProvider.Api.Providers;
using System.Web.Http;
using System.Web.Http.Filters;

namespace CaradaAuthProvider.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // 既存のフィルタープロバイダーを削除する。
            config.Services.Replace(typeof(IFilterProvider), new ConfigurationFilterProvider());

            // フィルターの実行順序付けができるカスタムフィルタープロバイダーを設定する。
            config.Services.Add(typeof(IFilterProvider), new OrderedFilterProvider());


            // 通常のAPIルーティング
            config.Routes.MapHttpRoute(
                name: "ApiDefault",
                routeTemplate: "api/{controller}/{action}"
            );

            config.MessageHandlers.Add(new ApiLogRequestHandler());
        }
    }
}
