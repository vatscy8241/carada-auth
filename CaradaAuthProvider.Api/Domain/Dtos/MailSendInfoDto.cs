﻿using System.Collections.Generic;

namespace CaradaAuthProvider.Api.Domain.Dtos
{
    /// <summary>
    /// メール送信情報DTO。
    /// </summary>
    public class MailSendInfoDto
    {
        /// <summary>
        /// 宛先のリスト。
        /// </summary>
        public List<string> ToList { get; set; }

        /// <summary>
        /// 送信元アドレス。
        /// </summary>
        public string From { get; set; }

        /// <summary>
        /// 送信元表示名。
        /// </summary>
        public string FromName { get; set; }

        /// <summary>
        /// CCのリスト。
        /// </summary>
        public List<string> CcList { get; set; }

        /// <summary>
        /// BCCのリスト。
        /// </summary>
        public List<string> BccList { get; set; }

        /// <summary>
        /// 件名。
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// メッセージテキスト。
        /// </summary>
        public string Text { get; set; }
    }
}