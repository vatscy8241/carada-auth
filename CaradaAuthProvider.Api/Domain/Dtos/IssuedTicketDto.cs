﻿using CaradaAuthProvider.Api.Persistence.Storage.Entities;

namespace CaradaAuthProvider.Api.Domain.Dtos
{
    public class IssuedTicketDto
    {
        /// <summary>
        /// チケットID
        /// </summary>
        public string TicketId { get; set; }

        /// <summary>
        /// エンティティ
        /// </summary>
        public Ticket Ticket { get; set; }
    }
}