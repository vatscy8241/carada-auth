﻿using CaradaAuthProvider.Api.Persistence.Storage.Entities;

namespace CaradaAuthProvider.Api.Domain.Logics
{
    /// <summary>
    /// 代理アクセストークンロジックのインターフェース
    /// </summary>
    public interface AgencyAccessTokenLogic
    {
        /// <summary>
        /// アクセストークンを取得する
        /// </summary>
        /// <param name="accessToken">アクセストークンID</param>
        /// <returns>アクセストークン</returns>
        AgencyAccessToken FindAgencyAccessToken(string accessToken);

        /// <summary>
        /// アクセストークンを保持する
        /// </summary>
        /// <param name="accessToken">アクセストークンID</param>
        /// <param name="agencyAccessToken">アクセストークン</param>
        /// <param name="expiresAt">有効期限</param>
        void RegisterAgencyAccessToken(string accessToken, AgencyAccessToken agencyAccessToken, long expiresAt);

        /// <summary>
        /// アクセストークンを削除する
        /// </summary>
        /// <param name="accessToken">アクセストークンID</param>
        void RemoveAgencyAccessToken(string accessToken);
    }
}
