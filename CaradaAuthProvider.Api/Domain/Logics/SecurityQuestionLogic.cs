﻿using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using System.Collections.Generic;

namespace CaradaAuthProvider.Api.Domain.Logics
{
    /// <summary>
    /// 秘密の質問ロジック
    /// </summary>
    public interface SecurityQuestionLogic
    {
        /// <summary>
        /// 秘密の質問リストを取得する
        /// </summary>
        /// <returns>秘密の質問リスト</returns>
        IEnumerable<SecurityQuestionMasters> GetSecurityQuestionList();

        /// <summary>
        /// 秘密の質問の登録件数を取得する
        /// </summary>
        /// <param name="userId">ユーザID</param>
        /// <returns>登録件数</returns>
        int CountSecurityQuestion(string userId);

        /// <summary>
        /// 秘密の質問を取得する
        /// </summary>
        /// <param name="userId">ユーザID</param>
        /// <returns>秘密の質問</returns>
        SecurityQuestionMasters FindSecurityQuestion(string userId);

        /// <summary>
        /// 秘密の質問を削除する
        /// </summary>
        /// <param name="userId">ユーザID</param>
        void RemoveSecurityQuestion(string userId);

        /// <summary>
        /// 秘密の質問の回答をチェックする
        /// </summary>
        /// <param name="userId">ユーザID</param>
        /// <param name="securityQuestionId">秘密の質問ID</param>
        /// <param name="answer">回答</param>
        /// <returns>チェック結果</returns>
        bool CheckAnswer(string userId, int securityQuestionId, string answer);

        /// <summary>
        /// 秘密の質問の回答を登録する
        /// </summary>
        /// <param name="userId">ユーザID</param>
        /// <param name="securityQuestionId">秘密の質問ID</param>
        /// <param name="answer">回答</param>
        void RegisterAnswer(string userId, int securityQuestionId, string answer);
    }
}