﻿using CaradaAuthProvider.Api.Domain.Dtos;

namespace CaradaAuthProvider.Api.Domain.Logics
{
    /// <summary>
    /// メールロジックのインターフェース。
    /// </summary>
    public interface MailLogic
    {
        /// <summary>
        /// SendGridでメールを送信する(非同期)。
        /// </summary>
        /// <param name="dto"><see cref="MailSendInfoDto"/></param>
        void SendMailAsync(MailSendInfoDto dto);
    }
}