﻿using CaradaAuthProvider.Api.Persistence.Repositories;
using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using CaradaAuthProvider.Api.Utils;
using System.Collections.Generic;

namespace CaradaAuthProvider.Api.Domain.Logics.Impl
{
    /// <summary>
    /// 秘密の質問ロジック
    /// </summary>
    public class SecurityQuestionLogicImpl : SecurityQuestionLogic
    {

        /// <summary>
        /// 秘密の質問マスタリポジトリ
        /// </summary>
        protected SecurityQuestionMastersRepository securityQuestionMastersRepository;

        /// <summary>
        /// 秘密の質問回答リポジトリ
        /// </summary>
        protected SecurityQuestionAnswersRepository securityQuestionAnswersRepository;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public SecurityQuestionLogicImpl(CaradaAuthProviderDbContext dbContext)
        {
            securityQuestionMastersRepository = new SecurityQuestionMastersRepository(dbContext);
            securityQuestionAnswersRepository = new SecurityQuestionAnswersRepository(dbContext);
        }

        /// <summary>
        /// 秘密の質問の登録件数を取得する
        /// </summary>
        /// <param name="userId">ユーザID</param>
        /// <returns>登録件数</returns>
        public int CountSecurityQuestion(string userId)
        {
            return securityQuestionAnswersRepository.CountById(userId);
        }

        /// <summary>
        /// 秘密の質問リストを取得
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SecurityQuestionMasters> GetSecurityQuestionList()
        {
            return securityQuestionMastersRepository.FindAllOrderByDisplayOrder();
        }

        /// <summary>
        /// 秘密の質問を削除する
        /// </summary>
        /// <param name="userId">ユーザID</param>
        public void RemoveSecurityQuestion(string userId)
        {
            securityQuestionAnswersRepository.RemoveById(userId);
        }

        /// <summary>
        /// 秘密の質問の回答を登録
        /// </summary>
        /// <param name="userId">ユーザID</param>
        /// <param name="securityQuestionId">秘密の質問ID</param>
        /// <param name="answer">回答</param>
        public void RegisterAnswer(string userId, int securityQuestionId, string answer)
        {
            SecurityQuestionAnswers entity = new SecurityQuestionAnswers()
            {
                UserId = userId,
                SecurityQuestionId = securityQuestionId,
                Answer = StringUtilities.CreateHashString(answer)
            };

            securityQuestionAnswersRepository.RemoveById(userId);
            securityQuestionAnswersRepository.Insert(entity);
        }

        /// <summary>
        /// 秘密の質問を取得する
        /// </summary>
        /// <param name="userId">ユーザID</param>
        /// <returns>秘密の質問</returns>
        public SecurityQuestionMasters FindSecurityQuestion(string userId)
        {
            SecurityQuestionAnswers securityQuestionAnswers = securityQuestionAnswersRepository.FindById(userId);
            if (securityQuestionAnswers == null)
            {
                return null;
            }

            return securityQuestionMastersRepository.FindById(securityQuestionAnswers.SecurityQuestionId);
        }

        /// <summary>
        /// 秘密の質問の回答をチェックする
        /// </summary>
        /// <param name="userId">ユーザID</param>
        /// <param name="securityQuestionId">秘密の質問ID</param>
        /// <param name="answer">回答</param>
        /// <returns>チェック結果</returns>
        public bool CheckAnswer(string userId, int securityQuestionId, string answer)
        {
            SecurityQuestionAnswers entity = securityQuestionAnswersRepository.FindByKeys(userId, securityQuestionId);
            if (entity == null)
            {
                return false;
            }
            return entity.Answer == StringUtilities.CreateHashString(answer);
        }
    }
}