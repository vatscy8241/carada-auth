﻿using CaradaAuthProvider.Api.Persistence.Storage;
using CaradaAuthProvider.Api.Persistence.Storage.Entities;
using CaradaAuthProvider.Api.Utils;

namespace CaradaAuthProvider.Api.Domain.Logics.Impl
{
    /// <summary>
    /// 代理アクセストークンロジック
    /// </summary>
    public class AgencyAccessTokenLogicImpl : AgencyAccessTokenLogic
    {
        /// <summary>
        /// 代理アクセストークンリポジトリ
        /// </summary>
        protected AgencyAccessTokenRepository agencyAccessTokenRepository;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public AgencyAccessTokenLogicImpl()
        {
            agencyAccessTokenRepository = new AgencyAccessTokenRepository();
        }

        /// <summary>
        /// アクセストークンを取得する
        /// </summary>
        /// <param name="accessToken">アクセストークンID</param>
        /// <returns>アクセストークン</returns>
        public AgencyAccessToken FindAgencyAccessToken(string accessToken)
        {
            return agencyAccessTokenRepository.Find(accessToken);
        }

        /// <summary>
        /// アクセストークンを保持する
        /// </summary>
        /// <param name="accessToken">アクセストークンID</param>
        /// <param name="agencyAccessToken">アクセストークン</param>
        /// <param name="expiresAt">有効期限</param>
        public void RegisterAgencyAccessToken(string accessToken, AgencyAccessToken agencyAccessToken, long expiresAt)
        {
            long cacheExpirationMinutes = expiresAt - DateUtilities.GetEpochTime(DateUtilities.UTCNow);
            if (cacheExpirationMinutes < 0)
            {
                return;
            }
            agencyAccessTokenRepository.Register(accessToken, agencyAccessToken, cacheExpirationMinutes);
        }

        /// <summary>
        /// アクセストークンを削除する
        /// </summary>
        /// <param name="accessToken">アクセストークンID</param>
        public void RemoveAgencyAccessToken(string accessToken)
        {
            agencyAccessTokenRepository.Remove(accessToken);
        }
    }
}