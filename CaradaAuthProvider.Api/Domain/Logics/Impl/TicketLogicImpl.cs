﻿using CaradaAuthProvider.Api.Domain.Dtos;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using CaradaAuthProvider.Api.Persistence.Storage;
using CaradaAuthProvider.Api.Persistence.Storage.Entities;
using CaradaAuthProvider.Api.Utils;

namespace CaradaAuthProvider.Api.Domain.Logics.Impl
{
    /// <summary>
    /// チケットロジック
    /// </summary>
    public class TicketLogicImpl : TicketLogic
    {
        /// <summary>
        /// チケットが無効とされる検証NG回数
        /// </summary>
        private const int INVALIDED_FAILURE_COUNT = 5;

        /// <summary>
        /// チケットリポジトリ
        /// </summary>
        protected TicketRepository ticketRepository;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public TicketLogicImpl()
        {
            ticketRepository = new TicketRepository();
        }

        /// <summary>
        /// チケットを取得する
        /// </summary>
        /// <param name="ticketId">チケットID</param>
        /// <returns>チケット</returns>
        public Ticket FindTicket(string ticketId)
        {
            return ticketRepository.Find(ticketId);
        }

        /// <summary>
        /// チケットを保持する
        /// </summary>
        /// <param name="ticketId">チケットID</param>
        /// <param name="ticket">チケット</param>
        public void RegisterTicket(string ticketId, Ticket ticket)
        {
            ticketRepository.Register(ticketId, ticket);
        }

        /// <summary>
        /// チケットを削除する
        /// </summary>
        /// <param name="ticketId">チケットID</param>
        public void RemoveTicket(string ticketId)
        {
            ticketRepository.Remove(ticketId);
        }

        /// <summary>
        /// チケットを発行する
        /// <param name="ticketKind">チケット種別</param>
        /// <param name="user">ユーザ</param>
        /// <param name="ticketExpiresMinute">チケット有効期限</param>
        /// <param name="verifyCode">認証コード</param>
        /// <param name="mailAddress">メールアドレス</param>
        /// <returns>発行チケットDTO</returns>
        public IssuedTicketDto IssueTicket(TicketKind ticketKind, CaradaIdUser user, long ticketExpiresMinute, string verifyCode = null, string mailAddress = null)
        {
            string ticketId = IdGenerator.GenerateTicketId();
            Ticket entity = new Ticket();
            entity.Uid = user.Id;
            entity.Kind = ticketKind;
            entity.ExpiresDateTime = DateUtilities.UTCNow.AddMinutes(ticketExpiresMinute);
            entity.InvalidCount = 0;
            entity.VerifiedFlag = false;

            if (!string.IsNullOrEmpty(verifyCode))
            {
                entity.VerifyCode = verifyCode;
            }

            if (!string.IsNullOrEmpty(mailAddress))
            {
                entity.MailAddress = mailAddress;
            }

            ticketRepository.Register(ticketId, entity);

            return new IssuedTicketDto()
            {
                TicketId = ticketId,
                Ticket = entity
            };
        }

        /// <summary>
        /// 利用開始時のチケットを検証する
        /// </summary>
        /// <param name="ticketId">チケットID</param>
        /// <param name="verifyCode">認証コード</param>
        /// <returns>チケット検証結果</returns>
        public VerifyResult VerifyUseStartTicket(string ticketId, string verifyCode)
        {
            Ticket entity = ticketRepository.Find(ticketId);
            if (entity == null)
            {
                return VerifyResult.TICKET_NOT_FOUND;
            }
            if (entity.ExpiresDateTime.CompareTo(DateUtilities.UTCNow) < 0)
            {
                return VerifyResult.EXPIRED;
            }
            if (entity.InvalidCount >= INVALIDED_FAILURE_COUNT)
            {
                return VerifyResult.INVALID;
            }

            VerifyResult result;
            if (!string.IsNullOrEmpty(entity.VerifyCode) && verifyCode == entity.VerifyCode)
            {
                entity.VerifiedFlag = true;
                result = VerifyResult.SUCCESS;
            }
            else
            {
                entity.VerifiedFlag = false;
                entity.InvalidCount++;

                if (entity.InvalidCount >= INVALIDED_FAILURE_COUNT)
                {
                    result = VerifyResult.INVALIDED;
                }
                else
                {
                    result = VerifyResult.CODE_MISMATCH;
                }
            }

            ticketRepository.Register(ticketId, entity);
            return result;
        }
    }
}