﻿using CaradaAuthProvider.Api.Domain.Dtos;
using CaradaAuthProvider.Api.Persistence.Http;
using Mti.Cpp.Logging;
using SendGrid;
using System;
using System.Collections.Generic;
using System.Net.Mail;

namespace CaradaAuthProvider.Api.Domain.Logics.Impl
{
    /// <summary>
    /// メールロジックの実装クラス。
    /// </summary>
    public class MailLogicImpl : MailLogic
    {
        /// <summary>
        /// API Key
        /// </summary>
        private readonly string apiKey;

        /// <summary>
        /// SendGridApiV3。
        /// </summary>
        protected SendGridApi sendGridApi;

        /// <summary>
        /// コンストラクタ。
        /// </summary>
        /// <param name="apiKey">API Key</param>
        public MailLogicImpl(string apiKey)
        {
            this.apiKey = apiKey;
            sendGridApi = new SendGridApi(apiKey);
        }

        /// <summary>
        /// <see cref="MailLogic.SendMailAsync(MailSendInfoDto, string)"/>
        /// </summary>
        /// <param name="dto"><see cref="MailSendInfoDto"/></param>
        public void SendMailAsync(MailSendInfoDto dto)
        {
            if (!BeforeSend(dto))
            {
                AppLog.Error("メール送信の事前処理に失敗しました。");
            }

            var sendMessage = CreateSendMessage(dto);

            var web = new Web(apiKey);
            web.DeliverAsync(sendMessage);
        }

        #region private Methods

        /// <summary>
        /// メール送信の事前処理。
        /// SendGridのバウンスリストを検索し、バウンスリストに対象のメールアドレスが存在すれば削除する。
        /// </summary>
        /// <param name="dto"><see cref="MailSendInfoDto"/></param>
        /// <returns>true:事前処理成功/false:事前処理失敗</returns>
        private bool BeforeSend(MailSendInfoDto dto)
        {
            var param = new SendGridApi.DeleteBouncesParameters();
            param.Emails = new List<string>();
            foreach (var email in dto.ToList)
            {
                var bounces = sendGridApi.GetBounce(email);
                if (bounces == null || bounces.Count == 0 || string.IsNullOrEmpty(bounces[0].email))
                {
                    continue;
                }
                param.Emails.Add(bounces[0].email);
                AppLog.Info($"SendGrid バウンスリスト登録あり(Email={email})");
            }

            if (param.Emails.Count == 0)
            {
                return true;
            }

            return sendGridApi.DeleteBounces(param);
        }

        /// <summary>
        /// SendGridメッセージを作成します。
        /// </summary>
        /// <param name="dto"><see cref="MailSendInfoDto"/></param>
        /// <returns><see cref="SendGridMessage"/></returns>
        private SendGridMessage CreateSendMessage(MailSendInfoDto dto)
        {
            if (dto == null)
            {
                AppLog.Error("引数にnulllが渡されました。");
                throw new ArgumentNullException("引数にNullが渡されました。");
            }

            if (string.IsNullOrEmpty(dto.From) || dto.ToList == null || dto.ToList.Count == 0 || string.IsNullOrEmpty(dto.Subject) || string.IsNullOrEmpty(dto.Text))
            {
                AppLog.Error("引数に無効な値が渡されました。");
                throw new ArgumentException("引数に無効な値が渡されました。");
            }

            var sendMessage = new SendGridMessage();
            // ダミーの宛先。この宛先は使用されないが、設定が無いとBad Requestになる。
            sendMessage.AddTo(dto.From);
            // こっちで指定した宛先が使用される。
            sendMessage.Header.SetTo(dto.ToList);

            if (dto.BccList != null)
            {

                foreach (var bcc in dto.BccList)
                {
                    sendMessage.AddBcc(bcc);
                }
            }

            if (dto.CcList != null)
            {
                foreach (var cc in dto.CcList)
                {
                    sendMessage.AddCc(cc);
                }
            }

            if (string.IsNullOrEmpty(dto.FromName))
            {
                sendMessage.From = new MailAddress(dto.From);
            }
            else
            {
                sendMessage.From = new MailAddress(dto.From, dto.FromName);
            }

            sendMessage.Subject = dto.Subject;
            sendMessage.Text = dto.Text;

            return sendMessage;
        }

        #endregion
    }
}