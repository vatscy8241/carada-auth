﻿using CaradaAuthProvider.Api.Domain.Dtos;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using CaradaAuthProvider.Api.Persistence.Storage.Entities;

namespace CaradaAuthProvider.Api.Domain.Logics
{
    /// <summary>
    /// チケットロジックのインターフェース
    /// </summary>
    public interface TicketLogic
    {
        /// <summary>
        /// チケットを取得する
        /// </summary>
        /// <param name="ticketId">チケットID</param>
        /// <returns>チケット</returns>
        Ticket FindTicket(string ticketId);

        /// <summary>
        /// チケットを保持する
        /// </summary>
        /// <param name="ticketId">チケットID</param>
        /// <param name="ticket">チケット</param>
        void RegisterTicket(string ticketId, Ticket ticket);

        /// <summary>
        /// チケットを削除する
        /// </summary>
        /// <param name="ticketId">チケットID</param>
        void RemoveTicket(string ticketId);

        /// <summary>
        /// チケットを発行する
        /// <param name="ticketKind">チケット種別</param>
        /// <param name="user">ユーザ</param>
        /// <param name="ticketExpiresMinute">チケット有効期限</param>
        /// <param name="verifyCode">認証コード</param>
        /// <param name="mailAddress">メールアドレス</param>
        /// <returns>発行チケットDTO</returns>
        IssuedTicketDto IssueTicket(TicketKind ticketKind, CaradaIdUser user, long ticketExpiresMinute, string verifyCode = null, string mailAddress = null);

        /// <summary>
        /// 利用開始時のチケットを検証する
        /// </summary>
        /// <param name="ticketId">チケットID</param>
        /// <param name="verifyCode">認証コード</param>
        /// <returns>チケット検証結果</returns>
        VerifyResult VerifyUseStartTicket(string ticketId, string verifyCode);
    }

    /// <summary>
    /// チケット検証結果
    /// </summary>
    public enum VerifyResult
    {
        SUCCESS,                // 成功
        TICKET_NOT_FOUND,       // チケットが存在しない
        EXPIRED,                // 有効期限切れ
        INVALID,                // 無効
        INVALIDED,              // 無効となった(5回目の失敗)
        CODE_MISMATCH,          // コードが一致しない
        UNDEFINED               // 未定義（基本ありえない）
    }
}
