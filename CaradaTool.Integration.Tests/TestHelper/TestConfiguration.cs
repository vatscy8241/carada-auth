﻿using CaradaTool.Integration.Tests.Properties;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;

namespace CaradaTool.Integration.Tests.TestHelper
{
    /// <summary>
    /// テスト全体の設定を行う。
    /// </summary>
    [TestClass]
    public class TestConfiguration
    {
        /// <summary>
        /// プロジェクトネームのキーの定数
        /// </summary>
        private const string PROJECT_NAME = "projName";

        /// <summary>
        /// ディレクトリネームのキーの定数
        /// </summary>
        private const string DIRECTORY_NAME = "directoryName";

        /// <summary>
        /// ディレクトリネームのキーの定数
        /// </summary>
        public static string ExePath { get; set; }

        /// <summary>
        /// Toolプロジェクト設定情報のDictionary
        /// </summary>
        private static readonly Dictionary<string, string> _toolSettingsInfo = new Dictionary<string, string>()
        {
            {PROJECT_NAME, Settings.Default.ProjectName},
            {DIRECTORY_NAME, ".Integrate.Tmp." + Settings.Default.ProjectName}
        };

        public TestContext TestContext { get; set; }

        /// <summary>
        /// 初期化を行う
        /// </summary>
        [AssemblyInitialize]
        public static void Initialize(TestContext testContext)
        {
            Console.WriteLine("テストの初期化処理を行います。");

            ExecuteMsBuild(_toolSettingsInfo[PROJECT_NAME], _toolSettingsInfo[DIRECTORY_NAME]);

            // カレントをEXEのパスに設定する
            System.IO.Directory.SetCurrentDirectory(Settings.Default.ExePath);
            ExePath = System.IO.Directory.GetCurrentDirectory();

            Console.WriteLine("テストの初期化処理が完了しました。");
        }

        /// <summary>
        /// クリーンアップを行う
        /// </summary>
        [AssemblyCleanup]
        public static void CleanUp()
        {
            Console.WriteLine("テストの終了処理を行います。");
            Console.WriteLine("テストの終了処理が完了しました。");
        }

        private static void ExecuteMsBuild(string name, string directoryName)
        {
            MsBuild.StartMsBuild(Path.Combine(GetApplicationPath(name), (name + ".csproj")));

            // .Integrate.Tmp.のクリア-作成-プロジェクトコピー
            DirectoryUtils.Delete(GetApplicationPath(directoryName));
            DirectoryUtils.Create(GetApplicationPath(directoryName), new[] { FileAttributes.Hidden });
            DirectoryUtils.Copy(GetApplicationPath(name), GetApplicationPath(directoryName), true);

            Console.WriteLine("テスト対象プロジェクトのビルドを終了しました。");
        }

        /// <summary>
        /// アプリケーションのパスを取得する。
        /// </summary>
        /// <param name="applicationName">アプリケーション名</param>
        /// <returns>アプリケーションのパス</returns>
        private static string GetApplicationPath(string applicationName)
        {
            var solutionFolder = Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory)));
            string hoge = Path.Combine(solutionFolder, applicationName);
            return Path.Combine(solutionFolder, applicationName);
        }
    }
}
