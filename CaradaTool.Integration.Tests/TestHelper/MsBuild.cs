﻿using CaradaTool.Integration.Tests.Properties;
using System;
using System.Diagnostics;
using System.IO;

namespace CaradaTool.Integration.Tests.TestHelper
{
    /// <summary>
    /// MSBuild用のクラス。
    /// </summary>
    public static class MsBuild
    {
        /// <summary>
        /// MSBuildの実行ファイル名
        /// </summary>
        private const string MS_BUILD_EXE_NAME = "MSBuild.exe";

        /// <summary>
        /// MSBuildを行う。
        /// </summary>
        /// <param name="projectPath">パス</param>
        public static void StartMsBuild(string projectPath)
        {
            var arguments = string.Format(@"""{0}"" /p:Configuration=CI;OutputPath=bin\ /t:Clean;rebuild /p:DefineConstants=""CI;DEBUG""", projectPath);

            var dotnetHome = Settings.Default.MsBuildHome;

            var msBuildRunExecutePath = Path.Combine(dotnetHome, MS_BUILD_EXE_NAME);

            var processStartInfo = new ProcessStartInfo(msBuildRunExecutePath, arguments)
            {
                RedirectStandardOutput = false,
                RedirectStandardError = true,
                UseShellExecute = false,
                CreateNoWindow = true,
                LoadUserProfile = true,
                WindowStyle = ProcessWindowStyle.Hidden
            };

            using (var process = Process.Start(processStartInfo))
            {
                process.WaitForExit();

                using (var reader = process.StandardError)
                {
                    Console.WriteLine(reader.ReadToEnd());
                }
            }
        }
    }
}
