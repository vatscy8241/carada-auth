﻿using CaradaAuthProvider.Api.Domain.Logics;
using CaradaAuthProvider.Api.Domain.Logics.Impl;
using CaradaAuthProvider.Api.Persistence.Repositories;
using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using CaradaAuthProvider.Api.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Reflection;

namespace CaradaAuthProvider.Api.Test.Domain.Logics
{
    /// <summary>
    /// 秘密の質問ロジックのテスト
    /// </summary>
    [TestClass]
    public class SecurityQuestionLogicTest
    {
        private SecurityQuestionLogic target;

        private Mock<SecurityQuestionMastersRepository> mockSecurityQuestionMastersRepository;

        private Mock<SecurityQuestionAnswersRepository> mockSecurityQuestionAnswersRepository;

        /// <summary>
        /// 初期化
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            // DbContextの設定
            CaradaAuthProviderDbContext context = new DbContextFactory().NewCaradaAuthProviderDbContext();

            // テスト対象のインスタンス化
            target = new SecurityQuestionLogicImpl(context);

            // リポジトリのモック設定
            mockSecurityQuestionMastersRepository = new Mock<SecurityQuestionMastersRepository>(context);
            BindField(target, "securityQuestionMastersRepository", mockSecurityQuestionMastersRepository.Object);

            mockSecurityQuestionAnswersRepository = new Mock<SecurityQuestionAnswersRepository>(context);
            BindField(target, "securityQuestionAnswersRepository", mockSecurityQuestionAnswersRepository.Object);
        }


        protected void BindField<T>(T target, string field, object obj)
        {

            target.GetType().GetField(field, BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Instance).SetValue(target, obj);
        }

        //---------------------------------------------------------------------------------------------------------------
        // GetSecurity
        //---------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// [対象] 秘密の質問マスタリスト取得
        /// [条件] 秘密の質問マスタリストが2件ソートされて返却される
        /// [結果] 秘密の質問マスタリストが2件取得できる
        /// </summary>
        [TestMethod]
        public void GetSecurity()
        {
            mockSecurityQuestionMastersRepository.Setup(r => r.FindAllOrderByDisplayOrder()).Returns(new List<SecurityQuestionMasters>()
            {

                new SecurityQuestionMasters {
                    SecurityQuestionId = 2,
                    Question = "探しものはなんですか？",
                    DisplayOrder = 1
                },

                new SecurityQuestionMasters {
                    SecurityQuestionId = 1,
                    Question = "2番じゃダメなんですか？",
                    DisplayOrder = 2
                }
            });

            IEnumerable<SecurityQuestionMasters> actual = target.GetSecurityQuestionList();
            Assert.IsNotNull(actual);

            SecurityQuestionMasters a1 = null;
            SecurityQuestionMasters a2 = null;
            var count = 0;
            foreach (SecurityQuestionMasters e in actual)
            {

                if (count == 0)
                {
                    a1 = e;
                }
                else if (count == 1)
                {
                    a2 = e;
                }
                count++;
            }


            // 取得件数
            Assert.AreEqual(2, count);

            // 質問1
            Assert.AreEqual(1, a1.DisplayOrder);
            Assert.AreEqual("探しものはなんですか？", a1.Question);

            // 質問2
            Assert.AreEqual(2, a2.DisplayOrder);
            Assert.AreEqual("2番じゃダメなんですか？", a2.Question);
        }


        //---------------------------------------------------------------------------------------------------------------
        // RegisterAnswer
        //---------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// [対象] RegisterAnswer
        /// [条件] 新規インサート
        /// [結果] インサートが実行される
        /// </summary>
        [TestMethod]
        public void RegisterAnswer_Insert()
        {

            // ---------- パラメータ ----------
            string userId = "user_id";
            int securityQuestionId = 1;
            string answer = "答え";


            // ---------- モック ----------
            // RemoveById
            mockSecurityQuestionAnswersRepository.Setup(_ => _.RemoveById(userId));
            // Insert
            mockSecurityQuestionAnswersRepository.Setup(_ => _.Insert(It.IsAny<SecurityQuestionAnswers>())).Returns<SecurityQuestionAnswers>((entity) =>
            {
                // ---------- 検証 ----------
                Assert.AreEqual(userId, entity.UserId);
                Assert.AreEqual(securityQuestionId, entity.SecurityQuestionId);
                Assert.AreEqual(StringUtilities.CreateHashString(answer), entity.Answer);
                return 1;

            });
            // Update
            mockSecurityQuestionAnswersRepository.Setup(_ => _.Update(It.IsAny<SecurityQuestionAnswers>())).Returns<SecurityQuestionAnswers>((entity) =>
            {
                Assert.Fail();
                return 0;
            });

            // ---------- 実施 ----------
            target.RegisterAnswer(userId, securityQuestionId, answer);

        }


        //---------------------------------------------------------------------------------------------------------------
        // FindSecurityQuestion
        //---------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// [対象] FindSecurityQuestion
        /// [条件] 該当マスタデータし
        /// [結果] 結果にnullが返却される
        /// </summary>
        [TestMethod]
        public void FindSecurityQuestion_Null()
        {

            // ---------- パラメータ ----------
            string userId = "user_id";


            // ---------- モック ----------
            // FindById
            mockSecurityQuestionAnswersRepository.Setup(_ => _.FindById(It.IsAny<string>())).Returns<string>((_userId) => { return null; });

            // FindById
            mockSecurityQuestionMastersRepository.Setup(_ => _.FindById(It.IsAny<string>())).Returns<string>((entity) =>
            {
                Assert.Fail();
                return null;
            });

            // ---------- 実施 ----------
            SecurityQuestionMasters actual = target.FindSecurityQuestion(userId);

            // ---------- 検証 ----------
            Assert.IsNull(actual);
        }

        /// <summary>
        /// [対象] FindSecurityQuestion
        /// [条件] 正常系
        /// [結果] 秘密の質問エンティティが返却される
        /// </summary>
        [TestMethod]
        public void FindSecurityQuestion()
        {

            // ---------- パラメータ ----------
            string userId = "user_id";


            // ---------- モック ----------
            // FindById
            mockSecurityQuestionAnswersRepository.Setup(_ => _.FindById(It.IsAny<string>())).Returns<string>((_userId) =>
            {
                return new SecurityQuestionAnswers()
                {
                    SecurityQuestionId = 123
                };
            });

            // FindById
            mockSecurityQuestionMastersRepository.Setup(_ => _.FindById(It.IsAny<int>())).Returns<int>((_id) =>
            {
                Assert.AreEqual(123, _id);
                return new SecurityQuestionMasters()
                {
                    Question = "正解！"
                };
            });

            // ---------- 実施 ----------
            SecurityQuestionMasters actual = target.FindSecurityQuestion(userId);

            // ---------- 検証 ----------
            Assert.IsNotNull(actual);
            Assert.AreEqual("正解！", actual.Question);
        }

        //---------------------------------------------------------------------------------------------------------------
        // RemoveSecurityQuestion
        //---------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// [対象] RemoveSecurityQuestion
        /// [条件] なし
        /// [結果] 正常終了
        /// </summary>
        [TestMethod]
        public void RemoveSecurityQuestion()
        {

            // ---------- パラメータ ----------
            string userId = "user_id";


            // ---------- モック ----------
            // RemoveById
            mockSecurityQuestionAnswersRepository.Setup(_ => _.RemoveById(userId));

            // ---------- 実施 ----------
            target.RemoveSecurityQuestion(userId);
        }

        //---------------------------------------------------------------------------------------------------------------
        // CheckAnswer
        //---------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// [対象] CheckAnswer
        /// [条件] 該当データがない
        /// [結果] 結果にfalseが返ってくる
        /// </summary>
        [TestMethod]
        public void CheckAnswer＿Null()
        {

            // ---------- パラメータ ----------
            string userId = "user_id";
            int securityQuestionId = 1;
            string answer = "答え";


            // ---------- モック ----------
            // FindByKeys
            mockSecurityQuestionAnswersRepository.Setup(_ => _.FindByKeys(It.IsAny<object[]>())).Returns<object[]>((_param) => { return new SecurityQuestionAnswers(); });

            // ---------- 実施 ----------
            bool actual = target.CheckAnswer(userId, securityQuestionId, answer);

            // ---------- 検証 ----------
            Assert.IsFalse(actual);
        }


        /// <summary>
        /// [対象] CheckAnswer
        /// [条件] 該当データがそz内する
        /// [結果] 結果にtrueが返却される
        /// </summary>
        [TestMethod]
        public void CheckAnswer()
        {

            // ---------- パラメータ ----------
            string userId = "user_id";
            int securityQuestionId = 1;
            string answer = "答え";


            // ---------- モック ----------
            // FindByKeys
            mockSecurityQuestionAnswersRepository.Setup(_ => _.FindByKeys(It.IsAny<object[]>())).Returns<object[]>((_param) =>
            {
                return new SecurityQuestionAnswers()
                {
                    Answer = StringUtilities.CreateHashString("答え")
                };
            });

            // ---------- 実施 ----------
            bool actual = target.CheckAnswer(userId, securityQuestionId, answer);

            // ---------- 検証 ----------
            Assert.IsTrue(actual);
        }

    }
}
