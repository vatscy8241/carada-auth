﻿using CaradaAuthProvider.Api.Domain.Logics;
using NUnit.Framework;

namespace CaradaAuthProvider.Api.Test.Domain.Logics
{
    /// <summary>
    /// <see cref="MailLogic"/>のテストクラス。
    /// </summary>
    [TestFixture]
    public class MailLogicTest
    {
        /// <summary>
        /// テスト対象。
        /// </summary>
        private MailLogic target;

        /// <summary>
        /// <see cref="MailLogic.SendMailAsync(Api.Domain.Dtos.MailSendInfoDto, string)"/>
        /// CASE：バウンス情報を1件取得することができるか。
        /// 事前条件：バウンス情報1件分のレスポンスがあること。
        /// 事後条件：バウンス情報1件分のレスポンスを正常にデシリアライズできること。
        /// </summary>
        public void TestSendMail()
        {

        }
    }
}