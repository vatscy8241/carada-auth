﻿using CaradaAuthProvider.Api.Domain.Dtos;
using CaradaAuthProvider.Api.Domain.Logics;
using CaradaAuthProvider.Api.Domain.Logics.Impl;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using CaradaAuthProvider.Api.Persistence.Storage;
using CaradaAuthProvider.Api.Persistence.Storage.Entities;
using CaradaAuthProvider.Api.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace CaradaAuthProvider.Api.Test.Domain.Logics
{
    [TestClass]
    public class TicketLogicTest
    {
        private TicketLogic target;
        private class MockTicketLogicImpl : TicketLogicImpl
        {
            public MockTicketLogicImpl(Mock<TicketRepository> mockTicketRepository)
            {
                this.ticketRepository = mockTicketRepository.Object;
            }
        }

        private Mock<TicketRepository> mockTicketRepository;

        [TestInitialize]
        public void Initialize()
        {
            mockTicketRepository = new Mock<TicketRepository>();
            target = new MockTicketLogicImpl(mockTicketRepository);
        }

        [TestMethod]
        public void FindTicket()
        {
            string ticketId = "ticketId";
            Ticket ticket = new Ticket();

            mockTicketRepository.Setup(p => p.Find(ticketId));

            Ticket actual = target.FindTicket(ticketId);
            Assert.IsNull(actual);

            mockTicketRepository.Setup(p => p.Find(ticketId)).Returns(ticket);

            actual = target.FindTicket(ticketId);
            Assert.IsNotNull(actual);
            Assert.AreSame(ticket, actual);
        }

        [TestMethod]
        public void RegisterTicket()
        {
            string ticketId = "ticketId";
            Ticket ticket = new Ticket();

            mockTicketRepository.Setup(p => p.Register(ticketId, ticket));

            try
            {
                target.RegisterTicket(ticketId, ticket);
            }
            catch
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void RemoveTicket()
        {
            string ticketId = "ticketId";

            mockTicketRepository.Setup(p => p.Remove(ticketId));

            try
            {
                target.RemoveTicket(ticketId);
            }
            catch
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void IssueTicket_SuccessWithoutVerifyCodeAndMailAddress()
        {
            CaradaIdUser user = new CaradaIdUser()
            {
                Id = "user"
            };
            DateTime now = DateUtilities.UTCNow;

            mockTicketRepository.Setup(p => p.Register(It.IsAny<string>(), It.IsAny<Ticket>())).Callback<string, Ticket>((x, y) =>
            {
                Assert.AreEqual(user.Id, y.Uid);
                Assert.AreEqual(TicketKind.UseStart, y.Kind);
                Assert.IsNotNull(y.ExpiresDateTime);
                Assert.IsNull(y.MailAddress);
                Assert.IsNull(y.VerifyCode);
                Assert.AreEqual(0, y.InvalidCount);
                Assert.IsFalse(y.VerifiedFlag);
            });

            IssuedTicketDto actual = target.IssueTicket(TicketKind.UseStart, user, 20);

            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.TicketId);
            Assert.IsNotNull(actual.Ticket);
            Assert.AreEqual(user.Id, actual.Ticket.Uid);
            Assert.AreEqual(TicketKind.UseStart, actual.Ticket.Kind);
            Assert.IsNotNull(actual.Ticket.ExpiresDateTime);
            Assert.IsNull(actual.Ticket.MailAddress);
            Assert.IsNull(actual.Ticket.VerifyCode);
            Assert.AreEqual(0, actual.Ticket.InvalidCount);
            Assert.IsFalse(actual.Ticket.VerifiedFlag);

            TimeSpan span = actual.Ticket.ExpiresDateTime.Subtract(now);
            Assert.IsTrue(span.TotalMinutes >= 20 && span.TotalMinutes < 21);
        }

        [TestMethod]
        public void IssueTicket_SuccessWithVerifyCode()
        {
            CaradaIdUser user = new CaradaIdUser()
            {
                Id = "user"
            };
            DateTime now = DateUtilities.UTCNow;
            string verifyCode = "verifyCode";

            mockTicketRepository.Setup(p => p.Register(It.IsAny<string>(), It.IsAny<Ticket>())).Callback<string, Ticket>((x, y) =>
            {
                Assert.AreEqual(user.Id, y.Uid);
                Assert.AreEqual(TicketKind.UseStart, y.Kind);
                Assert.IsNotNull(y.ExpiresDateTime);
                Assert.IsNull(y.MailAddress);
                Assert.AreEqual(verifyCode, y.VerifyCode);
                Assert.AreEqual(0, y.InvalidCount);
                Assert.IsFalse(y.VerifiedFlag);
            });

            IssuedTicketDto actual = target.IssueTicket(TicketKind.UseStart, user, 20, verifyCode);

            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.TicketId);
            Assert.IsNotNull(actual.Ticket);
            Assert.AreEqual(user.Id, actual.Ticket.Uid);
            Assert.AreEqual(TicketKind.UseStart, actual.Ticket.Kind);
            Assert.IsNotNull(actual.Ticket.ExpiresDateTime);
            Assert.IsNull(actual.Ticket.MailAddress);
            Assert.AreEqual(verifyCode, actual.Ticket.VerifyCode);
            Assert.AreEqual(0, actual.Ticket.InvalidCount);
            Assert.IsFalse(actual.Ticket.VerifiedFlag);

            TimeSpan span = actual.Ticket.ExpiresDateTime.Subtract(now);
            Assert.IsTrue(span.TotalMinutes >= 20 && span.TotalMinutes < 21);
        }

        [TestMethod]
        public void IssueTicket_SuccessWithMailAddress()
        {
            CaradaIdUser user = new CaradaIdUser()
            {
                Id = "user"
            };
            DateTime now = DateUtilities.UTCNow;
            string mailAddress = "test@test.com";

            mockTicketRepository.Setup(p => p.Register(It.IsAny<string>(), It.IsAny<Ticket>())).Callback<string, Ticket>((x, y) =>
            {
                Assert.AreEqual(user.Id, y.Uid);
                Assert.AreEqual(TicketKind.UseStart, y.Kind);
                Assert.IsNotNull(y.ExpiresDateTime);
                Assert.AreEqual(mailAddress, y.MailAddress);
                Assert.IsNull(y.VerifyCode);
                Assert.AreEqual(0, y.InvalidCount);
                Assert.IsFalse(y.VerifiedFlag);
            });

            IssuedTicketDto actual = target.IssueTicket(TicketKind.UseStart, user, 20, null, mailAddress);

            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.TicketId);
            Assert.IsNotNull(actual.Ticket);
            Assert.AreEqual(user.Id, actual.Ticket.Uid);
            Assert.AreEqual(TicketKind.UseStart, actual.Ticket.Kind);
            Assert.IsNotNull(actual.Ticket.ExpiresDateTime);
            Assert.AreEqual(mailAddress, actual.Ticket.MailAddress);
            Assert.IsNull(actual.Ticket.VerifyCode);
            Assert.AreEqual(0, actual.Ticket.InvalidCount);
            Assert.IsFalse(actual.Ticket.VerifiedFlag);

            TimeSpan span = actual.Ticket.ExpiresDateTime.Subtract(now);
            Assert.IsTrue(span.TotalMinutes >= 20 && span.TotalMinutes < 21);
        }

        [TestMethod]
        public void IssueTicket_SuccessWithVerifyCodeAndMailAddress()
        {
            CaradaIdUser user = new CaradaIdUser()
            {
                Id = "user"
            };
            DateTime now = DateUtilities.UTCNow;
            string verifyCode = "verifyCode";
            string mailAddress = "test@test.com";

            mockTicketRepository.Setup(p => p.Register(It.IsAny<string>(), It.IsAny<Ticket>())).Callback<string, Ticket>((x, y) =>
            {
                Assert.AreEqual(user.Id, y.Uid);
                Assert.AreEqual(TicketKind.UseStart, y.Kind);
                Assert.IsNotNull(y.ExpiresDateTime);
                Assert.AreEqual(mailAddress, y.MailAddress);
                Assert.AreEqual(verifyCode, y.VerifyCode);
                Assert.AreEqual(0, y.InvalidCount);
                Assert.IsFalse(y.VerifiedFlag);
            });

            IssuedTicketDto actual = target.IssueTicket(TicketKind.UseStart, user, 20, verifyCode, mailAddress);

            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.TicketId);
            Assert.IsNotNull(actual.Ticket);
            Assert.AreEqual(user.Id, actual.Ticket.Uid);
            Assert.AreEqual(TicketKind.UseStart, actual.Ticket.Kind);
            Assert.IsNotNull(actual.Ticket.ExpiresDateTime);
            Assert.AreEqual(mailAddress, actual.Ticket.MailAddress);
            Assert.AreEqual(verifyCode, actual.Ticket.VerifyCode);
            Assert.AreEqual(0, actual.Ticket.InvalidCount);
            Assert.IsFalse(actual.Ticket.VerifiedFlag);

            TimeSpan span = actual.Ticket.ExpiresDateTime.Subtract(now);
            Assert.IsTrue(span.TotalMinutes >= 20 && span.TotalMinutes < 21);
        }
        [TestMethod]
        public void IssueTicket_Failure()
        {
            mockTicketRepository.Setup(p => p.Register(It.IsAny<string>(), It.IsAny<Ticket>())).Throws(new Exception("Redis store failed."));

            CaradaIdUser user = new CaradaIdUser()
            {
                Id = "user"
            };

            try
            {
                target.IssueTicket(TicketKind.UseStart, user, 20);
                Assert.Fail();
            }
            catch (Exception e)
            {
                Assert.AreEqual("Redis store failed.", e.Message);
            }
        }

        [TestMethod]
        public void VerifyUseStartTicket_Success()
        {
            Ticket entity = new Ticket()
            {
                Uid = "userId",
                Kind = TicketKind.UseStart,
                ExpiresDateTime = DateUtilities.UTCNow.AddMinutes(1),
                VerifyCode = "verifyCode",
                VerifiedFlag = false,
                InvalidCount = 0
            };
            mockTicketRepository.Setup(p => p.Find(It.IsAny<string>())).Returns(entity);
            mockTicketRepository.Setup(p => p.Register(It.IsAny<string>(), It.IsAny<Ticket>()));

            VerifyResult actual = target.VerifyUseStartTicket("ticketId", "verifyCode");
            Assert.AreEqual(VerifyResult.SUCCESS, actual);
        }

        [TestMethod]
        public void VerifyUseStartTicket_TicketNotFound()
        {
            Ticket entity = new Ticket()
            {
                Uid = "userId",
                Kind = TicketKind.UseStart,
                ExpiresDateTime = DateUtilities.UTCNow.AddMinutes(1),
                VerifyCode = "verifyCode",
                VerifiedFlag = false,
                InvalidCount = 0
            };
            mockTicketRepository.Setup(p => p.Find(It.IsAny<string>()));
            mockTicketRepository.Setup(p => p.Register(It.IsAny<string>(), It.IsAny<Ticket>()));

            VerifyResult actual = target.VerifyUseStartTicket("ticketId", "verifyCode");
            Assert.AreEqual(VerifyResult.TICKET_NOT_FOUND, actual);
        }

        [TestMethod]
        public void VerifyUseStartTicket_Expired()
        {
            Ticket entity = new Ticket()
            {
                Uid = "userId",
                Kind = TicketKind.UseStart,
                ExpiresDateTime = DateUtilities.UTCNow.AddSeconds(-1),
                VerifyCode = "verifyCode",
                VerifiedFlag = false,
                InvalidCount = 0
            };
            mockTicketRepository.Setup(p => p.Find(It.IsAny<string>())).Returns(entity);
            mockTicketRepository.Setup(p => p.Register(It.IsAny<string>(), It.IsAny<Ticket>()));

            VerifyResult actual = target.VerifyUseStartTicket("ticketId", "verifyCode");
            Assert.AreEqual(VerifyResult.EXPIRED, actual);
        }

        [TestMethod]
        public void VerifyUseStartTicket_Invalid()
        {
            Ticket entity = new Ticket()
            {
                Uid = "userId",
                Kind = TicketKind.UseStart,
                ExpiresDateTime = DateUtilities.UTCNow.AddMinutes(1),
                VerifyCode = "verifyCode",
                VerifiedFlag = false,
                InvalidCount = 5
            };
            mockTicketRepository.Setup(p => p.Find(It.IsAny<string>())).Returns(entity);
            mockTicketRepository.Setup(p => p.Register(It.IsAny<string>(), It.IsAny<Ticket>()));

            VerifyResult actual = target.VerifyUseStartTicket("ticketId", "verifyCode");
            Assert.AreEqual(VerifyResult.INVALID, actual);
        }

        [TestMethod]
        public void VerifyUseStartTicket_Code_Mismatch()
        {
            Ticket entity = new Ticket()
            {
                Uid = "userId",
                Kind = TicketKind.UseStart,
                ExpiresDateTime = DateUtilities.UTCNow.AddMinutes(1),
                VerifyCode = null,
                VerifiedFlag = false,
                InvalidCount = 3
            };
            mockTicketRepository.Setup(p => p.Find(It.IsAny<string>())).Returns(entity);
            mockTicketRepository.Setup(p => p.Register(It.IsAny<string>(), It.IsAny<Ticket>()));
            VerifyResult actual;

            actual = target.VerifyUseStartTicket("ticketId", "verifyCode");
            Assert.AreEqual(VerifyResult.CODE_MISMATCH, actual);
            Assert.AreEqual(4, entity.InvalidCount);

            actual = target.VerifyUseStartTicket("ticketId", "verifyCode");
            Assert.AreEqual(VerifyResult.INVALIDED, actual);
            Assert.AreEqual(5, entity.InvalidCount);

            actual = target.VerifyUseStartTicket("ticketId", "verifyCode");
            Assert.AreEqual(VerifyResult.INVALID, actual);
            Assert.AreEqual(5, entity.InvalidCount);

            entity.VerifyCode = "otherverifyCode";
            entity.InvalidCount = 3;

            actual = target.VerifyUseStartTicket("ticketId", "verifyCode");
            Assert.AreEqual(VerifyResult.CODE_MISMATCH, actual);
            Assert.AreEqual(4, entity.InvalidCount);

            actual = target.VerifyUseStartTicket("ticketId", "verifyCode");
            Assert.AreEqual(VerifyResult.INVALIDED, actual);
            Assert.AreEqual(5, entity.InvalidCount);

            actual = target.VerifyUseStartTicket("ticketId", "verifyCode");
            Assert.AreEqual(VerifyResult.INVALID, actual);
            Assert.AreEqual(5, entity.InvalidCount);
        }
    }
}
