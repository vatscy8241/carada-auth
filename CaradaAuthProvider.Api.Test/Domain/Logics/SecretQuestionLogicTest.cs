﻿using CaradaAuthProvider.Api.Domain.Logics;
using CaradaAuthProvider.Api.Persistence.Repositories;
using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace CaradaAuthProvider.Api.Test.Domain.Logics
{
    /// <summary>
    /// 秘密の質問ロジックのテスト
    /// </summary>
    [TestClass]
    public class SecurityQuestionLogicTest
    {
        private SecurityQuestionLogic target;

        private Mock<SecurityQuestionMastersRepository> mockSecurityQuestionMastersRepository;

        private Mock<SecurityQuestionAnswersRepository> mockSecurityQuestionAnswersRepository;

        /// <summary>
        /// 初期化
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            // DbContextの設定
            CaradaAuthProviderDbContext context = new DbContextFactory().NewCaradaAuthProviderDbContext();

            // テスト対象のインスタンス化
            target = new SecurityQuestionLogicImpl(context);

            // リポジトリのモック設定
            mockSecurityQuestionMastersRepository = new Mock<SecurityQuestionMastersRepository>(context);
            var repMaster = typeof(SecurityQuestionLogicImpl).GetField("securityQuestionMastersRepository", BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Instance);
            repMaster.SetValue(target, mockSecurityQuestionMastersRepository.Object);

            mockSecurityQuestionAnswersRepository = new Mock<SecurityQuestionAnswersRepository>(context);
            var repAnswer = typeof(SecurityQuestionLogicImpl).GetField("securityQuestionAnswersRepository", BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Instance);
            repAnswer.SetValue(target, mockSecurityQuestionAnswersRepository.Object);
        }


        /// <summary>
        /// [対象] 秘密の質問マスタリスト取得
        /// [条件] 秘密の質問マスタリストが2件ソートされて返却される
        /// [結果] 秘密の質問マスタリストが2件取得できる
        /// </summary>
        [TestMethod]
        public void GetSecurityQuestion()
        {
            mockSecurityQuestionMastersRepository.Setup(r => r.FindAllOrderByDisplayOrder()).Returns(new List<SecurityQuestionMasters>()
            {

                new SecurityQuestionMasters {
                    SecurityQuestionId = 2,
                    Question = "探しものはなんですか？",
                    DisplayOrder = 1
                },

                new SecurityQuestionMasters {
                    SecurityQuestionId = 1,
                    Question = "2番じゃダメなんですか？",
                    DisplayOrder = 2
                }
            });

            IEnumerable<SecurityQuestionMasters> actual = target.GetSecurityQuestion();
            Assert.IsNotNull(actual);

            SecurityQuestionMasters a1 = null;
            SecurityQuestionMasters a2 = null;
            var count = 0;
            foreach (SecurityQuestionMasters e in actual)
            {

                if (count == 0)
                {
                    a1 = e;
                }
                else if (count == 1)
                {
                    a2 = e;
                }
                count++;
            }


            // 取得件数
            Assert.AreEqual(2, count);

            // 質問1
            Assert.AreEqual(1, a1.DisplayOrder);
            Assert.AreEqual("探しものはなんですか？", a1.Question);

            // 質問2
            Assert.AreEqual(2, a2.DisplayOrder);
            Assert.AreEqual("2番じゃダメなんですか？", a2.Question);
        }

        [TestMethod]
        public void RegisterAnswer_EntityExist()
        {
            string userId = "userId";
            int securityQuestionId = 1;
            string answer = "質問の回答";

            SecurityQuestionAnswers entity = new SecurityQuestionAnswers();
            mockSecurityQuestionAnswersRepository.Setup(p => p.FindByKeys(userId, securityQuestionId)).Returns(entity);
            mockSecurityQuestionAnswersRepository.Setup(p => p.Delete(entity));
            mockSecurityQuestionAnswersRepository.Setup(p => p.Insert(It.IsAny<SecurityQuestionAnswers>())).Callback<SecurityQuestionAnswers>(x =>
            {
                Assert.AreEqual(userId, x.UserId);
                Assert.AreEqual(securityQuestionId, x.SecurityQuestionId);
                Assert.AreEqual("ae87dc4fa58e49472eb7b425dfcbdf66f68241997915a9611ccdb92ba447a776", x.Answer);
            });

            target.RegisterAnswer(userId, securityQuestionId, answer);
        }

        [TestMethod]
        public void RegisterAnswer_EntityNotExist()
        {
            string userId = "userId";
            int securityQuestionId = 1;
            string answer = "質問の回答";

            SecurityQuestionAnswers entity = new SecurityQuestionAnswers();
            mockSecurityQuestionAnswersRepository.Setup(p => p.FindByKeys(userId, securityQuestionId));
            mockSecurityQuestionAnswersRepository.Setup(p => p.Delete(entity)).Throws(new Exception("Assert Fail"));
            mockSecurityQuestionAnswersRepository.Setup(p => p.Insert(It.IsAny<SecurityQuestionAnswers>())).Callback<SecurityQuestionAnswers>(x =>
            {
                Assert.AreEqual(userId, x.UserId);
                Assert.AreEqual(securityQuestionId, x.SecurityQuestionId);
                Assert.AreEqual("ae87dc4fa58e49472eb7b425dfcbdf66f68241997915a9611ccdb92ba447a776", x.Answer);
            });

            target.RegisterAnswer(userId, securityQuestionId, answer);
        }
    }
}
