﻿using CaradaAuthProvider.Api.Domain.Logics;
using CaradaAuthProvider.Api.Domain.Logics.Impl;
using CaradaAuthProvider.Api.Persistence.Storage;
using CaradaAuthProvider.Api.Persistence.Storage.Entities;
using CaradaAuthProvider.Api.Utils;
using Moq;
using NUnit.Framework;
using System;
using System.Reflection;

namespace CaradaAuthProvider.Api.Test.Domain.Logics
{
    /// <summary>
    /// 代理アクセストークンロジックのテスト
    /// </summary>
    [TestFixture]
    public class AgencyAccessTokenLogicTest
    {
        private AgencyAccessTokenLogic target;

        private Mock<AgencyAccessTokenRepository> mAgencyAccessTokenRepository;

        /// <summary>
        /// 初期化
        /// </summary>
        [SetUp]
        public void Initialize()
        {
            target = new AgencyAccessTokenLogicImpl();
        }

        /// <summary>
        /// [対象] 代理アクセストークン取得
        /// [条件] 正常に代理アクセストークンが取得される
        /// [結果] ユーザー情報が格納された代理アクセストークン情報が返却される
        /// </summary>

        [Test]
        public void FindAgencyAccessToken()
        {
            string accessToken = "client_id";

            mAgencyAccessTokenRepository = new Mock<AgencyAccessTokenRepository>();
            mAgencyAccessTokenRepository.Setup(r => r.Find(It.IsAny<string>())).Returns(new AgencyAccessToken
            {
                User = "イケメンゴリラ"
            });
            typeof(AgencyAccessTokenLogicImpl).GetField("agencyAccessTokenRepository", BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Instance).SetValue(target, mAgencyAccessTokenRepository.Object);

            AgencyAccessToken actual = target.FindAgencyAccessToken(accessToken);
            NUnit.Framework.Assert.IsNotNull(actual);
            Assert.AreEqual("イケメンゴリラ", actual.User);
        }


        /// <summary>
        /// [対象] 代理アクセストークン保持
        /// [条件] 有効期限あり
        /// [結果] 正常に代理アクセストークンがセーブされる
        /// </summary>

        [Test]
        public void RegisterAgencyAccessToken()
        {
            string accessToken = "accessToken";
            AgencyAccessToken agencyAccessToken = new AgencyAccessToken() { };
            long expiresAt = DateUtilities.GetEpochTime(DateUtilities.UTCNow) + 1000;


            mAgencyAccessTokenRepository = new Mock<AgencyAccessTokenRepository>();
            mAgencyAccessTokenRepository.Setup(r => r.Register(It.IsAny<string>(), It.IsAny<AgencyAccessToken>(), It.IsAny<long>())).Callback((string _accessToken, AgencyAccessToken _agencyAccessToken, long _expiresAt) =>
            {
                Console.WriteLine("Callback");
                Assert.AreEqual(accessToken, _accessToken);
                Assert.AreEqual(agencyAccessToken, _agencyAccessToken);
                Assert.True(0 < _expiresAt);
            });
            typeof(AgencyAccessTokenLogicImpl).GetField("agencyAccessTokenRepository", BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Instance).SetValue(target, mAgencyAccessTokenRepository.Object);

            target.RegisterAgencyAccessToken(accessToken, agencyAccessToken, expiresAt);

        }


        /// <summary>
        /// [対象] 代理アクセストークン保持
        /// [条件] 有効期限なし
        /// [結果] ユーザー情報が格納された代理アクセストークン情報が返却される
        /// </summary>

        [Test]
        public void RegisterAgencyAccessToken_NotRegister()
        {
            string accessToken = "accessToken";
            AgencyAccessToken agencyAccessToken = new AgencyAccessToken() { };
            long expiresAt = DateUtilities.GetEpochTime(DateUtilities.UTCNow) - 1;


            mAgencyAccessTokenRepository = new Mock<AgencyAccessTokenRepository>();
            mAgencyAccessTokenRepository.Setup(r => r.Register(It.IsAny<string>(), It.IsAny<AgencyAccessToken>(), It.IsAny<long>())).Callback((string _accessToken, AgencyAccessToken _agencyAccessToken, long _expiresAt) =>
            {
                Assert.Fail();
            });
            typeof(AgencyAccessTokenLogicImpl).GetField("agencyAccessTokenRepository", BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Instance).SetValue(target, mAgencyAccessTokenRepository.Object);

            target.RegisterAgencyAccessToken(accessToken, agencyAccessToken, expiresAt);

        }



        /// <summary>
        /// [対象] 代理アクセストークン保持
        /// [条件] 有効期限あり
        /// [結果] 正常に代理アクセストークンがセーブされる
        /// </summary>

        [Test]
        public void RemoveAgencyAccessToken()
        {
            string accessToken = "accessToken";

            mAgencyAccessTokenRepository = new Mock<AgencyAccessTokenRepository>();
            mAgencyAccessTokenRepository.Setup(r => r.Remove(It.IsAny<string>())).Callback((string _accessToken) =>
            {
                Console.WriteLine("Callback");
                Assert.AreEqual(accessToken, _accessToken);
            });
            typeof(AgencyAccessTokenLogicImpl).GetField("agencyAccessTokenRepository", BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Instance).SetValue(target, mAgencyAccessTokenRepository.Object);

            target.RemoveAgencyAccessToken(accessToken);

        }
    }
}
