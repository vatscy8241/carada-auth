USE [local_carada_idp_ut]
GO

/* === 削除 Start === */

/* === 外部キー削除 Start === */

ALTER TABLE [dbo].[UserAuthCodes] DROP CONSTRAINT [FK_dbo.UserAuthCodes_dbo.RedirectUriMasters_RedirectUriSeqNo]
GO
ALTER TABLE [dbo].[UserAuthCodes] DROP CONSTRAINT [FK_dbo.UserAuthCodes_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[SecurityQuestionAnswers] DROP CONSTRAINT [FK_dbo.SecurityQuestionAnswers_dbo.SecurityQuestionMasters_SecurityQuestionId]
GO
ALTER TABLE [dbo].[SecurityQuestionAnswers] DROP CONSTRAINT [FK_dbo.SecurityQuestionAnswers_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[RedirectUriMasters] DROP CONSTRAINT [FK_dbo.RedirectUriMasters_dbo.ClientMasters_ClientId]
GO
ALTER TABLE [dbo].[AuthorizedUsers] DROP CONSTRAINT [FK_dbo.AuthorizedUsers_dbo.ClientMasters_ClientId]
GO
ALTER TABLE [dbo].[AuthorizedUsers] DROP CONSTRAINT [FK_dbo.AuthorizedUsers_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins] DROP CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserClaims] DROP CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO

/* === 外部キー削除 End === */

/* === テーブル削除 Start === */

DROP TABLE [dbo].[UserAuthCodes]
GO
DROP TABLE [dbo].[SecurityQuestionMasters]
GO
DROP TABLE [dbo].[SecurityQuestionAnswers]
GO
DROP TABLE [dbo].[AspNetRoles]
GO
DROP TABLE [dbo].[RedirectUriMasters]
GO
DROP TABLE [dbo].[Issuers]
GO
DROP TABLE [dbo].[ClientMasters]
GO
DROP TABLE [dbo].[AspNetUserRoles]
GO
DROP TABLE [dbo].[AspNetUserLogins]
GO
DROP TABLE [dbo].[AspNetUserClaims]
GO
DROP TABLE [dbo].[AspNetUsers]
GO
DROP TABLE [dbo].[AuthorizedUsers]
GO

/* === テーブル削除 End === */

/* === 削除 End === */

/* === テーブル生成 Start === */

CREATE TABLE [dbo].[AuthorizedUsers] (
    [UserId] [nvarchar](128) NOT NULL,
    [ClientId] [nvarchar](32) NOT NULL,
    [Sub] [nvarchar](32) NOT NULL,
    CONSTRAINT [PK_dbo.AuthorizedUsers] PRIMARY KEY ([UserId], [ClientId])
)
CREATE INDEX [IX_UserId] ON [dbo].[AuthorizedUsers]([UserId])
CREATE INDEX [IX_ClientId] ON [dbo].[AuthorizedUsers]([ClientId])
CREATE UNIQUE INDEX [IX_Sub] ON [dbo].[AuthorizedUsers]([Sub])
CREATE TABLE [dbo].[AspNetUsers] (
    [Id] [nvarchar](128) NOT NULL,
    [EmployeeFlag] [bit] NOT NULL,
    [CreateDateUtc] [datetime] NOT NULL,
    [UseStartDateUtc] [datetime],
    [UpdateDateUtc] [datetime],
    [AuthCodeExpireDateUtc] [datetime],
    [AuthCodeFailedCount] [int] NOT NULL,
    [Email] [nvarchar](256),
    [EmailConfirmed] [bit] NOT NULL,
    [PasswordHash] [nvarchar](max),
    [SecurityStamp] [nvarchar](max),
    [PhoneNumber] [nvarchar](max),
    [PhoneNumberConfirmed] [bit] NOT NULL,
    [TwoFactorEnabled] [bit] NOT NULL,
    [LockoutEndDateUtc] [datetime],
    [LockoutEnabled] [bit] NOT NULL,
    [AccessFailedCount] [int] NOT NULL,
    [UserName] [nvarchar](256) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY ([Id])
)
CREATE UNIQUE INDEX [UserNameIndex] ON [dbo].[AspNetUsers]([UserName])
CREATE TABLE [dbo].[AspNetUserClaims] (
    [Id] [int] NOT NULL IDENTITY,
    [UserId] [nvarchar](128) NOT NULL,
    [ClaimType] [nvarchar](max),
    [ClaimValue] [nvarchar](max),
    CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]([UserId])
CREATE TABLE [dbo].[AspNetUserLogins] (
    [LoginProvider] [nvarchar](128) NOT NULL,
    [ProviderKey] [nvarchar](128) NOT NULL,
    [UserId] [nvarchar](128) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY ([LoginProvider], [ProviderKey], [UserId])
)
CREATE INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]([UserId])
CREATE TABLE [dbo].[AspNetUserRoles] (
    [UserId] [nvarchar](128) NOT NULL,
    [RoleId] [nvarchar](128) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY ([UserId], [RoleId])
)
CREATE INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]([UserId])
CREATE INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]([RoleId])
CREATE TABLE [dbo].[ClientMasters] (
    [ClientId] [nvarchar](32) NOT NULL,
    [ClientName] [nvarchar](100) NOT NULL,
    [ClientSecret] [nvarchar](256) NOT NULL,
    [AuthorizeConfirmFlag] [bit] NOT NULL,
    CONSTRAINT [PK_dbo.ClientMasters] PRIMARY KEY ([ClientId])
)
CREATE UNIQUE INDEX [IX_ClientName] ON [dbo].[ClientMasters]([ClientName])
CREATE TABLE [dbo].[Issuers] (
    [IssuerId] [nvarchar](32) NOT NULL,
    [SecretKey] [nvarchar](64) NOT NULL,
    [Remark] [nvarchar](256) NOT NULL,
    CONSTRAINT [PK_dbo.Issuers] PRIMARY KEY ([IssuerId])
)
CREATE TABLE [dbo].[RedirectUriMasters] (
    [SeqNo] [bigint] NOT NULL IDENTITY,
    [ClientId] [nvarchar](32) NOT NULL,
    [RedirectUri] [nvarchar](300) NOT NULL,
    CONSTRAINT [PK_dbo.RedirectUriMasters] PRIMARY KEY ([SeqNo])
)
CREATE UNIQUE INDEX [IX_ClientId_RedirectUri] ON [dbo].[RedirectUriMasters]([ClientId], [RedirectUri])
CREATE TABLE [dbo].[AspNetRoles] (
    [Id] [nvarchar](128) NOT NULL,
    [Name] [nvarchar](256) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY ([Id])
)
CREATE UNIQUE INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]([Name])
CREATE TABLE [dbo].[SecurityQuestionAnswers] (
    [UserId] [nvarchar](128) NOT NULL,
    [SecurityQuestionId] [int] NOT NULL,
    [Answer] [nvarchar](64) NOT NULL,
    CONSTRAINT [PK_dbo.SecurityQuestionAnswers] PRIMARY KEY ([UserId], [SecurityQuestionId])
)
CREATE INDEX [IX_UserId] ON [dbo].[SecurityQuestionAnswers]([UserId])
CREATE INDEX [IX_SecurityQuestionId] ON [dbo].[SecurityQuestionAnswers]([SecurityQuestionId])
CREATE TABLE [dbo].[SecurityQuestionMasters] (
    [SecurityQuestionId] [int] NOT NULL IDENTITY,
    [Question] [nvarchar](128) NOT NULL,
    [DisplayOrder] [int] NOT NULL,
    CONSTRAINT [PK_dbo.SecurityQuestionMasters] PRIMARY KEY ([SecurityQuestionId])
)
CREATE INDEX [IX_DisplayOrder] ON [dbo].[SecurityQuestionMasters]([DisplayOrder])
CREATE TABLE [dbo].[UserAuthCodes] (
    [AuthorizationCode] [nvarchar](32) NOT NULL,
    [UserId] [nvarchar](128) NOT NULL,
    [ExpireDateUtc] [datetime] NOT NULL,
    [RedirectUriSeqNo] [bigint] NOT NULL,
    CONSTRAINT [PK_dbo.UserAuthCodes] PRIMARY KEY ([AuthorizationCode])
)
CREATE INDEX [IX_UserId] ON [dbo].[UserAuthCodes]([UserId])
CREATE INDEX [IX_RedirectUriSeqNo] ON [dbo].[UserAuthCodes]([RedirectUriSeqNo])
ALTER TABLE [dbo].[AuthorizedUsers] ADD CONSTRAINT [FK_dbo.AuthorizedUsers_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[AuthorizedUsers] ADD CONSTRAINT [FK_dbo.AuthorizedUsers_dbo.ClientMasters_ClientId] FOREIGN KEY ([ClientId]) REFERENCES [dbo].[ClientMasters] ([ClientId]) ON DELETE CASCADE
ALTER TABLE [dbo].[AspNetUserClaims] ADD CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[AspNetUserLogins] ADD CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[AspNetUserRoles] ADD CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[AspNetUserRoles] ADD CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[AspNetRoles] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[RedirectUriMasters] ADD CONSTRAINT [FK_dbo.RedirectUriMasters_dbo.ClientMasters_ClientId] FOREIGN KEY ([ClientId]) REFERENCES [dbo].[ClientMasters] ([ClientId]) ON DELETE CASCADE
ALTER TABLE [dbo].[SecurityQuestionAnswers] ADD CONSTRAINT [FK_dbo.SecurityQuestionAnswers_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[SecurityQuestionAnswers] ADD CONSTRAINT [FK_dbo.SecurityQuestionAnswers_dbo.SecurityQuestionMasters_SecurityQuestionId] FOREIGN KEY ([SecurityQuestionId]) REFERENCES [dbo].[SecurityQuestionMasters] ([SecurityQuestionId]) ON DELETE CASCADE
ALTER TABLE [dbo].[UserAuthCodes] ADD CONSTRAINT [FK_dbo.UserAuthCodes_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[UserAuthCodes] ADD CONSTRAINT [FK_dbo.UserAuthCodes_dbo.RedirectUriMasters_RedirectUriSeqNo] FOREIGN KEY ([RedirectUriSeqNo]) REFERENCES [dbo].[RedirectUriMasters] ([SeqNo]) ON DELETE CASCADE

/* === テーブル生成 End === */