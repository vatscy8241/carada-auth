﻿using CaradaAuthProvider.Api.Persistence.Http;
using CaradaAuthProvider.Api.Persistence.Http.Manager;
using Codeplex.Data;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net;
using System.Reflection;

namespace CaradaAuthProvider.Api.Test.Persistence.Http
{
    /// <summary>
    /// <see cref="SendGridApi"/>のテストクラス。
    /// </summary>
    [TestFixture]
    public class SendGridApiTest
    {
        /// <summary>
        /// テスト対象。
        /// </summary>
        private SendGridApi target;

        #region GetBounce(string)

        /// <summary>
        /// <see cref="SendGridApi.GetBounce(string)"/>のテスト。
        /// CASE：バウンス情報を1件取得することができるか。
        /// 事前条件：バウンス情報1件分のレスポンスがあること。
        /// 事後条件：バウンス情報1件分のレスポンスを正常にデシリアライズできること。
        /// </summary>
        [Test]
        public void TestGetBounce_SUCCESS_Single_Data()
        {
            target = new SendGridApi("123456"); ;

            var httpMock = new Mock<HttpConnectionManager>();

            var webHeaderColection = new WebHeaderCollection();
            webHeaderColection.Set("Content-Type", "application/json");

            httpMock.Setup(m => m.DoGetRequest(It.IsAny<string>(), null, It.IsAny<Dictionary<string, string>>())).Returns(
            new Response
            {
                StatusCode = HttpStatusCode.OK,
                ResponseBody = DynamicJson.Parse("[{\"created\":1234567890,\"email\":\"test@mail.com\",\"reason\":\"test reason\",\"status\":\"5.1.1\"}]"),
                ResponseHeaders = webHeaderColection
            }
            );

            var HttpInfo = target.GetType().GetField("Http", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetField);
            HttpInfo.SetValue(target, httpMock.Object);

            var actual = target.GetBounce("test@mail.com");
            httpMock.VerifyAll();

            Assert.That(actual, Is.Not.Null);
            Assert.AreEqual(1, actual.Count);

            Assert.AreEqual(1234567890, actual[0].created);
            Assert.AreEqual("test@mail.com", actual[0].email);
            Assert.AreEqual("test reason", actual[0].reason);
            Assert.AreEqual("5.1.1", actual[0].status);
        }

        /// <summary>
        /// <see cref="SendGridApi.GetBounce(string)"/>のテスト。
        /// CASE：バウンス情報を複数件取得することができるか。
        /// 事前条件：バウンス情報3件分のレスポンスがあること。
        /// 事後条件：バウンス情報3件分のレスポンスを正常にデシリアライズできること。
        /// </summary>
        [Test]
        public void TestGetBounce_SUCCESS_Multi_Data()
        {
            target = new SendGridApi("123456"); ;

            var httpMock = new Mock<HttpConnectionManager>();

            var webHeaderColection = new WebHeaderCollection();
            webHeaderColection.Set("Content-Type", "application/json");

            httpMock.Setup(m => m.DoGetRequest(It.IsAny<string>(), null, It.IsAny<Dictionary<string, string>>())).Returns(
            new Response
            {
                StatusCode = HttpStatusCode.OK,
                ResponseBody = DynamicJson.Parse("[{\"created\":1234567890,\"email\":\"test@mail.com\",\"reason\":\"test reason\",\"status\":\"5.1.1\"},{\"created\":222444888,\"email\":\"test@mail.com\",\"reason\":\"second reason\",\"status\":\"5.2.2\"},{\"created\":333666999,\"email\":\"test@mail.com\",\"reason\":\"final reason\",\"status\":\"3.6.9\"}]"),
                ResponseHeaders = webHeaderColection
            }
            );

            var HttpInfo = target.GetType().GetField("Http", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetField);
            HttpInfo.SetValue(target, httpMock.Object);

            var actual = target.GetBounce("test@mail.com");
            httpMock.VerifyAll();

            Assert.That(actual, Is.Not.Null);
            Assert.AreEqual(3, actual.Count);

            Assert.AreEqual(1234567890, actual[0].created);
            Assert.AreEqual("test@mail.com", actual[0].email);
            Assert.AreEqual("test reason", actual[0].reason);
            Assert.AreEqual("5.1.1", actual[0].status);

            Assert.AreEqual(222444888, actual[1].created);
            Assert.AreEqual("test@mail.com", actual[1].email);
            Assert.AreEqual("second reason", actual[1].reason);
            Assert.AreEqual("5.2.2", actual[1].status);

            Assert.AreEqual(333666999, actual[2].created);
            Assert.AreEqual("test@mail.com", actual[2].email);
            Assert.AreEqual("final reason", actual[2].reason);
            Assert.AreEqual("3.6.9", actual[2].status);
        }

        /// <summary>
        /// <see cref="SendGridApi.GetBounce(string)"/>のテスト。
        /// CASE：バウンス情報が0件取得することができるか。
        /// 事前条件：バウンス情報0件分のレスポンスがあること。
        /// 事後条件：バウンス情報0件分のレスポンスを正常にデシリアライズできること。
        /// </summary>
        [Test]
        public void TestGetBounce_SUCCESS_Empty()
        {
            target = new SendGridApi("123456"); ;

            var httpMock = new Mock<HttpConnectionManager>();

            var webHeaderColection = new WebHeaderCollection();
            webHeaderColection.Set("Content-Type", "application/json");

            httpMock.Setup(m => m.DoGetRequest(It.IsAny<string>(), null, It.IsAny<Dictionary<string, string>>())).Returns(
            new Response
            {
                StatusCode = HttpStatusCode.OK,
                ResponseBody = DynamicJson.Parse("[{}]"),
                ResponseHeaders = webHeaderColection
            }
            );

            var HttpInfo = target.GetType().GetField("Http", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetField);
            HttpInfo.SetValue(target, httpMock.Object);

            var actual = target.GetBounce("test@mail.com");
            httpMock.VerifyAll();

            Assert.That(actual, Is.Not.Null);
            Assert.AreEqual(1, actual.Count);
            Assert.IsTrue(string.IsNullOrEmpty(actual[0].email));
        }

        /// <summary>
        /// <see cref="SendGridApi.GetBounce(string)"/>のテスト。
        /// CASE：バウンス情報取得失敗した場合(HTTPステータスコードがOKではない場合)nullが返却されるか。
        /// 事前条件：HTTPステータスコードがOKではないこと。
        /// 事後条件：nullが返却されること。
        /// </summary>
        [Test]
        public void TestGetBounce_BadRequest()
        {
            target = new SendGridApi("123456"); ;

            var httpMock = new Mock<HttpConnectionManager>();

            var webHeaderColection = new WebHeaderCollection();
            webHeaderColection.Set("Content-Type", "application/json");

            httpMock.Setup(m => m.DoGetRequest(It.IsAny<string>(), null, It.IsAny<Dictionary<string, string>>())).Returns(
            new Response
            {
                StatusCode = HttpStatusCode.BadRequest,
                ResponseBody = DynamicJson.Parse("[{}]"),
                ResponseHeaders = webHeaderColection
            }
            );

            var HttpInfo = target.GetType().GetField("Http", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetField);
            HttpInfo.SetValue(target, httpMock.Object);

            var actual = target.GetBounce("test@mail.com");
            httpMock.VerifyAll();

            Assert.That(actual, Is.Null);
        }

        /// <summary>
        /// <see cref="SendGridApi.GetBounce(string)"/>のテスト。
        /// CASE：バウンス情報取得失敗した場合(レスポンスがnullだった場合)nullが返却されるか。
        /// 事前条件：レスポンスがnullであること。
        /// 事後条件：nullが返却されること。
        /// </summary>
        [Test]
        public void TestGetBounce_No_Response()
        {
            target = new SendGridApi("123456"); ;

            var httpMock = new Mock<HttpConnectionManager>();

            var webHeaderColection = new WebHeaderCollection();
            webHeaderColection.Set("Content-Type", "application/json");

            httpMock.Setup(m => m.DoGetRequest(It.IsAny<string>(), null, It.IsAny<Dictionary<string, string>>())).Returns(null);

            var HttpInfo = target.GetType().GetField("Http", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetField);
            HttpInfo.SetValue(target, httpMock.Object);

            var actual = target.GetBounce("test@mail.com");
            httpMock.VerifyAll();

            Assert.That(actual, Is.Null);
        }

        /// <summary>
        /// <see cref="SendGridApi.GetBounce(string)"/>のテスト。
        /// CASE：パラメータがnullだった場合、<see cref="ArgumentNullException"/>がthrowされるか。
        /// 事前条件：パラメータにnullを指定すること。
        /// 事後条件：<see cref="ArgumentNullException"/>がthrowされること。
        /// </summary>
        [Test]
        public void TestGetBounce_Throw_ArgumentNullException_Null_Parameter()
        {
            target = new SendGridApi("123456"); ;

            Assert.Throws<ArgumentNullException>(() => target.GetBounce(null));
        }

        /// <summary>
        /// <see cref="SendGridApi.GetBounce(string)"/>のテスト。
        /// CASE：パラメータが空文字だった場合、<see cref="ArgumentNullException"/>がthrowされるか。
        /// 事前条件：パラメータに空文字を指定すること。
        /// 事後条件：<see cref="ArgumentNullException"/>がthrowされること。
        /// </summary>
        [Test]
        public void TestGetBounce_Throw_ArgumentNullException_Empty_Parameter()
        {
            target = new SendGridApi("123456"); ;

            Assert.Throws<ArgumentNullException>(() => target.GetBounce(string.Empty));
        }

        #endregion

        #region DeleteBounce(string)

        /// <summary>
        /// <see cref="SendGridApi.DeleteBounce(string)"/>のテスト。
        /// CASE：バウンス情報が正常に削除できるか。
        /// 事前条件：HTTPステータスコードがNoContentであること。
        /// 事後条件：trueが返却されること。
        /// </summary>
        [Test]
        public void TestDeleteBounce_SUCCESS()
        {
            target = new SendGridApi("123456"); ;

            var httpMock = new Mock<HttpConnectionManager>();

            var webHeaderColection = new WebHeaderCollection();
            webHeaderColection.Set("Content-Type", "application/json");

            httpMock.Setup(m => m.DoDeleteRequest(It.IsAny<string>(), It.IsAny<Dictionary<string, string>>())).Returns(
            new Response
            {
                StatusCode = HttpStatusCode.NoContent,
                ResponseBody = string.Empty,
                ResponseHeaders = webHeaderColection
            }
            );

            var HttpInfo = target.GetType().GetField("Http", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetField);
            HttpInfo.SetValue(target, httpMock.Object);

            var actual = target.DeleteBounce("test@mail.com");
            httpMock.VerifyAll();

            Assert.IsTrue(actual);
        }

        /// <summary>
        /// <see cref="SendGridApi.DeleteBounce(string)"/>のテスト。
        /// CASE：バウンス情報の削除に失敗した場合(HTTPステータスコードがOKではない場合)、falseが返却されるか。
        /// 事前条件：HTTPステータスコードがNoContentではないこと。
        /// 事後条件：falseが返却されること。
        /// </summary>
        [Test]
        public void TestDeleteBounce_BadRequest()
        {
            target = new SendGridApi("123456"); ;

            var httpMock = new Mock<HttpConnectionManager>();

            var webHeaderColection = new WebHeaderCollection();
            webHeaderColection.Set("Content-Type", "application/json");

            httpMock.Setup(m => m.DoDeleteRequest(It.IsAny<string>(), It.IsAny<Dictionary<string, string>>())).Returns(
            new Response
            {
                StatusCode = HttpStatusCode.BadRequest,
                ResponseBody = string.Empty,
                ResponseHeaders = webHeaderColection
            }
            );

            var HttpInfo = target.GetType().GetField("Http", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetField);
            HttpInfo.SetValue(target, httpMock.Object);

            var actual = target.DeleteBounce("test@mail.com");
            httpMock.VerifyAll();

            Assert.IsFalse(actual);
        }

        /// <summary>
        /// <see cref="SendGridApi.DeleteBounce(string)"/>のテスト。
        /// CASE：バウンス情報の削除に失敗した場合(レスポンスがnullだった場合)nullが返却されるか。
        /// 事前条件：レスポンスがnullであること。
        /// 事後条件：falseが返却されること。
        /// </summary>
        [Test]
        public void TestDeleteBounce_No_Response()
        {
            target = new SendGridApi("123456"); ;

            var httpMock = new Mock<HttpConnectionManager>();

            var webHeaderColection = new WebHeaderCollection();
            webHeaderColection.Set("Content-Type", "application/json");

            httpMock.Setup(m => m.DoDeleteRequest(It.IsAny<string>(), It.IsAny<Dictionary<string, string>>())).Returns(
            new Response
            {
                StatusCode = HttpStatusCode.BadRequest,
                ResponseBody = string.Empty,
                ResponseHeaders = webHeaderColection
            }
            );

            var HttpInfo = target.GetType().GetField("Http", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetField);
            HttpInfo.SetValue(target, httpMock.Object);

            var actual = target.DeleteBounce("test@mail.com");
            httpMock.VerifyAll();

            Assert.IsFalse(actual);
        }

        /// <summary>
        /// <see cref="SendGridApi.DeleteBounce(string)"/>のテスト。
        /// CASE：パラメータがnullだった場合、<see cref="ArgumentNullException"/>がthrowされるか。
        /// 事前条件：パラメータにnullを指定すること。
        /// 事後条件：<see cref="ArgumentNullException"/>がthrowされること。
        /// </summary>
        [Test]
        public void TestDeleteBounce_Throw_ArgumentNullException_Null_Parameter()
        {
            target = new SendGridApi("123456"); ;

            Assert.Throws<ArgumentNullException>(() => target.DeleteBounce(""));
        }

        /// <summary>
        /// <see cref="SendGridApi.DeleteBounce(string)"/>のテスト。
        /// CASE：パラメータが空文字だった場合、<see cref="ArgumentNullException"/>がthrowされるか。
        /// 事前条件：パラメータに空文字を指定すること。
        /// 事後条件：<see cref="ArgumentNullException"/>がthrowされること。
        /// </summary>
        [Test]
        public void TestDeleteBounce_Throw_ArgumentNullException_Empty_Parameter()
        {
            target = new SendGridApi("123456"); ;

            Assert.Throws<ArgumentNullException>(() => target.DeleteBounce(string.Empty));
        }

        #endregion

        #region DeleteBounces(SendGridApi.DeleteBouncesParameters

        /// <summary>
        /// <see cref="SendGridApi.DeleteBounces(SendGridApi.DeleteBouncesParameters)"/>のテスト。
        /// CASE：バウンス情報が正常に削除できるか。
        /// 事前条件：HTTPステータスコードがNoContentであること。
        /// 事後条件：trueが返却されること。
        /// </summary>
        [Test]
        public void TestDeleteBounces_SUCCESS()
        {
            target = new SendGridApi("123456"); ;

            var httpMock = new Mock<HttpConnectionManager>();

            var webHeaderColection = new WebHeaderCollection();
            webHeaderColection.Set("Content-Type", "application/json");

            httpMock.Setup(m => m.DoDeleteRequest(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Dictionary<string, string>>())).Returns(
            new Response
            {
                StatusCode = HttpStatusCode.NoContent,
                ResponseBody = string.Empty,
                ResponseHeaders = webHeaderColection
            }
            );

            var HttpInfo = target.GetType().GetField("Http", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetField);
            HttpInfo.SetValue(target, httpMock.Object);

            var parameters = new SendGridApi.DeleteBouncesParameters();
            parameters.Emails = new List<string>();
            parameters.Emails.Add("test@mail.com");
            parameters.Emails.Add("second@mail.jp");

            var actual = target.DeleteBounces(parameters);
            httpMock.VerifyAll();

            Assert.IsTrue(actual);
        }

        /// <summary>
        /// <see cref="SendGridApi.DeleteBounces(SendGridApi.DeleteBouncesParameters)"/>のテスト。
        /// CASE：バウンス情報の削除に失敗した場合(HTTPステータスコードがOKではない場合)、falseが返却されるか。
        /// 事前条件：HTTPステータスコードがNoContentではないこと。
        /// 事後条件：falseが返却されること。
        /// </summary>
        [Test]
        public void TestDeleteBounces_BadRequest()
        {
            target = new SendGridApi("123456"); ;

            var httpMock = new Mock<HttpConnectionManager>();

            var webHeaderColection = new WebHeaderCollection();
            webHeaderColection.Set("Content-Type", "application/json");

            httpMock.Setup(m => m.DoDeleteRequest(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Dictionary<string, string>>())).Returns(
            new Response
            {
                StatusCode = HttpStatusCode.BadRequest,
                ResponseBody = string.Empty,
                ResponseHeaders = webHeaderColection
            }
            );

            var HttpInfo = target.GetType().GetField("Http", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetField);
            HttpInfo.SetValue(target, httpMock.Object);

            var parameters = new SendGridApi.DeleteBouncesParameters();
            parameters.Emails = new List<string>();
            parameters.Emails.Add("test@mail.com");
            parameters.Emails.Add("second@mail.jp");

            var actual = target.DeleteBounces(parameters);
            httpMock.VerifyAll();

            Assert.IsFalse(actual);
        }

        /// <summary>
        /// <see cref="SendGridApi.DeleteBounces(SendGridApi.DeleteBouncesParameters)"/>のテスト。
        /// CASE：バウンス情報の削除に失敗した場合(レスポンスがnullだった場合)nullが返却されるか。
        /// 事前条件：レスポンスがnullであること。
        /// 事後条件：falseが返却されること。
        /// </summary>
        [Test]
        public void TestDeleteBounces_No_Response()
        {
            target = new SendGridApi("123456"); ;

            var httpMock = new Mock<HttpConnectionManager>();

            var webHeaderColection = new WebHeaderCollection();
            webHeaderColection.Set("Content-Type", "application/json");

            httpMock.Setup(m => m.DoDeleteRequest(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Dictionary<string, string>>())).Returns(
            new Response
            {
                StatusCode = HttpStatusCode.BadRequest,
                ResponseBody = string.Empty,
                ResponseHeaders = webHeaderColection
            }
            );

            var HttpInfo = target.GetType().GetField("Http", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetField);
            HttpInfo.SetValue(target, httpMock.Object);

            var parameters = new SendGridApi.DeleteBouncesParameters();
            parameters.Emails = new List<string>();
            parameters.Emails.Add("test@mail.com");
            parameters.Emails.Add("second@mail.jp");

            var actual = target.DeleteBounces(parameters);
            httpMock.VerifyAll();

            Assert.IsFalse(actual);
        }

        /// <summary>
        /// <see cref="SendGridApi.DeleteBounces(SendGridApi.DeleteBouncesParameters)"/>のテスト。
        /// CASE：パラメータがNullだった場合、<see cref="ArgumentNullException"/>がthrowされるか。
        /// 事前条件：パラメータにNullを指定すること。
        /// 事後条件：<see cref="ArgumentNullException"/>がthrowされること。
        /// </summary>
        [Test]
        public void TestDeleteBounces_Throw_ArgumentNullException_Null_Parameter()
        {
            target = new SendGridApi("123456"); ;

            Assert.Throws<ArgumentNullException>(() => target.DeleteBounces(null));
        }

        /// <summary>
        /// <see cref="SendGridApi.DeleteBounces(SendGridApi.DeleteBouncesParameters)"/>のテスト。
        /// CASE：パラメータのemailのListがNullだった場合、<see cref="ArgumentNullException"/>がthrowされるか。
        /// 事前条件：パラメータのemailのListにNullを指定すること。
        /// 事後条件：<see cref="ArgumentNullException"/>がthrowされること。
        /// </summary>
        [Test]
        public void TestDeleteBounces_Throw_ArgumentNullException_Null_Email_List()
        {
            target = new SendGridApi("123456"); ;

            var parameters = new SendGridApi.DeleteBouncesParameters();
            Assert.Throws<ArgumentNullException>(() => target.DeleteBounces(parameters));
        }

        /// <summary>
        /// <see cref="SendGridApi.DeleteBounces(SendGridApi.DeleteBouncesParameters)"/>のテスト。
        /// CASE：パラメータのemailのListが空だった場合、<see cref="ArgumentNullException"/>がthrowされるか。
        /// 事前条件：パラメータのemailのListに空を指定すること。
        /// 事後条件：<see cref="ArgumentNullException"/>がthrowされること。
        /// </summary>
        [Test]
        public void TestDeleteBounces_Throw_ArgumentNullException_List_Empty()
        {
            target = new SendGridApi("123456"); ;

            var parameters = new SendGridApi.DeleteBouncesParameters();
            parameters.Emails = new List<string>();
            Assert.Throws<ArgumentNullException>(() => target.DeleteBounces(parameters));
        }

        #endregion

        #region TestClass

        /// <summary>
        /// テスト用APIレスポンスクラス。<br/>
        /// moqでAnonymousTypeを返すとプロパティにアクセスできなくなるため暫定対応。
        /// </summary>
        public class Response
        {
            /// <summary>
            /// HTTPステータスコード。
            /// </summary>
            public HttpStatusCode StatusCode { get; set; }

            /// <summary>
            /// レスポンスボディ。
            /// 
            /// </summary>
            public dynamic ResponseBody { get; set; }

            /// <summary>
            /// レスポンスヘッダー。
            /// </summary>
            public WebHeaderCollection ResponseHeaders { get; set; }
        }

        #endregion
    }
}