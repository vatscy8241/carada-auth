﻿using CaradaAuthProvider.Api.Persistence.Repositories;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using NUnit.Framework;

namespace CaradaAuthProvider.Api.Test.Persistence.Repositories
{
    /// <summary>
    /// <see cref="SecurityQuestionAnswersRepository"/>のテスト。
    /// </summary>
    [TestFixture]
    public class SecurityQuestionAnswersRepositoryTest : AbstractDbUnitTestBase<SecurityQuestionAnswersRepository>
    {
        #region SetUp/TearDown

        /// <summary>
        /// <see cref="AbstractDbUnitTestBase.OneTimeSetUp"/>
        /// </summary>
        [OneTimeSetUp]
        public override void OneTimeSetUp()
        {
            base.OneTimeSetUp();
        }

        /// <summary>
        /// <see cref="AbstractDbUnitTestBase.OneTimeTearDown"/>
        /// </summary>
        [OneTimeTearDown]
        public override void OneTimeTearDown()
        {
            base.OneTimeTearDown();
        }

        /// <summary>
        /// <see cref="AbstractDbUnitTestBase.SetUp"/>
        /// </summary>
        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
        }

        /// <summary>
        /// <see cref="AbstractDbUnitTestBase.TearDown"/>
        /// </summary>
        [TearDown]
        public override void TearDown()
        {
            base.TearDown();
        }

        protected override SecurityQuestionAnswersRepository CreateRepositoryInstance()
        {
            return new SecurityQuestionAnswersRepository(testDbContext);
        }

        #endregion


        /// <summary>
        /// [対象] FindById
        /// [条件] 条件にヒットするレコードあり
        /// [結果] ヒットしてレコードのエンティティが返却される
        /// 
        /// </summary>
        [Test]
        public void Test_FindById()
        {
            string userId = "756bb61e-9127-4b59-9421-c6100f00363e";
            SecurityQuestionAnswers actual = target.FindById(userId);
            Assert.That(actual, Is.Not.Null);
        }


        /// <summary>
        /// [対象] FindById
        /// [条件] 条件にヒットするレコードなし
        /// [結果] nullが返却される
        /// </summary>
        [Test]
        public void Test_FindById_Empty()
        {
            string userId = "756bb61e-9127-4b59-9421-c6100f00363b";
            SecurityQuestionAnswers actual = target.FindById(userId);
            Assert.That(actual, Is.Null);
        }

        /// <summary>
        /// [対象] RemoveById
        /// [条件] 条件にヒットするレコードあり
        /// [結果] ヒットしてレコードのエンティティが返却される
        /// 
        /// </summary>
        [Test]
        public void Test_RemoveById()
        {
            string userId = "756bb61e-9127-4b59-9421-c6100f00363e";
            target.RemoveById(userId);

            SecurityQuestionAnswers actual = target.FindById(userId);
            Assert.That(actual, Is.Not.Null);
        }
    }
}