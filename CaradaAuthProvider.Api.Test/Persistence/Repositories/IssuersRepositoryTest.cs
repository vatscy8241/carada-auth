﻿using CaradaAuthProvider.Api.Persistence.Repositories;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace CaradaAuthProvider.Api.Test.Persistence.Repositories
{
    /// <summary>
    /// <see cref="IssuersRepository"/>のテスト。
    /// </summary>
    [TestFixture]
    public class IssuersRepositoryTest : AbstractDbUnitTestBase<IssuersRepository>
    {
        #region SetUp/TearDown

        /// <summary>
        /// <see cref="AbstractDbUnitTestBase.OneTimeSetUp"/>
        /// </summary>
        [OneTimeSetUp]
        public override void OneTimeSetUp()
        {
            base.OneTimeSetUp();
        }

        /// <summary>
        /// <see cref="AbstractDbUnitTestBase.OneTimeTearDown"/>
        /// </summary>
        [OneTimeTearDown]
        public override void OneTimeTearDown()
        {
            base.OneTimeTearDown();
        }

        /// <summary>
        /// <see cref="AbstractDbUnitTestBase.SetUp"/>
        /// </summary>
        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
        }

        /// <summary>
        /// <see cref="AbstractDbUnitTestBase.TearDown"/>
        /// </summary>
        [TearDown]
        public override void TearDown()
        {
            base.TearDown();
        }

        #endregion

        /// <summary>
        /// <see cref="AbstractRepository{TEntity, TDbContext}.FindById(object)"/>
        /// CASE：IDからエンティティを取得できるか。
        /// 事前条件：テーブルに初期データが投入されていること。
        /// 事後条件：エンティティが取得できること。
        /// </summary>
        [Test]
        public void TestFindById()
        {
            var actual = target.FindById("1");

            Assert.That(actual, Is.Not.Null);
            Assert.AreEqual("1", actual.IssuerId);
            Assert.AreEqual("8F+Gy+L{O6x{SfugME*%A/!Di&l*tY$QaMH0gyzYoL(%4JvfbP&t5*Hpl4l-.|aX", actual.SecretKey);
            Assert.AreEqual("品管利用のローカルツールユーザー01", actual.Remark);
        }

        /// <summary>
        /// <see cref="AbstractRepository{TEntity, TDbContext}.FindAll"/>
        /// CASE：全てのエンティティを取得できるか。
        /// 事前条件：テーブルに初期データが投入されていること。
        /// 事後条件：全てのエンティティが取得できること。
        /// </summary>
        [Test]
        public void TestFindAll()
        {
            var actual = target.FindAll();
            var actualList = actual.ToList<Issuers>();

            Assert.That(actual, Is.Not.Null);
            Assert.AreEqual(2, actualList.Count);
            Assert.AreEqual("1", actualList[0].IssuerId);
            Assert.AreEqual("8F+Gy+L{O6x{SfugME*%A/!Di&l*tY$QaMH0gyzYoL(%4JvfbP&t5*Hpl4l-.|aX", actualList[0].SecretKey);
            Assert.AreEqual("品管利用のローカルツールユーザー01", actualList[0].Remark);

            Assert.AreEqual("2", actualList[1].IssuerId);
            Assert.AreEqual("TestSecretKey", actualList[1].SecretKey);
            Assert.AreEqual("UnitTest02", actualList[1].Remark);
        }

        /// <summary>
        /// <see cref="AbstractRepository{TEntity, TDbContext}.Insert(TEntity)"/>
        /// CASE：指定のエンティティを登録できるか。
        /// 事前条件：特に無し。
        /// 事後条件：指定のエンティティが登録されていること。
        /// </summary>
        [Test]
        public void TestInsert()
        {
            var insertData = new Issuers
            {
                IssuerId = "3",
                SecretKey = "InsertSecretKey",
                Remark = "InsertRemark"
            };

            var insertCount = target.Insert(insertData);

            Assert.AreEqual(1, insertCount);

            var actual = target.FindById(insertData.IssuerId);

            Assert.That(actual, Is.Not.Null);
            Assert.AreEqual(insertData.IssuerId, actual.IssuerId);
            Assert.AreEqual(insertData.SecretKey, actual.SecretKey);
            Assert.AreEqual(insertData.Remark, actual.Remark);
        }

        /// <summary>
        /// <see cref="AbstractRepository{TEntity, TDbContext}.InsertBatch(System.Collections.Generic.IEnumerable{TEntity})"/>
        /// CASE：指定のエンティティリストを登録できるか。
        /// 事前条件：特に無し。
        /// 事後条件：指定のエンティティリストが登録されていること。
        /// </summary>
        [Test]
        public void TestInsertBatch()
        {
            var insertData = new List<Issuers>();
            insertData.Add(new Issuers
            {
                IssuerId = "3",
                SecretKey = "InsertBatchSecretKey01",
                Remark = "InsertBatchRemark01"
            });

            insertData.Add(new Issuers
            {
                IssuerId = "4",
                SecretKey = "InsertBatchSecretKey02",
                Remark = "InsertBatchRemark02"
            });

            var insertCount = target.InsertBatch(insertData);

            Assert.AreEqual(2, insertCount);

            var actual1 = target.FindById(insertData[0].IssuerId);

            Assert.That(actual1, Is.Not.Null);
            Assert.AreEqual(insertData[0].IssuerId, actual1.IssuerId);
            Assert.AreEqual(insertData[0].SecretKey, actual1.SecretKey);
            Assert.AreEqual(insertData[0].Remark, actual1.Remark);

            var actual2 = target.FindById(insertData[1].IssuerId);

            Assert.That(actual2, Is.Not.Null);
            Assert.AreEqual(insertData[1].IssuerId, actual2.IssuerId);
            Assert.AreEqual(insertData[1].SecretKey, actual2.SecretKey);
            Assert.AreEqual(insertData[1].Remark, actual2.Remark);
        }

        /// <summary>
        /// <see cref="AbstractRepository{TEntity, TDbContext}.Update(TEntity)"/>
        /// CASE：指定のエンティティを更新できるか。
        /// 事前条件：テーブルに初期データが投入されていること。
        /// 事後条件：指定のエンティティが更新されていること。
        /// </summary>
        [Test]
        public void TestUpdate()
        {
            var actual1 = target.FindById("1");

            Assert.That(actual1, Is.Not.Null);
            Assert.AreEqual("1", actual1.IssuerId);
            Assert.AreEqual("8F+Gy+L{O6x{SfugME*%A/!Di&l*tY$QaMH0gyzYoL(%4JvfbP&t5*Hpl4l-.|aX", actual1.SecretKey);
            Assert.AreEqual("品管利用のローカルツールユーザー01", actual1.Remark);

            actual1.SecretKey = "UpdateSecretKey";
            actual1.Remark = "UpdateRemark";

            var updateCount = target.Update(actual1);

            Assert.AreEqual(1, updateCount);

            var actual2 = target.FindById(actual1.IssuerId);

            Assert.That(actual2, Is.Not.Null);
            Assert.AreEqual("UpdateSecretKey", actual2.SecretKey);
            Assert.AreEqual("UpdateRemark", actual2.Remark);
        }

        /// <summary>
        /// <see cref="AbstractRepository{TEntity, TDbContext}.UpdateBatch(System.Collections.Generic.IEnumerable{TEntity})"/>
        /// CASE：指定のエンティティリストを更新できるか。
        /// 事前条件：テーブルに初期データが投入されていること。
        /// 事後条件：指定のエンティティリストが更新されていること
        /// </summary>
        [Test]
        public void TestUpdateBatch()
        {
            var actual1 = target.FindAll();
            var actualList = actual1.ToList<Issuers>();

            Assert.That(actual1, Is.Not.Null);
            Assert.AreEqual(2, actualList.Count);
            Assert.AreEqual("1", actualList[0].IssuerId);
            Assert.AreEqual("8F+Gy+L{O6x{SfugME*%A/!Di&l*tY$QaMH0gyzYoL(%4JvfbP&t5*Hpl4l-.|aX", actualList[0].SecretKey);
            Assert.AreEqual("品管利用のローカルツールユーザー01", actualList[0].Remark);

            Assert.AreEqual("2", actualList[1].IssuerId);
            Assert.AreEqual("TestSecretKey", actualList[1].SecretKey);
            Assert.AreEqual("UnitTest02", actualList[1].Remark);

            actualList[0].SecretKey = "UpdateBatchSecretKey01";
            actualList[0].Remark = "UpdatebatchBatchRemark01";

            actualList[1].SecretKey = "UpdateBatchSecretKey02";
            actualList[1].Remark = "UpdatebatchBatchRemark02";

            var updateCount = target.UpdateBatch(actualList);

            Assert.AreEqual(2, updateCount);

            var actual2 = target.FindById(actualList[0].IssuerId);

            Assert.That(actual2, Is.Not.Null);
            Assert.AreEqual("UpdateBatchSecretKey01", actual2.SecretKey);
            Assert.AreEqual("UpdatebatchBatchRemark01", actual2.Remark);

            var actual3 = target.FindById(actualList[1].IssuerId);

            Assert.That(actual3, Is.Not.Null);
            Assert.AreEqual("UpdateBatchSecretKey02", actual3.SecretKey);
            Assert.AreEqual("UpdatebatchBatchRemark02", actual3.Remark);
        }

        /// <summary>
        /// <see cref="AbstractRepository{TEntity, TDbContext}.Delete(TEntity)"/>
        /// CASE：指定のエンティティを削除できるか。
        /// 事前条件：テーブルに初期データが投入されていること。
        /// 事後条件：指定のエンティティが削除されていること
        /// </summary>
        [Test]
        public void TestDelete()
        {
            var actual1 = target.FindById("1");

            Assert.That(actual1, Is.Not.Null);

            var deleteCount = target.Delete(actual1);

            Assert.AreEqual(1, deleteCount);

            var actual2 = target.FindById(actual1.IssuerId);

            Assert.That(actual2, Is.Null);
        }

        /// <summary>
        /// <see cref="AbstractRepository{TEntity, TDbContext}.DeleteBatch(System.Collections.Generic.IEnumerable{TEntity})"/>
        /// CASE：指定のエンティティリストを削除できるか。
        /// 事前条件：テーブルに初期データが投入されていること。
        /// 事後条件：指定のエンティティリストが削除されていること
        /// </summary>
        [Test]
        public void TestDeleteBatch()
        {
            var actual = target.FindAll();
            var actualList = actual.ToList<Issuers>();

            Assert.That(actual, Is.Not.Null);
            Assert.AreEqual(2, actualList.Count);

            var deleteCount = target.DeleteBatch(actualList);

            Assert.AreEqual(2, deleteCount);

            var actual2 = target.FindAll();

            Assert.That(actual2, Is.Null.Or.Empty);
        }

        /// <summary>
        /// <see cref="AbstractDbUnitTestBase{TRepository}.CreateRepositoryInstance"/>
        /// </summary>
        /// <returns><see cref="IssuersRepository"/>のインスタンス</returns>
        protected override IssuersRepository CreateRepositoryInstance()
        {
            return new IssuersRepository(testDbContext);
        }
    }
}