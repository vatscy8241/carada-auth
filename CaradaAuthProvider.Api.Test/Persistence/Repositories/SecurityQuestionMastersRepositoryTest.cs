﻿using CaradaAuthProvider.Api.Persistence.Repositories;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using NUnit.Framework;
using System.Collections.Generic;

namespace CaradaAuthProvider.Api.Test.Persistence.Repositories
{
    /// <summary>
    /// <see cref="SecurityQuestionMastersRepository"/>のテスト。
    /// </summary>
    [TestFixture]
    public class SecurityQuestionMastersRepositoryTest : AbstractDbUnitTestBase<SecurityQuestionMastersRepository>
    {
        #region SetUp/TearDown

        /// <summary>
        /// <see cref="AbstractDbUnitTestBase.OneTimeSetUp"/>
        /// </summary>
        [OneTimeSetUp]
        public override void OneTimeSetUp()
        {
            base.OneTimeSetUp();
        }

        /// <summary>
        /// <see cref="AbstractDbUnitTestBase.OneTimeTearDown"/>
        /// </summary>
        [OneTimeTearDown]
        public override void OneTimeTearDown()
        {
            base.OneTimeTearDown();
        }

        /// <summary>
        /// <see cref="AbstractDbUnitTestBase.SetUp"/>
        /// </summary>
        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
        }

        /// <summary>
        /// <see cref="AbstractDbUnitTestBase.TearDown"/>
        /// </summary>
        [TearDown]
        public override void TearDown()
        {
            base.TearDown();
        }

        #endregion



        // -----------------------------------------------------------------------------------------
        // FindAllOrderByDisplayOrder
        // -----------------------------------------------------------------------------------------

        /// <summary>
        /// <see cref="AbstractRepository{TEntity, TDbContext}.FindAllOrderByDisplayOrder()"/>
        /// [対象] FindAllOrderByDisplayOrder
        /// [条件] 秘密の質問リストが2レコード表示順と異なる順にDBに登録されていること
        /// [結果] DisplayOrderでソートされた秘密の質問リストが返ってくること
        /// 事後条件：エンティティが取得できること。
        /// </summary>
        [Test]
        public void Test_FindAllOrderByDisplayOrder()
        {
            IEnumerable<SecurityQuestionMasters> actual = target.FindAllOrderByDisplayOrder();

            Assert.That(actual, Is.Not.Null);

            SecurityQuestionMasters a1 = null;
            SecurityQuestionMasters a2 = null;
            var count = 0;
            foreach (SecurityQuestionMasters e in actual)
            {
                if (count == 0)
                {
                    a1 = e;
                }
                else if (count == 1)
                {
                    a2 = e;
                }
                count++;
            }

            Assert.AreEqual("好きな食べ物は？", a1.Question);
            Assert.AreEqual(2, a1.SecurityQuestionId);
            Assert.AreEqual(1, a1.DisplayOrder);

            Assert.AreEqual("あなたの最初のペットの名前は？", a2.Question);
            Assert.AreEqual(1, a2.SecurityQuestionId);
            Assert.AreEqual(2, a2.DisplayOrder);

        }

        /// <summary>
        /// <see cref="AbstractRepository{TEntity, TDbContext}.FindAllOrderByDisplayOrder()"/>
        /// [対象] FindAllOrderByDisplayOrder
        /// [条件] 秘密の質問レコードが0件の場合
        /// [結果] 0件のリストが返却されること
        /// 事後条件：エンティティが取得できること。
        /// </summary>
        [Test]
        public void Test_FindAllOrderByDisplayOrder_Empty()
        {

            DeleteTestData();

            IEnumerable<SecurityQuestionMasters> actual = target.FindAllOrderByDisplayOrder();

            Assert.That(actual, Is.Not.Null);

            var count = 0;
            foreach (SecurityQuestionMasters e in actual)
            {
                count++;
            }

            Assert.AreEqual(0, count);

        }

        /// <summary>
        /// <see cref="AbstractDbUnitTestBase{TRepository}.CreateRepositoryInstance"/>
        /// </summary>
        /// <returns><see cref="SecurityQuestionMastersRepository"/>のインスタンス</returns>
        protected override SecurityQuestionMastersRepository CreateRepositoryInstance()
        {
            return new SecurityQuestionMastersRepository(testDbContext);
        }

    }
}