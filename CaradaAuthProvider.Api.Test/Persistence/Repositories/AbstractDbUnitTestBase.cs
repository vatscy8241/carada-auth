﻿using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using NDbUnit.Core;
using NDbUnit.Core.SqlClient;
using NUnit.Framework;
using System;
using System.Collections.Specialized;
using System.Data;
using System.IO;

namespace CaradaAuthProvider.Api.Test.Persistence.Repositories
{
    /// <summary>
    /// データベースを使用する単体テストのベースクラス。
    /// </summary>
    public abstract class AbstractDbUnitTestBase<TRepository>
    {
        /// <summary>
        /// テスト対象。
        /// </summary>
        protected TRepository target;

        /// <summary>
        /// テスト用のデータベース。
        /// </summary>
        private INDbUnitTest testDatabase;

        /// <summary>
        /// データベースコンテキスト。
        /// </summary>
        protected CaradaAuthProviderDbContext testDbContext;

        /// <summary>
        /// テストデータが保存されているパスのルート。
        /// </summary>
        private const string TEST_DATA_STORE_PATH_ROOT = @"Persistence\Resources\Database\";

        /// <summary>
        /// データベースのファイル名。
        /// </summary>
        private const string DATABASE_FILENAME = @"TestDatabase.mdf";

        /// <summary>
        /// プロジェクトディレクトリ。
        /// </summary>
        protected static string PROJECT_PATH_ROOT
        {
            get
            {
                return new DirectoryInfo(TestContext.CurrentContext.TestDirectory).Parent.Parent.FullName;
            }
        }

        /// <summary>
        /// データベース接続文字列。
        /// </summary>
        protected static string TEST_DATABASE_CONNECTION_STRING
        {
            get
            {
                return string.Format(@"Data Source=.\SQLEXPRESS;AttachDbFilename={0};Integrated Security=true;User Instance=true",
                    Path.Combine(TestContext.CurrentContext.TestDirectory, DATABASE_FILENAME));
            }
        }

        /// <summary>
        /// テスト対象のリポジトリのインスタンスを生成する。
        /// </summary>
        /// <returns>テスト対象のリポジトリのインスタンス</returns>
        protected abstract TRepository CreateRepositoryInstance();

        #region SetUp/TearDown

        /// <summary>
        /// テストクラス毎の初期処理。
        /// </summary>
        public virtual void OneTimeSetUp()
        {
            var dblog = Path.Combine(TestContext.CurrentContext.TestDirectory, @"TestDatabase_log.ldf");
            if (File.Exists(dblog))
            {
                try
                {
                    File.Delete(dblog);
                }
                catch (IOException e)
                {
                    Console.WriteLine("削除失敗:" + e.Message);
                }
            }

            testDatabase = new SqlDbUnitTest(TEST_DATABASE_CONNECTION_STRING);
            testDatabase.ReadXmlSchema(MakeSchemaDefinitionFilePath());

            ReadOrCreateExpectedDataFile();
            ReadOrCreateTestDataFile();
        }

        /// <summary>
        /// テストクラス毎の終了処理。
        /// </summary>
        public virtual void OneTimeTearDown()
        {
            DeleteTestData();
            DisposeDbContext();
        }

        /// <summary>
        /// テスト毎の初期処理。
        /// </summary>
        public virtual void SetUp()
        {
            DisposeDbContext();
            testDbContext = new DbContextFactory().NewCaradaAuthProviderDbContext();
            testDbContext.Database.Connection.ConnectionString = TEST_DATABASE_CONNECTION_STRING;

            target = CreateRepositoryInstance();
            ReadOrCreateTestDataFile();
        }

        /// <summary>
        /// テスト毎の終了処理。
        /// </summary>
        public virtual void TearDown()
        {
            DeleteTestData();

            DisposeDbContext();
        }

        /// <summary>
        /// <see cref="AbstractDbUnitTestBase{TRepository}.testDbContext"/>を破棄する。
        /// </summary>
        private void DisposeDbContext()
        {
            if (testDbContext == null)
            {
                return;
            }
            testDbContext.Dispose();
            testDbContext = null;
        }

        #endregion

        #region データ読み込み

        /// <summary>
        /// テストデータファイルを読み込む。
        /// テストデータが存在しない場合、テストデータファイルに現状のデータベースの内容を反映する。
        /// </summary>
        protected void ReadOrCreateTestDataFile()
        {
            var testDataXmlFilePath = MakeTestDataFilePath();
            if (File.Exists(testDataXmlFilePath))
            {
                testDatabase.ReadXml(testDataXmlFilePath);
                testDatabase.PerformDbOperation(DbOperationFlag.CleanInsertIdentity);
            }
            else
            {
                DataSet ds = testDatabase.GetDataSetFromDb();
                ds.WriteXml(testDataXmlFilePath);
            }
        }

        /// <summary>
        /// データベースのテストデータを削除する。
        /// </summary>
        protected void DeleteTestData()
        {
            testDatabase.PerformDbOperation(DbOperationFlag.DeleteAll);
        }

        /// <summary>
        /// 期待値データファイルを読み込む。
        /// 期待値データが存在しない場合は、期待値データファイルに現状のデータベースの内容を反映する。
        /// </summary>
        private void ReadOrCreateExpectedDataFile()
        {
            var expectedDataXmlFilePath = MakeExpectedDataFilePath();
            if (!File.Exists(expectedDataXmlFilePath))
            {
                DataSet ds = testDatabase.GetDataSetFromDb();
                ds.WriteXml(expectedDataXmlFilePath);
            }
        }

        /// <summary>
        /// スキーマ定義のファイルパスを作成する。
        /// </summary>
        /// <returns>スキーマ定義のファイルパス</returns>
        private string MakeSchemaDefinitionFilePath()
        {
            return Path.Combine(PROJECT_PATH_ROOT, TEST_DATA_STORE_PATH_ROOT, "SchemaDefinition", string.Format(@"{0}.xsd", GetType().Name.Replace("Test", "")));
        }

        /// <summary>
        /// テストデータのファイルパスを作成する。
        /// </summary>
        /// <returns>テストデータのファイルパス</returns>
        private string MakeTestDataFilePath()
        {
            return Path.Combine(PROJECT_PATH_ROOT, TEST_DATA_STORE_PATH_ROOT, "TestData", string.Format(@"{0}.xml", GetType().Name.Replace("Test", "")));
        }

        /// <summary>
        /// 期待値データのファイルパスを作成する。
        /// </summary>
        /// <returns>期待値データのファイルパス</returns>
        private string MakeExpectedDataFilePath()
        {
            return Path.Combine(PROJECT_PATH_ROOT, TEST_DATA_STORE_PATH_ROOT, "ExpectedData", string.Format(@"{0}.xml", GetType().Name.Replace("Test", "")));
        }

        #endregion

        #region データ検証

        /// <summary>
        /// 検証用のデータセットを取得する。
        /// </summary>
        /// <returns>検証用のデータセット</returns>
        protected DataSet ExpectedDataSet()
        {
            DataSet expectedDataset = new DataSet();
            expectedDataset.ReadXmlSchema(MakeSchemaDefinitionFilePath());
            expectedDataset.ReadXml(MakeExpectedDataFilePath());

            return expectedDataset;
        }

        /// <summary>
        /// 指定のテーブル群のデータセットを取得する。
        /// </summary>
        /// <param name="tableNames">テーブル名</param>
        /// <returns>データセット</returns>
        protected DataSet TableDataSet(string[] tableNames)
        {
            StringCollection tables = new StringCollection();
            tables.AddRange(tableNames);
            return testDatabase.GetDataSetFromDb(tables);
        }

        #endregion
    }
}
