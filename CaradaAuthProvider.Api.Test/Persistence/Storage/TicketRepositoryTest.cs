﻿using CaradaAuthProvider.Api.Persistence.Storage;
using CaradaAuthProvider.Api.Persistence.Storage.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using StackExchange.Redis;
using System;

namespace CaradaAuthProvider.Api.Test.Persistence.Storage
{
    [TestClass]
    public class TicketRepositoryTest
    {
        private TicketRepository target;

        private Mock<IDatabase> mockDatabase;
        private class MockTicketRepository : TicketRepository
        {
            private Mock<IDatabase> mockDatabase;
            public MockTicketRepository(Mock<IDatabase> mockDatabase)
            {
                this.mockDatabase = mockDatabase;
            }
            protected override IDatabase CreateCache()
            {
                return mockDatabase.Object;
            }
        }

        [TestInitialize]
        public void Initialize()
        {
            mockDatabase = new Mock<IDatabase>();
            target = new MockTicketRepository(mockDatabase);
        }

        [TestMethod]
        public void Find_Found()
        {
            mockDatabase.Setup(p => p.StringGet(It.IsAny<RedisKey>(), It.IsAny<CommandFlags>())).Returns("{UID:'uid', Kind:'UseStart', ExpiresDateTime:'2016-01-01 12:00:00'}");

            Ticket actual = target.Find("ticketId");
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void Find_NotFound()
        {
            mockDatabase.Setup(p => p.StringGet(It.IsAny<RedisKey>(), It.IsAny<CommandFlags>())).Returns(RedisValue.Null);

            Ticket actual = target.Find("ticketId");
            Assert.IsNull(actual);
        }

        [TestMethod]
        public void Register_Success()
        {
            mockDatabase.Setup(p => p.StringSet(It.IsAny<RedisKey>(), It.IsAny<RedisValue>(), It.IsAny<TimeSpan>(), When.Always, CommandFlags.None)).Returns(true);

            Ticket entity = new Ticket();
            try
            {
                target.Register("ticketId", entity);
            }
            catch
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void Register_Failure()
        {
            mockDatabase.Setup(p => p.StringSet(It.IsAny<RedisKey>(), It.IsAny<RedisValue>(), It.IsAny<TimeSpan>(), When.Always, CommandFlags.None)).Returns(false);

            Ticket entity = new Ticket();
            try
            {
                target.Register("ticketId", entity);
                Assert.Fail();
            }
            catch (Exception e)
            {
                Assert.AreEqual("Redis store failed.", e.Message);
            }
        }

        [TestMethod]
        public void Register_NullEntity()
        {
            mockDatabase.Setup(p => p.StringSet(It.IsAny<RedisKey>(), It.IsAny<RedisValue>(), It.IsAny<TimeSpan>(), When.Always, CommandFlags.None)).Throws(new Exception("Assert Fail"));

            try
            {
                target.Register("ticketId", null);
                Assert.Fail();
            }
            catch (Exception e)
            {
                Assert.AreEqual("Redis store failed.", e.Message);
            }
        }

        [TestMethod]
        public void Remove_Success()
        {
            mockDatabase.Setup(p => p.StringGet(It.IsAny<RedisKey>(), It.IsAny<CommandFlags>())).Returns("{UID:'uid', Kind:'UseStart', ExpiresDateTime:'2016-01-01 12:00:00'}");
            mockDatabase.Setup(p => p.KeyDelete(It.IsAny<RedisKey>(), CommandFlags.None)).Returns(true);

            try
            {
                target.Remove("ticketId");
            }
            catch
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void Remove_EntityNotFound()
        {
            mockDatabase.Setup(p => p.StringGet(It.IsAny<RedisKey>(), It.IsAny<CommandFlags>()));
            mockDatabase.Setup(p => p.KeyDelete(It.IsAny<RedisKey>(), CommandFlags.None)).Throws(new Exception("Assert Fail"));

            try
            {
                target.Remove("ticketId");
            }
            catch
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void Remove_Failure()
        {
            mockDatabase.Setup(p => p.StringGet(It.IsAny<RedisKey>(), It.IsAny<CommandFlags>())).Returns("{UID:'uid', Kind:'UseStart', ExpiresDateTime:'2016-01-01 12:00:00'}");
            mockDatabase.Setup(p => p.KeyDelete(It.IsAny<RedisKey>(), CommandFlags.None)).Returns(false);

            try
            {
                target.Remove("ticketId");
                Assert.Fail();
            }
            catch (Exception e)
            {
                Assert.AreEqual("Redis remove failed.", e.Message);
            }
        }
    }
}
