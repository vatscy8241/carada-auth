﻿using CaradaAuthProvider.Api.IdentityManagers;
using CaradaAuthProvider.Api.Persistence.Repositories;
using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using CaradaAuthProvider.Api.Proxy;
using CaradaAuthProvider.Api.Services;
using Microsoft.AspNet.Identity;
using Moq;
using NUnit.Framework;
using System;

namespace CaradaAuthProvider.Api.Test.Services
{
    /// <summary>
    /// サービス層のプロキシテスト
    /// </summary>
    [TestFixture]
    public class ServiceProxyTest
    {
        // DBコンテキスト
        private static CaradaAuthProviderDbContext dbContext = null;


        // ユーザー管理
        private static Mock<ApplicationUserManager> mUserManager = null;


        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ServiceProxyTest()
        {
            dbContext = new DbContextFactory().NewCaradaAuthProviderDbContext();
            dbContext.Database.ExecuteSqlCommand("DELETE FROM [dbo].[Issuers]");
        }


        /// <summary>
        /// イニシャライズ
        /// </summary>
        [SetUp]
        public void Initialize()
        {
            Mock<IUserStore<CaradaIdUser>> mockStore = new Mock<IUserStore<CaradaIdUser>>();
            mUserManager = new Mock<ApplicationUserManager>(mockStore.Object);

        }

        /// <summary>
        /// テスト用のサービス
        /// </summary>
        private class TestService : AbstractService
        {


            // チケット発行レポジトリ
            private IssuersRepository issuersRepository;

            // コンストラクタ
            public TestService(CaradaAuthProviderDbContext dbContext, ApplicationUserManager userManager) : base(dbContext, userManager)
            {
                issuersRepository = new IssuersRepository(dbContext);
            }

            /// <summary>
            /// チケット発行登録処理
            /// </summary>
            /// <param name="success">処理が成功するかどうかのフラグ falseの場合はチケット発行登録処理後例外を投げる</param>
            /// <param name="id">チケットID</param>
            public void Entry(bool success, string id)
            {

                Issuers e = new Issuers()
                {
                    IssuerId = id,
                    SecretKey = "SecretKey",
                    Remark = success ? "成功" : "失敗"
                };

                issuersRepository.Insert(e);

                if (!success)
                {
                    throw new Exception();
                }
            }
        }


        /// <summary>
        /// コントローラーからサービスの登録メソッドを呼び出しサービス内で例外が発生して登録したデータがロールバックされるケース
        /// </summary>
        [Test]
        public void test_トランザクション境界外の外でメソッドを1回呼び出し_1回目ロールバック()
        {


            ServiceProxy sp = new ServiceProxy(typeof(TestService), dbContext, mUserManager.Object);
            TestService testService = (TestService)sp.GetTransparentProxy();


            try
            {
                testService.Entry(false, "11");
                Assert.Fail();
            }
            catch (Exception exception)
            {
                Assert.IsNotNull(exception);
            }
            // テストが終わるまではロールバックされないので、テスト実行後下記の状態かどうかを目視で確認
            // 11はロールバックされ挿入されない
        }


        /// <summary>
        /// コントローラーからサービスの登録メソッドを呼び出しサービス内で正常に登録されてコミットされるケース
        /// </summary>
        [Test]
        public void test_トランザクション境界外の外でメソッドを1回呼び出し_1回目コミット()
        {


            ServiceProxy sp = new ServiceProxy(typeof(TestService), dbContext, mUserManager.Object);
            TestService testService = (TestService)sp.GetTransparentProxy();


            try
            {
                testService.Entry(true, "21");
            }
            catch (Exception exception)
            {
                Assert.Fail();
            }
            // テストが終わるまではロールバックされないので、テスト実行後下記の状態かどうかを目視で確認
            // 21が挿入される

        }


        /// <summary>
        /// コントローラーからサービスの登録メソッドを2回呼び出すケース
        /// 1回目は登録後例外をスローし、2回目は正常に登録される
        /// </summary>
        [Test]
        public void test_トランザクション境界外の外でメソッドを２回呼び出し_1回目ロールバック_２回目コミット()
        {


            ServiceProxy sp = new ServiceProxy(typeof(TestService), dbContext, mUserManager.Object);
            TestService testService = (TestService)sp.GetTransparentProxy();


            try
            {
                testService.Entry(false, "31");
                Assert.Fail();

            }
            catch (Exception exception)
            {
                Assert.IsNotNull(exception);
            }

            testService.Entry(true, "32");
            // テストが終わるまではロールバックされないので、テスト実行後下記の状態かどうかを目視で確認
            // 31はロールバックされ挿入されてないが、32は挿入される

        }

        /// <summary>
        /// コントローラーからサービスの登録メソッドを2回呼び出すケース
        /// 1回目、2回目ともに正常に登録される
        /// </summary>
        [Test]
        public void test_トランザクション境界外の外でメソッドを２回呼び出し_1回目コミット_２回目コミット()
        {


            ServiceProxy sp = new ServiceProxy(typeof(TestService), dbContext, mUserManager.Object);
            TestService testService = (TestService)sp.GetTransparentProxy();


            testService.Entry(true, "41");
            testService.Entry(true, "42");
            // テストが終わるまではロールバックされないので、テスト実行後下記の状態かどうかを目視で確認
            // 41と42がともに挿入される

        }


    }
}