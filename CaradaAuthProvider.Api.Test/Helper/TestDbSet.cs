﻿using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Linq;

namespace CaradaAuthProvider.Api.Test.Helper
{
    /// <summary>
    /// ユーザー情報のテスト用DBSet
    /// </summary>
    public class TestAspNetUsersDbSet : TestDbSet<CaradaIdUser>
    {
        public override CaradaIdUser Find(params object[] keyValues)
        {
            var id = keyValues.Single() as string;
            return this.SingleOrDefault(b => b.Id == id);
        }
    }

    public class TestAspNetRolesDbSet : TestDbSet<IdentityRole>
    {
        public override IdentityRole Find(params object[] keyValues)
        {
            var id = keyValues.Single() as string;
            return this.SingleOrDefault(a => a.Id == id);
        }
    }

    public class TestAspNetUserClaimsDbSet : TestDbSet<IdentityUserClaim>
    {
        public override IdentityUserClaim Find(params object[] keyValues)
        {
            var id = keyValues.Single();
            return this.SingleOrDefault(a => a.Id == (int)id);
        }
    }

    public class TestAspNetUserLoginsDbSet : TestDbSet<IdentityUserLogin>
    {
        public override IdentityUserLogin Find(params object[] keyValues)
        {
            var loginProvider = keyValues[0] as string;
            var providerKey = keyValues[1] as string;
            var userId = keyValues[2] as string;
            return this.SingleOrDefault(a => a.LoginProvider == loginProvider && a.ProviderKey == providerKey && a.UserId == userId);
        }
    }

    public class TestAspNetUserRolesDbSet : TestDbSet<IdentityUserRole>
    {
        public override IdentityUserRole Find(params object[] keyValues)
        {
            var userId = keyValues[0] as string;
            var roleId = keyValues[1] as string;
            return this.SingleOrDefault(a => a.UserId == userId && a.RoleId == roleId);
        }
    }

    public class TestSecurityQuestionMasters : TestDbSet<SecurityQuestionMasters>
    {

        public override SecurityQuestionMasters Find(params object[] keyValues)
        {
            var securityQuestionId = keyValues.Single();
            return this.SingleOrDefault(a => a.SecurityQuestionId == (int)securityQuestionId);
        }
    }

    public class TestSecurityQuestionAnswers : TestDbSet<SecurityQuestionAnswers>
    {

        public override SecurityQuestionAnswers Find(params object[] keyValues)
        {
            var userId = keyValues.Single();
            return this.SingleOrDefault(a => a.UserId == (string)userId);
        }
    }

    public class TestIssuers : TestDbSet<Issuers>
    {

        public override Issuers Find(params object[] keyValues)
        {
            return base.Find(keyValues);
        }
    }
}
