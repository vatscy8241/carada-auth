﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace MTI.CaradaIdProvider.Web.Tests.Helper
{
    /// <summary>
    /// Type#GetRuntimeMethods()で対象Typeにソース記載された匿名メソッドが取得できないときに使用します。
    /// Visual Studio 2015用にcsprojをコンバートしたところ、アセンブリにおける匿名メソッドの取り扱いが変わったため作成。
    /// </summary>
    public static class LambdaDelegateTypeTestExtensions
    {
        /// <summary>
        /// 対象ソース上に出現する匿名メソッドを含めて、指定した型で定義されるすべてのメソッドを表すコレクションを取得します。
        /// </summary>
        /// <param name="type">メソッドを含む型。</param>
        /// <returns>指定した種類のメソッドのコレクション。</returns>
        public static IEnumerable<MethodInfo> GetRuntimeMethodsWithAnonymous(this Type type)
        {
            var ret = type.GetRuntimeMethods().ToList();

            if (ret.Any(a => a.DeclaringType.Name.StartsWith("<")))
            {
                return ret;
            }

            var assembly = Assembly.GetAssembly(type);

            var types = (from t in assembly.GetTypes()
                         where t.FullName.Contains(type.FullName) && t.Name.StartsWith("<")
                         select t);

            foreach (var tt in types)
            {
                ret.AddRange(tt.GetRuntimeMethods());
            }

            return ret;
        }

        /// <summary>
        /// 対象ソース上に出現する匿名メソッドまたはコンストラクターを呼び出します。
        /// </summary>
        /// <param name="memberInfo">メソッドまたはコンストラクター。</param>
        /// <param name="obj">呼び出し対象がソースに存在するオブジェクト。</param>
        /// <param name="parameters">呼び出すメソッドまたはコンストラクターの引数リスト。</param>
        /// <returns>呼び出されたメソッドの戻り値を格納しているオブジェクト。コンストラクターの場合は null。</returns>
        public static object InvokeWithAnonymous(this MethodBase method, object obj, object[] parameters)
        {
            var declaringType = method.DeclaringType;

            if (declaringType == obj.GetType())
            {
                return method.Invoke(obj, parameters);
            }

            return method.Invoke(declaringType.GetConstructors()[0].Invoke(new object[] { }), parameters);
        }
    }
}
