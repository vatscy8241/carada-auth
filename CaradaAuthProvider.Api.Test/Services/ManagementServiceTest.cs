﻿using CaradaAuthProvider.Api.Controllers.Conditions;
using CaradaAuthProvider.Api.Domain.Logics;
using CaradaAuthProvider.Api.Exceptions;
using CaradaAuthProvider.Api.Exceptions.Enums;
using CaradaAuthProvider.Api.IdentityManagers;
using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using CaradaAuthProvider.Api.Services;
using CaradaAuthProvider.Api.Services.Dtos;
using CaradaAuthProvider.Api.Utils;
using Microsoft.AspNet.Identity;
using Moq;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace CaradaAuthProvider.Api.Test.Services
{
    [TestFixture]
    public class ManagementServiceTest
    {
        private ManagementService target;

        private Mock<CaradaAuthProviderDbContext> mockDbContext;

        private Mock<ApplicationUserManager> mockUserManager;

        private Mock<SecurityQuestionLogic> mockSecurityQuestionLogic;

        /// <summary>
        /// <see cref="ManagementService"/>のテスト用モッククラス。
        /// </summary>
        private class MockManagementService : ManagementService
        {
            /// <summary>
            /// コンストラクタ。
            /// </summary>
            /// <param name="dbContext">DBコンテキスト</param>
            /// <param name="userManager">アプリケーションユーザーマネージャー</param>
            /// <param name="mockMailLogic">メールロジックのモック</param>
            public MockManagementService(CaradaAuthProviderDbContext dbContext, ApplicationUserManager userManager, Mock<SecurityQuestionLogic> mockSecurityQuestionLogic)
                : base(dbContext, userManager)
            {
                securityQuestionLogic = mockSecurityQuestionLogic.Object;
            }
        }

        [SetUp]
        public void Initialize()
        {
            Mock<IUserStore<CaradaIdUser>> mockStore = new Mock<IUserStore<CaradaIdUser>>();
            mockDbContext = new Mock<CaradaAuthProviderDbContext>();
            mockUserManager = new Mock<ApplicationUserManager>(mockStore.Object);
            mockSecurityQuestionLogic = new Mock<SecurityQuestionLogic>();
            target = new MockManagementService(mockDbContext.Object, mockUserManager.Object, mockSecurityQuestionLogic);
        }

        [Test]
        public void AddUser_Success()
        {
            RegistUserAddCondition condition = new RegistUserAddCondition
            {
                CaradaId = "caradaId"
            };

            CaradaIdUser user = new CaradaIdUser
            {
                UserName = condition.CaradaId,
                EmailConfirmed = false,
                TwoFactorEnabled = true,
                CreateDateUtc = DateUtilities.UTCNow,
                LockoutEnabled = true,
                EmployeeFlag = true,
                AccessFailedCount = 0
            };

            string password = "";

            mockUserManager.Setup(p => p.FindByNameAsync("caradaId")).ReturnsAsync(null);
            mockUserManager.Setup(p => p.CreateAsync(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).ReturnsAsync(null).Callback<CaradaIdUser, string>((x, y) =>
            {
                Assert.AreEqual(condition.CaradaId, x.UserName);
                Assert.IsFalse(x.EmailConfirmed);
                Assert.IsTrue(x.TwoFactorEnabled);
                Assert.IsNotNull(x.CreateDateUtc);
                Assert.IsTrue(x.LockoutEnabled);
                Assert.IsTrue(x.EmployeeFlag);
                Assert.AreEqual(0, x.AccessFailedCount);
                password = y;
                condition.CaradaId = "caradaId2";
            });
            mockUserManager.Setup(p => p.AddToRoleAsync(user.Id, It.IsAny<string>())).ReturnsAsync(null);
            mockUserManager.Setup(p => p.FindByNameAsync("caradaId2")).ReturnsAsync(user);

            Task<AddUserResultDto> actual = target.AddUser(condition);
            Assert.IsNotNull(actual);
            Assert.IsTrue(actual.IsCompleted);

            AddUserResultDto actualDto = actual.Result;
            Assert.IsNotNull(actualDto);
            Assert.IsNotNull(actualDto.User);
            Assert.AreEqual(condition.CaradaId, actualDto.User.carada_id);
            Assert.AreEqual(password, actualDto.User.password);
            Assert.AreEqual(user.Id, actualDto.User.uid);
        }

        [Test]
        public void AddUser_UserError()
        {
            RegistUserAddCondition condition = new RegistUserAddCondition
            {
                CaradaId = "caradaId"
            };

            mockUserManager.Setup(p => p.FindByNameAsync("caradaId")).ReturnsAsync(new CaradaIdUser());

            var actual1 = Assert.ThrowsAsync<CaradaApplicationException>(() => target.AddUser(condition));
            Assert.AreEqual(ErrorKind.registered_carada_id.GetName(), actual1.GetErrorCode());
            Assert.AreEqual("登録済みのCARADA IDです。", actual1.GetErrorMessage());
        }

        [Test]
        public void ResetUserInfo_SuccessInitializationOn()
        {
            ResetUserCondition condition = new ResetUserCondition
            {
                CaradaId = "caradaId",
                Initialization = "1"
            };

            CaradaIdUser user = new CaradaIdUser
            {
                UserName = condition.CaradaId,
                Email = "test@test.com",
                EmailConfirmed = true,
                TwoFactorEnabled = true,
                CreateDateUtc = DateUtilities.UTCNow,
                LockoutEnabled = true,
                EmployeeFlag = true,
                AccessFailedCount = 0
            };

            string token = "token";
            string password = "";

            mockUserManager.Setup(p => p.FindByNameAsync("caradaId")).ReturnsAsync(user);

            mockUserManager.Setup(p => p.UpdateAsync(It.IsAny<CaradaIdUser>())).ReturnsAsync(null).Callback<CaradaIdUser>((x) =>
            {
                Assert.AreEqual(user.Id, x.Id);
                Assert.AreEqual(0, x.AccessFailedCount);
                Assert.IsNull(x.LockoutEndDateUtc);
                Assert.IsNotNull(x.UpdateDateUtc);
                Assert.IsNull(x.Email);
                Assert.IsFalse(x.EmailConfirmed);
            });

            mockUserManager.Setup(p => p.GeneratePasswordResetTokenAsync(user.Id)).ReturnsAsync(token);
            mockUserManager.Setup(p => p.ResetPasswordAsync(user.Id, token, It.IsAny<string>())).ReturnsAsync(null).Callback<string, string, string>((x, y, z) =>
            {
                password = z;
            });

            mockSecurityQuestionLogic.Setup(p => p.RemoveSecurityQuestion(user.Id));

            Task<ResetUserResultDto> actual = target.ResetUserInfo(condition);
            Assert.IsNotNull(actual);
            Assert.IsTrue(actual.IsCompleted);

            ResetUserResultDto actualDto = actual.Result;
            Assert.IsNotNull(actualDto);
            Assert.IsNotNull(actualDto.User);
            Assert.AreEqual(condition.CaradaId, actualDto.User.carada_id);
            Assert.AreEqual(password, actualDto.User.password);
            Assert.AreEqual("1", actualDto.User.initialization);
        }

        [Test]
        public void ResetUserInfo_SuccessInitializationOff()
        {
            ResetUserCondition condition = new ResetUserCondition
            {
                CaradaId = "caradaId",
                Initialization = "0"
            };

            CaradaIdUser user = new CaradaIdUser
            {
                UserName = condition.CaradaId,
                Email = "test@test.com",
                EmailConfirmed = true,
                TwoFactorEnabled = true,
                CreateDateUtc = DateUtilities.UTCNow,
                LockoutEnabled = true,
                EmployeeFlag = true,
                AccessFailedCount = 0
            };

            string token = "token";
            string password = "";

            mockUserManager.Setup(p => p.FindByNameAsync("caradaId")).ReturnsAsync(user);

            mockUserManager.Setup(p => p.UpdateAsync(It.IsAny<CaradaIdUser>())).ReturnsAsync(null).Callback<CaradaIdUser>((x) =>
            {
                Assert.AreEqual(user.Id, x.Id);
                Assert.AreEqual(0, x.AccessFailedCount);
                Assert.IsNull(x.LockoutEndDateUtc);
                Assert.IsNotNull(x.UpdateDateUtc);
                Assert.AreEqual(user.Email, x.Email);
                Assert.IsTrue(x.EmailConfirmed);
            });

            mockUserManager.Setup(p => p.GeneratePasswordResetTokenAsync(user.Id)).ReturnsAsync(token);
            mockUserManager.Setup(p => p.ResetPasswordAsync(user.Id, token, It.IsAny<string>())).ReturnsAsync(null).Callback<string, string, string>((x, y, z) =>
            {
                password = z;
            });

            mockSecurityQuestionLogic.Setup(p => p.RemoveSecurityQuestion(user.Id)).Throws(new Exception());

            Task<ResetUserResultDto> actual = target.ResetUserInfo(condition);
            Assert.IsNotNull(actual);
            Assert.IsTrue(actual.IsCompleted);

            ResetUserResultDto actualDto = actual.Result;
            Assert.IsNotNull(actualDto);
            Assert.IsNotNull(actualDto.User);
            Assert.AreEqual(condition.CaradaId, actualDto.User.carada_id);
            Assert.AreEqual(password, actualDto.User.password);
            Assert.AreEqual("0", actualDto.User.initialization);
        }

        [Test]
        public void ResetUserInfo_UserError()
        {
            ResetUserCondition condition = new ResetUserCondition
            {
                CaradaId = "caradaId",
                Initialization = "1"
            };

            mockUserManager.Setup(p => p.FindByNameAsync("caradaId")).ReturnsAsync(null);

            var actual1 = Assert.ThrowsAsync<CaradaApplicationException>(() => target.ResetUserInfo(condition));
            Assert.AreEqual(ErrorKind.invalid_carada_id.GetName(), actual1.GetErrorCode());
            Assert.AreEqual("該当のユーザが見つかりませんでした。", actual1.GetErrorMessage());
        }
    }
}