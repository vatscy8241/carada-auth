﻿using CaradaAuthProvider.Api.Controllers.Conditions;
using CaradaAuthProvider.Api.Domain.Dtos;
using CaradaAuthProvider.Api.Domain.Logics;
using CaradaAuthProvider.Api.Exceptions;
using CaradaAuthProvider.Api.IdentityManagers;
using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using CaradaAuthProvider.Api.Persistence.Storage.Entities;
using CaradaAuthProvider.Api.Services;
using Microsoft.AspNet.Identity;
using Moq;
using NUnit.Framework;
using System.Threading.Tasks;

namespace CaradaAuthProvider.Api.Test.Services
{
    /// <summary>
    /// <see cref="MailService"/>のテスト。
    /// </summary>
    [TestFixture]
    public class MailServiceTest
    {
        /// <summary>
        /// テスト対象。
        /// </summary>
        private MailService target;

        /// <summary>
        /// メールロジックのモック。
        /// </summary>
        private Mock<MailLogic> mockMailLogic;

        /// <summary>
        /// チケットロジックのモック。
        /// </summary>
        private Mock<TicketLogic> mockTicketLogic;

        /// <summary>
        /// ユーザーマネージャーのモック。
        /// </summary>
        private Mock<ApplicationUserManager> mockUserManager;

        #region SetUp/TearDown

        /// <summary>
        /// テスト毎の初期化。
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            var mockUserStore = new Mock<IUserStore<CaradaIdUser>>();
            mockMailLogic = new Mock<MailLogic>();
            mockTicketLogic = new Mock<TicketLogic>();
            mockUserManager = new Mock<ApplicationUserManager>(mockUserStore.Object);
        }

        #endregion

        /// <summary>
        /// <see cref="MailService.SendMail(SendMailCondition)"/>のテスト。
        /// CASE：正常にメール送信できるか。
        /// 事前条件：UIDからメールアドレスを登録済みのユーザー情報を取得できること。
        /// 事後条件：期待通りのパラメーターで<see cref="MailLogic.SendMailAsync(MailSendInfoDto, string)"/>が呼び出されること。
        /// </summary>
        /// <returns>なし</returns>
        [Test]
        public async Task TestSendMail()
        {
            var condition = new SendMailCondition()
            {
                Uid = "uid",
                Subject = "subject",
                Body = "body"
            };

            var entity = new CaradaIdUser()
            {
                Email = "email"
            };

            mockUserManager.Setup(m => m.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(entity);

            mockMailLogic.Setup(m => m.SendMailAsync(It.IsAny<MailSendInfoDto>())).Callback<MailSendInfoDto>((x) =>
            {
                Assert.That(x, Is.Not.Null);
                Assert.AreEqual(condition.Subject, x.Subject);
                Assert.AreEqual(condition.Body, x.Text);
                Assert.AreEqual("carada-test@mti.co.jp", x.From);
                Assert.AreEqual("テストメール送信者", x.FromName);
                Assert.That(x.ToList, Is.Not.Null);
                Assert.AreEqual(1, x.ToList.Count);
                Assert.AreEqual(entity.Email, x.ToList[0]);
                Assert.AreEqual(null, x.CcList);
                Assert.AreEqual(null, x.BccList);
                Assert.AreEqual(condition.Body, x.Text);
            });

            target = new MockMailService(new DbContextFactory().NewCaradaAuthProviderDbContext(), mockUserManager.Object, mockMailLogic, mockTicketLogic);

            await target.SendMail(condition);

            mockUserManager.Verify(m => m.FindByIdAsync(condition.Uid), Times.Once);
            mockMailLogic.Verify(m => m.SendMailAsync(It.IsAny<MailSendInfoDto>()), Times.Once);
        }

        /// <summary>
        /// <see cref="MailService.SendMail(SendMailCondition)"/>のテスト。
        /// CASE：正常にメール送信できるか。
        /// 事前条件：UIDからメールアドレスを登録済みのユーザー情報を取得できること。
        /// 事後条件：期待通りのパラメーターで<see cref="MailLogic.SendMailAsync(MailSendInfoDto, string)"/>が呼び出されること。
        /// </summary>
        /// <returns>なし</returns>
        [Test]
        public async Task TestSendMail_WithTicketId()
        {
            var condition = new SendMailCondition()
            {
                TicketId = "ticketId",
                Subject = "subject",
                Body = "body"
            };

            var entity = new CaradaIdUser()
            {
                Email = "email"
            };

            Ticket ticket = new Ticket()
            {
                Uid = "uid",
                Kind = TicketKind.ResetPasseord,
                ExpiresDateTime = System.DateTime.Parse("9999-12-31 23:00:00"),
                ProcessedFlag = true
            };
            mockTicketLogic.Setup(m => m.FindTicket(condition.TicketId)).Returns(ticket);

            mockUserManager.Setup(m => m.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(entity);

            mockMailLogic.Setup(m => m.SendMailAsync(It.IsAny<MailSendInfoDto>())).Callback<MailSendInfoDto>((x) =>
            {
                Assert.That(x, Is.Not.Null);
                Assert.AreEqual(condition.Subject, x.Subject);
                Assert.AreEqual(condition.Body, x.Text);
                Assert.AreEqual("carada-test@mti.co.jp", x.From);
                Assert.AreEqual("テストメール送信者", x.FromName);
                Assert.That(x.ToList, Is.Not.Null);
                Assert.AreEqual(1, x.ToList.Count);
                Assert.AreEqual(entity.Email, x.ToList[0]);
                Assert.AreEqual(null, x.CcList);
                Assert.AreEqual(null, x.BccList);
                Assert.AreEqual(condition.Body, x.Text);
            });

            target = new MockMailService(new DbContextFactory().NewCaradaAuthProviderDbContext(), mockUserManager.Object, mockMailLogic, mockTicketLogic);

            await target.SendMail(condition);

            mockTicketLogic.Verify(m => m.FindTicket(condition.TicketId), Times.Once);
            mockUserManager.Verify(m => m.FindByIdAsync(ticket.Uid), Times.Once);
            mockMailLogic.Verify(m => m.SendMailAsync(It.IsAny<MailSendInfoDto>()), Times.Once);
        }

        /// <summary>
        /// <see cref="MailService.SendMail(SendMailCondition)"/>のテスト。
        /// CASE：正常にメール送信できるか。
        /// 事前条件：UIDからメールアドレスを登録済みのユーザー情報を取得できること。
        /// 事後条件：期待通りのパラメーターで<see cref="MailLogic.SendMailAsync(MailSendInfoDto, string)"/>が呼び出されること。
        /// </summary>
        /// <returns>なし</returns>
        [Test]
        public async Task TestSendMail_WithUidAndTicketId()
        {
            var condition = new SendMailCondition()
            {
                Uid = "uid",
                TicketId = "ticketId",
                Subject = "subject",
                Body = "body"
            };

            var entity = new CaradaIdUser()
            {
                Email = "email"
            };

            Ticket ticket = new Ticket()
            {
                Uid = "ticketUid",
                Kind = TicketKind.ResetPasseord,
                ExpiresDateTime = System.DateTime.Parse("9999-12-31 23:00:00"),
                ProcessedFlag = true
            };
            mockTicketLogic.Setup(m => m.FindTicket(condition.TicketId)).Returns(ticket);

            mockUserManager.Setup(m => m.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(entity);

            mockMailLogic.Setup(m => m.SendMailAsync(It.IsAny<MailSendInfoDto>())).Callback<MailSendInfoDto>((x) =>
            {
                Assert.That(x, Is.Not.Null);
                Assert.AreEqual(condition.Subject, x.Subject);
                Assert.AreEqual(condition.Body, x.Text);
                Assert.AreEqual("carada-test@mti.co.jp", x.From);
                Assert.AreEqual("テストメール送信者", x.FromName);
                Assert.That(x.ToList, Is.Not.Null);
                Assert.AreEqual(1, x.ToList.Count);
                Assert.AreEqual(entity.Email, x.ToList[0]);
                Assert.AreEqual(null, x.CcList);
                Assert.AreEqual(null, x.BccList);
                Assert.AreEqual(condition.Body, x.Text);
            });

            target = new MockMailService(new DbContextFactory().NewCaradaAuthProviderDbContext(), mockUserManager.Object, mockMailLogic, mockTicketLogic);

            await target.SendMail(condition);

            mockTicketLogic.Verify(m => m.FindTicket(condition.TicketId), Times.Once);
            mockUserManager.Verify(m => m.FindByIdAsync(ticket.Uid), Times.Once);
            mockMailLogic.Verify(m => m.SendMailAsync(It.IsAny<MailSendInfoDto>()), Times.Once);
        }

        /// <summary>
        /// <see cref="MailService.SendMail(SendMailCondition)"/>のテスト。
        /// CASE：UIDに紐付くユーザーが登録されていない場合。
        /// 事前条件：UIDからユーザー情報が取得できないこと。
        /// 事後条件：<see cref="CaradaApplicationException"/>が投げられること。エラーコードがdata_mismatchであること。
        /// </summary>
        [Test]
        public void TestSendMail_Throw_CaradaApplicationException_DataMismatch()
        {
            var condition = new SendMailCondition()
            {
                Uid = "uid"
            };

            mockUserManager.Setup(m => m.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(null);

            target = new MailService(new DbContextFactory().NewCaradaAuthProviderDbContext(), mockUserManager.Object);

            var actual = Assert.ThrowsAsync<CaradaApplicationException>(() => target.SendMail(condition));
            Assert.AreEqual("data_mismatch", actual.GetErrorCode());
            Assert.AreEqual("該当のユーザが見つかりませんでした。", actual.GetErrorMessage());

            mockUserManager.Verify(m => m.FindByIdAsync(condition.Uid), Times.Once);
        }

        /// <summary>
        /// <see cref="MailService.SendMail(SendMailCondition)"/>のテスト。
        /// CASE：UIDに紐付くユーザーがメールアドレスを登録していない場合。
        /// 事前条件：対象ユーザーのメールアドレスが取得できないこと。
        /// 事後条件：<see cref="CaradaApplicationException"/>が投げられること。エラーコードがhas_not_emailであること。
        /// </summary>
        [Test]
        public void TestSendMail_Throw_CaradaApplicationException_HasNotEmail()
        {
            var condition = new SendMailCondition()
            {
                Uid = "uid"
            };

            var entity = new CaradaIdUser();

            mockUserManager.Setup(m => m.FindByIdAsync("uid")).ReturnsAsync(entity);

            target = new MailService(new DbContextFactory().NewCaradaAuthProviderDbContext(), mockUserManager.Object);

            var actual = Assert.ThrowsAsync<CaradaApplicationException>(() => target.SendMail(condition));
            Assert.AreEqual("has_not_email", actual.GetErrorCode());
            Assert.AreEqual("メールアドレスが登録されていません。", actual.GetErrorMessage());

            mockUserManager.Verify(m => m.FindByIdAsync(condition.Uid), Times.Once);
        }

        /// <summary>
        /// <see cref="MailService"/>のテスト用モッククラス。
        /// </summary>
        private class MockMailService : MailService
        {
            /// <summary>
            /// コンストラクタ。
            /// </summary>
            /// <param name="dbContext">DBコンテキスト</param>
            /// <param name="userManager">アプリケーションユーザーマネージャー</param>
            /// <param name="mockMailLogic">メールロジックのモック</param>
            /// <param name="mockTicketLogic">チケットロジックのモック</param>
            public MockMailService(CaradaAuthProviderDbContext dbContext, ApplicationUserManager userManager, Mock<MailLogic> mockMailLogic, Mock<TicketLogic> mockTicketLogic)
                : base(dbContext, userManager)
            {
                mailLogic = mockMailLogic.Object;
                ticketLogic = mockTicketLogic.Object;
            }
        }
    }
}