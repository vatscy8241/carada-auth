﻿using CaradaAuthProvider.Api.Controllers.Conditions;
using CaradaAuthProvider.Api.Domain.Dtos;
using CaradaAuthProvider.Api.Domain.Logics;
using CaradaAuthProvider.Api.Exceptions;
using CaradaAuthProvider.Api.Exceptions.Enums;
using CaradaAuthProvider.Api.IdentityManagers;
using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using CaradaAuthProvider.Api.Persistence.Storage.Entities;
using CaradaAuthProvider.Api.Services;
using CaradaAuthProvider.Api.Services.Dtos;
using CaradaAuthProvider.Api.Utils;
using Microsoft.AspNet.Identity;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CaradaAuthProvider.Api.Test.Services
{
    [TestFixture]
    public class AuthenticateServiceTest
    {
        private AuthenticateService target;
        private class MockAuthenticateService : AuthenticateService
        {
            public MockAuthenticateService(
                TicketLogic ticketLogic,
                SecurityQuestionLogic securityQuestionLogic,
                MailLogic mailLogic,
                CaradaAuthProviderDbContext dbContext,
                ApplicationUserManager userManager) : base(dbContext, userManager)
            {
                this.ticketLogic = ticketLogic;
                this.securityQuestionLogic = securityQuestionLogic;
                this.mailLogic = mailLogic;
            }
        }

        private Mock<CaradaAuthProviderDbContext> mockDbContext;

        private Mock<ApplicationUserManager> mockUserManager;

        private Mock<TicketLogic> mockTicketLogic;
        private Mock<SecurityQuestionLogic> mockSecurityQuestionLogic;
        private Mock<MailLogic> mockMailLogic;

        private class MockIdentityResult : IdentityResult
        {
            public MockIdentityResult(bool success) : base(success)
            {
            }
        }
        [SetUp]
        public void Initialize()
        {
            Mock<IUserStore<CaradaIdUser>> mockStore = new Mock<IUserStore<CaradaIdUser>>();
            mockDbContext = new Mock<CaradaAuthProviderDbContext>();
            mockUserManager = new Mock<ApplicationUserManager>(mockStore.Object);
            mockTicketLogic = new Mock<TicketLogic>();
            mockSecurityQuestionLogic = new Mock<SecurityQuestionLogic>();
            mockMailLogic = new Mock<MailLogic>();
            target = new MockAuthenticateService(mockTicketLogic.Object, mockSecurityQuestionLogic.Object, mockMailLogic.Object, mockDbContext.Object, mockUserManager.Object);
        }

        [Test]
        public void Authenticate_SuccessWithTicket()
        {
            CaradaIdUser user = new CaradaIdUser()
            {
                Id = "userId",
                EmailConfirmed = false
            };
            mockUserManager.Setup(p => p.FindByName(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.Success);
            mockUserManager.Setup(p => p.ResetAccessFailedCount(user.Id));

            DateTime expiresDateTime = new DateTime(2016, 1, 1, 12, 0, 0);
            IssuedTicketDto issuedTicketDto = new IssuedTicketDto()
            {
                TicketId = "ticketId",
                Ticket = new Ticket()
                {
                    ExpiresDateTime = expiresDateTime
                }
            };
            mockTicketLogic.Setup(p => p.IssueTicket(TicketKind.UseStart, It.IsAny<CaradaIdUser>(), It.IsAny<long>(), null, null)).Returns(issuedTicketDto);

            AuthenticateCondition condition = new AuthenticateCondition()
            {
                CaradaId = "caradaId",
                Password = "password",
                ConfirmedFlag = true,
                TicketExpiresMinute = 10
            };

            AuthenticateResultDto actual = target.Authenticate(condition);
            Assert.IsNotNull(actual);
            Assert.AreEqual(user.Id, actual.Uid);
            Assert.IsNotNull(actual.Ticket);
            Assert.AreEqual(issuedTicketDto.TicketId, actual.Ticket.Id);
            Assert.AreEqual(DateUtilities.GetEpochTime(expiresDateTime), actual.Ticket.ExpiresAt);
        }

        [Test]
        public void Authenticate_SuccessWithoutTicket()
        {
            CaradaIdUser user = new CaradaIdUser()
            {
                Id = "userId",
                EmailConfirmed = true
            };
            mockUserManager.Setup(p => p.FindByName(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.Success);
            mockUserManager.Setup(p => p.ResetAccessFailedCount(user.Id));

            DateTime expiresDateTime = new DateTime(2016, 1, 1, 12, 0, 0);
            IssuedTicketDto issuedTicketDto = new IssuedTicketDto()
            {
                TicketId = "ticketId",
                Ticket = new Ticket()
                {
                    ExpiresDateTime = expiresDateTime
                }
            };
            mockTicketLogic.Setup(p => p.IssueTicket(TicketKind.UseStart, It.IsAny<CaradaIdUser>(), It.IsAny<long>(), null, null)).Returns(issuedTicketDto);

            AuthenticateCondition condition = new AuthenticateCondition()
            {
                CaradaId = "caradaId",
                Password = "password",
                ConfirmedFlag = true,
                TicketExpiresMinute = 10
            };

            AuthenticateResultDto actual = target.Authenticate(condition);
            Assert.IsNotNull(actual);
            Assert.AreEqual(user.Id, actual.Uid);
            Assert.IsNull(actual.Ticket);

            condition.ConfirmedFlag = false;

            actual = target.Authenticate(condition);
            Assert.IsNotNull(actual);
            Assert.AreEqual(user.Id, actual.Uid);
            Assert.IsNull(actual.Ticket);

            user.EmailConfirmed = false;

            actual = target.Authenticate(condition);
            Assert.IsNotNull(actual);
            Assert.AreEqual(user.Id, actual.Uid);
            Assert.IsNull(actual.Ticket);
        }

        [Test]
        public void Authenticate_FailureUserNotFound()
        {
            mockUserManager.Setup(p => p.FindByName(It.IsAny<string>()));

            AuthenticateCondition condition = new AuthenticateCondition()
            {
                CaradaId = "caradaId",
                Password = "password",
                ConfirmedFlag = true,
                TicketExpiresMinute = 10
            };

            var actual = Assert.Throws<CaradaApplicationException>(() => target.Authenticate(condition));
            Assert.AreEqual(ErrorKind.user_not_exist.GetName(), actual.GetErrorCode());
            Assert.AreEqual("該当のユーザが見つかりませんでした。", actual.GetErrorMessage());
        }

        [Test]
        public void Authenticate_FailurePasswordAlreadyLockedOut()
        {
            CaradaIdUser user = new CaradaIdUser()
            {
                Id = "userId",
                EmailConfirmed = false
            };
            mockUserManager.Setup(p => p.FindByName(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.AlreadyLockedOut);

            AuthenticateCondition condition = new AuthenticateCondition()
            {
                CaradaId = "caradaId",
                Password = "password",
                ConfirmedFlag = true,
                TicketExpiresMinute = 10
            };

            var actual = NUnit.Framework.Assert.Throws<CommitCaradaApplicationException>(() => target.Authenticate(condition));
            Assert.AreEqual(ErrorKind.account_lockout.GetName(), actual.GetErrorCode());
            Assert.AreEqual("アカウントロック中です。", actual.GetErrorMessage());
        }

        [Test]
        public void Authenticate_FailurePasswordLockedOut()
        {
            CaradaIdUser user = new CaradaIdUser()
            {
                Id = "userId",
                EmailConfirmed = false
            };
            mockUserManager.Setup(p => p.FindByName(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.LockedOut);

            AuthenticateCondition condition = new AuthenticateCondition()
            {
                CaradaId = "caradaId",
                Password = "password",
                ConfirmedFlag = true,
                TicketExpiresMinute = 10
            };

            var actual = NUnit.Framework.Assert.Throws<CommitCaradaApplicationException>(() => target.Authenticate(condition));
            Assert.AreEqual(ErrorKind.account_lockout.GetName(), actual.GetErrorCode());
            Assert.AreEqual("アカウントがロックされました。", actual.GetErrorMessage());
        }

        [Test]
        public void Authenticate_FailurePasswordFailure()
        {
            CaradaIdUser user = new CaradaIdUser()
            {
                Id = "userId",
                EmailConfirmed = false
            };
            mockUserManager.Setup(p => p.FindByName(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.Failure);

            AuthenticateCondition condition = new AuthenticateCondition()
            {
                CaradaId = "caradaId",
                Password = "password",
                ConfirmedFlag = true,
                TicketExpiresMinute = 10
            };

            var actual = Assert.Throws<CommitCaradaApplicationException>(() => target.Authenticate(condition));
            Assert.AreEqual(ErrorKind.password_mismatch.GetName(), actual.GetErrorCode());
            Assert.AreEqual("パスワードが一致しませんでした。", actual.GetErrorMessage());
        }

        [Test]
        public void VerifyTicket()
        {
            //VerifyResultDto actual;

            AuthCodeVerifyCondition condition = new AuthCodeVerifyCondition()
            {
                TicketId = "ticketId",
                VerifyCode = "verifyCode"
            };

            mockTicketLogic.Setup(p => p.VerifyUseStartTicket(condition.TicketId, condition.VerifyCode)).Returns(VerifyResult.SUCCESS);
            var actual = target.VerifyTicket(condition);
            Assert.IsNotNull(actual);

            mockTicketLogic.Setup(p => p.VerifyUseStartTicket(condition.TicketId, condition.VerifyCode)).Returns(VerifyResult.TICKET_NOT_FOUND);
            var actual1 = Assert.Throws<CaradaApplicationException>(() => target.VerifyTicket(condition));
            Assert.AreEqual(ErrorKind.invalid_ticket.GetName(), actual1.GetErrorCode());
            Assert.AreEqual("該当のチケットが見つかりませんでした。", actual1.GetErrorMessage());

            mockTicketLogic.Setup(p => p.VerifyUseStartTicket(condition.TicketId, condition.VerifyCode)).Returns(VerifyResult.EXPIRED);
            var actual2 = Assert.Throws<CaradaApplicationException>(() => target.VerifyTicket(condition));
            Assert.AreEqual(ErrorKind.invalid_ticket.GetName(), actual2.GetErrorCode());
            Assert.AreEqual("チケットの有効期限が切れています。", actual2.GetErrorMessage());

            mockTicketLogic.Setup(p => p.VerifyUseStartTicket(condition.TicketId, condition.VerifyCode)).Returns(VerifyResult.INVALID);
            var actual4 = Assert.Throws<CaradaApplicationException>(() => target.VerifyTicket(condition));
            Assert.AreEqual(ErrorKind.invalid_code.GetName(), actual4.GetErrorCode());
            Assert.AreEqual("無効なチケットです。", actual4.GetErrorMessage());

            mockTicketLogic.Setup(p => p.VerifyUseStartTicket(condition.TicketId, condition.VerifyCode)).Returns(VerifyResult.INVALIDED);
            var actual5 = Assert.Throws<CaradaApplicationException>(() => target.VerifyTicket(condition));
            Assert.AreEqual(ErrorKind.invalid_code.GetName(), actual5.GetErrorCode());
            Assert.AreEqual("チケットが無効となりました。", actual5.GetErrorMessage());

            mockTicketLogic.Setup(p => p.VerifyUseStartTicket(condition.TicketId, condition.VerifyCode)).Returns(VerifyResult.CODE_MISMATCH);
            var actual6 = Assert.Throws<CaradaApplicationException>(() => target.VerifyTicket(condition));
            Assert.AreEqual(ErrorKind.code_mismatch.GetName(), actual6.GetErrorCode());
            Assert.AreEqual("認証コードが一致しませんでした。", actual6.GetErrorMessage());

            mockTicketLogic.Setup(p => p.VerifyUseStartTicket(condition.TicketId, condition.VerifyCode)).Returns(VerifyResult.UNDEFINED);
            var actual7 = Assert.Throws<Exception>(() => target.VerifyTicket(condition));
            Assert.AreEqual("予期せぬエラーが発生しました。", actual7.Message);
        }

        [Test]
        public void RegisterUseStart_Success()
        {
            UseStartRegisterCondition condition = new UseStartRegisterCondition()
            {
                TicketId = "ticketId",
                SecretQuestionId = 1,
                SecretQuestionAnswer = "質問の回答"
            };

            Ticket ticket = new Ticket()
            {
                Uid = "userId",
                Kind = TicketKind.UseStart,
                ExpiresDateTime = DateUtilities.UTCNow.AddMinutes(1),
                MailAddress = "test@test.com",
                VerifyCode = "verifyCode",
                VerifiedFlag = true,
                InvalidCount = 0
            };
            CaradaIdUser user = new CaradaIdUser()
            {
                Id = "userId",
                EmailConfirmed = false
            };

            mockTicketLogic.Setup(p => p.FindTicket(condition.TicketId)).Returns(ticket);
            mockTicketLogic.Setup(p => p.RemoveTicket(condition.TicketId));

            mockUserManager.Setup(p => p.FindById(ticket.Uid)).Returns(user);
            mockUserManager.Setup(p => p.FindByEmail(ticket.MailAddress));
            mockUserManager.Setup(p => p.Update(It.IsAny<CaradaIdUser>())).Returns(new MockIdentityResult(true));

            mockSecurityQuestionLogic.Setup(p => p.RegisterAnswer(condition.TicketId, condition.SecretQuestionId, condition.SecretQuestionAnswer));

            RegisterUseStartResultDto actual = target.RegisterUseStart(condition);
            Assert.AreEqual(ticket.Uid, actual.Uid);
        }

        [Test]
        public void RegisterUseStart_TicketError()
        {
            UseStartRegisterCondition condition = new UseStartRegisterCondition()
            {
                TicketId = "ticketId",
                SecretQuestionId = 1,
                SecretQuestionAnswer = "質問の回答"
            };

            Ticket ticket = new Ticket()
            {
                Uid = "userId",
                Kind = TicketKind.UseStart,
                ExpiresDateTime = DateUtilities.UTCNow.AddMinutes(1),
                MailAddress = "test@test.com",
                VerifyCode = "verifyCode",
                VerifiedFlag = true,
                InvalidCount = 0
            };
            CaradaIdUser user = new CaradaIdUser()
            {
                Id = "userId",
                EmailConfirmed = false
            };

            mockTicketLogic.Setup(p => p.FindTicket(condition.TicketId));

            var actual = Assert.Throws<CaradaApplicationException>(() => target.RegisterUseStart(condition));
            Assert.AreEqual(ErrorKind.invalid_ticket.GetName(), actual.GetErrorCode());
            Assert.AreEqual("該当のチケットが見つかりませんでした。", actual.GetErrorMessage());

            mockTicketLogic.Setup(p => p.FindTicket(condition.TicketId)).Returns(ticket);

            ticket.Kind = TicketKind.General;

            var actual1 = Assert.Throws<CaradaApplicationException>(() => target.RegisterUseStart(condition));
            Assert.AreEqual(ErrorKind.invalid_ticket.GetName(), actual1.GetErrorCode());
            Assert.AreEqual("種別の異なるチケットが指定されました。", actual1.GetErrorMessage());

            ticket.Kind = TicketKind.UseStart;
            ticket.ExpiresDateTime = DateUtilities.UTCNow.AddMinutes(-1);

            var actual2 = Assert.Throws<CaradaApplicationException>(() => target.RegisterUseStart(condition));
            Assert.AreEqual(ErrorKind.invalid_ticket.GetName(), actual2.GetErrorCode());
            Assert.AreEqual("チケットの有効期限が切れています。", actual2.GetErrorMessage());

            ticket.ExpiresDateTime = DateUtilities.UTCNow.AddMinutes(1);
            ticket.VerifiedFlag = false;

            var actual3 = Assert.Throws<CaradaApplicationException>(() => target.RegisterUseStart(condition));
            Assert.AreEqual(ErrorKind.invalid_ticket.GetName(), actual3.GetErrorCode());
            Assert.AreEqual("認証コード検証が完了していません。", actual3.GetErrorMessage());

            ticket.VerifiedFlag = true;
            ticket.MailAddress = null;

            var actual4 = Assert.Throws<CaradaApplicationException>(() => target.RegisterUseStart(condition));
            Assert.AreEqual(ErrorKind.invalid_ticket.GetName(), actual4.GetErrorCode());
            Assert.AreEqual("メールアドレスが登録されていません。", actual4.GetErrorMessage());
        }

        [Test]
        public void RegisterUseStart_UserError()
        {
            UseStartRegisterCondition condition = new UseStartRegisterCondition()
            {
                TicketId = "ticketId",
                SecretQuestionId = 1,
                SecretQuestionAnswer = "質問の回答"
            };

            Ticket ticket = new Ticket()
            {
                Uid = "userId",
                Kind = TicketKind.UseStart,
                ExpiresDateTime = DateUtilities.UTCNow.AddMinutes(1),
                MailAddress = "test@test.com",
                VerifyCode = "verifyCode",
                VerifiedFlag = true,
                InvalidCount = 0
            };
            CaradaIdUser user = new CaradaIdUser()
            {
                Id = "userId",
                EmailConfirmed = true
            };

            mockTicketLogic.Setup(p => p.FindTicket(condition.TicketId)).Returns(ticket);

            mockUserManager.Setup(p => p.FindById(ticket.Uid));

            var actual = Assert.Throws<CaradaApplicationException>(() => target.RegisterUseStart(condition));
            Assert.AreEqual(ErrorKind.data_mismatch.GetName(), actual.GetErrorCode());
            Assert.AreEqual("該当のユーザが見つかりませんでした。", actual.GetErrorMessage());

            mockUserManager.Setup(p => p.FindById(ticket.Uid)).Returns(user);

            var actual1 = Assert.Throws<CaradaApplicationException>(() => target.RegisterUseStart(condition));
            Assert.AreEqual(ErrorKind.already_completed.GetName(), actual1.GetErrorCode());
            Assert.AreEqual("既にメールアドレス登録が完了しています。", actual1.GetErrorMessage());
        }

        [Test]
        public void RegisterUseStart_EmailError()
        {
            UseStartRegisterCondition condition = new UseStartRegisterCondition()
            {
                TicketId = "ticketId",
                SecretQuestionId = 1,
                SecretQuestionAnswer = "質問の回答"
            };

            Ticket ticket = new Ticket()
            {
                Uid = "userId",
                Kind = TicketKind.UseStart,
                ExpiresDateTime = DateUtilities.UTCNow.AddMinutes(1),
                MailAddress = "test@test.com",
                VerifyCode = "verifyCode",
                VerifiedFlag = true,
                InvalidCount = 0
            };
            CaradaIdUser user = new CaradaIdUser()
            {
                Id = "userId",
                EmailConfirmed = false
            };

            mockTicketLogic.Setup(p => p.FindTicket(condition.TicketId)).Returns(ticket);

            mockUserManager.Setup(p => p.FindById(ticket.Uid)).Returns(user);
            mockUserManager.Setup(p => p.FindByEmail(ticket.MailAddress)).Returns(new CaradaIdUser());

            var actual = Assert.Throws<CaradaApplicationException>(() => target.RegisterUseStart(condition));
            Assert.AreEqual(ErrorKind.registered_email.GetName(), actual.GetErrorCode());
            Assert.AreEqual("メールアドレスが既に登録されています。", actual.GetErrorMessage());
        }

        [Test]
        public void RegisterUseStart_UpdateError()
        {
            UseStartRegisterCondition condition = new UseStartRegisterCondition()
            {
                TicketId = "ticketId",
                SecretQuestionId = 1,
                SecretQuestionAnswer = "質問の回答"
            };

            Ticket ticket = new Ticket()
            {
                Uid = "userId",
                Kind = TicketKind.UseStart,
                ExpiresDateTime = DateUtilities.UTCNow.AddMinutes(1),
                MailAddress = "test@test.com",
                VerifyCode = "verifyCode",
                VerifiedFlag = true,
                InvalidCount = 0
            };
            CaradaIdUser user = new CaradaIdUser()
            {
                Id = "userId",
                EmailConfirmed = false
            };

            mockTicketLogic.Setup(p => p.FindTicket(condition.TicketId)).Returns(ticket);
            mockTicketLogic.Setup(p => p.RemoveTicket(condition.TicketId));

            mockUserManager.Setup(p => p.FindById(ticket.Uid)).Returns(user);
            mockUserManager.Setup(p => p.FindByEmail(ticket.MailAddress));
            mockUserManager.Setup(p => p.Update(It.IsAny<CaradaIdUser>())).Returns(new MockIdentityResult(false));

            mockSecurityQuestionLogic.Setup(p => p.RegisterAnswer(condition.TicketId, condition.SecretQuestionId, condition.SecretQuestionAnswer));

            var actual = Assert.Throws<CaradaApplicationException>(() => target.RegisterUseStart(condition));
            Assert.AreEqual(ErrorKind.server_error.GetName(), actual.GetErrorCode());
            Assert.AreEqual("ユーザ情報の更新に失敗しました。", actual.GetErrorMessage());
        }

        [Test]
        public void SendUseStartVerifyCode_Success()
        {
            SendUseStartVerifyCodeCondition condition = new SendUseStartVerifyCodeCondition()
            {
                TicketId = "ticketId",
                Email = "test@test.com",
                Subject = "件名",
                Body = "本文：{verifycode}/{verifycode}/{verifycode}"
            };

            Ticket ticket = new Ticket()
            {
                Uid = "userId",
                Kind = TicketKind.UseStart,
                ExpiresDateTime = DateUtilities.UTCNow.AddMinutes(1),
                MailAddress = null,
                VerifyCode = null,
                VerifiedFlag = false,
                InvalidCount = 1
            };
            CaradaIdUser user = new CaradaIdUser()
            {
                Id = "userId",
                EmailConfirmed = false
            };

            mockTicketLogic.Setup(p => p.FindTicket(condition.TicketId)).Returns(ticket);
            mockTicketLogic.Setup(p => p.RegisterTicket(condition.TicketId, It.IsAny<Ticket>())).Callback<string, Ticket>((x, y) =>
            {
                Assert.AreEqual(condition.Email, y.MailAddress);
                Assert.IsTrue(Regex.IsMatch(y.VerifyCode, @"^[a-z0-9]{32}$"), y.VerifyCode);
                Assert.AreEqual(0, y.InvalidCount);
            });

            mockUserManager.Setup(p => p.FindById(ticket.Uid)).Returns(user);
            mockUserManager.Setup(p => p.FindByEmailAsync(ticket.MailAddress)).ReturnsAsync(null);

            mockMailLogic.Setup(p => p.SendMailAsync(It.IsAny<MailSendInfoDto>())).Callback<MailSendInfoDto>((x) =>
            {
                Assert.AreEqual("carada-test@mti.co.jp", x.From);
                Assert.AreEqual("テストメール送信者", x.FromName);
                Assert.AreEqual(1, x.ToList.Count);
                Assert.AreEqual(condition.Email, x.ToList[0]);
                Assert.AreEqual(condition.Subject, x.Subject);
                Assert.IsTrue(Regex.IsMatch(x.Text, @"^本文：[a-z0-9]{32}/[a-z0-9]{32}/[a-z0-9]{32}$"), x.Text);
            });

            SendMailResultDto actual = target.SendUseStartVerifyCode(condition);
            Assert.IsNotNull(actual);
        }

        [Test]
        public void SendUseStartVerifyCode_TicketError()
        {
            SendUseStartVerifyCodeCondition condition = new SendUseStartVerifyCodeCondition()
            {
                TicketId = "ticketId",
                Email = "test@test.com",
                Subject = "件名",
                Body = "本文：{verifycode}/{verifycode}/{verifycode}",
            };

            Ticket ticket = new Ticket()
            {
                Uid = "userId",
                Kind = TicketKind.UseStart,
                ExpiresDateTime = DateUtilities.UTCNow.AddMinutes(1),
                MailAddress = null,
                VerifyCode = null,
                VerifiedFlag = false,
                InvalidCount = 1
            };

            mockTicketLogic.Setup(p => p.FindTicket(condition.TicketId));

            var actual = Assert.Throws<CaradaApplicationException>(() => target.SendUseStartVerifyCode(condition));
            Assert.AreEqual(ErrorKind.invalid_ticket.GetName(), actual.GetErrorCode());
            Assert.AreEqual("該当のチケットが見つかりませんでした。", actual.GetErrorMessage());

            mockTicketLogic.Setup(p => p.FindTicket(condition.TicketId)).Returns(ticket);

            ticket.Kind = TicketKind.General;

            var actual1 = Assert.Throws<CaradaApplicationException>(() => target.SendUseStartVerifyCode(condition));
            Assert.AreEqual(ErrorKind.invalid_ticket.GetName(), actual1.GetErrorCode());
            Assert.AreEqual("種別の異なるチケットが指定されました。", actual1.GetErrorMessage());

            ticket.Kind = TicketKind.UseStart;
            ticket.ExpiresDateTime = DateUtilities.UTCNow.AddMinutes(-1);

            var actual2 = Assert.Throws<CaradaApplicationException>(() => target.SendUseStartVerifyCode(condition));
            Assert.AreEqual(ErrorKind.invalid_ticket.GetName(), actual2.GetErrorCode());
            Assert.AreEqual("チケットの有効期限が切れています。", actual2.GetErrorMessage());

            ticket.ExpiresDateTime = DateUtilities.UTCNow.AddMinutes(1);
            ticket.VerifiedFlag = true;

            var actual3 = Assert.Throws<CaradaApplicationException>(() => target.SendUseStartVerifyCode(condition));
            Assert.AreEqual(ErrorKind.invalid_ticket.GetName(), actual3.GetErrorCode());
            Assert.AreEqual("認証コード検証が既に完了しています。", actual3.GetErrorMessage());
        }

        [Test]
        public void SendUseStartVerifyCode_UserError()
        {
            SendUseStartVerifyCodeCondition condition = new SendUseStartVerifyCodeCondition()
            {
                TicketId = "ticketId",
                Email = "test@test.com",
                Subject = "件名",
                Body = "本文：{verifycode}/{verifycode}/{verifycode}",
            };

            Ticket ticket = new Ticket()
            {
                Uid = "userId",
                Kind = TicketKind.UseStart,
                ExpiresDateTime = DateUtilities.UTCNow.AddMinutes(1),
                MailAddress = null,
                VerifyCode = null,
                VerifiedFlag = false,
                InvalidCount = 1
            };
            CaradaIdUser user = new CaradaIdUser()
            {
                Id = "userId",
                EmailConfirmed = true
            };

            mockTicketLogic.Setup(p => p.FindTicket(condition.TicketId)).Returns(ticket);

            mockUserManager.Setup(p => p.FindById(ticket.Uid));

            var actual = Assert.Throws<CaradaApplicationException>(() => target.SendUseStartVerifyCode(condition));
            Assert.AreEqual(ErrorKind.data_mismatch.GetName(), actual.GetErrorCode());
            Assert.AreEqual("該当のユーザが見つかりませんでした。", actual.GetErrorMessage());

            mockUserManager.Setup(p => p.FindById(ticket.Uid)).Returns(user);

            var actual1 = Assert.Throws<CaradaApplicationException>(() => target.SendUseStartVerifyCode(condition));
            Assert.AreEqual(ErrorKind.already_completed.GetName(), actual1.GetErrorCode());
            Assert.AreEqual("既にメールアドレス登録が完了しています。", actual1.GetErrorMessage());
        }

        [Test]
        public void SendUseStartVerifyCode_EmailError()
        {
            SendUseStartVerifyCodeCondition condition = new SendUseStartVerifyCodeCondition()
            {
                TicketId = "ticketId",
                Email = "test@test.com",
                Subject = "件名",
                Body = "本文：{verifycode}/{verifycode}/{verifycode}",
            };

            Ticket ticket = new Ticket()
            {
                Uid = "userId",
                Kind = TicketKind.UseStart,
                ExpiresDateTime = DateUtilities.UTCNow.AddMinutes(1),
                MailAddress = null,
                VerifyCode = null,
                VerifiedFlag = false,
                InvalidCount = 1
            };
            CaradaIdUser user = new CaradaIdUser()
            {
                Id = "userId",
                EmailConfirmed = false
            };

            mockTicketLogic.Setup(p => p.FindTicket(condition.TicketId)).Returns(ticket);

            mockUserManager.Setup(p => p.FindById(ticket.Uid)).Returns(user);
            mockUserManager.Setup(p => p.FindByEmail(condition.Email)).Returns(new CaradaIdUser());

            var actual = Assert.Throws<CaradaApplicationException>(() => target.SendUseStartVerifyCode(condition));
            Assert.AreEqual(ErrorKind.registered_email.GetName(), actual.GetErrorCode());
            Assert.AreEqual("メールアドレスが既に登録されています。", actual.GetErrorMessage());
        }

        [Test]
        public void SendTwoFactorVerifyCode_Success()
        {
            SendTwoFactorVerifyCodeCondition condition = new SendTwoFactorVerifyCodeCondition()
            {
                Uid = "uid",
                Subject = "件名",
                Body = "本文：{verifycode}/{verifycode}/{verifycode}",
                TicketExpiresMinute = 10
            };

            CaradaIdUser user = new CaradaIdUser()
            {
                Id = "userId",
                Email = "test@test.com",
                EmailConfirmed = true
            };

            mockUserManager.Setup(p => p.FindById(condition.Uid)).Returns(user);

            mockMailLogic.Setup(p => p.SendMailAsync(It.IsAny<MailSendInfoDto>())).Callback<MailSendInfoDto>((x) =>
            {
                Assert.AreEqual("carada-test@mti.co.jp", x.From);
                Assert.AreEqual("テストメール送信者", x.FromName);
                Assert.AreEqual(1, x.ToList.Count);
                Assert.AreEqual(user.Email, x.ToList[0]);
                Assert.AreEqual(condition.Subject, x.Subject);
                Assert.IsTrue(Regex.IsMatch(x.Text, @"^本文：[a-z0-9]{32}/[a-z0-9]{32}/[a-z0-9]{32}$"), x.Text);
            });

            DateTime expiresDateTime = new DateTime(2016, 1, 1, 12, 0, 0);
            IssuedTicketDto issuedTicketDto = new IssuedTicketDto()
            {
                TicketId = "ticketId",
                Ticket = new Ticket()
                {
                    ExpiresDateTime = expiresDateTime
                }
            };
            mockTicketLogic.Setup(p => p.IssueTicket(TicketKind.TwoFactor, It.IsAny<CaradaIdUser>(), It.IsAny<long>(), It.IsAny<string>(), user.Email)).Returns(issuedTicketDto);

            TwoFactorSendMailResultDto actual = target.SendTwoFactorVerifyCode(condition);
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Ticket);
            Assert.AreEqual(issuedTicketDto.TicketId, actual.Ticket.Id);
            Assert.AreEqual(DateUtilities.GetEpochTime(expiresDateTime), actual.Ticket.ExpiresAt);
        }

        [Test]
        public void SendTwoFactorVerifyCode_UserError()
        {
            SendTwoFactorVerifyCodeCondition condition = new SendTwoFactorVerifyCodeCondition()
            {
                Uid = "uid",
                Subject = "件名",
                Body = "本文：{verifycode}/{verifycode}/{verifycode}",
                TicketExpiresMinute = 10
            };

            CaradaIdUser user = new CaradaIdUser()
            {
                Id = "userId",
                Email = "test@test.com",
                EmailConfirmed = false
            };

            mockUserManager.Setup(p => p.FindById(condition.Uid));

            var actual = Assert.Throws<CaradaApplicationException>(() => target.SendTwoFactorVerifyCode(condition));
            Assert.AreEqual(ErrorKind.data_mismatch.GetName(), actual.GetErrorCode());
            Assert.AreEqual("該当のユーザが見つかりませんでした。", actual.GetErrorMessage());

            mockUserManager.Setup(p => p.FindById(condition.Uid)).Returns(user);

            var actual1 = Assert.Throws<CaradaApplicationException>(() => target.SendTwoFactorVerifyCode(condition));
            Assert.AreEqual(ErrorKind.data_mismatch.GetName(), actual1.GetErrorCode());
            Assert.AreEqual("メールアドレスが登録されていません。", actual1.GetErrorMessage());

            user.Email = null;
            user.EmailConfirmed = true;

            var actual2 = Assert.Throws<CaradaApplicationException>(() => target.SendTwoFactorVerifyCode(condition));
            Assert.AreEqual(ErrorKind.data_mismatch.GetName(), actual2.GetErrorCode());
            Assert.AreEqual("メールアドレスが登録されていません。", actual2.GetErrorMessage());
        }

        /// <summary>
        /// [対象] 秘密の質問リスト取得
        /// [条件] ロジックの取得結果が2件の場合
        /// [結果] 質問1と質問2が格納されたリストが返却される
        /// </summary>
        [Test]
        public void GeetSecurityQuestion()
        {
            mockSecurityQuestionLogic.Setup<IEnumerable<SecurityQuestionMasters>>(p => p.GetSecurityQuestionList()).Returns(new List<SecurityQuestionMasters>() {

                new SecurityQuestionMasters()
                {
                    SecurityQuestionId = 2,
                    Question = "今何時？",
                    DisplayOrder = 1,
                },

                new SecurityQuestionMasters()
                {
                    SecurityQuestionId = 1,
                    Question = "そうねだいたいね",
                    DisplayOrder = 2,
                }
            });

            List<SecurityQuestionResultDto> actual = target.GetSecurityQuestionList();
            Assert.IsNotNull(actual);

            // 取得件数
            Assert.AreEqual(2, actual.Count);

            // 質問1
            SecurityQuestionResultDto a1 = actual[0];
            Assert.AreEqual(1, a1.SecretQuestionId);
            Assert.AreEqual("今何時？", a1.SecretQuestionSentence);

            // 質問2
            SecurityQuestionResultDto a2 = actual[1];
            Assert.AreEqual(2, a2.SecretQuestionId);
            Assert.AreEqual("そうねだいたいね", a2.SecretQuestionSentence);
        }

        /// <summary>
        /// [対象] 秘密の質問リスト取得
        /// [条件] ロジックの取得結果が0件の場合
        /// [結果] 空の秘密の質問リストが返却される
        /// </summary>
        [Test]
        public void GeetSecurityQuestion_Result_Empty()
        {
            mockSecurityQuestionLogic.Setup<IEnumerable<SecurityQuestionMasters>>(p => p.GetSecurityQuestionList()).Returns(new List<SecurityQuestionMasters>()
            {
            });

            List<SecurityQuestionResultDto> actual = target.GetSecurityQuestionList();
            Assert.IsNotNull(actual);

            // 取得件数
            Assert.AreEqual(0, actual.Count);
        }
    }
}