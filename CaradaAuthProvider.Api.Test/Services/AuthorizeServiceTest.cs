﻿using CaradaAuthProvider.Api.Controllers.Conditions;
using CaradaAuthProvider.Api.Domain.Logics;
using CaradaAuthProvider.Api.Exceptions;
using CaradaAuthProvider.Api.Exceptions.Enums;
using CaradaAuthProvider.Api.IdentityManagers;
using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using CaradaAuthProvider.Api.Persistence.Storage.Entities;
using CaradaAuthProvider.Api.Services;
using CaradaAuthProvider.Api.Services.Authlete;
using CaradaAuthProvider.Api.Services.Dtos;
using Microsoft.AspNet.Identity;
using Moq;
using Mti.Hc.Authlete.Common.Dto;
using Mti.Hc.Authlete.Common.Services;
using NUnit.Framework;
using System;
using System.Reflection;

namespace CaradaAuthProvider.Api.Test.Services
{
    [TestFixture]
    public class AuthorizeServiceTest
    {
        private MockAuthorizeService target;

        private Mock<CaradaAuthProviderDbContext> mDbContext;

        private Mock<ApplicationUserManager> mUserManeger;

        private Mock<AgencyAccessTokenLogic> mAgencyAccessTokenLogic;

        private class MockAuthorizeService : AuthorizeService
        {
            public MockAuthorizeService(CaradaAuthProviderDbContext dbContext, ApplicationUserManager userManager) : base(dbContext, userManager)
            {
            }

            public AuthorizationServiceWrapper AuthorizationServiceWrapper
            {
                get
                {
                    return authorizationService;
                }
            }

            public TokenServiceWrapper TokenServiceWrapper
            {
                get
                {
                    return tokenService;
                }
            }

            public ClientScopesServiceWrapper ClientScopesServiceWrapper
            {
                get
                {
                    return clientScopesService;
                }
            }
        }

        [SetUp]
        public void Initialize()
        {
            mDbContext = new Mock<CaradaAuthProviderDbContext>();
            mUserManeger = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mAgencyAccessTokenLogic = new Mock<AgencyAccessTokenLogic>();
            target = new MockAuthorizeService(mDbContext.Object, mUserManeger.Object);
            typeof(AuthorizeService).GetField("agencyAccessTokenLogic", BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Instance).SetValue(target, mAgencyAccessTokenLogic.Object);

        }

        /// <summary>
        /// [対象] 認可
        /// [条件] コンディションがnull
        /// [結果] invalid_parameterが返ってくる
        /// </summary>
        [Test]
        public void Authorize_Condition_Empty()
        {

            AuthorizeCondition condition = null;
            var actual = Assert.Throws<CaradaApplicationException>(() => target.Authorize(condition));
            Assert.AreEqual(ErrorKind.invalid_parameter.GetName(), actual.GetErrorCode());
            Assert.AreEqual("リクエストパラメータが設定されていません。", actual.GetErrorMessage());
        }

        /// <summary>
        /// [対象] 認可
        /// [条件] クライアントIDがnullもしくは空文字
        /// [結果] invalid_parameterが返ってくる
        /// </summary>
        [Test]
        public void Authorize_ClientId_Empty()
        {

            AuthorizeCondition condition = new AuthorizeCondition
            {
                ClientId = "",
                Uid = ""
            };
            var actual = Assert.Throws<CaradaApplicationException>(() => target.Authorize(condition));
            Assert.AreEqual(ErrorKind.invalid_parameter.GetName(), actual.GetErrorCode());
            Assert.AreEqual("クライアントIDが設定されていません。", actual.GetErrorMessage());
        }

        /// <summary>
        /// [対象] 認可
        /// [条件] UIDがnullもしくは空文字
        /// [結果] invalid_parameterが返ってくる
        /// </summary>
        [Test]
        public void Authorize_UID_Empty()
        {

            AuthorizeCondition condition = new AuthorizeCondition
            {
                ClientId = "client_id",
                Uid = ""
            };
            var actual = Assert.Throws<CaradaApplicationException>(() => target.Authorize(condition));
            Assert.AreEqual(ErrorKind.invalid_parameter.GetName(), actual.GetErrorCode());
            Assert.AreEqual("ユーザー一意識別子が設定されていません。", actual.GetErrorMessage());
        }

        /// <summary>
        /// [対象] 認可
        /// [条件] UIDに紐づくユーザー情報が取得出来ない場合
        /// [結果] data_mismatchが返ってくる
        /// </summary>
        [Test]
        public void Authorize_UserInfo_NotFound()
        {
            var uid = "1234567890";

            mUserManeger.Setup(i => i.FindById(It.IsAny<string>()));

            AuthorizeCondition condition = new AuthorizeCondition
            {
                ClientId = "client_id",
                Uid = uid
            };
            var actual = Assert.Throws<CaradaApplicationException>(() => target.Authorize(condition));
            Assert.AreEqual(ErrorKind.data_mismatch.GetName(), actual.GetErrorCode());
            Assert.AreEqual("該当のユーザが見つかりませんでした。", actual.GetErrorMessage());
        }

        /// <summary>
        /// [対象] 認可
        /// [条件] 認証認可サービスの結果がエラーの場合
        /// [結果] server_errorが返ってくる
        /// </summary>
        [Test]
        public void Authorize_GetAuthorizationCode_HasError()
        {

            var clientId = "clientId";
            var uid = "uid";

            mUserManeger.Setup(i => i.FindById(It.IsAny<string>())).Returns(new CaradaIdUser());

            target.ClientScopesServiceWrapper.GetClientScopes = (c) =>
            {
                return new ClientScopesServiceResult
                {
                    RequestableScopes = new string[] { "scope" }
                };
            };

            target.AuthorizationServiceWrapper.GetAuthorizationCode = (c, u, s) =>
            {
                return null;
            };

            AuthorizeCondition condition = new AuthorizeCondition
            {
                ClientId = clientId,
                Uid = uid
            };
            var actual = Assert.Throws<AuthleteException>(() => target.Authorize(condition));
            Assert.AreEqual(ErrorKind.server_error.GetName(), actual.GetErrorCode());
            Assert.AreEqual("認可コード取得に失敗しました。", actual.GetErrorMessage());

            AuthorizationServiceResult errorResult = new AuthorizationServiceResult();
            Func<int, bool> mockErrorPredicate = (e) => true;
            typeof(AbstractServiceResult<int>).GetField("<ErrorPredicate>k__BackingField", BindingFlags.NonPublic | BindingFlags.Instance).SetValue(errorResult, mockErrorPredicate);

            target.AuthorizationServiceWrapper.GetAuthorizationCode = (c, u, s) =>
            {
                return errorResult;
            };

            condition = new AuthorizeCondition
            {
                ClientId = clientId,
                Uid = uid
            };
            var actual1 = Assert.Throws<AuthleteException>(() => target.Authorize(condition));
            Assert.AreEqual(ErrorKind.server_error.GetName(), actual1.GetErrorCode());
            Assert.AreEqual("認可コード取得に失敗しました。", actual1.GetErrorMessage());
        }

        /// <summary>
        /// [対象] 認可
        /// [条件] アクセストークン取得サービスの結果がエラーの場合
        /// [結果] server_errorが返ってくる
        /// </summary>
        [Test]
        public void Authorize_GetToken_HasError()
        {

            var clientId = "clientId";
            var uid = "uid";
            var code = "12345";

            mUserManeger.Setup(i => i.FindById(It.IsAny<string>())).Returns(new CaradaIdUser());

            target.ClientScopesServiceWrapper.GetClientScopes = (c) =>
            {
                return new ClientScopesServiceResult
                {
                    RequestableScopes = new string[] { "scope" }
                };
            };


            target.AuthorizationServiceWrapper.GetAuthorizationCode = (c, u, s) =>
            {
                return new AuthorizationServiceResult
                {
                    Code = code
                };
            };

            target.TokenServiceWrapper.GetToken = (u) =>
            {
                return null;
            };

            AuthorizeCondition condition = new AuthorizeCondition
            {
                ClientId = clientId,
                Uid = uid
            };
            var actual = Assert.Throws<AuthleteException>(() => target.Authorize(condition));
            Assert.AreEqual(ErrorKind.server_error.GetName(), actual.GetErrorCode());
            Assert.AreEqual("アクセストークンの取得に失敗しました。", actual.GetErrorMessage());

            TokenServiceResult errorResult = new TokenServiceResult();
            Func<int, bool> mockErrorPredicate = (e) => true;
            typeof(AbstractServiceResult<int>).GetField("<ErrorPredicate>k__BackingField", BindingFlags.NonPublic | BindingFlags.Instance).SetValue(errorResult, mockErrorPredicate);

            target.TokenServiceWrapper.GetToken = (u) =>
            {
                return errorResult;
            };

            var actual1 = Assert.Throws<AuthleteException>(() => target.Authorize(condition));
            Assert.AreEqual(ErrorKind.server_error.GetName(), actual1.GetErrorCode());
            Assert.AreEqual("アクセストークンの取得に失敗しました。", actual1.GetErrorMessage());
        }


        /// <summary>
        /// [対象] 認可
        /// [条件] 正常
        /// [結果] 正常にアクセストークンが取得できる
        /// </summary>
        [Test]
        public void Authorize()
        {

            var clientId = "clientId";
            var uid = "uid";
            var code = "12345";
            var accessToken = "accessToken";
            var refreshToken = "refreshToken";

            mUserManeger.Setup(i => i.FindById(It.IsAny<string>())).Returns(new CaradaIdUser());

            target.ClientScopesServiceWrapper.GetClientScopes = (c) =>
            {
                return new ClientScopesServiceResult
                {
                    RequestableScopes = new string[] { "scope" }
                };
            };

            target.AuthorizationServiceWrapper.GetAuthorizationCode = (c, u, s) =>
            {
                return new AuthorizationServiceResult
                {
                    Code = code
                };
            };

            target.TokenServiceWrapper.GetToken = (u) =>
            {
                return new TokenServiceResult
                {
                    AccessToken = accessToken,
                    RefreshToken = refreshToken
                };
            };

            AuthorizeCondition condition = new AuthorizeCondition
            {
                ClientId = clientId,
                Uid = uid
            };
            AuthorizeResultDto ret = target.Authorize(condition);

            Assert.IsNotNull(ret);

            Assert.AreEqual(accessToken, ret.AccessToken);
            Assert.AreEqual(refreshToken, ret.RefreshToken);
        }

        /// <summary>
        /// [対象] 検証
        /// [条件] 正常
        /// [結果] エラー
        /// </summary>
        [Test]
        public void Introspect_HasError()
        {
            target.TokenServiceWrapper.Introspect = (x) =>
            {
                return null;
            };

            TokenIntrospectCondition condition = new TokenIntrospectCondition
            {
                AccessToken = "accessToken"
            };

            mAgencyAccessTokenLogic.Setup(i => i.FindAgencyAccessToken(condition.AccessToken));

            var actual = Assert.Throws<AuthleteException>(() => target.Introspect(condition));
            Assert.AreEqual(ErrorKind.server_error.GetName(), actual.GetErrorCode());
            Assert.AreEqual("アクセストークンの検証に失敗しました。", actual.GetErrorMessage());

            IntrospectTokenServiceResult errorResult = new IntrospectTokenServiceResult();
            Func<int, bool> mockErrorPredicate = (e) => true;
            typeof(AbstractServiceResult<int>).GetField("<ErrorPredicate>k__BackingField", BindingFlags.NonPublic | BindingFlags.Instance).SetValue(errorResult, mockErrorPredicate);
            target.TokenServiceWrapper.Introspect = (x) =>
            {
                return errorResult;
            };

            var actual1 = Assert.Throws<AuthleteException>(() => target.Introspect(condition));
            Assert.AreEqual(ErrorKind.server_error.GetName(), actual1.GetErrorCode());
            Assert.AreEqual("アクセストークンの検証に失敗しました。", actual1.GetErrorMessage());
        }

        /// <summary>
        /// [対象] 検証
        /// [条件] 正常
        /// [結果] 正常終了
        /// </summary>
        [Test]
        public void Introspect()
        {
            IntrospectTokenServiceResult result = new IntrospectTokenServiceResult
            {
                Usable = true,
                Uid = "uid",
                ClientId = "ClientId",
                ExpiresAt = 1,
                Scopes = new string[] { "openid", "vital", "health_check" },
                Refreshable = true
            };

            target.TokenServiceWrapper.Introspect = (x) =>
            {
                return result;
            };

            TokenIntrospectCondition condition = new TokenIntrospectCondition
            {
                AccessToken = "accessToken"
            };

            mAgencyAccessTokenLogic.Setup(i => i.FindAgencyAccessToken(condition.AccessToken));

            TokenIntrospectResultDto actual = target.Introspect(condition);

            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.TokenIntrospect);
            Assert.AreEqual(result.Usable, actual.TokenIntrospect.Usable);
            Assert.AreEqual(result.Uid, actual.TokenIntrospect.Uid);
            Assert.AreEqual(result.ClientId, actual.TokenIntrospect.ClientId);
            Assert.AreEqual(result.ExpiresAt, actual.TokenIntrospect.ExpiresAt);
            Assert.AreEqual(result.Scopes, actual.TokenIntrospect.Scopes);
            Assert.AreEqual(result.Refreshable, actual.TokenIntrospect.Refreshable);
            Assert.IsNull(actual.TokenIntrospect.Agent);
        }

        /// <summary>
        /// [対象] 検証
        /// [条件] 正常
        /// [結果] 正常終了
        /// </summary>
        [Test]
        public void Introspect_NoExpiresAt()
        {
            IntrospectTokenServiceResult result = new IntrospectTokenServiceResult
            {
                Usable = true,
                Uid = "uid",
                ClientId = "ClientId",
                Scopes = new string[] { "openid", "vital", "health_check" },
                Refreshable = true
            };

            target.TokenServiceWrapper.Introspect = (x) =>
            {
                return result;
            };

            TokenIntrospectCondition condition = new TokenIntrospectCondition
            {
                AccessToken = "accessToken"
            };

            mAgencyAccessTokenLogic.Setup(i => i.FindAgencyAccessToken(condition.AccessToken));

            TokenIntrospectResultDto actual = target.Introspect(condition);

            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.TokenIntrospect);
            Assert.AreEqual(result.Usable, actual.TokenIntrospect.Usable);
            Assert.AreEqual(result.Uid, actual.TokenIntrospect.Uid);
            Assert.AreEqual(result.ClientId, actual.TokenIntrospect.ClientId);
            Assert.AreEqual(-1, actual.TokenIntrospect.ExpiresAt);
            Assert.AreEqual(result.Scopes, actual.TokenIntrospect.Scopes);
            Assert.AreEqual(result.Refreshable, actual.TokenIntrospect.Refreshable);
            Assert.IsNull(actual.TokenIntrospect.Agent);
        }

        /// <summary>
        /// [対象] 検証
        /// [条件] 正常
        /// [結果] 正常終了
        /// </summary>
        [Test]
        public void Introspect_IsAgency()
        {
            IntrospectTokenServiceResult result = new IntrospectTokenServiceResult
            {
                Usable = true,
                Uid = "uid",
                ClientId = "ClientId",
                ExpiresAt = 1,
                Scopes = new string[] { "openid", "vital", "health_check" },
                Refreshable = true
            };

            target.TokenServiceWrapper.Introspect = (x) =>
            {
                return result;
            };

            TokenIntrospectCondition condition = new TokenIntrospectCondition
            {
                AccessToken = "accessToken"
            };

            AgencyAccessToken token = new AgencyAccessToken
            {
                User = "agent"
            };
            mAgencyAccessTokenLogic.Setup(i => i.FindAgencyAccessToken(condition.AccessToken)).Returns(token);

            TokenIntrospectResultDto actual = target.Introspect(condition);

            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.TokenIntrospect);
            Assert.AreEqual(result.Usable, actual.TokenIntrospect.Usable);
            Assert.AreEqual(result.Uid, actual.TokenIntrospect.Uid);
            Assert.AreEqual(result.ClientId, actual.TokenIntrospect.ClientId);
            Assert.AreEqual(result.ExpiresAt, actual.TokenIntrospect.ExpiresAt);
            Assert.AreEqual(result.Scopes, actual.TokenIntrospect.Scopes);
            Assert.AreEqual(result.Refreshable, actual.TokenIntrospect.Refreshable);
            Assert.AreEqual(token.User, actual.TokenIntrospect.Agent);
        }

        /// <summary>
        /// [対象] リフレッシュ
        /// [条件] 正常
        /// [結果] エラー
        /// </summary>
        [Test]
        public void Refresh_NullError()
        {
            target.TokenServiceWrapper.RefreshToken = (c) =>
            {
                return null;
            };

            TokenRefreshCondition condition = new TokenRefreshCondition
            {
                RefreshToken = "RefreshToken"
            };
            var actual1 = Assert.Throws<AuthleteException>(() => target.Refresh(condition));
            Assert.AreEqual(ErrorKind.server_error.GetName(), actual1.GetErrorCode());
            Assert.AreEqual("アクセストークンのリフレッシュに失敗しました。", actual1.GetErrorMessage());
        }

        /// <summary>
        /// [対象] リフレッシュ
        /// [条件] 正常
        /// [結果] エラー
        /// </summary>
        [Test]
        public void Refresh_HasError()
        {
            TokenServiceResult errorResult = new TokenServiceResult();
            Func<int, bool> mockErrorPredicate = (e) => true;
            typeof(AbstractServiceResult<int>).GetField("<ErrorPredicate>k__BackingField", BindingFlags.NonPublic | BindingFlags.Instance).SetValue(errorResult, mockErrorPredicate);
            errorResult.ResultMessage = "ResultMessage";
            target.TokenServiceWrapper.RefreshToken = (c) =>
            {
                return errorResult;
            };

            TokenRefreshCondition condition = new TokenRefreshCondition
            {
                RefreshToken = "RefreshToken"
            };
            var actual1 = Assert.Throws<AuthleteException>(() => target.Refresh(condition));
            Assert.AreEqual(ErrorKind.server_error.GetName(), actual1.GetErrorCode());
            Assert.AreEqual("アクセストークンのリフレッシュに失敗しました。", actual1.GetErrorMessage());
        }

        /// <summary>
        /// [対象] リフレッシュ
        /// [条件] 正常
        /// [結果] 正常終了
        /// </summary>
        [Test]
        public void Refresh()
        {
            TokenServiceResult result = new TokenServiceResult
            {
                AccessToken = "AccessToken",
                RefreshToken = "RefreshToken"
            };

            target.TokenServiceWrapper.RefreshToken = (c) =>
            {
                return result;
            };

            TokenRefreshCondition condition = new TokenRefreshCondition
            {
                RefreshToken = "RefreshToken"
            };
            TokenRefreshResultDto actual = target.Refresh(condition);

            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.TokenRefresh);
            Assert.AreEqual(result.AccessToken, actual.TokenRefresh.AccessToken);
            Assert.AreEqual(result.RefreshToken, actual.TokenRefresh.RefreshToken);
        }
    }
}