﻿using CaradaAuthProvider.Api.Controllers.Conditions;
using CaradaAuthProvider.Api.Domain.Dtos;
using CaradaAuthProvider.Api.Domain.Logics;
using CaradaAuthProvider.Api.Exceptions;
using CaradaAuthProvider.Api.Exceptions.Enums;
using CaradaAuthProvider.Api.IdentityManagers;
using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using CaradaAuthProvider.Api.Persistence.Storage.Entities;
using CaradaAuthProvider.Api.Services;
using CaradaAuthProvider.Api.Services.Dtos;
using CaradaAuthProvider.Api.Utils;
using Microsoft.AspNet.Identity;
using Moq;
using NUnit.Framework;
using System;

namespace CaradaAuthProvider.Api.Test.Services
{
    [TestFixture]
    public class UserServiceTest
    {

        // テスト対象クラス
        private UserService target;


        // テスト対象クラスのモック
        private class MockUserService : UserService
        {
            public MockUserService(
                TicketLogic ticketLogic,
                SecurityQuestionLogic securityQuestionLogic,
                MailLogic mailLogic,
                CaradaAuthProviderDbContext dbContext,
                ApplicationUserManager userManager) : base(dbContext, userManager)
            {
                this.ticketLogic = ticketLogic;
                this.securityQuestionLogic = securityQuestionLogic;
                this.mailLogic = mailLogic;
            }
        }

        // DBコンストモック
        private Mock<CaradaAuthProviderDbContext> mockDbContext;
        // ユーザーマネージャーモック
        private Mock<ApplicationUserManager> mockUserManager;
        // チケットロジックモック
        private Mock<TicketLogic> mockTicketLogic;
        // 秘密の質問ロジックモック
        private Mock<SecurityQuestionLogic> mockSecurityQuestionLogic;
        // メールロジックモック
        private Mock<MailLogic> mockMailLogic;

        private CaradaIdUser user;

        private class MockIdentityResult : IdentityResult
        {
            public MockIdentityResult(bool success) : base(success)
            {
            }
        }

        // イニシャライズ
        [SetUp]
        public void Initialize()
        {
            Mock<IUserStore<CaradaIdUser>> mockStore = new Mock<IUserStore<CaradaIdUser>>();
            mockDbContext = new Mock<CaradaAuthProviderDbContext>();
            mockUserManager = new Mock<ApplicationUserManager>(mockStore.Object);
            mockTicketLogic = new Mock<TicketLogic>();
            mockSecurityQuestionLogic = new Mock<SecurityQuestionLogic>();
            mockMailLogic = new Mock<MailLogic>();
            target = new MockUserService(mockTicketLogic.Object, mockSecurityQuestionLogic.Object, mockMailLogic.Object, mockDbContext.Object, mockUserManager.Object);
            user = new CaradaIdUser()
            {
                Id = "userId",
                EmailConfirmed = false,
                UserName = "太郎",
                Email = "email@test.com"
            };
        }


        // -----------------------------------------------------------------------------------------------------------------------------------------------------
        // UserInfo
        // -----------------------------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void UserInfo_UserInfoNull()
        {
            // パラメータ
            UserInfoCondition condition = new UserInfoCondition()
            {
                Uid = "124"
            };

            // モック
            mockUserManager.Setup(p => p.FindById(It.IsAny<string>()));

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.UserInfo(condition));

            // 検証
            Assert.AreEqual(ErrorKind.data_mismatch.GetName(), actual.GetErrorCode());
            Assert.AreEqual("該当のユーザが見つかりませんでした。", actual.GetErrorMessage());
        }

        [Test]
        public void UserInfo_SecurityQuestionNull()
        {
            // パラメータ
            UserInfoCondition condition = new UserInfoCondition()
            {
                Uid = "124"
            };

            // モック
            mockUserManager.Setup(p => p.FindById(It.IsAny<string>())).Returns(user);
            SecurityQuestionMasters ret = null;
            mockSecurityQuestionLogic.Setup(p => p.FindSecurityQuestion(It.IsAny<string>())).Returns(ret);

            // 実施
            UserInfoResultDto actual = target.UserInfo(condition);

            // 検証
            Assert.AreEqual("太郎", actual.CaradaId);
            Assert.AreEqual("email@test.com", actual.Email);
            Assert.AreEqual(false, actual.EmailConfirmed);
            Assert.IsNull(actual.SecretQuestion);
        }

        [Test]
        public void UserInfo()
        {
            // パラメータ
            UserInfoCondition condition = new UserInfoCondition()
            {
                Uid = "124"
            };


            // モック
            mockUserManager.Setup(p => p.FindById(It.IsAny<string>())).Returns(user);
            mockSecurityQuestionLogic.Setup(p => p.FindSecurityQuestion(It.IsAny<string>())).Returns(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 777,
                Question = "ヒ・ミ・ツ"
            });

            // 実施
            UserInfoResultDto actual = target.UserInfo(condition);

            // 検証
            Assert.AreEqual("太郎", actual.CaradaId);
            Assert.AreEqual("email@test.com", actual.Email);
            Assert.AreEqual(false, actual.EmailConfirmed);
            Assert.AreEqual(777, actual.SecretQuestion.SecretQuestionId);
            Assert.AreEqual("ヒ・ミ・ツ", actual.SecretQuestion.SecretQuestionSentence);
        }


        // -----------------------------------------------------------------------------------------------------------------------------------------------------
        // VerifyPassword
        // -----------------------------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void VerifyPassword_UserInfoNull()
        {
            // パラメータ
            VerifyPasswordCondition condition = new VerifyPasswordCondition()
            {
                Uid = "124"
            };

            // モック
            mockUserManager.Setup(p => p.FindById(It.IsAny<string>()));

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.VerifyPassword(condition));

            // 検証
            Assert.AreEqual(ErrorKind.data_mismatch.GetName(), actual.GetErrorCode());
            Assert.AreEqual("該当のユーザが見つかりませんでした。", actual.GetErrorMessage());
        }

        [Test]
        public void VerifyPassword_AccountLockout()
        {
            // パラメータ
            VerifyPasswordCondition condition = new VerifyPasswordCondition()
            {
                Uid = "124"
            };

            // モック
            mockUserManager.Setup(p => p.FindById(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.AlreadyLockedOut);
            mockUserManager.Setup(p => p.ResetAccessFailedCount(user.Id)).Throws(new Exception());


            // 実施
            var actual = Assert.Throws<CommitCaradaApplicationException>(() => target.VerifyPassword(condition));

            // 検証
            Assert.AreEqual(ErrorKind.account_lockout.GetName(), actual.GetErrorCode());
            Assert.AreEqual("アカウントロック中です。", actual.GetErrorMessage());
        }

        [Test]
        public void VerifyPassword_AccountLockoutStart()
        {
            // パラメータ
            VerifyPasswordCondition condition = new VerifyPasswordCondition()
            {
                Uid = "124"
            };

            // モック
            mockUserManager.Setup(p => p.FindById(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.LockedOut);
            mockUserManager.Setup(p => p.ResetAccessFailedCount(user.Id)).Throws(new Exception());

            // 実施
            var actual = Assert.Throws<CommitCaradaApplicationException>(() => target.VerifyPassword(condition));

            // 検証
            Assert.AreEqual(ErrorKind.account_lockout.GetName(), actual.GetErrorCode());
            Assert.AreEqual("アカウントがロックされました。", actual.GetErrorMessage());
        }


        [Test]
        public void VerifyPassword_PasswordMismatch()
        {
            // パラメータ
            VerifyPasswordCondition condition = new VerifyPasswordCondition()
            {
                Uid = "124"
            };

            // モック
            mockUserManager.Setup(p => p.FindById(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.Failure);
            mockUserManager.Setup(p => p.ResetAccessFailedCount(user.Id)).Throws(new Exception());

            // 実施
            var actual = Assert.Throws<CommitCaradaApplicationException>(() => target.VerifyPassword(condition));

            // 検証
            Assert.AreEqual(ErrorKind.password_mismatch.GetName(), actual.GetErrorCode());
            Assert.AreEqual("パスワードが一致しませんでした。", actual.GetErrorMessage());
        }


        [Test]
        public void VerifyPassword()
        {
            // パラメータ
            VerifyPasswordCondition condition = new VerifyPasswordCondition()
            {
                Uid = "124"
            };

            // モック
            mockUserManager.Setup(p => p.FindById(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.Success);
            mockUserManager.Setup(p => p.ResetAccessFailedCount(user.Id));

            mockTicketLogic.Setup(p => p.IssueTicket(It.IsAny<TicketKind>(), It.IsAny<CaradaIdUser>(), It.IsAny<long>(), It.IsAny<string>(), It.IsAny<string>())).Returns(new IssuedTicketDto()
            {
                TicketId = "asdf",
                Ticket = new Ticket()
                {
                    ExpiresDateTime = new System.DateTime(123456789)
                }
            });

            // 実施
            VerifyPasswordResultDto actual = target.VerifyPassword(condition);

            // 検証
            Assert.AreEqual("asdf", actual.Ticket.Id);
            Assert.AreEqual(DateUtilities.GetEpochTime(new System.DateTime(123456789)), actual.Ticket.ExpiresAt);
        }


        // -----------------------------------------------------------------------------------------------------------------------------------------------------
        // RenewPassword
        // -----------------------------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void RenewPassword_TicketNull()
        {
            // パラメータ
            RenewPasswordCondition condition = new RenewPasswordCondition();

            // モック
            Ticket ret = null;
            mockTicketLogic.Setup(p => p.FindTicket(It.IsAny<string>())).Returns(ret);

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.RenewPassword(condition));

            // 検証
            Assert.AreEqual(ErrorKind.invalid_ticket.GetName(), actual.GetErrorCode());
            Assert.AreEqual("該当のチケットが見つかりませんでした。", actual.GetErrorMessage());
        }

        [Test]
        public void RenewPasswarod_InvalidKind()
        {
            // パラメータ
            RenewPasswordCondition condition = new RenewPasswordCondition();

            // モック
            Ticket ret = new Ticket()
            {
                Kind = TicketKind.General
            };
            mockTicketLogic.Setup(p => p.FindTicket(It.IsAny<string>())).Returns(ret);

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.RenewPassword(condition));

            // 検証
            Assert.AreEqual(ErrorKind.invalid_ticket.GetName(), actual.GetErrorCode());
            Assert.AreEqual("種別の異なるチケットが指定されました。", actual.GetErrorMessage());
        }

        [Test]
        public void RenewPasswarod_Expired()
        {
            // パラメータ
            RenewPasswordCondition condition = new RenewPasswordCondition();

            // モック
            Ticket ret = new Ticket()
            {
                Kind = TicketKind.PasswordValid,
                ExpiresDateTime = System.DateTime.Parse("2016-05-01 00:00:00")
            };
            mockTicketLogic.Setup(p => p.FindTicket(It.IsAny<string>())).Returns(ret);

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.RenewPassword(condition));

            // 検証
            Assert.AreEqual(ErrorKind.invalid_ticket.GetName(), actual.GetErrorCode());
            Assert.AreEqual("チケットの有効期限が切れています。", actual.GetErrorMessage());
        }

        [Test]
        public void RenewPasswarod_UserInfoNull()
        {
            // パラメータ
            RenewPasswordCondition condition = new RenewPasswordCondition();

            // モック
            Ticket ret = new Ticket()
            {
                Kind = TicketKind.PasswordValid,
                ExpiresDateTime = System.DateTime.Parse("9999-12-31 23:59:59")
            };
            mockTicketLogic.Setup(p => p.FindTicket(It.IsAny<string>())).Returns(ret);
            mockUserManager.Setup(p => p.FindById(It.IsAny<string>()));

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.RenewPassword(condition));

            // 検証
            Assert.AreEqual(ErrorKind.data_mismatch.GetName(), actual.GetErrorCode());
            Assert.AreEqual("該当のユーザが見つかりませんでした。", actual.GetErrorMessage());
        }

        [Test]
        public void RenewPassword()
        {
            // パラメータ
            RenewPasswordCondition condition = new RenewPasswordCondition();

            // モック
            Ticket ret = new Ticket()
            {
                Kind = TicketKind.PasswordValid,
                ExpiresDateTime = System.DateTime.Parse("9999-12-31 23:00:00")
            };
            mockTicketLogic.Setup(p => p.FindTicket(It.IsAny<string>())).Returns(ret);
            mockTicketLogic.Setup(p => p.RegisterTicket(condition.TicketId, It.IsAny<Ticket>())).Callback<string, Ticket>((x, y) =>
            {
                Assert.AreEqual(System.DateTime.Parse("9999-12-31 23:01:00"), y.ExpiresDateTime);
                Assert.True(y.ProcessedFlag);
            });
            mockUserManager.Setup(p => p.FindById(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.Update(It.IsAny<CaradaIdUser>()));
            mockUserManager.Setup(p => p.ResetPassword(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));
            mockUserManager.Setup(p => p.UpdateSecurityStamp(It.IsAny<string>()));

            // 実施
            ResultDto actual = target.RenewPassword(condition);

            // 検証
            Assert.IsNotNull(actual);
        }


        // -----------------------------------------------------------------------------------------------------------------------------------------------------
        // RenewPassword
        // -----------------------------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void ResetPassword_UserInfoNull()
        {
            // パラメータ
            ResetPasswordCondition condition = new ResetPasswordCondition();

            // モック
            mockUserManager.Setup(p => p.FindByEmail(It.IsAny<string>()));

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.ResetPassword(condition));

            // 検証
            Assert.AreEqual(ErrorKind.user_not_exist.GetName(), actual.GetErrorCode());
            Assert.AreEqual("該当のユーザが見つかりませんでした。", actual.GetErrorMessage());
        }

        [Test]
        public void ResetPassword_UpdateFaill()
        {
            // パラメータ
            ResetPasswordCondition condition = new ResetPasswordCondition();

            // モック
            mockUserManager.Setup(p => p.FindByEmail(It.IsAny<string>())).Returns(user);
            IdentityResult result = new IdentityResult();
            mockUserManager.Setup(p => p.Update(It.IsAny<CaradaIdUser>())).Returns(result);

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.ResetPassword(condition));

            // 検証
            Assert.AreEqual(ErrorKind.server_error.GetName(), actual.GetErrorCode());
            Assert.AreEqual("ユーザ情報の更新に失敗しました。", actual.GetErrorMessage());
        }


        [Test]
        public void ResetPassword()
        {
            // パラメータ
            ResetPasswordCondition condition = new ResetPasswordCondition()
            {
                Subject = "パスワード変更のお願い",
                Body = "システム統合の為下記URLからパスワード変更をお願いします。"
            };


            // モック
            mockUserManager.Setup(p => p.FindByEmail(It.IsAny<string>())).Returns(user);
            IdentityResult result = IdentityResult.Success;
            mockUserManager.Setup(p => p.Update(It.IsAny<CaradaIdUser>())).Returns(result);
            mockTicketLogic.Setup(p => p.IssueTicket(It.IsAny<TicketKind>(), It.IsAny<CaradaIdUser>(), It.IsAny<long>(), It.IsAny<string>(), It.IsAny<string>())).Returns(new IssuedTicketDto()
            {
                TicketId = "asdf",
                Ticket = new Ticket()
                {
                    ExpiresDateTime = new System.DateTime(123456789)
                }
            });
            mockMailLogic.Setup(p => p.SendMailAsync(It.IsAny<MailSendInfoDto>()));

            // 実施
            ResetPasswordResultDto actual = target.ResetPassword(condition);

            // 検証
            Assert.IsNotNull(actual);
            Assert.AreEqual("asdf", actual.Ticket.Id);
        }


        // -----------------------------------------------------------------------------------------------------------------------------------------------------
        // StartRenewEmail
        // -----------------------------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void StartRenewRmail_UserInfoNull()
        {
            // パラメータ
            StartRenewEmailCondition condition = new StartRenewEmailCondition();

            // モック
            mockUserManager.Setup(p => p.FindById(It.IsAny<string>()));

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.StartRenewEmail(condition));

            // 検証
            Assert.AreEqual(ErrorKind.data_mismatch.GetName(), actual.GetErrorCode());
            Assert.AreEqual("該当のユーザが見つかりませんでした。", actual.GetErrorMessage());
        }


        [Test]
        public void StartRenewRmail_SameEmail()
        {
            // パラメータ
            StartRenewEmailCondition condition = new StartRenewEmailCondition()
            {
                Email = "email@test.com"
            };

            // モック
            mockUserManager.Setup(p => p.FindById(It.IsAny<string>())).Returns(user);

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.StartRenewEmail(condition));

            // 検証
            Assert.AreEqual(ErrorKind.same_email.GetName(), actual.GetErrorCode());
            Assert.AreEqual("現在のメールアドレスと同じものが指定されています。", actual.GetErrorMessage());
        }


        [Test]
        public void StartRenewRmail_AccountLockout()
        {
            // パラメータ
            StartRenewEmailCondition condition = new StartRenewEmailCondition()
            {
                Uid = "124"
            };

            // モック
            mockUserManager.Setup(p => p.FindById(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.AlreadyLockedOut);
            mockUserManager.Setup(p => p.ResetAccessFailedCount(user.Id)).Throws(new Exception());

            // 実施
            var actual = Assert.Throws<CommitCaradaApplicationException>(() => target.StartRenewEmail(condition));

            // 検証
            Assert.AreEqual(ErrorKind.account_lockout.GetName(), actual.GetErrorCode());
            Assert.AreEqual("アカウントロック中です。", actual.GetErrorMessage());
        }

        [Test]
        public void StartRenewRmail_AccountLockoutStart()
        {
            // パラメータ
            StartRenewEmailCondition condition = new StartRenewEmailCondition()
            {
                Uid = "124"
            };

            // モック
            mockUserManager.Setup(p => p.FindById(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.LockedOut);
            mockUserManager.Setup(p => p.ResetAccessFailedCount(user.Id)).Throws(new Exception());

            // 実施
            var actual = Assert.Throws<CommitCaradaApplicationException>(() => target.StartRenewEmail(condition));

            // 検証
            Assert.AreEqual(ErrorKind.account_lockout.GetName(), actual.GetErrorCode());
            Assert.AreEqual("アカウントがロックされました。", actual.GetErrorMessage());
        }


        [Test]
        public void StartRenewRmail_PasswordMismatch()
        {
            // パラメータ
            StartRenewEmailCondition condition = new StartRenewEmailCondition()
            {
                Uid = "124"
            };

            // モック
            mockUserManager.Setup(p => p.FindById(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.Failure);
            mockUserManager.Setup(p => p.ResetAccessFailedCount(user.Id)).Throws(new Exception());

            // 実施
            var actual = Assert.Throws<CommitCaradaApplicationException>(() => target.StartRenewEmail(condition));

            // 検証
            Assert.AreEqual(ErrorKind.password_mismatch.GetName(), actual.GetErrorCode());
            Assert.AreEqual("パスワードが一致しませんでした。", actual.GetErrorMessage());
        }

        [Test]
        public void StartRenewRmail_RegisteredEmail()
        {
            // パラメータ
            StartRenewEmailCondition condition = new StartRenewEmailCondition()
            {
                Uid = "124"
            };

            // モック
            mockUserManager.Setup(p => p.FindById(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.Success);
            mockUserManager.Setup(p => p.FindByEmail(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ResetAccessFailedCount(user.Id));

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.StartRenewEmail(condition));

            // 検証
            Assert.AreEqual(ErrorKind.registered_email.GetName(), actual.GetErrorCode());
            Assert.AreEqual("メールアドレスが既に登録されています。", actual.GetErrorMessage());
        }

        [Test]
        public void StartRenewRmail()
        {
            // パラメータ
            StartRenewEmailCondition condition = new StartRenewEmailCondition()
            {
                Uid = "124"
            };

            // モック
            mockUserManager.Setup(p => p.FindById(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.Success);
            mockUserManager.Setup(p => p.ResetAccessFailedCount(user.Id));
            mockUserManager.Setup(p => p.FindByEmail(It.IsAny<string>()));
            mockTicketLogic.Setup(p => p.IssueTicket(It.IsAny<TicketKind>(), It.IsAny<CaradaIdUser>(), It.IsAny<long>(), It.IsAny<string>(), It.IsAny<string>())).Returns(new IssuedTicketDto()
            {
                TicketId = "asdf",
                Ticket = new Ticket()
                {
                    ExpiresDateTime = new System.DateTime(123456789)
                }
            });

            // 実施
            StartRenewEmailResultDto actual = target.StartRenewEmail(condition);

            // 検証
            Assert.IsNotNull(actual);
            Assert.AreEqual("asdf", actual.Ticket.Id);
        }


        // -----------------------------------------------------------------------------------------------------------------------------------------------------
        // SendRenewEmailVerifyCode
        // -----------------------------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void SendRenewEmailVerifyCode_TicketNull()
        {
            // パラメータ
            SendRenewEmailVerifyCodeCondition condition = new SendRenewEmailVerifyCodeCondition();

            // モック
            Ticket ret = null;
            mockTicketLogic.Setup(p => p.FindTicket(It.IsAny<string>())).Returns(ret);

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.SendRenewEmailVerifyCode(condition));

            // 検証
            Assert.AreEqual(ErrorKind.invalid_ticket.GetName(), actual.GetErrorCode());
            Assert.AreEqual("該当のチケットが見つかりませんでした。", actual.GetErrorMessage());
        }

        [Test]
        public void SendRenewEmailVerifyCode_InvalidKind()
        {
            // パラメータ
            SendRenewEmailVerifyCodeCondition condition = new SendRenewEmailVerifyCodeCondition();

            // モック
            Ticket ret = new Ticket()
            {
                Kind = TicketKind.General
            };
            mockTicketLogic.Setup(p => p.FindTicket(It.IsAny<string>())).Returns(ret);

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.SendRenewEmailVerifyCode(condition));

            // 検証
            Assert.AreEqual(ErrorKind.invalid_ticket.GetName(), actual.GetErrorCode());
            Assert.AreEqual("種別の異なるチケットが指定されました。", actual.GetErrorMessage());
        }

        [Test]
        public void SendRenewEmailVerifyCode_Expired()
        {
            // パラメータ
            SendRenewEmailVerifyCodeCondition condition = new SendRenewEmailVerifyCodeCondition();

            // モック
            Ticket ret = new Ticket()
            {
                Kind = TicketKind.RenewEmail,
                ExpiresDateTime = System.DateTime.Parse("2016-05-01 00:00:00")
            };
            mockTicketLogic.Setup(p => p.FindTicket(It.IsAny<string>())).Returns(ret);

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.SendRenewEmailVerifyCode(condition));

            // 検証
            Assert.AreEqual(ErrorKind.invalid_ticket.GetName(), actual.GetErrorCode());
            Assert.AreEqual("チケットの有効期限が切れています。", actual.GetErrorMessage());
        }

        [Test]
        public void SendRenewEmailVerifyCode_RegisteredEmail()
        {
            // パラメータ
            StartRenewEmailCondition condition = new StartRenewEmailCondition()
            {
                Uid = "124"
            };

            // モック
            Ticket ret = new Ticket()
            {
                Kind = TicketKind.RenewEmail,
                ExpiresDateTime = System.DateTime.Parse("2016-05-01 00:00:00")
            };
            mockTicketLogic.Setup(p => p.FindTicket(It.IsAny<string>())).Returns(ret);
            mockUserManager.Setup(p => p.FindById(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.Success);
            mockUserManager.Setup(p => p.ResetAccessFailedCount(user.Id));
            mockUserManager.Setup(p => p.FindByEmail(It.IsAny<string>())).Returns(user);


            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.StartRenewEmail(condition));

            // 検証
            Assert.AreEqual(ErrorKind.registered_email.GetName(), actual.GetErrorCode());
            Assert.AreEqual("メールアドレスが既に登録されています。", actual.GetErrorMessage());
        }

        [Test]
        public void SendRenewEmailVerifyCode()
        {
            // パラメータ
            SendRenewEmailVerifyCodeCondition condition = new SendRenewEmailVerifyCodeCondition()
            {
                Subject = "表題",
                Body = "本文"
            };

            // モック
            Ticket ret = new Ticket()
            {
                Kind = TicketKind.RenewEmail,
                ExpiresDateTime = System.DateTime.Parse("9999-12-31 00:00:00")
            };
            mockTicketLogic.Setup(p => p.FindTicket(It.IsAny<string>())).Returns(ret);
            mockUserManager.Setup(p => p.FindById(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.Success);
            mockUserManager.Setup(p => p.ResetAccessFailedCount(user.Id));
            mockUserManager.Setup(p => p.FindByEmail(It.IsAny<string>()));
            mockMailLogic.Setup(p => p.SendMailAsync(It.IsAny<MailSendInfoDto>()));
            mockTicketLogic.Setup(p => p.RegisterTicket(It.IsAny<string>(), It.IsAny<Ticket>()));

            // 実施
            ResultDto actual = target.SendRenewEmailVerifyCode(condition);

            // 検証
            Assert.IsNotNull(actual);
        }



        // -----------------------------------------------------------------------------------------------------------------------------------------------------
        // RenewEmail
        // -----------------------------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void RenewEmail_TicketNull()
        {
            // パラメータ
            RenewEmailCondition condition = new RenewEmailCondition();

            // モック
            Ticket ret = null;
            mockTicketLogic.Setup(p => p.FindTicket(It.IsAny<string>())).Returns(ret);

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.RenewEmail(condition));

            // 検証
            Assert.AreEqual(ErrorKind.invalid_ticket.GetName(), actual.GetErrorCode());
            Assert.AreEqual("該当のチケットが見つかりませんでした。", actual.GetErrorMessage());
        }

        [Test]
        public void RenewEmail_InvalidKind()
        {
            // パラメータ
            RenewEmailCondition condition = new RenewEmailCondition();

            // モック
            Ticket ret = new Ticket()
            {
                Kind = TicketKind.General
            };
            mockTicketLogic.Setup(p => p.FindTicket(It.IsAny<string>())).Returns(ret);

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.RenewEmail(condition));

            // 検証
            Assert.AreEqual(ErrorKind.invalid_ticket.GetName(), actual.GetErrorCode());
            Assert.AreEqual("種別の異なるチケットが指定されました。", actual.GetErrorMessage());
        }

        [Test]
        public void RenewEmail_Expired()
        {
            // パラメータ
            RenewEmailCondition condition = new RenewEmailCondition();

            // モック
            Ticket ret = new Ticket()
            {
                Kind = TicketKind.RenewEmail,
                ExpiresDateTime = System.DateTime.Parse("2016-05-01 00:00:00")
            };
            mockTicketLogic.Setup(p => p.FindTicket(It.IsAny<string>())).Returns(ret);

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.RenewEmail(condition));

            // 検証
            Assert.AreEqual(ErrorKind.invalid_ticket.GetName(), actual.GetErrorCode());
            Assert.AreEqual("チケットの有効期限が切れています。", actual.GetErrorMessage());
        }

        [Test]
        public void RenewEmail_UserInfoNull()
        {
            // パラメータ
            RenewEmailCondition condition = new RenewEmailCondition();

            // モック
            Ticket ret = new Ticket()
            {
                Kind = TicketKind.RenewEmail,
                ExpiresDateTime = System.DateTime.Parse("9999-05-01 00:00:00")
            };
            mockTicketLogic.Setup(p => p.FindTicket(It.IsAny<string>())).Returns(ret);
            mockUserManager.Setup(p => p.FindById(It.IsAny<string>()));

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.RenewEmail(condition));

            // 検証
            Assert.AreEqual(ErrorKind.data_mismatch.GetName(), actual.GetErrorCode());
            Assert.AreEqual("該当のユーザが見つかりませんでした。", actual.GetErrorMessage());
        }

        [Test]
        public void RenewEmail_RegisteredEmail()
        {
            // パラメータ
            RenewEmailCondition condition = new RenewEmailCondition();

            // モック
            Ticket ret = new Ticket()
            {
                Kind = TicketKind.RenewEmail,
                ExpiresDateTime = System.DateTime.Parse("9999-05-01 00:00:00")
            };
            mockTicketLogic.Setup(p => p.FindTicket(It.IsAny<string>())).Returns(ret);
            mockUserManager.Setup(p => p.FindById(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.FindByEmail(It.IsAny<string>())).Returns(user);

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.RenewEmail(condition));

            // 検証
            Assert.AreEqual(ErrorKind.registered_email.GetName(), actual.GetErrorCode());
            Assert.AreEqual("メールアドレスが既に登録されています。", actual.GetErrorMessage());
        }


        [Test]
        public void RenewEmail_UpdateFail()
        {
            // パラメータ
            RenewEmailCondition condition = new RenewEmailCondition();

            // モック
            Ticket ret = new Ticket()
            {
                Kind = TicketKind.RenewEmail,
                ExpiresDateTime = System.DateTime.Parse("9999-05-01 00:00:00")
            };
            mockTicketLogic.Setup(p => p.FindTicket(It.IsAny<string>())).Returns(ret);
            mockUserManager.Setup(p => p.FindById(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.FindByEmail(It.IsAny<string>()));
            mockUserManager.Setup(p => p.Update(It.IsAny<CaradaIdUser>())).Returns(new IdentityResult());

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.RenewEmail(condition));

            // 検証
            Assert.AreEqual(ErrorKind.server_error.GetName(), actual.GetErrorCode());
            Assert.AreEqual("ユーザ情報の更新に失敗しました。", actual.GetErrorMessage());
        }


        [Test]
        public void RenewEmail()
        {
            // パラメータ
            RenewEmailCondition condition = new RenewEmailCondition()
            {
                Subject = "表題",
                Body = "本文"
            };

            // モック
            Ticket ret = new Ticket()
            {
                Kind = TicketKind.RenewEmail,
                ExpiresDateTime = System.DateTime.Parse("9999-05-01 00:00:00")
            };
            mockTicketLogic.Setup(p => p.FindTicket(It.IsAny<string>())).Returns(ret);
            mockUserManager.Setup(p => p.FindById(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.FindByEmail(It.IsAny<string>()));
            IdentityResult uret = IdentityResult.Success;
            mockUserManager.Setup(p => p.Update(It.IsAny<CaradaIdUser>())).Returns(uret);

            // 実施
            ResultDto actual = target.RenewEmail(condition);

            // 検証
            Assert.IsNotNull(actual);
        }


        // -----------------------------------------------------------------------------------------------------------------------------------------------------
        // ResetEmail
        // -----------------------------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void ResetEmail_UserInfoNull()
        {
            // パラメータ
            ResetEmailCondition condition = new ResetEmailCondition();

            // モック
            mockUserManager.Setup(p => p.FindByName(It.IsAny<string>()));

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.ResetEmail(condition));

            // 検証
            Assert.AreEqual(ErrorKind.user_not_exist.GetName(), actual.GetErrorCode());
            Assert.AreEqual("該当のユーザが見つかりませんでした。", actual.GetErrorMessage());
        }



        [Test]
        public void ResetEmail_AccountLockout()
        {
            // パラメータ
            ResetEmailCondition condition = new ResetEmailCondition()
            {
            };

            // モック
            mockUserManager.Setup(p => p.FindByName(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.AlreadyLockedOut);

            // 実施
            var actual = Assert.Throws<CommitCaradaApplicationException>(() => target.ResetEmail(condition));

            // 検証
            Assert.AreEqual(ErrorKind.account_lockout.GetName(), actual.GetErrorCode());
            Assert.AreEqual("アカウントロック中です。", actual.GetErrorMessage());
        }

        [Test]
        public void ResetEmail_AccountLockoutStart()
        {
            // パラメータ
            ResetEmailCondition condition = new ResetEmailCondition()
            {
            };

            // モック
            mockUserManager.Setup(p => p.FindByName(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.LockedOut);

            // 実施
            var actual = Assert.Throws<CommitCaradaApplicationException>(() => target.ResetEmail(condition));

            // 検証
            Assert.AreEqual(ErrorKind.account_lockout.GetName(), actual.GetErrorCode());
            Assert.AreEqual("アカウントがロックされました。", actual.GetErrorMessage());
        }


        [Test]
        public void ResetEmail_PasswordMismatch()
        {
            // パラメータ
            ResetEmailCondition condition = new ResetEmailCondition()
            {
            };

            // モック
            mockUserManager.Setup(p => p.FindByName(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.Failure);

            // 実施
            var actual = Assert.Throws<CommitCaradaApplicationException>(() => target.ResetEmail(condition));

            // 検証
            Assert.AreEqual(ErrorKind.password_mismatch.GetName(), actual.GetErrorCode());
            Assert.AreEqual("パスワードが一致しませんでした。", actual.GetErrorMessage());
        }

        [Test]
        public void ResetEmail_AccountLockoutStartAnswer()
        {
            // パラメータ
            ResetEmailCondition condition = new ResetEmailCondition()
            {
            };

            // モック
            mockUserManager.Setup(p => p.FindByName(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.Success);

            mockUserManager.Setup(p => p.AccessFailed(It.IsAny<string>()));
            mockSecurityQuestionLogic.Setup(p => p.CheckAnswer(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>())).Returns(false);
            mockUserManager.Setup(p => p.IsLockedOut(It.IsAny<string>())).Returns(true);

            // 実施
            var actual = Assert.Throws<CommitCaradaApplicationException>(() => target.ResetEmail(condition));

            // 検証
            Assert.AreEqual(ErrorKind.account_lockout.GetName(), actual.GetErrorCode());
            Assert.AreEqual("秘密の質問の回答不一致によりアカウントがロックされました。", actual.GetErrorMessage());
        }


        [Test]
        public void ResetEmail_AnswerMismatch()
        {
            // パラメータ
            ResetEmailCondition condition = new ResetEmailCondition()
            {
            };

            // モック
            mockUserManager.Setup(p => p.FindByName(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.Success);

            mockUserManager.Setup(p => p.AccessFailed(It.IsAny<string>()));
            mockSecurityQuestionLogic.Setup(p => p.CheckAnswer(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>())).Returns(false);
            mockUserManager.Setup(p => p.IsLockedOut(It.IsAny<string>())).Returns(false);

            // 実施
            var actual = Assert.Throws<CommitCaradaApplicationException>(() => target.ResetEmail(condition));

            // 検証
            Assert.AreEqual(ErrorKind.answer_mismatch.GetName(), actual.GetErrorCode());
            Assert.AreEqual("秘密の質問の回答が一致しません。", actual.GetErrorMessage());
        }


        [Test]
        public void ResetEmail_SameRmail()
        {
            // パラメータ
            ResetEmailCondition condition = new ResetEmailCondition()
            {
            };

            // モック
            mockUserManager.Setup(p => p.FindByName(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.Success);

            mockUserManager.Setup(p => p.AccessFailed(It.IsAny<string>()));
            mockSecurityQuestionLogic.Setup(p => p.CheckAnswer(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>())).Returns(true);
            mockUserManager.Setup(p => p.FindByEmail(It.IsAny<string>())).Returns(user);


            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.ResetEmail(condition));

            // 検証
            Assert.AreEqual(ErrorKind.same_email.GetName(), actual.GetErrorCode());
            Assert.AreEqual("現在のメールアドレスと同じものが指定されています。", actual.GetErrorMessage());
        }


        [Test]
        public void ResetEmail_RegisteredEmail()
        {
            // パラメータ
            ResetEmailCondition condition = new ResetEmailCondition()
            {
            };

            // モック
            mockUserManager.Setup(p => p.FindByName(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.Success);

            mockUserManager.Setup(p => p.AccessFailed(It.IsAny<string>()));
            mockSecurityQuestionLogic.Setup(p => p.CheckAnswer(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>())).Returns(true);
            mockUserManager.Setup(p => p.FindByEmail(It.IsAny<string>())).Returns(new CaradaIdUser()
            {
                Id = "userId2",
                EmailConfirmed = false,
                UserName = "太郎",
                Email = "email@test.com"
            });


            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.ResetEmail(condition));

            // 検証
            Assert.AreEqual(ErrorKind.registered_email.GetName(), actual.GetErrorCode());
            Assert.AreEqual("メールアドレスが既に登録されています。", actual.GetErrorMessage());
        }


        [Test]
        public void ResetEmail_UpdateFaie()
        {
            // パラメータ
            ResetEmailCondition condition = new ResetEmailCondition()
            {
            };

            // モック
            mockUserManager.Setup(p => p.FindByName(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.Success);

            mockUserManager.Setup(p => p.AccessFailed(It.IsAny<string>()));
            mockSecurityQuestionLogic.Setup(p => p.CheckAnswer(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>())).Returns(true);
            mockUserManager.Setup(p => p.FindByEmail(It.IsAny<string>()));
            IdentityResult iret = new IdentityResult();
            mockUserManager.Setup(p => p.Update(It.IsAny<CaradaIdUser>())).Returns(iret);


            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.ResetEmail(condition));

            // 検証
            Assert.AreEqual(ErrorKind.server_error.GetName(), actual.GetErrorCode());
            Assert.AreEqual("ユーザ情報の更新に失敗しました。", actual.GetErrorMessage());
        }



        [Test]
        public void ResetEmail()
        {
            // パラメータ
            ResetEmailCondition condition = new ResetEmailCondition()
            {
                Subject = "表題",
                Body = "本文"
            };

            // モック
            mockUserManager.Setup(p => p.FindByName(It.IsAny<string>())).Returns(user);
            mockUserManager.Setup(p => p.ValidatePassword(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).Returns(PasswordValidationStatus.Success);

            mockUserManager.Setup(p => p.AccessFailed(It.IsAny<string>()));
            mockSecurityQuestionLogic.Setup(p => p.CheckAnswer(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>())).Returns(true);
            mockUserManager.Setup(p => p.FindByEmail(It.IsAny<string>()));
            IdentityResult iret = IdentityResult.Success;
            mockUserManager.Setup(p => p.Update(It.IsAny<CaradaIdUser>())).Returns(iret);
            mockTicketLogic.Setup(p => p.IssueTicket(It.IsAny<TicketKind>(), It.IsAny<CaradaIdUser>(), It.IsAny<long>(), It.IsAny<string>(), It.IsAny<string>())).Returns(new IssuedTicketDto()
            {
                TicketId = "asdf",
                Ticket = new Ticket()
                {
                    ExpiresDateTime = new System.DateTime(123456789)
                }
            });
            mockMailLogic.Setup(p => p.SendMailAsync(It.IsAny<MailSendInfoDto>()));


            // 実施
            ResetEmailResultDto actual = target.ResetEmail(condition);

            // 検証
            Assert.IsNotNull(actual);
            Assert.AreEqual("asdf", actual.Ticket.Id);
        }



        // -----------------------------------------------------------------------------------------------------------------------------------------------------
        // RenewSecretQuestion
        // -----------------------------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void RenewSecretQuestion_TicketNull()
        {
            // パラメータ
            RenewSecretQuestionCondition condition = new RenewSecretQuestionCondition();

            // モック
            Ticket ret = null;
            mockTicketLogic.Setup(p => p.FindTicket(It.IsAny<string>())).Returns(ret);

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.RenewSecretQuestion(condition));

            // 検証
            Assert.AreEqual(ErrorKind.invalid_ticket.GetName(), actual.GetErrorCode());
            Assert.AreEqual("該当のチケットが見つかりませんでした。", actual.GetErrorMessage());
        }

        [Test]
        public void RenewSecretQuestion_InvalidKind()
        {
            // パラメータ
            RenewSecretQuestionCondition condition = new RenewSecretQuestionCondition();

            // モック
            Ticket ret = new Ticket()
            {
                Kind = TicketKind.General
            };
            mockTicketLogic.Setup(p => p.FindTicket(It.IsAny<string>())).Returns(ret);

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.RenewSecretQuestion(condition));

            // 検証
            Assert.AreEqual(ErrorKind.invalid_ticket.GetName(), actual.GetErrorCode());
            Assert.AreEqual("種別の異なるチケットが指定されました。", actual.GetErrorMessage());
        }

        [Test]
        public void RenewSecretQuestion_Expired()
        {
            // パラメータ
            RenewSecretQuestionCondition condition = new RenewSecretQuestionCondition();

            // モック
            Ticket ret = new Ticket()
            {
                Kind = TicketKind.PasswordValid,
                ExpiresDateTime = System.DateTime.Parse("2016-05-01 00:00:00")
            };
            mockTicketLogic.Setup(p => p.FindTicket(It.IsAny<string>())).Returns(ret);

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.RenewSecretQuestion(condition));

            // 検証
            Assert.AreEqual(ErrorKind.invalid_ticket.GetName(), actual.GetErrorCode());
            Assert.AreEqual("チケットの有効期限が切れています。", actual.GetErrorMessage());
        }


        [Test]
        public void RenewSecretQuestion()
        {
            // パラメータ
            RenewSecretQuestionCondition condition = new RenewSecretQuestionCondition()
            {

                TicketId = "ttt",
                SecretQuestionId = 1777,
                SecretQuestionAnswer = "aaa"
            };

            // モック
            Ticket ret = new Ticket()
            {
                Uid = "uid",
                Kind = TicketKind.PasswordValid,
                ExpiresDateTime = System.DateTime.Parse("9999-05-01 00:00:00")
            };
            mockTicketLogic.Setup(p => p.FindTicket(It.IsAny<string>())).Returns(ret);
            mockSecurityQuestionLogic.Setup(p => p.RegisterAnswer(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>())).Callback<string, int, string>((uid, bsqid, sqa) =>
            {

                // 検証
                Assert.AreEqual("uid", uid);
                Assert.AreEqual(1777, bsqid);
                Assert.AreEqual("aaa", sqa);
            });

            // 実施
            target.RenewSecretQuestion(condition);
        }


        // -----------------------------------------------------------------------------------------------------------------------------------------------------
        // SendCaradaId
        // -----------------------------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void SendCaradaId_UserInfoNull()
        {
            // パラメータ
            SendCaradaIdCondition condition = new SendCaradaIdCondition();

            // モック
            mockUserManager.Setup(p => p.FindByEmail(It.IsAny<string>()));

            // 実施
            var actual = Assert.Throws<CaradaApplicationException>(() => target.SendCaradaId(condition));

            // 検証
            Assert.AreEqual(ErrorKind.user_not_exist.GetName(), actual.GetErrorCode());
            Assert.AreEqual("該当のユーザが見つかりませんでした。", actual.GetErrorMessage());
        }

        [Test]
        public void SendCaradaId()
        {
            // パラメータ
            SendCaradaIdCondition condition = new SendCaradaIdCondition()
            {
                Subject = "表題",
                Body = "本文"
            };

            // モック
            mockUserManager.Setup(p => p.FindByEmail(It.IsAny<string>())).Returns(user);
            mockMailLogic.Setup(p => p.SendMailAsync(It.IsAny<MailSendInfoDto>()));

            // 実施
            ResultDto actual = target.SendCaradaId(condition);

            // 検証
            Assert.IsNotNull(actual);
        }
    }
}