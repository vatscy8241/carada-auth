﻿using CaradaAuthProvider.Api.Filters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NLog;
using System;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Hosting;
using System.Web.Http.Routing;

namespace CaradaAuthProvider.Api.Test.Filters
{
    /// <summary>
    /// CustomActionFilterAttributeのテストクラス
    /// </summary>
    [TestClass]
    public class CustomActionFilterAttributeTest
    {

        /// <summary>
        /// [対象] OnActionExecuting
        /// [条件] 管理されているAPI情報がヘッダのBasic認証情報に埋め込まれている
        /// [結果] 正常終了
        /// </summary>
        [TestMethod]
        public void Test_OnActionExecuting()
        {

            var target = new CustomActionFilterAttribute();


            // 入力情報
            string apiKey = "api_key_2";
            string apiSecret = "api_secret_2";

            var httpContext = CreateActionContext();
            httpContext.Request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(apiKey + ":" + apiSecret)));


            // ログの出力先変更
            var log = new Mock<ILogger>();
            log.Setup(l => l.Debug((It.IsAny<string>()))).Callback<string>(l =>
            {
                Assert.Fail(l);
            });
            typeof(CustomActionFilterAttribute).GetField("APP_LOG", BindingFlags.Static | BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Instance).SetValue(target, log.Object);

            // 実施
            target.OnActionExecuting(httpContext);
        }



        /// <summary>
        /// [対象] OnActionExecuting
        /// [条件] Basic認証情報が存在しない
        /// [結果] 認証情報が取得出来ない旨のメッセージを表示
        /// </summary>
        [TestMethod]
        public void Test_OnActionExecuting_Authorization_Empty()
        {

            var target = new CustomActionFilterAttribute();


            // 入力情報

            var httpContext = CreateActionContext();


            // ログの出力先変更
            var log = new Mock<ILogger>();
            log.Setup(l => l.Debug((It.IsAny<string>()))).Callback<string>(l =>
            {
                Assert.AreEqual("認証情報が取得出来ません。", l);
            });
            typeof(CustomActionFilterAttribute).GetField("APP_LOG", BindingFlags.Static | BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Instance).SetValue(target, log.Object);

            // 実施
            target.OnActionExecuting(httpContext);
        }



        /// <summary>
        /// [対象] OnActionExecuting
        /// [条件] Basic認証情報の認証形式がBasicでない
        /// [結果] 認証方法が不正な旨のメッセージを表示
        /// </summary>
        [TestMethod]
        public void Test_OnActionExecuting_Invalid_Scheme()
        {

            var target = new CustomActionFilterAttribute();


            // 入力情報
            string apiKey = "api_key_2";
            string apiSecret = "api_secret_2";

            var httpContext = CreateActionContext();
            httpContext.Request.Headers.Add("Authorization", "Digest " + Convert.ToBase64String(Encoding.UTF8.GetBytes(apiKey + ":" + apiSecret)));


            // ログの出力先変更
            var log = new Mock<ILogger>();
            log.Setup(l => l.Debug((It.IsAny<string>()))).Callback<string>(l =>
            {
                Assert.AreEqual("認証方法が不正です。(scheme=Digest)", l);
            });
            typeof(CustomActionFilterAttribute).GetField("APP_LOG", BindingFlags.Static | BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Instance).SetValue(target, log.Object);

            // 実施
            target.OnActionExecuting(httpContext);
        }



        /// <summary>
        /// [対象] OnActionExecuting
        /// [条件] 認証情報のフォーマットが不正
        /// [結果] 認証情報のフォーマットが不正な旨のメッセージを表示
        /// </summary>
        [TestMethod]
        public void Test_OnActionExecuting_Invalid_Format()
        {

            var target = new CustomActionFilterAttribute();


            // 入力情報
            string apiKey = "api_key_2";
            string apiSecret = "api_secret_2";

            var httpContext = CreateActionContext();
            httpContext.Request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(apiKey + apiSecret)));


            // ログの出力先変更
            var log = new Mock<ILogger>();
            log.Setup(l => l.Debug((It.IsAny<string>()))).Callback<string>(l =>
            {
                Assert.AreEqual("認証情報が不正です。(authorization=api_key_2api_secret_2)", l);
            });
            typeof(CustomActionFilterAttribute).GetField("APP_LOG", BindingFlags.Static | BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Instance).SetValue(target, log.Object);

            // 実施
            target.OnActionExecuting(httpContext);
        }



        /// <summary>
        /// [対象] OnActionExecuting
        /// [条件] APIキーが設定されていない
        /// [結果] APIキーが設定されていない旨のメッセージを表示
        /// </summary>
        [TestMethod]
        public void Test_OnActionExecuting_ApiKey_Empty()
        {

            var target = new CustomActionFilterAttribute();


            // 入力情報
            string apiKey = "";
            string apiSecret = "api_secret_2";

            var httpContext = CreateActionContext();
            httpContext.Request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(apiKey + ":" + apiSecret)));


            // ログの出力先変更
            var log = new Mock<ILogger>();
            log.Setup(l => l.Debug((It.IsAny<string>()))).Callback<string>(l =>
            {
                Assert.AreEqual("認証情報にAPIキーが設定されていません。。(authorization=:api_secret_2)", l);
            });
            typeof(CustomActionFilterAttribute).GetField("APP_LOG", BindingFlags.Static | BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Instance).SetValue(target, log.Object);

            // 実施
            target.OnActionExecuting(httpContext);
        }



        /// <summary>
        /// [対象] OnActionExecuting
        /// [条件] APIシークレットが設定されていない
        /// [結果] APIシークレットが設定されてない旨のメッセージを表示
        /// </summary>
        [TestMethod]
        public void Test_OnActionExecuting_ApiSecret_Empty()
        {

            var target = new CustomActionFilterAttribute();


            // 入力情報
            string apiKey = "api_key_2";
            string apiSecret = "";

            var httpContext = CreateActionContext();
            httpContext.Request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(apiKey + ":" + apiSecret)));


            // ログの出力先変更
            var log = new Mock<ILogger>();
            log.Setup(l => l.Debug((It.IsAny<string>()))).Callback<string>(l =>
            {
                Assert.AreEqual("認証情報にAPIシークレットが設定されていません。。(authorization=api_key_2:)", l);
            });
            typeof(CustomActionFilterAttribute).GetField("APP_LOG", BindingFlags.Static | BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Instance).SetValue(target, log.Object);

            // 実施
            target.OnActionExecuting(httpContext);
        }



        /// <summary>
        /// [対象] OnActionExecuting
        /// [条件] 指定されたAPIキーが認可サーバーの許可一覧に存在しない
        /// [結果] 指定されたAPIキーが存在しない旨のメッセージを表示
        /// </summary>
        [TestMethod]
        public void Test_OnActionExecuting_ApiKey_NotFound()
        {

            var target = new CustomActionFilterAttribute();


            // 入力情報
            string apiKey = "dummy";
            string apiSecret = "api_secret_2";

            var httpContext = CreateActionContext();
            httpContext.Request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(apiKey + ":" + apiSecret)));


            // ログの出力先変更
            var log = new Mock<ILogger>();
            log.Setup(l => l.Debug((It.IsAny<string>()))).Callback<string>(l =>
            {
                Assert.AreEqual("指定されたAPIキーは存在しません。(api_key=dummy, apiSecret=api_secret_2)", l);
            });
            typeof(CustomActionFilterAttribute).GetField("APP_LOG", BindingFlags.Static | BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Instance).SetValue(target, log.Object);

            // 実施
            target.OnActionExecuting(httpContext);
        }


        /// <summary>
        /// [対象] OnActionExecuting
        /// [条件] APIシークレットが一致しない
        /// [結果] APiシークレットが一致しない旨のメッセージを表示
        /// </summary>
        [TestMethod]
        public void Test_OnActionExecuting_ApiSecret_MissMatch()
        {

            var target = new CustomActionFilterAttribute();


            // 入力情報
            string apiKey = "api_key_1";
            string apiSecret = "api_secret_2";

            var httpContext = CreateActionContext();
            httpContext.Request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(apiKey + ":" + apiSecret)));


            // ログの出力先変更
            var log = new Mock<ILogger>();
            log.Setup(l => l.Debug((It.IsAny<string>()))).Callback<string>(l =>
            {
                Assert.AreEqual("指定されたAPIキーとAPIシークレットの組み合わせは許可されていません。(api_key=api_key_1, apiSecret=api_secret_2)", l);
            });
            typeof(CustomActionFilterAttribute).GetField("APP_LOG", BindingFlags.Static | BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Instance).SetValue(target, log.Object);

            // 実施
            target.OnActionExecuting(httpContext);
        }


        /// <summary>
        /// [対象] OnActionExecuting
        /// [条件] 検証中に予期せぬエラーが発生した場合
        /// [結果] 予期せぬエラーが発生した旨のメッセージを表示
        /// </summary>
        [TestMethod]
        public void Test_OnActionExecuting_Exception()
        {

            var target = new CustomActionFilterAttribute();


            // 入力情報
            string apiKey = "api_key_2";
            string apiSecret = "api_secret_2";

            var httpContext = CreateActionContext();
            httpContext.Request.Headers.Add("Authorization", "Basic " + apiKey + ":" + apiSecret);


            // ログの出力先変更
            var log = new Mock<ILogger>();
            log.Setup(l => l.Debug((It.IsAny<string>()))).Callback<string>(l =>
            {
                Assert.AreEqual("予期せぬエラーが発生しました。(error=入力は有効な Base-64 文字列ではありません。Base-64 以外の文字が含まれるか、3 個以上の埋め込み文字があるか、または埋め込み文字の間に無効な文字が含まれます。)", l);
            });
            typeof(CustomActionFilterAttribute).GetField("APP_LOG", BindingFlags.Static | BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Instance).SetValue(target, log.Object);

            // 実施
            target.OnActionExecuting(httpContext);
        }



        /// <summary>
        /// アクションコンテキスト生成
        /// </summary>
        /// <returns>アクションコンテキスト</returns>
        public static HttpActionContext CreateActionContext()
        {
            var config = new HttpConfiguration();
            var request = new HttpRequestMessage(HttpMethod.Post, "http://localhost/api/test");
            var route = new HttpRoute();
            var routeData = new HttpRouteData(route, new HttpRouteValueDictionary { { "controller", "test" } });
            HttpControllerContext controllerContext = new HttpControllerContext(config, routeData, request);
            controllerContext.Request = request;
            controllerContext.Request.Properties[HttpPropertyKeys.HttpConfigurationKey] = config;
            HttpActionDescriptor descriptor = new Mock<HttpActionDescriptor>() { CallBase = true }.Object;
            return new HttpActionContext(controllerContext, descriptor);
        }
    }
}
