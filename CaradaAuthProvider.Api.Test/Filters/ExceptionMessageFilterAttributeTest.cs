﻿using CaradaAuthProvider.Api.Exceptions;
using CaradaAuthProvider.Api.Filters;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.Hosting;
using System.Web.Http.Routing;

namespace CaradaAuthProvider.Api.Test.Filters
{
    /// <summary>
    /// <see cref="ExceptionMessageFilterAttribute"/>のテストクラス。
    /// </summary>
    [TestFixture]
    public class ExceptionMessageFilterAttributeTest
    {
        /// <summary>
        /// テスト対象。
        /// </summary>
        ExceptionMessageFilterAttribute target;

        /// <summary>
        /// HTTPアクション実行コンテキスト。
        /// </summary>
        HttpActionExecutedContext httpActionExecutedContext;

        /// <summary>
        /// テスト毎の初期処理。
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            target = new ExceptionMessageFilterAttribute();
        }

        /// <summary>
        /// <see cref="ExceptionFilterAttribute.OnException(HttpActionExecutedContext)"/>のテスト。
        /// CASE：システム例外が発生した場合、システム例外用レスポンスが作成できるか。
        /// 事前条件：システム例外が発生すること。
        /// 事後条件：期待通りのシステム例外用レスポンスが取得できること。
        /// </summary>
        [Test]
        public void TestOnException_SystemException()
        {
            var exception = new Exception();
            var descriptor = CreateHttpActionDescriptor(false, null);
            httpActionExecutedContext = CreateHttpActionExecutedContext(exception, descriptor);

            target.OnException(httpActionExecutedContext);

            var actual = httpActionExecutedContext.Response;
            Assert.AreEqual(HttpStatusCode.InternalServerError, actual.StatusCode);
            Assert.AreEqual("{\"Error\":\"server_error\",\"ErrorDescription\":\"予期せぬエラーが発生しました。\"}", actual.Content.ReadAsStringAsync().Result);
        }

        /// <summary>
        /// <see cref="ExceptionFilterAttribute.OnException(HttpActionExecutedContext)"/>のテスト。
        /// CASE：業務例外にエラーメッセージがなく、例外メッセージが未定義だった場合、システム例外用レスポンスが作成できるか。
        /// 事前条件：システム例外が発生すること。
        /// 事後条件：期待通りのシステム例外用レスポンスが取得できること。
        /// </summary>
        [Test]
        public void TestOnException_Message_Not_Defined_Exception()
        {
            var exception = new MockApplicationException("test_error_code", "");
            var attributes = new Collection<ExceptionOccuredAttribute>();
            attributes.Add(new ExceptionOccuredAttribute(typeof(MockApplicationException), HttpStatusCode.BadRequest));

            var descriptor = CreateHttpActionDescriptor(true, attributes);
            httpActionExecutedContext = CreateHttpActionExecutedContext(exception, descriptor);

            target.OnException(httpActionExecutedContext);

            var actual = httpActionExecutedContext.Response;
            Assert.AreEqual(HttpStatusCode.InternalServerError, actual.StatusCode);
            Assert.AreEqual("{\"Error\":\"server_error\",\"ErrorDescription\":\"予期せぬエラーが発生しました。\"}", actual.Content.ReadAsStringAsync().Result);
        }

        /// <summary>
        /// <see cref="ExceptionFilterAttribute.OnException(HttpActionExecutedContext)"/>のテスト。
        /// CASE：業務例外にエラーメッセージがないが、例外メッセージ定義済みだった場合、システム例外用レスポンスが作成できるか。
        /// 事前条件：業務例外が発生すること。
        /// 事後条件：期待通りのシステム例外用レスポンスが取得できること。
        /// </summary>
        [Test]
        public void TestOnException_Attribute_Not_Given_Exception()
        {
            var exception = new MockApplicationException("invalid_code", "てすてす");
            var descriptor = CreateHttpActionDescriptor(false, null);
            httpActionExecutedContext = CreateHttpActionExecutedContext(exception, descriptor);

            target.OnException(httpActionExecutedContext);

            var actual = httpActionExecutedContext.Response;
            Assert.AreEqual(HttpStatusCode.InternalServerError, actual.StatusCode);
            Assert.AreEqual("{\"Error\":\"server_error\",\"ErrorDescription\":\"予期せぬエラーが発生しました。\"}", actual.Content.ReadAsStringAsync().Result);
        }

        /// <summary>
        /// <see cref="ExceptionFilterAttribute.OnException(HttpActionExecutedContext)"/>のテスト。
        /// CASE：業務例外にエラーメッセージがないが、例外メッセージ定義済みだった場合、業務例外用レスポンスが作成できるか。
        /// 事前条件：業務例外が発生すること。
        /// 事後条件：期待通りの業務例外用レスポンスが取得できること。
        /// </summary>
        [Test]
        public void TestOnException_Application_Exception_Not_Error_Message_Defined_Message()
        {
            var exception = new MockApplicationException("invalid_code", "");
            var attributes = new Collection<ExceptionOccuredAttribute>();
            attributes.Add(new ExceptionOccuredAttribute(typeof(MockApplicationException), HttpStatusCode.BadRequest));

            var descriptor = CreateHttpActionDescriptor(true, attributes);
            httpActionExecutedContext = CreateHttpActionExecutedContext(exception, descriptor);

            target.OnException(httpActionExecutedContext);

            var actual = httpActionExecutedContext.Response;
            Assert.AreEqual(HttpStatusCode.BadRequest, actual.StatusCode);
            Assert.AreEqual("{\"Error\":\"invalid_code\",\"ErrorDescription\":\"無効なチケットです。\"}", actual.Content.ReadAsStringAsync().Result);
        }

        /// <summary>
        /// <see cref="ExceptionFilterAttribute.OnException(HttpActionExecutedContext)"/>のテスト。
        /// CASE：業務例外にエラーメッセージがある場合、業務例外用レスポンスが作成できるか。
        /// 事前条件：業務例外が発生すること。
        /// 事後条件：期待通りの業務例外用レスポンスが取得できること。
        /// </summary>
        [Test]
        public void TestOnException_Application_Exception()
        {
            var exception = new MockApplicationException("invalid_code", "てすてす。");
            var attributes = new Collection<ExceptionOccuredAttribute>();
            attributes.Add(new ExceptionOccuredAttribute(typeof(MockApplicationException), HttpStatusCode.BadRequest));

            var descriptor = CreateHttpActionDescriptor(true, attributes);
            httpActionExecutedContext = CreateHttpActionExecutedContext(exception, descriptor);

            target.OnException(httpActionExecutedContext);

            var actual = httpActionExecutedContext.Response;
            Assert.AreEqual(HttpStatusCode.BadRequest, actual.StatusCode);
            Assert.AreEqual("{\"Error\":\"invalid_code\",\"ErrorDescription\":\"てすてす。\"}", actual.Content.ReadAsStringAsync().Result);
        }

        #region private Method

        /// <summary>
        /// HTTPアクション記述子を作成する。
        /// </summary>
        /// <param name="setExceptionOccuredAttributeFlag">例外が発生した場合のアトリビュートをHTTPアクション記述子に含むかどうかのフラグ</param>
        /// <param name="attributes">例外が発生した場合のアトリビュートのコレクション</param>
        /// <returns></returns>
        private HttpActionDescriptor CreateHttpActionDescriptor(bool setExceptionOccuredAttributeFlag, Collection<ExceptionOccuredAttribute> attributes)
        {
            var descriptor = new Mock<HttpActionDescriptor>() { CallBase = true };
            if (setExceptionOccuredAttributeFlag)
            {
                descriptor.Setup(m => m.GetCustomAttributes<ExceptionOccuredAttribute>()).Returns(attributes);
            }
            return descriptor.Object;
        }

        /// <summary>
        /// 例外を含むHTTPアクション実行コンテキストを作成する。
        /// </summary>
        /// <param name="exception">例外</param>
        /// <param name="descriptor">HTTPアクション記述子</param>
        /// <returns>HTTPアクション実行コンテキスト</returns>
        private HttpActionExecutedContext CreateHttpActionExecutedContext(Exception exception, HttpActionDescriptor descriptor)
        {
            var config = new HttpConfiguration();
            var request = new HttpRequestMessage(HttpMethod.Post, "http://localhost/api/test");
            var route = new HttpRoute();
            var routeData = new HttpRouteData(route, new HttpRouteValueDictionary { { "controller", "test" } });

            var controllerContext = new HttpControllerContext(config, routeData, request);
            controllerContext.Request = request;
            controllerContext.Request.Properties[HttpPropertyKeys.HttpConfigurationKey] = config;

            var actionContext = new HttpActionContext(controllerContext, descriptor);

            return new HttpActionExecutedContext(actionContext, exception);
        }

        /// <summary>
        /// <see cref="AbstractApplicationException"/>を継承したテスト用例外クラス。
        /// </summary>
        private class MockApplicationException : AbstractApplicationException
        {
            /// <summary>
            /// エラーコード。
            /// </summary>
            private string errorCode;

            /// <summary>
            /// テスト用のエラーコードとエラーメッセージを指定してコンストラクトする。
            /// </summary>
            /// <param name="errorCode">エラーコード</param>
            /// <param name="errorMessage">エラーメッセージ</param>
            public MockApplicationException(string errorCode, string errorMessage)
            {
                this.errorCode = errorCode;
                this.errorMessage = errorMessage;
            }

            /// <summary>
            /// エラーコードを返す。
            /// </summary>
            /// <returns>エラーコード</returns>
            public override string GetErrorCode()
            {
                return errorCode;
            }

            /// <summary>
            /// エラーメッセージを返す。
            /// </summary>
            /// <returns>エラーメッセージ</returns>
            public override string GetErrorMessage()
            {
                return errorMessage;
            }
        }

        #endregion
    }
}