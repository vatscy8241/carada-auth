﻿using CaradaAuthProvider.Api.Filters;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Hosting;
using System.Web.Http.Routing;

namespace CaradaAuthProvider.Api.Test.Filters
{
    /// <summary>
    /// <see cref="ConditionValidationFilterAttribute"/>のテストクラス。
    /// </summary>
    public class ConditionValidationFilterAttributeTest
    {
        /// <summary>
        /// /テスト対象。
        /// </summary>
        ConditionValidationFilterAttribute target;

        /// <summary>
        /// HTTPアクションコンテキスト。
        /// </summary>
        HttpActionContext httpActionContext;

        /// <summary>
        /// テスト毎の初期処理。
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            target = new ConditionValidationFilterAttribute(1);
        }

        /// <summary>
        /// <see cref="ConditionValidationFilterAttribute.OnActionExecuting(HttpActionContext)"/>のテスト。
        /// CASE：アクション引数の一覧がなく、モデルステートにエラーがない場合、モデルステートが有効か。
        /// 事前条件：アクション引数の一覧がなく、モデルステートにエラーがないこと。
        /// 事後条件：モデルステートが有効になっていること。
        /// </summary>
        [Test]
        public void TestOnActionExecuting_Valid()
        {
            httpActionContext = CreateHttpActionContext();

            target.OnActionExecuting(httpActionContext);

            Assert.IsTrue(httpActionContext.ModelState.IsValid);
        }

        /// <summary>
        /// <see cref="ConditionValidationFilterAttribute.OnActionExecuting(HttpActionContext)"/>のテスト。
        /// CASE：アクション引数の値がnullになっている場合、リクエストパラメータが設定されていないレスポンスが作成できるか。。
        /// 事前条件：アクション引数の値がnullになっていること。
        /// 事後条件：期待通りのレスポンスが取得できること。
        /// </summary>
        //[Test]
        //public void TestOnActionExecuting_No_Parameter()
        //{
        //    // TODO ActionArgumentに値設定できずテストできてない。
        //    httpActionContext = CreateHttpActionContext();

        //    target.OnActionExecuting(httpActionContext);

        //    var actual = httpActionContext.Response;
        //    Assert.AreEqual(HttpStatusCode.BadRequest, actual.StatusCode);
        //    Assert.AreEqual("{\"Error\":\"invalid_parameter\",\"ErrorDescription\":\"リクエストパラメータが設定されていません。\"}", actual.Content.ReadAsStringAsync().Result);
        //}

        /// <summary>
        /// <see cref="ConditionValidationFilterAttribute.OnActionExecuting(HttpActionContext)"/>のテスト。
        /// CASE：アクション引数の一覧がなく、モデルステートに1つエラーがある場合、1つのパラメータが不正のレスポンスが作成できるか。
        /// 事前条件：アクション引数の一覧がなく、モデルステートに1つだけエラーがあること。
        /// 事後条件：1つのパラメータが不正のレスポンスが取得できること。
        /// </summary>
        [Test]
        public void TestOnActionExecuting_Single_Model_State_Error()
        {
            httpActionContext = CreateHttpActionContext();
            httpActionContext.ModelState.AddModelError("condition.test", "エラーです。");

            target.OnActionExecuting(httpActionContext);

            var actual = httpActionContext.Response;
            Assert.AreEqual(HttpStatusCode.BadRequest, actual.StatusCode);
            Assert.AreEqual("{\"Error\":\"invalid_parameter\",\"ErrorDescription\":\"[test]エラーです。\"}", actual.Content.ReadAsStringAsync().Result);
        }

        /// <summary>
        /// <see cref="ConditionValidationFilterAttribute.OnActionExecuting(HttpActionContext)"/>のテスト。
        /// CASE：アクション引数の一覧がなく、モデルステートに複数エラーがある場合、複数のパラメータが不正のレスポンスが作成できるか。
        /// 事前条件：アクション引数の一覧がなく、モデルステートに複数エラーがあること。
        /// 事後条件：複数のパラメータが不正のレスポンスが取得できること。
        /// </summary>
        [Test]
        public void TestOnActionExecuting_Multiple_Model_State_Error()
        {
            httpActionContext = CreateHttpActionContext();
            httpActionContext.ModelState.AddModelError("condition.test", "エラーです。");
            httpActionContext.ModelState.AddModelError("condition.arg", "引数がおかしいです。");
            httpActionContext.ModelState.AddModelError("condition.bug", "値がバグってます。");

            target.OnActionExecuting(httpActionContext);

            var actual = httpActionContext.Response;
            Assert.AreEqual(HttpStatusCode.BadRequest, actual.StatusCode);
            Assert.AreEqual("{\"Error\":\"invalid_parameter\",\"ErrorDescription\":\"[test]エラーです。,[arg]引数がおかしいです。,[bug]値がバグってます。\"}", actual.Content.ReadAsStringAsync().Result);
        }

        /// <summary>
        /// <see cref="ConditionValidationFilterAttribute.OnActionExecuting(HttpActionContext)"/>のテスト。
        /// CASE：アクション引数の一覧がなく、モデルステートにJSON例外がある場合、JSONフォーマットが不正のレスポンスが作成できるか。
        /// 事前条件：アクション引数の一覧がなく、モデルステートにJSON例外があること。
        /// 事後条件：JSONフォーマットが不正のレスポンスが取得できること。
        /// </summary>
        [Test]
        public void TestOnActionExecuting_Model_State_Json_Exception()
        {
            var exception = new Exception();
            httpActionContext = CreateHttpActionContext();

            httpActionContext.ModelState.AddModelError("test", new JsonException());

            target.OnActionExecuting(httpActionContext);

            var actual = httpActionContext.Response;
            Assert.AreEqual(HttpStatusCode.BadRequest, actual.StatusCode);
            Assert.AreEqual("{\"Error\":\"invalid_parameter\",\"ErrorDescription\":\"[test]JSONフォーマットが不正です。\"}", actual.Content.ReadAsStringAsync().Result);
        }

        /// <summary>
        /// <see cref="ConditionValidationFilterAttribute.OnActionExecuting(HttpActionContext)"/>のテスト。
        /// CASE：アクション引数の一覧がなく、モデルステートに例外がある場合、JSONフォーマットが不正のレスポンスが作成できるか。
        /// 事前条件：アクション引数の一覧がなく、モデルステートにJSON例外があること。
        /// 事後条件：JSONフォーマットが不正のレスポンスが取得できること。
        /// </summary>
        [Test]
        public void TestOnActionExecuting_Model_State_Exception()
        {
            var exception = new Exception();
            httpActionContext = CreateHttpActionContext();

            httpActionContext.ModelState.AddModelError("test", new Exception());

            target.OnActionExecuting(httpActionContext);

            var actual = httpActionContext.Response;
            Assert.AreEqual(HttpStatusCode.BadRequest, actual.StatusCode);
            Assert.AreEqual("{\"Error\":\"invalid_parameter\",\"ErrorDescription\":\"[test]JSONフォーマットが不正です。\"}", actual.Content.ReadAsStringAsync().Result);
        }

        #region private Method

        /// <summary>
        /// HTTPアクションコンテキストを作成する。
        /// </summary>
        /// <returns>HTTPアクションコンテキスト</returns>
        private HttpActionContext CreateHttpActionContext()
        {
            var config = new HttpConfiguration();
            var request = new HttpRequestMessage(HttpMethod.Post, "http://localhost/api/test");
            var route = new HttpRoute();
            var routeData = new HttpRouteData(route, new HttpRouteValueDictionary { { "controller", "test" } });

            var descriptor = new Mock<HttpActionDescriptor>() { CallBase = true };

            var controllerContext = new HttpControllerContext(config, routeData, request);
            controllerContext.Request = request;
            controllerContext.Request.Properties[HttpPropertyKeys.HttpConfigurationKey] = config;

            return new HttpActionContext(controllerContext, descriptor.Object);
        }

        #endregion
    }
}