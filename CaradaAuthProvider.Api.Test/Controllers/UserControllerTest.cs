﻿using CaradaAuthProvider.Api.Controllers;
using CaradaAuthProvider.Api.Controllers.Conditions;
using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Services;
using CaradaAuthProvider.Api.Services.Dtos;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Hosting;

namespace CaradaAuthProvider.Api.Test.Controllers
{
    [TestClass]
    public class UserControllerTest
    {

        // テスト対象
        private UserController target;

        // モック
        private class MockUserController : UserController
        {
            private UserService userService;

            public MockUserController(Mock<UserService> mUserService)
            {
                this.userService = mUserService.Object;
            }
            protected override UserService CreateUserService()
            {
                return userService;
            }
        }

        private Mock<UserService> mUserService;

        private Mock<CaradaAuthProviderDbContext> mCaradaAuthProviderDbContext;


        [TestInitialize]
        public void initialize()
        {
            mUserService = new Mock<UserService>(null, null);
            target = new MockUserController(mUserService);

            // DbContextの設定

            mCaradaAuthProviderDbContext = new Mock<CaradaAuthProviderDbContext>();

            var dbContext = typeof(AbstractApiController).GetField("_caradaIdPDbContext", BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Instance);
            dbContext.SetValue(target, mCaradaAuthProviderDbContext.Object);

            // Requestの設定
            target.Request = new HttpRequestMessage()
            {
                Properties = { { HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration() } }
            };
        }

        /// <summary>
        /// ユーザー情報取得API
        /// </summary>
        [TestMethod]
        public void UserInfo()
        {
            // パラメータ
            UserInfoCondition condition = new UserInfoCondition();

            // モック
            mUserService.Setup(_ => _.UserInfo(It.IsAny<UserInfoCondition>())).Returns(new UserInfoResultDto()
            {
                CaradaId = "carada",
                Email = "mail",
                EmailConfirmed = true,
                SecretQuestion = null
            });

            // 実施
            HttpResponseMessage actual = target.UserInfo(condition);

            // 検証
            Assert.IsNotNull(actual);
            Assert.AreEqual("{\"CaradaId\":\"carada\",\"Email\":\"mail\",\"EmailConfirmed\":true,\"SecretQuestion\":null}", actual.Content.ReadAsStringAsync().Result);
        }

        /// <summary>
        /// パスワード検証API
        /// </summary>
        [TestMethod]
        public void VerifyPassword()
        {
            // パラメータ
            VerifyPasswordCondition condition = new VerifyPasswordCondition();

            // モック
            mUserService.Setup(_ => _.VerifyPassword(It.IsAny<VerifyPasswordCondition>())).Returns(new VerifyPasswordResultDto()
            {
                Ticket = new TicketDto
                {
                    Id = "reregazema",
                    ExpiresAt = 1234567890

                }
            });

            // 実施
            HttpResponseMessage actual = target.VerifyPassword(condition);

            // 検証
            Assert.IsNotNull(actual);
            Assert.AreEqual("{\"Ticket\":{\"Id\":\"reregazema\",\"ExpiresAt\":1234567890}}", actual.Content.ReadAsStringAsync().Result);
        }

        /// <summary>
        /// パスワード更新API
        /// </summary>
        [TestMethod]
        public void RenewPassword()
        {
            // パラメータ
            RenewPasswordCondition condition = new RenewPasswordCondition();

            // モック
            mUserService.Setup(_ => _.RenewPassword(It.IsAny<RenewPasswordCondition>())).Returns(new ResultDto()
            {

            });

            // 実施
            HttpResponseMessage actual = target.RenewPassword(condition);

            // 検証
            Assert.IsNotNull(actual);
            Assert.AreEqual("{\"Result\":\"SUCCESS\"}", actual.Content.ReadAsStringAsync().Result);
        }

        /// <summary>
        /// メールアドレス変更開始API
        /// </summary>
        [TestMethod]
        public void StartRenewEmail()
        {
            // パラメータ
            StartRenewEmailCondition condition = new StartRenewEmailCondition();

            // モック
            mUserService.Setup(_ => _.StartRenewEmail(It.IsAny<StartRenewEmailCondition>())).Returns(new StartRenewEmailResultDto()
            {
                Ticket = new TicketDto
                {
                    Id = "reregazema",
                    ExpiresAt = 1234567890

                }
            }
                );

            // 実施
            HttpResponseMessage actual = target.StartRenewEmail(condition);

            // 検証
            Assert.IsNotNull(actual);
            Assert.AreEqual("{\"Ticket\":{\"Id\":\"reregazema\",\"ExpiresAt\":1234567890}}", actual.Content.ReadAsStringAsync().Result);
        }

        /// <summary>
        /// メールアドレス変更用認証コードメール送信API
        /// </summary>
        [TestMethod]
        public void SendRenewEmailVerifyCode()
        {
            // パラメータ
            SendRenewEmailVerifyCodeCondition condition = new SendRenewEmailVerifyCodeCondition();

            // モック
            mUserService.Setup(_ => _.SendRenewEmailVerifyCode(It.IsAny<SendRenewEmailVerifyCodeCondition>())).Returns(new ResultDto());

            // 実施
            HttpResponseMessage actual = target.SendRenewEmailVerifyCode(condition);

            // 検証
            Assert.IsNotNull(actual);
            Assert.AreEqual("{\"Result\":\"SUCCESS\"}", actual.Content.ReadAsStringAsync().Result);
        }

        /// <summary>
        /// メールアドレス変更用認証コードメール送信API
        /// </summary>
        [TestMethod]
        public void RenewEmail()
        {
            // パラメータ
            RenewEmailCondition condition = new RenewEmailCondition();

            // モック
            mUserService.Setup(_ => _.RenewEmail(It.IsAny<RenewEmailCondition>())).Returns(new ResultDto());

            // 実施
            HttpResponseMessage actual = target.RenewEmail(condition);

            // 検証
            Assert.IsNotNull(actual);
            Assert.AreEqual("{\"Result\":\"SUCCESS\"}", actual.Content.ReadAsStringAsync().Result);
        }

        /// <summary>
        /// 秘密の質問更新API
        /// </summary>
        [TestMethod]
        public void RenewSecretQuestion()
        {
            // パラメータ
            RenewSecretQuestionCondition condition = new RenewSecretQuestionCondition();

            // モック
            mUserService.Setup(_ => _.RenewSecretQuestion(It.IsAny<RenewSecretQuestionCondition>()));

            // 実施
            HttpResponseMessage actual = target.RenewSecretQuestion(condition);

            // 検証
            Assert.IsNotNull(actual);
            Assert.AreEqual("{\"Result\":\"SUCCESS\"}", actual.Content.ReadAsStringAsync().Result);
        }

        /// <summary>
        /// 秘密の質問更新API
        /// </summary>
        [TestMethod]
        public void SendCaradaId()
        {
            // パラメータ
            SendCaradaIdCondition condition = new SendCaradaIdCondition();

            // モック
            mUserService.Setup(_ => _.SendCaradaId(It.IsAny<SendCaradaIdCondition>())).Returns(new ResultDto());

            // 実施
            HttpResponseMessage actual = target.SendCaradaId(condition);

            // 検証
            Assert.IsNotNull(actual);
            Assert.AreEqual("{\"Result\":\"SUCCESS\"}", actual.Content.ReadAsStringAsync().Result);
        }


        /// <summary>
        /// パスワード再設定API
        /// </summary>
        [TestMethod]
        public void ResetPassword()
        {
            // パラメータ
            ResetPasswordCondition condition = new ResetPasswordCondition();

            // モック
            mUserService.Setup(_ => _.ResetPassword(It.IsAny<ResetPasswordCondition>())).Returns(new ResetPasswordResultDto()
            {
                Ticket = new TicketDto
                {
                    Id = "reregazema",
                    ExpiresAt = 1234567890

                }
            });

            // 実施
            HttpResponseMessage actual = target.ResetPassword(condition);

            // 検証
            Assert.IsNotNull(actual);
            Assert.AreEqual("{\"Ticket\":{\"Id\":\"reregazema\",\"ExpiresAt\":1234567890}}", actual.Content.ReadAsStringAsync().Result);
        }


        /// <summary>
        /// 秘密の質問更新API
        /// </summary>
        [TestMethod]
        public void ResetEmail()
        {
            // パラメータ
            ResetEmailCondition condition = new ResetEmailCondition();

            // モック
            mUserService.Setup(_ => _.ResetEmail(It.IsAny<ResetEmailCondition>())).Returns(new ResetEmailResultDto()
            {
                Ticket = new TicketDto
                {
                    Id = "reregazema",
                    ExpiresAt = 1234567890

                }
            });

            // 実施
            HttpResponseMessage actual = target.ResetEmail(condition);

            // 検証
            Assert.IsNotNull(actual);
            Assert.AreEqual("{\"Ticket\":{\"Id\":\"reregazema\",\"ExpiresAt\":1234567890}}", actual.Content.ReadAsStringAsync().Result);
        }
    }
}
