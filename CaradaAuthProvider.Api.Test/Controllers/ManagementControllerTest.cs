﻿using CaradaAuthProvider.Api.Controllers;
using CaradaAuthProvider.Api.Controllers.Conditions;
using CaradaAuthProvider.Api.Controllers.Jwt;
using CaradaAuthProvider.Api.Services;
using CaradaAuthProvider.Api.Services.Dtos;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;

namespace CaradaAuthProvider.Api.Test.Controllers
{
    [TestClass]
    public class ManagementControllerTest
    {
        // テスト対象
        private ManagementController target;
        private class MockManagementController : ManagementController
        {
            private ManagementService mockManagementService;

            public MockManagementController(Mock<ManagementService> mockManagementService)
            {
                this.mockManagementService = mockManagementService.Object;
            }
            protected override ManagementService CreateManagementService()
            {
                return mockManagementService;
            }
        }

        private class MockJwt<TParameter> : Jwt<TParameter>
            where TParameter : AbstractCondition, new()
        {
            public MockJwt(TParameter condition)
            {
                this.Parameter = condition;
            }
        }

        private Mock<ManagementService> mockManagementService;

        [TestInitialize]
        public void initialize()
        {
            mockManagementService = new Mock<ManagementService>(null, null);
            target = new MockManagementController(mockManagementService);
            // Requestの設定
            target.Request = new HttpRequestMessage()
            {
                Properties = { { HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration() } }
            };
        }

        [TestMethod]
        public void AddUser_Success()
        {
            Jwt<RegistUserAddCondition> jwt = new MockJwt<RegistUserAddCondition>(new RegistUserAddCondition());
            AddUserResultDto dto = new AddUserResultDto
            {
                User = new AddUserDto
                {
                    carada_id = "caradaId",
                    password = "password",
                    uid = "uid"
                }
            };
            mockManagementService.Setup(p => p.AddUser(jwt.Parameter)).ReturnsAsync(dto);

            HttpResponseMessage actual = target.AddUser(jwt).Result;
            Assert.IsNotNull(actual);
            Assert.AreEqual(HttpStatusCode.OK, actual.StatusCode);
            Assert.AreEqual("{\"carada_id\":\"caradaId\",\"password\":\"password\",\"uid\":\"uid\"}", actual.Content.ReadAsStringAsync().Result);
        }

        [TestMethod]
        public void ResetUserInfo_Success()
        {
            Jwt<ResetUserCondition> jwt = new MockJwt<ResetUserCondition>(new ResetUserCondition());
            ResetUserResultDto dto = new ResetUserResultDto
            {
                User = new ResetUserDto
                {
                    carada_id = "caradaId",
                    password = "password",
                    initialization = "1"
                }
            };
            mockManagementService.Setup(p => p.ResetUserInfo(jwt.Parameter)).ReturnsAsync(dto);

            HttpResponseMessage actual = target.ResetUserInfo(jwt).Result;
            Assert.IsNotNull(actual);
            Assert.AreEqual(HttpStatusCode.OK, actual.StatusCode);
            Assert.AreEqual("{\"carada_id\":\"caradaId\",\"password\":\"password\",\"initialization\":\"1\"}", actual.Content.ReadAsStringAsync().Result);
        }
    }
}
