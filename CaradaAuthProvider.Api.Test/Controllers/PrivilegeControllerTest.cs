﻿using CaradaAuthProvider.Api.Controllers;
using CaradaAuthProvider.Api.Controllers.Conditions;
using CaradaAuthProvider.Api.Controllers.Jwt;
using CaradaAuthProvider.Api.Exceptions;
using CaradaAuthProvider.Api.Exceptions.Enums;
using CaradaAuthProvider.Api.IdentityManagers;
using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Persistence.Repositories.Entities;
using CaradaAuthProvider.Api.Services;
using CaradaAuthProvider.Api.Services.Dtos;
using Microsoft.AspNet.Identity;
using Moq;
using NUnit.Framework;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Hosting;

namespace CaradaAuthProvider.Api.Test.Controllers
{
    [TestFixture]
    public class PrivilegeControllerTest
    {

        /// <summary>
        /// テスト対象
        /// </summary>
        private TPrivilegeController target = null;


        /// <summary>
        /// JWTモック
        /// </summary>
        /// <typeparam name="T"></typeparam>
        private class MockJwt<T> : Jwt<T>
            where T : AbstractCondition, new()
        {
            public MockJwt(T condition)
            {
                this.Parameter = condition;
            }

        }

        /// <summary>
        /// イニシャライズ
        /// </summary>
        [SetUp]
        public void Initialize()
        {
            mCaradaAuthProviderDbContext = new Mock<CaradaAuthProviderDbContext>();
            mUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mAuthorizeService = new Mock<AuthorizeService>(mCaradaAuthProviderDbContext.Object, mUserManager.Object);
            target = new TPrivilegeController(mAuthorizeService.Object);
            target.Request = new HttpRequestMessage()
            {
                Properties = { { HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration() } }
            };

            typeof(AbstractApiController).GetField("_caradaIdPDbContext", BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Instance).SetValue(target, mCaradaAuthProviderDbContext.Object);
            typeof(AbstractApiController).GetField("_userManager", BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Instance).SetValue(target, mUserManager.Object);
        }

        /// <summary>
        /// 代理アクセストークン取得API_正常系
        /// </summary>
        [Test]
        public void IssueAccessToken()
        {
            // モック
            mAuthorizeService.Setup(_ => _.IssueAgencyAccessToken(It.IsAny<IssueAgencyAccessTokenCondition>(), It.IsAny<string>())).Returns(new IssueAgencyAccessTokenResultDto()
            {
                access_token = "access_token",
                expires_at = 123456789
            });

            // パラメータ
            Jwt<IssueAgencyAccessTokenCondition> condition = new MockJwt<IssueAgencyAccessTokenCondition>(new IssueAgencyAccessTokenCondition());

            // 実施
            HttpResponseMessage actual = target.IssueAccessToken(condition);

            // 検証
            Assert.IsNotNull(actual);
            Assert.AreEqual("{\"access_token\":\"access_token\",\"expires_at\":123456789}", actual.Content.ReadAsStringAsync().Result);
        }


        /// <summary>
        /// 代理アクセストークン取得API_異常系
        /// </summary>
        [Test]
        public void IssueAccessToken_Exception()
        {
            // モック
            mAuthorizeService.Setup(_ => _.IssueAgencyAccessToken(It.IsAny<IssueAgencyAccessTokenCondition>(), It.IsAny<string>())).Throws(new CaradaApplicationException(ErrorKind.data_mismatch, "テスト"));

            // パラメータ
            Jwt<IssueAgencyAccessTokenCondition> condition = new MockJwt<IssueAgencyAccessTokenCondition>(new IssueAgencyAccessTokenCondition());

            // 実施
            HttpResponseMessage actual = target.IssueAccessToken(condition);

            // 検証
            Assert.IsNotNull(actual);
            Assert.AreEqual("{\"error\":\"data_mismatch\",\"error_description\":\"テスト\"}", actual.Content.ReadAsStringAsync().Result);
        }


        private Mock<CaradaAuthProviderDbContext> mCaradaAuthProviderDbContext;

        private Mock<ApplicationUserManager> mUserManager;

        private Mock<AuthorizeService> mAuthorizeService;

        private class TPrivilegeController : PrivilegeController
        {
            private AuthorizeService authorizeService;

            public TPrivilegeController(AuthorizeService _authorizeService)
            {
                this.authorizeService = _authorizeService;
            }

            protected override AuthorizeService CreateAuthorizeService()
            {
                return authorizeService;
            }
        }
    }
}
