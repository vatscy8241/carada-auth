﻿using CaradaAuthProvider.Api.Controllers;
using CaradaAuthProvider.Api.Controllers.Conditions;
using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Services;
using CaradaAuthProvider.Api.Services.Dtos;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Hosting;

namespace CaradaAuthProvider.Api.Test.Controllers
{
    [TestClass]
    public class AuthenticateControllerTest
    {

        // テスト対象
        private AuthenticateController target;
        private class MockAuthenticateController : AuthenticateController
        {
            private AuthenticateService mockAuthenticateService;

            public MockAuthenticateController(Mock<AuthenticateService> mockAuthenticateService)
            {
                this.mockAuthenticateService = mockAuthenticateService.Object;
            }
            protected override AuthenticateService CreateAuthenticateService()
            {
                return mockAuthenticateService;
            }
        }

        private Mock<AuthenticateService> mockAuthenticateService;

        [TestInitialize]
        public void initialize()
        {
            mockAuthenticateService = new Mock<AuthenticateService>(null, null);
            target = new MockAuthenticateController(mockAuthenticateService);

            // DbContextの設定
            var dbContext = typeof(AbstractApiController).GetField("_caradaIdPDbContext", BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Instance);
            dbContext.SetValue(target, new DbContextFactory().NewCaradaAuthProviderDbContext());

            // Requestの設定
            target.Request = new HttpRequestMessage()
            {
                Properties = { { HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration() } }
            };
        }

        [TestMethod]
        public void Authenticate_SuccessWithTicket()
        {
            AuthenticateCondition condition = new AuthenticateCondition();
            AuthenticateResultDto dto = new AuthenticateResultDto()
            {
                Uid = "userId",
                Ticket = new TicketDto()
                {
                    Id = "ticketId",
                    ExpiresAt = 100
                }
            };
            mockAuthenticateService.Setup(p => p.Authenticate(condition)).Returns(dto);

            HttpResponseMessage actual = target.Authenticate(condition);
            Assert.IsNotNull(actual);
            Assert.AreEqual(HttpStatusCode.OK, actual.StatusCode);
            Assert.AreEqual("{\"Ticket\":{\"Id\":\"ticketId\",\"ExpiresAt\":100},\"Uid\":\"userId\"}", actual.Content.ReadAsStringAsync().Result);
        }

        [TestMethod]
        public void Authenticate_SuccessWithoutTicket()
        {
            AuthenticateCondition condition = new AuthenticateCondition();
            AuthenticateResultDto dto = new AuthenticateResultDto()
            {
                Uid = "userId",
                Ticket = null
            };
            mockAuthenticateService.Setup(p => p.Authenticate(condition)).Returns(dto);

            HttpResponseMessage actual = target.Authenticate(condition);
            Assert.IsNotNull(actual);
            Assert.AreEqual(HttpStatusCode.OK, actual.StatusCode);
            Assert.AreEqual("{\"Uid\":\"userId\"}", actual.Content.ReadAsStringAsync().Result);
        }

        [TestMethod]
        public void SendUseStartVerifyCode_Success()
        {
            SendUseStartVerifyCodeCondition condition = new SendUseStartVerifyCodeCondition();
            SendMailResultDto dto = new SendMailResultDto();
            mockAuthenticateService.Setup(p => p.SendUseStartVerifyCode(condition)).Returns(dto);

            HttpResponseMessage actual = target.SendUseStartVerifyCode(condition);
            Assert.IsNotNull(actual);
            Assert.AreEqual(HttpStatusCode.OK, actual.StatusCode);
            Assert.AreEqual("{\"Result\":\"SUCCESS\"}", actual.Content.ReadAsStringAsync().Result);
        }

        [TestMethod]
        public void SendTwoFactorVerifyCode_Success()
        {
            SendTwoFactorVerifyCodeCondition condition = new SendTwoFactorVerifyCodeCondition();
            TwoFactorSendMailResultDto dto = new TwoFactorSendMailResultDto()
            {
                Ticket = new TicketDto()
                {
                    Id = "ticketId",
                    ExpiresAt = 100
                }
            };
            mockAuthenticateService.Setup(p => p.SendTwoFactorVerifyCode(condition)).Returns(dto);

            HttpResponseMessage actual = target.SendTwoFactorVerifyCode(condition);
            Assert.IsNotNull(actual);
            Assert.AreEqual(HttpStatusCode.OK, actual.StatusCode);
            Assert.AreEqual("{\"Ticket\":{\"Id\":\"ticketId\",\"ExpiresAt\":100}}", actual.Content.ReadAsStringAsync().Result);
        }

        [TestMethod]
        public void Verify_Success()
        {
            AuthCodeVerifyCondition condition = new AuthCodeVerifyCondition();
            VerifyResultDto dto = new VerifyResultDto();
            mockAuthenticateService.Setup(p => p.VerifyTicket(condition)).Returns(dto);

            HttpResponseMessage actual = target.Verify(condition);
            Assert.IsNotNull(actual);
            Assert.AreEqual(HttpStatusCode.OK, actual.StatusCode);
            Assert.AreEqual("{\"Result\":\"SUCCESS\"}", actual.Content.ReadAsStringAsync().Result);
        }

        [TestMethod]
        public void Register_Success()
        {
            UseStartRegisterCondition condition = new UseStartRegisterCondition();
            RegisterUseStartResultDto dto = new RegisterUseStartResultDto();
            dto.Uid = "uid";
            mockAuthenticateService.Setup(p => p.RegisterUseStart(condition)).Returns(dto);

            HttpResponseMessage actual = target.Register(condition);
            Assert.IsNotNull(actual);
            Assert.AreEqual(HttpStatusCode.OK, actual.StatusCode);
            Assert.AreEqual("{\"Uid\":\"uid\"}", actual.Content.ReadAsStringAsync().Result);
        }

        [TestMethod]
        public void GetSecurityQuestion()
        {
            List<SecurityQuestionResultDto> secretQuestions = new List<SecurityQuestionResultDto>
            {
                new SecurityQuestionResultDto()
                {
                    SecretQuestionId = 1,
                    SecretQuestionSentence = "質問1"
                },
                new SecurityQuestionResultDto()
                {
                    SecretQuestionId = 2,
                    SecretQuestionSentence = "質問2"
                }
            };
            mockAuthenticateService.Setup(p => p.GetSecurityQuestionList()).Returns(secretQuestions);

            // 実行
            HttpResponseMessage result = target.GetSecurityQuestion() as HttpResponseMessage;

            // アサート
            Assert.IsNotNull(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
            Assert.AreEqual(
                "{\"SecretQuestions\":[{\"SecretQuestionId\":1,\"SecretQuestionSentence\":\"質問1\"},{\"SecretQuestionId\":2,\"SecretQuestionSentence\":\"質問2\"}]}",
                result.Content.ReadAsStringAsync().Result);
        }
    }
}
