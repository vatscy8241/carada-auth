﻿using CaradaAuthProvider.Api.Controllers.DataAnnotations;
using NUnit.Framework;
using System.Collections.Generic;

namespace CaradaAuthProvider.Api.Test.Controllers.DataAnnotations
{
    /// <summary>
    /// <see cref="CaradaIdTypeAttribute"/>のテストクラス。
    /// </summary>
    [TestFixture]
    public class EmailAddressJPAttributeTest
    {
        /// <summary>
        /// テスト対象。
        /// </summary>
        private EmailAddressJPAttribute target;

        /// <summary>
        /// SetUp。
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            target = new EmailAddressJPAttribute();
        }

        /// <summary>
        /// <see cref="EmailAddressJPAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトがnullの時有効か。。
        /// 事前条件：パラメータにnullを渡すこと。
        /// 事後条件：trueが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Valid_Null()
        {
            var actual = target.IsValid(null);
            Assert.IsTrue(actual);
        }

        /// <summary>
        /// <see cref="EmailAddressJPAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトが文字列ではない時無効か。
        /// 事前条件：パラメータに文字列以外を渡すこと。
        /// 事後条件：falseが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Invalid_Not_String()
        {
            var actual1 = target.IsValid(12345);
            Assert.IsFalse(actual1);

            var actual2 = target.IsValid(new object { });
            Assert.IsFalse(actual2);

            var actual3 = target.IsValid(new List<string>());
            Assert.IsFalse(actual3);
        }

        /// <summary>
        /// <see cref="EmailAddressJPAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトが空文字の時有効か。
        /// 事前条件：パラメータに空文字を渡すこと。
        /// 事後条件：trueが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Valid_String_Empty()
        {
            var actual = target.IsValid(string.Empty);
            Assert.IsTrue(actual);
        }

        /// <summary>
        /// <see cref="EmailAddressJPAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトがメールアドレスの形式にマッチしない文字列の時無効か。
        /// 事前条件：パラメータにメールアドレスの形式にマッチしない文字列を渡すこと。
        /// 事後条件：falseが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Invalid_Email()
        {
            var actual1 = target.IsValid("test@...");
            Assert.IsFalse(actual1);

            var actual2 = target.IsValid("tes123");
            Assert.IsFalse(actual2);

            var actual3 = target.IsValid("tes23st@jfl@fa.jp");
            Assert.IsFalse(actual3);

            var actual4 = target.IsValid(".jds3fdjsdfj@com.jp");
            Assert.IsFalse(actual4);

            var actual5 = target.IsValid("hfias@fd{.jf].jp");
            Assert.IsFalse(actual5);

            var actual6 = target.IsValid("hfias@[10.20.30.1]");
            Assert.IsFalse(actual6);
        }

        /// <summary>
        /// <see cref="EmailAddressJPAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトがメールアドレスの形式にマッチした文字列の時有効か。
        /// 事前条件：パラメータにメールアドレスの形式にマッチした文字列を渡すこと。
        /// 事後条件：trueが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Valid_Email()
        {
            var actual1 = target.IsValid("test@ne.jp");
            Assert.IsTrue(actual1);

            var actual2 = target.IsValid("test.123@ne.n.e.jp");
            Assert.IsTrue(actual2);

            var actual3 = target.IsValid("te..12...fsd53@ne.jp");
            Assert.IsTrue(actual3);

            var actual4 = target.IsValid("532{f.co}m@fds.je");
            Assert.IsTrue(actual4);
        }
    }
}
