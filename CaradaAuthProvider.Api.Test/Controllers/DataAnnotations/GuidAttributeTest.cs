﻿using CaradaAuthProvider.Api.Controllers.DataAnnotations;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace CaradaAuthProvider.Api.Test.Controllers.DataAnnotations
{
    /// <summary>
    /// <see cref="GuidAttribute"/>のテストクラス。
    /// </summary>
    [TestFixture]
    public class GuidAttributeTest
    {
        /// <summary>
        /// テスト対象
        /// </summary>
        private GuidAttribute target;

        /// <summary>
        /// SetUp。
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            target = new GuidAttribute();
        }

        /// <summary>
        /// <see cref="GuidAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトがnullの時有効か。
        /// 事前条件：パラメータにnullを渡すこと。
        /// 事後条件：trueが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Valid_Null()
        {
            var actual = target.IsValid(null);
            Assert.IsTrue(actual);
        }

        /// <summary>
        /// <see cref="GuidAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトがGuidの時有効か。
        /// 事前条件：パラメータにGuidを渡すこと。
        /// 事後条件：trueが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Valid_Guid()
        {
            var guid = Guid.NewGuid();
            var actual = target.IsValid(guid);
            Assert.IsTrue(actual);
        }

        /// <summary>
        /// <see cref="GuidAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトが値が全て0のGuidの時有効か。
        /// 事前条件：パラメータに値が全て0のGuidを渡すこと。
        /// 事後条件：trueが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Valid_Guid_Empty()
        {
            var guid = Guid.Empty;
            var actual = target.IsValid(guid);
            Assert.IsTrue(actual);
        }

        /// <summary>
        /// <see cref="GuidAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトがGuid形式の文字列の時有効か。
        /// 事前条件：パラメータにGuid形式の文字列を渡すこと。
        /// 事後条件：trueが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Valid_Guid_String()
        {
            var actual = target.IsValid("e9f71d40-f4e6-41ac-a61c-e3d4be58ad92");
            Assert.IsTrue(actual);
        }

        /// <summary>
        /// <see cref="GuidAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトがハイフン無しのGuid形式の文字列の時有効か。
        /// 事前条件：パラメータにハイフン無しのGuid形式の文字列を渡すこと。
        /// 事後条件：trueが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Valid_Not_Hyphen_Guid_String()
        {
            var actual = target.IsValid("e9f71d40f4e641aca61ce3d4be58ad92");
            Assert.IsTrue(actual);
        }

        /// <summary>
        /// <see cref="GuidAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトがGuid形式ではない文字列の時無効か。
        /// 事前条件：パラメータにGuid形式ではない文字列を渡すこと。
        /// 事後条件：falseが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Invalid_Not_Guid_String()
        {
            var actual1 = target.IsValid("e9f71d401f4e6541ac4a61c8e3d4be58ad92");
            Assert.IsFalse(actual1);

            var actual2 = target.IsValid("e9f71-e-6-5-4-1-a-4a61c8e3d4be58ad92");
            Assert.IsFalse(actual2);

            var actual3 = target.IsValid("あいうえお");
            Assert.IsFalse(actual3);
        }

        /// <summary>
        /// <see cref="GuidAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトが文字列ではない時無効か。
        /// 事前条件：パラメータに文字列以外を渡すこと。
        /// 事後条件：falseが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Invalid_Not_String()
        {
            var actual1 = target.IsValid(12345);
            Assert.IsFalse(actual1);

            var actual2 = target.IsValid(new object { });
            Assert.IsFalse(actual2);

            var actual3 = target.IsValid(new List<string>());
            Assert.IsFalse(actual3);
        }
    }
}