﻿using CaradaAuthProvider.Api.Controllers.DataAnnotations;
using NUnit.Framework;
using System.Collections.Generic;

namespace CaradaAuthProvider.Api.Test.Controllers.DataAnnotations
{
    /// <summary>
    /// <see cref="CaradaIdTypeAttribute"/>のテストクラス。
    /// </summary>
    [TestFixture]
    public class CaradaIdTypeAttributeTest
    {
        /// <summary>
        /// テスト対象。
        /// </summary>
        private CaradaIdTypeAttribute target;

        /// <summary>
        /// SetUp。
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            target = new CaradaIdTypeAttribute();
        }

        /// <summary>
        /// <see cref="CaradaIdTypeAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトがnullの時有効か。。
        /// 事前条件：パラメータにnullを渡すこと。
        /// 事後条件：trueが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Valid_Null()
        {
            var actual = target.IsValid(null);
            Assert.IsTrue(actual);
        }

        /// <summary>
        /// <see cref="CaradaIdTypeAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトが文字列ではない時無効か。
        /// 事前条件：パラメータに文字列以外を渡すこと。
        /// 事後条件：falseが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Invalid_Not_String()
        {
            var actual1 = target.IsValid(12345);
            Assert.IsFalse(actual1);

            var actual2 = target.IsValid(new object { });
            Assert.IsFalse(actual2);

            var actual3 = target.IsValid(new List<string>());
            Assert.IsFalse(actual3);
        }

        /// <summary>
        /// <see cref="CaradaIdTypeAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトが空文字の時有効か。
        /// 事前条件：パラメータに空文字を渡すこと。
        /// 事後条件：trueが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Valid_String_Empty()
        {
            var actual = target.IsValid(string.Empty);
            Assert.IsTrue(actual);
        }

        /// <summary>
        /// <see cref="CaradaIdTypeAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトが129文字以上の時無効か。
        /// 事前条件：パラメータに129文字以上の文字列を渡すこと。
        /// 事後条件：falseが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Invalid_String_Length()
        {
            var actual = target.IsValid("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890abcdefghijklmnopqrstuvwxyzABC");
            Assert.IsFalse(actual);
        }

        /// <summary>
        /// <see cref="CaradaIdTypeAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトが128文字以内で【^(?=[0-9a-z])([-_.]?[a-z0-9])*[-_.]?$】にマッチしない文字列の時無効か。
        /// 事前条件：パラメータに128文字以内で【^(?=[0-9a-z])([-_.]?[a-z0-9])*[-_.]?$】にマッチしない文字列を渡すこと。
        /// 事後条件：falseが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Invalid_Carada_Id()
        {
            var actual1 = target.IsValid("-0123456789.abcdefg-hijklmnopqrstu_vwxyz");
            Assert.IsFalse(actual1);

            var actual2 = target.IsValid("_0123456789.abcdefg-hijklmnopqrstu_vwxyz-");
            Assert.IsFalse(actual2);

            var actual3 = target.IsValid(".0123456789.abcdefg-hijklmnopqrstu_vwxyz_");
            Assert.IsFalse(actual3);

            var actual4 = target.IsValid("0123456789.abcdefg-hijklmnopqrstu_vwxyz..");
            Assert.IsFalse(actual4);

            var actual5 = target.IsValid("0123456789.abcdefg-._hijklmnopqrstu_vwxyz");
            Assert.IsFalse(actual5);
        }

        /// <summary>
        /// <see cref="CaradaIdTypeAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトが128文字以内で【^(?=[0-9a-z])([-_.]?[a-z0-9])*[-_.]?$】にマッチした文字列の時有効か。
        /// 事前条件：パラメータに128文字以内で【^(?=[0-9a-z])([-_.]?[a-z0-9])*[-_.]?$】にマッチした文字列を渡すこと。
        /// 事後条件：trueが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Valid_Carada_Id()
        {
            var actual1 = target.IsValid("0123456789.abcdefg-hijklmnopqrstu_vwxyz");
            Assert.IsTrue(actual1);

            var actual2 = target.IsValid("0123456789.abcdefg-hijklmnopqrstu_vwxyz-");
            Assert.IsTrue(actual2);

            var actual3 = target.IsValid("0123456789.abcdefg-hijklmnopqrstu_vwxyz_");
            Assert.IsTrue(actual3);

            var actual4 = target.IsValid("0123456789.abcdefg-hijklmnopqrstu_vwxyz.");
            Assert.IsTrue(actual4);
        }
    }
}
