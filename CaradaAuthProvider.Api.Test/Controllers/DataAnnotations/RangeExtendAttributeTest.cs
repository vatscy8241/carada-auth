﻿using CaradaAuthProvider.Api.Controllers.DataAnnotations;
using NUnit.Framework;

namespace CaradaAuthProvider.Api.Test.Controllers.DataAnnotations
{
    /// <summary>
    /// <see cref="RangeExtendAttribute"/>のテストクラス。
    /// </summary>
    [TestFixture]
    public class RangeExtendAttributeTest
    {
        /// <summary>
        /// テスト対象。
        /// </summary>
        private RangeExtendAttribute target;

        /// <summary>
        /// SetUp。
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            target = new RangeExtendAttribute(10, 100);
        }

        /// <summary>
        /// <see cref="RangeExtendAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトがnullの時有効か。。
        /// 事前条件：パラメータにnullを渡すこと。
        /// 事後条件：trueが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Valid_Null()
        {
            var actual = target.IsValid(null);
            Assert.IsTrue(actual);
        }

        /// <summary>
        /// <see cref="RangeExtendAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトが9以下または101以上の時無効か。
        /// 事前条件：パラメータに9以下または101以上の値を渡すこと。
        /// 事後条件：falseが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Invalid_Range()
        {
            var actual1 = target.IsValid(9);
            Assert.IsFalse(actual1);

            var actual2 = target.IsValid(101);
            Assert.IsFalse(actual2);

            var actual3 = target.IsValid("9");
            Assert.IsFalse(actual3);

            var actual4 = target.IsValid("101");
            Assert.IsFalse(actual4);
        }

        /// <summary>
        /// <see cref="RangeExtendAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトが10以上100以下の時有効か。
        /// 事前条件：パラメータに10以上100以下の値を渡すこと。
        /// 事後条件：trueが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Valid_Range()
        {
            var actual1 = target.IsValid(10);
            Assert.IsTrue(actual1);

            var actual2 = target.IsValid(100);
            Assert.IsTrue(actual2);

            var actual3 = target.IsValid("10");
            Assert.IsTrue(actual3);

            var actual4 = target.IsValid("100");
            Assert.IsTrue(actual4);
        }
    }
}