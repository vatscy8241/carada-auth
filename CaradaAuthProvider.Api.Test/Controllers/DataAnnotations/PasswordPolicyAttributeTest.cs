﻿using CaradaAuthProvider.Api.Controllers.DataAnnotations;
using NUnit.Framework;
using System.Collections.Generic;

namespace CaradaAuthProvider.Api.Test.Controllers.DataAnnotations
{
    /// <summary>
    /// <see cref="PasswordPolicyAttribute"/>のテストクラス。
    /// </summary>
    [TestFixture]
    public class PasswordPolicyAttributeTest
    {
        /// <summary>
        /// テスト対象。
        /// </summary>
        private PasswordPolicyAttribute target;

        /// <summary>
        /// SetUp。
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            target = new PasswordPolicyAttribute();
        }

        /// <summary>
        /// <see cref="PasswordPolicyAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトがnullの時有効か。。
        /// 事前条件：パラメータにnullを渡すこと。
        /// 事後条件：trueが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Valid_Null()
        {
            var actual = target.IsValid(null);
            Assert.IsTrue(actual);
        }

        /// <summary>
        /// <see cref="PasswordPolicyAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトが文字列ではない時無効か。
        /// 事前条件：パラメータに文字列以外を渡すこと。
        /// 事後条件：falseが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Invalid_Not_String()
        {
            var actual1 = target.IsValid(12345);
            Assert.IsFalse(actual1);

            var actual2 = target.IsValid(new object { });
            Assert.IsFalse(actual2);

            var actual3 = target.IsValid(new List<string>());
            Assert.IsFalse(actual3);
        }

        /// <summary>
        /// <see cref="PasswordPolicyAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトが空文字の時有効か。
        /// 事前条件：パラメータに空文字を渡すこと。
        /// 事後条件：trueが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Valid_String_Empty()
        {
            var actual = target.IsValid(string.Empty);
            Assert.IsTrue(actual);
        }

        /// <summary>
        /// <see cref="PasswordPolicyAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトが7文字以下の時、または129文字以上の時無効か。
        /// 事前条件：パラメータに7文字以下または129文字以上の文字列を渡すこと。
        /// 事後条件：falseが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Invalid_String_Length()
        {
            var actual1 = target.IsValid("1234abc");
            Assert.IsFalse(actual1);

            var actual2 = target.IsValid("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890abcdefghijklmnopqrstuvwxyzABC");
            Assert.IsFalse(actual2);
        }

        /// <summary>
        /// <see cref="PasswordPolicyAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトが1種類の文字列で構成されている場合無効か。
        /// 事前条件：パラメータに1種類の文字列で構成されている文字列を渡すこと。
        /// 事後条件：falseが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Invalid_String_Consisted_Of_One_Char()
        {
            var actual1 = target.IsValid("11111111111111111");
            Assert.IsFalse(actual1);

            var actual2 = target.IsValid("aaaaaaaaaaaaaaa");
            Assert.IsFalse(actual2);
        }

        /// <summary>
        /// <see cref="PasswordPolicyAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトが8文字以上128文字以内で半角英数記号以外の文字も使用した文字列の時無効か。
        /// 事前条件：パラメータに8文字以上128文字以内で半角英数記号以外の文字も使用した文字列を渡すこと。
        /// 事後条件：falseが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Invalid_String_Not_Half_Size_Alphanumeric_Symbols()
        {
            var actual1 = target.IsValid("0987lkj./-Ａ");
            Assert.IsFalse(actual1);

            var actual2 = target.IsValid("12345asdfgあいう");
            Assert.IsFalse(actual2);
        }

        /// <summary>
        /// <see cref="PasswordPolicyAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトが8文字以上128文字以内で半角英数記号のみ使用した文字列の時有効か。
        /// 事前条件：パラメータに8文字以上128文字以内で半角英数記号のみ使用した文字列を渡すこと。
        /// 事後条件：trueが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Valid_Policy()
        {
            var actual1 = target.IsValid("0123456789abcdAVB-._adfs_p");
            Assert.IsTrue(actual1);

            var actual2 = target.IsValid("rtfyguhi___...--8960zm");
            Assert.IsTrue(actual2);

            var actual3 = target.IsValid("1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ,./<>?;':\"[]{}|-=_+`~!@#$%^&*()\\");
            Assert.IsTrue(actual3);
        }
    }
}