﻿using CaradaAuthProvider.Api.Controllers.DataAnnotations;
using NUnit.Framework;
using System.Collections.Generic;

namespace CaradaAuthProvider.Api.Test.Controllers.DataAnnotations
{
    /// <summary>
    /// <see cref="OriginalGuidAttribute"/>のテストクラス。
    /// </summary>
    [TestFixture]
    public class OriginalGuidAttributeTest
    {
        /// <summary>
        /// テスト対象。
        /// </summary>
        private OriginalGuidAttribute target;

        /// <summary>
        /// SetUp。
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            target = new OriginalGuidAttribute();
        }

        /// <summary>
        /// <see cref="OriginalGuidAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトがnullの時有効か。。
        /// 事前条件：パラメータにnullを渡すこと。
        /// 事後条件：trueが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Valid_Null()
        {
            var actual = target.IsValid(null);
            Assert.IsTrue(actual);
        }

        /// <summary>
        /// <see cref="OriginalGuidAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトが文字列ではない時無効か。
        /// 事前条件：パラメータに文字列以外を渡すこと。
        /// 事後条件：falseが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Invalid_Not_String()
        {
            var actual1 = target.IsValid(12345);
            Assert.IsFalse(actual1);

            var actual2 = target.IsValid(new object { });
            Assert.IsFalse(actual2);

            var actual3 = target.IsValid(new List<string>());
            Assert.IsFalse(actual3);
        }

        /// <summary>
        /// <see cref="OriginalGuidAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトが空文字の時有効か。
        /// 事前条件：パラメータに空文字を渡すこと。
        /// 事後条件：trueが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Valid_String_Empty()
        {
            var actual = target.IsValid(string.Empty);
            Assert.IsTrue(actual);
        }

        /// <summary>
        /// <see cref="OriginalGuidAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトが31文字以下の時、または33文字以上の時無効か。
        /// 事前条件：パラメータに31文字以下または33文字以上の文字列を渡すこと。
        /// 事後条件：falseが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Invalid_String_Length()
        {
            var actual1 = target.IsValid("123456789012345678901234567890A");
            Assert.IsFalse(actual1);

            var actual2 = target.IsValid("123456789012345678901234567890ABC");
            Assert.IsFalse(actual2);
        }

        /// <summary>
        /// <see cref="OriginalGuidAttribute.IsValid(object)"/>のテスト。
        /// CASE：オブジェクトが32文字で[0-9a-zA-Z]のみ使用した文字列の時有効か。
        /// 事前条件：パラメータに32文字で[0-9a-zA-Z]のみ使用した文字列を渡すこと。
        /// 事後条件：trueが返却されること。
        /// </summary>
        [Test]
        public void TestIsValid_Valid_Policy()
        {
            var actual1 = target.IsValid("123456789012345678901234567890AB");
            Assert.IsTrue(actual1);

            var actual2 = target.IsValid("abcdefghijklmnopqrstuvwxyz1234ZX");
            Assert.IsTrue(actual2);
        }
    }
}