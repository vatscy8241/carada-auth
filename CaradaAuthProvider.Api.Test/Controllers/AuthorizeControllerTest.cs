﻿using CaradaAuthProvider.Api.Controllers;
using CaradaAuthProvider.Api.Controllers.Conditions;
using CaradaAuthProvider.Api.Persistence.Repositories.Context;
using CaradaAuthProvider.Api.Services;
using CaradaAuthProvider.Api.Services.Dtos;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Hosting;

namespace CaradaAuthProvider.Api.Test.Controllers
{
    [TestClass]
    public class AuthorizeControllerTest
    {

        // テスト対象
        private AuthorizeController target;
        private class MockAuthorizeController : AuthorizeController
        {
            private AuthorizeService mockAuthorizeService;

            public MockAuthorizeController(Mock<AuthorizeService> mockAuthorizeService)
            {
                this.mockAuthorizeService = mockAuthorizeService.Object;
            }
            protected override AuthorizeService CreateAuthorizeService()
            {
                return mockAuthorizeService;
            }
        }

        private Mock<AuthorizeService> mockAuthorizeService;

        [TestInitialize]
        public void initialize()
        {
            mockAuthorizeService = new Mock<AuthorizeService>(null, null);
            target = new MockAuthorizeController(mockAuthorizeService);

            // DbContextの設定
            var dbContext = typeof(AbstractApiController).GetField("_caradaIdPDbContext", BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Instance);
            dbContext.SetValue(target, new DbContextFactory().NewCaradaAuthProviderDbContext());

            // Requestの設定
            target.Request = new HttpRequestMessage()
            {
                Properties = { { HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration() } }
            };
        }

        /// <summary>
        /// [対象] 認可処理
        /// [条件] 認可サービスの結果が正常の場合
        /// [結果] アクセストークン情報を返却
        /// </summary>
        [TestMethod]
        public void Authorize()
        {

            AuthorizeCondition condition = new AuthorizeCondition();
            AuthorizeResultDto dto = new AuthorizeResultDto()
            {
                AccessToken = "AaCbCcEdSeSfTgOhKiEjNk",
                RefreshToken = "R1E2F3R4E5S6H7T8O9K0EN"
            };

            mockAuthorizeService.Setup(p => p.Authorize(condition)).Returns(dto);

            // 実行
            HttpResponseMessage result = target.Authorize(condition) as HttpResponseMessage;

            // アサート
            Assert.IsNotNull(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
            Assert.AreEqual(
                "{\"AccessToken\":\"AaCbCcEdSeSfTgOhKiEjNk\",\"RefreshToken\":\"R1E2F3R4E5S6H7T8O9K0EN\"}",
                result.Content.ReadAsStringAsync().Result);
        }

        /// <summary>
        /// [対象] アクセストークン検証API
        /// [条件] 認可サービスの結果が正常の場合
        /// [結果] アクセストークン検証結果を返却
        /// </summary>
        [TestMethod]
        public void Introspect()
        {
            TokenIntrospectCondition condition = new TokenIntrospectCondition();
            TokenIntrospectResultDto dto = new TokenIntrospectResultDto
            {
                TokenIntrospect = new TokenIntrospectDto
                {
                    Usable = true,
                    Uid = "uid",
                    ClientId = "ClientId",
                    ExpiresAt = 1,
                    Scopes = new string[] { "openid", "vital", "health_check" },
                    Refreshable = true,
                    Agent = "agent"
                }
            };

            mockAuthorizeService.Setup(p => p.Introspect(condition)).Returns(dto);

            // 実行
            HttpResponseMessage result = target.Introspect(condition) as HttpResponseMessage;

            // アサート
            Assert.IsNotNull(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
            Assert.AreEqual(
                "{\"Usable\":true,\"Uid\":\"uid\",\"ClientId\":\"ClientId\",\"ExpiresAt\":1,\"Scopes\":[\"openid\",\"vital\",\"health_check\"],\"Refreshable\":true,\"Agent\":\"agent\"}",
                result.Content.ReadAsStringAsync().Result);
        }

        /// <summary>
        /// [対象] トークンリフレッシュAPI
        /// [条件] 認可サービスの結果が正常の場合
        /// [結果] アクセストークン検証結果を返却
        /// </summary>
        [TestMethod]
        public void Refresh()
        {
            TokenRefreshCondition condition = new TokenRefreshCondition();
            TokenRefreshResultDto dto = new TokenRefreshResultDto
            {
                TokenRefresh = new TokenRefreshDto
                {
                    AccessToken = "accessToken",
                    RefreshToken = "refreshToken"
                }
            };

            mockAuthorizeService.Setup(p => p.Refresh(condition)).Returns(dto);

            // 実行
            HttpResponseMessage result = target.Refresh(condition) as HttpResponseMessage;

            // アサート
            Assert.IsNotNull(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
            Assert.AreEqual(
                "{\"AccessToken\":\"accessToken\",\"RefreshToken\":\"refreshToken\"}",
                result.Content.ReadAsStringAsync().Result);
        }
    }
}
