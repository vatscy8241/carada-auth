﻿using CaradaAuthProvider.Api.Controllers;
using CaradaAuthProvider.Api.Controllers.Conditions;
using CaradaAuthProvider.Api.Services;
using Moq;
using NUnit.Framework;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Hosting;

namespace CaradaAuthProvider.Api.Test.Controllers
{
    /// <summary>
    /// <see cref="MailController"/>のテスト。
    /// </summary>
    [TestFixture]
    public class MailControllerTest
    {
        /// <summary>
        /// テスト対象。
        /// </summary>
        private MailController target;

        /// <summary>
        /// メールサービスのモック。
        /// </summary>
        private Mock<MailService> mockMailService;

        /// <summary>
        /// <see cref="MailController.Send(SendMailCondition)"/>のテスト。
        /// CASE：<see cref="MailController.Send(SendMailCondition)"/>が正常なレスポンスを返すか。
        /// 事前条件：特に無し。
        /// 事後条件：<see cref="MailService.SendMail(SendMailCondition)"/>が1度呼ばれていること。結果がSUCCESSであること。
        /// </summary>
        [Test]
        public void TestSend()
        {
            mockMailService = new Mock<MailService>(null, null);
            mockMailService.Setup(m => m.SendMail(It.IsAny<SendMailCondition>())).Returns(Task.FromResult(false));


            target = new MockMailController(mockMailService);
            target.Request = new HttpRequestMessage()
            {
                Properties = { { HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration() } }
            };

            var actual = target.Send(new SendMailCondition()).Result;

            mockMailService.Verify(m => m.SendMail(It.IsAny<SendMailCondition>()), Times.Once);

            Assert.That(actual, Is.Not.Null);
            Assert.AreEqual(HttpStatusCode.OK, actual.StatusCode);
            Assert.AreEqual("{\"Result\":\"SUCCESS\"}", actual.Content.ReadAsStringAsync().Result);
        }

        /// <summary>
        /// <see cref="MailController"/>のテスト用モッククラス。
        /// </summary>
        private class MockMailController : MailController
        {
            /// <summary>
            /// メールサービスのモック。
            /// </summary>
            private MailService mockMailService;

            /// <summary>
            /// コンストラクタ。
            /// </summary>
            /// <param name="mockMailService">メールサービスのモック</param>
            public MockMailController(Mock<MailService> mockMailService)
            {
                this.mockMailService = mockMailService.Object;
            }

            /// <summary>
            /// <see cref="MailController.CreateMailService"/>
            /// </summary>
            /// <returns>メールサービスのモック</returns>
            protected override MailService CreateMailService()
            {
                return mockMailService;
            }
        }
    }
}