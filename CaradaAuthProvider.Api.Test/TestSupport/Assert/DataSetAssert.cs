﻿using System;
using System.Collections.Generic;
using System.Data;

namespace CaradaAuthProvider.Api.Test.TestSupport.Assert
{
    /// <summary>
    /// <see cref="DataSet"/>用の検証クラス
    /// </summary>
    public class DataSetAssert
    {
        /// <summary>
        /// 指定のDataSetが同一かどうか検証する。
        /// </summary>
        /// <param name="expected">検証データ</param>
        /// <param name="actual">実行データ</param>
        public static void AreEqual(DataSet expected, DataSet actual)
        {
            NUnit.Framework.Assert.AreEqual(expected.Tables.Count, actual.Tables.Count, "テーブルの数が異なります。");
            for (int i = 0; i < expected.Tables.Count; i++)
            {
                string tableName = expected.Tables[i].TableName;
                Console.WriteLine(tableName);

                AreEqual(expected.Tables[tableName], actual.Tables[tableName], null);
            }
        }

        /// <summary>
        /// 指定のDataSetが同一かどうか検証する。
        /// </summary>
        /// <param name="expected">検証データ</param>
        /// <param name="actual">実行データ</param>
        /// <param name="ignoreColumnsPerTable">無視するカラム</param>
        public static void AreEqual(DataSet expected, DataSet actual, IDictionary<string, string[]> ignoreColumnsPerTable)
        {
            NUnit.Framework.Assert.AreEqual(expected.Tables.Count, actual.Tables.Count, "テーブルの数が異なります。");
            for (var i = 0; i < expected.Tables.Count; i++)
            {
                string tableName = expected.Tables[i].TableName;

                string[] ignoreColumns = null;
                if (ignoreColumnsPerTable.ContainsKey(tableName))
                {
                    ignoreColumns = ignoreColumnsPerTable[tableName];
                }

                AreEqual(expected.Tables[tableName], actual.Tables[tableName], ignoreColumns);
            }
        }

        /// <summary>
        /// 指定のDataTableが同一かどうか検証する。
        /// </summary>
        /// <param name="expected">検証データ</param>
        /// <param name="actual">実行データ</param>
        /// <param name="ignoreColumns">無視するカラム</param>
        private static void AreEqual(DataTable expected, DataTable actual, string[] ignoreColumns)
        {
            NUnit.Framework.Assert.AreEqual(expected.Rows.Count, actual.Rows.Count, "行の数が異なります。");
            for (var i = 0; i < expected.Rows.Count; i++)
            {
                DataSetAssert.AreEqual(expected.Rows[i], actual.Rows[i], i, ignoreColumns);
            }
        }

        /// <summary>
        /// 指定のDataRowが同一かどうか検証する。
        /// </summary>
        /// <param name="expected">検証データ</param>
        /// <param name="actual">実行データ</param>
        /// <param name="rowIndex">行番号</param>
        /// <param name="ignoreColumns">無視するカラム</param>
        private static void AreEqual(DataRow expected, DataRow actual, int rowIndex, string[] ignoreColumns)
        {
            NUnit.Framework.Assert.AreEqual(expected.ItemArray.Length, actual.ItemArray.Length, "カラムの数が異なります。");
            for (var i = 0; i < expected.Table.Columns.Count; i++)
            {
                if (IsIgnoreColumns(ignoreColumns, expected.Table.Columns[i].ColumnName))
                {
                    continue;
                }

                if (expected.IsNull(i))
                {
                    if (actual[i] is String)
                    {
                        NUnit.Framework.Assert.That((string)actual[i], NUnit.Framework.Is.Null.Or.Empty, "行のデータが異なります。【テーブル名】{0}【行番号】{1}【カラム】{2}", expected.Table.TableName, rowIndex.ToString(), expected.Table.Columns[i].ColumnName);
                    }
                    else
                    {
                        NUnit.Framework.Assert.IsTrue(actual.IsNull(i), "行のデータが異なります。【テーブル名】{0}【行番号】{1}【カラム】{2}", expected.Table.TableName, rowIndex.ToString(), expected.Table.Columns[i].ColumnName);
                    }

                    continue;
                }

                NUnit.Framework.Assert.AreEqual(expected[i], actual[i], "行のデータが異なります。【テーブル名】{0}【行番号】{1}【カラム】{2}", expected.Table.TableName, rowIndex.ToString(), expected.Table.Columns[i].ColumnName);
            }
        }

        /// <summary>
        /// 無視するカラムかどうか。
        /// </summary>
        /// <param name="ignoreColumns">無視するカラムの配列</param>
        /// <param name="targetColumn">対象カラム</param>
        /// <returns></returns>
        private static bool IsIgnoreColumns(string[] ignoreColumns, string targetColumn)
        {
            if (ignoreColumns == null)
            {
                return false;
            }

            foreach (string ignoreColumn in ignoreColumns)
            {
                if (ignoreColumn.Equals(targetColumn))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
