# CaradaAuthProvider

## 使用ライブラリ

| ライブラリ | バージョン | 備考 |
|:-------|:--------|:-----|
|.NET Framework|4.5.2|4.6.1が最新だが、エラーのためバージョン落とした。|
|ASP.NET Identity|2.0||
|AzureSDK|2.9||

## 環境構築手順

1. GitLabからソースをチェックアウトする。

1. ソリューションを開く。

1. メニュー => ツール => 拡張機能と更新プログラム => オンライン<br>
検索窓に【Spec Flow】を入力し、【SpeｃFlow for Visual Studio 2015】をインストール。

1. メニュー => ツール => 拡張機能と更新プログラム => オンライン<br>
検索窓に【power tools 2015】を入力し、【Productivity Power Tools 2015】をインストール。

1. メニュー => ツール => 拡張機能と更新プログラム => オンライン<br>
検索窓に【NUnit Test Adapter】を入力し、【NUnit3 Test Adapter】をインストール。

1. メニュー => ツール => オプション<br>
Productivity Power Tools => PowerCommands => General<br>
下記2つにチェックを入れる。

    - Format document on save
    - Remove and Sort Usings on save

1. メニュー => ツール => オプション<br>
テキストエディター => すべての言語 => タブ

    - タブのサイズ：4<br>
    - インデントのサイズ：4<br>
    - 空白の挿入にチェックを入れる。

1. 環境変数の設定<br>
下記を設定する。

    | 変数名 | 変数値 | 備考 |
    |:------|:-------|:-----|
    |MS_IIS_EXPRESS_HOME|C:\Program Files (x86)\IIS Express|**iisexpress.exeが格納されているディレクトリ**|
    |MS_DOTNET_HOME|C:\Program Files (x86)\MSBuild\14.0\Bin|**MSBuild.exeが格納されているディレクトリ**|

1. DBローカルDBの作成<br>
Nugetパッケージマネージャーコンソールを起動し、以下のコマンドを実行する。

    ```
    update-database -ProjectName CaradaAuthProvider.Api -StartUpProjectName CaradaAuthProvider.Api -ConnectionStringName CaradaAuthProviderDbContext -ConfigurationTypeName Configuration -Verbose
    ```

1. CaradaAuthProvider.Api.TestプロジェクトのSettings.settingsの値を下記のように設定する。

    | 名前 | 値 |
    |:-----|:---|
    |SolutionPathRoot|ソリューションを格納しているディレクトリまでのパス|

    ex) D:\Users\\{User名}\Projects\HC\carada-auth\

1. Visual Studio 2015の再起動とソリューション全体のクリーン・ビルドを行う。

## IntegrationTestプロジェクト追加手順

**※Gitに上がっているものは設定済みのため、今回は実施の必要はない。参考までにIntegrationTestの設定手順を載せておく。**

1. 【Unit Test Project】をソリューションに追加する。

1. 自動で作成される【UnitTest1.cs】ファイルを削除する。

1. SpecFlowランナーをインストールする。(ver.2.0.0 ※2016/04/27現在)

    1. プロジェクトを右クリックし、NuGetパッケージの管理を選択する。

    1. 【SpecRun】で検索し、SpecRun for SpecFlow2.0.0(SpecRun.SpecFlow)をインストールする。<br>
    **※SpecRun.SpecFlow.-x-x-xは旧バージョン**

    SpecFlow:2.0.0<br>
    SpecRun.SpecFlow:1.3.0<br>
    SpecRun.Runner:1.3.0<br>
    がインストールされる。<br>

    パッケージマネージャーコンソールで下記コマンドを入力してもインストールできる。<br>

    ```
    PM> Install-Package SpecRun.SpecFlow
    ```

1. Microsoft.CSharpを参照アセンブリに追加する。

    1. 参照を右クリックし、参照の追加を選択する。

    1. Microsoft.CSharpを選択し、OKをクリックする。

## プロジェクトへのMigrationの適用

**※Gitに上がっているものは設定済みのため、今回は実施の必要はない。参考までにMigrationの設定手順を載せておく。**

1. マイグレーション用のConfigrationファイルを作成する。<br>
Nugetパッケージマネージャーコンソールを起動し、以下のコマンドを実行する。

    ```
    enable-migrations -ProjectName 'プロジェクト名' -StartUpProjectName 'プロジェクト名' -ContextTypeName 'コンテキストタイプ名' -MigrationsDirectory 'Migrationのディレクトリ名' -ConnectionStringName 'ConnectionString名' -Verbose
    ```

    ex)CaradaAuthProvider.Apiプロジェクトに適用する場合。

    ```
    enable-migrations -ProjectName CaradaAuthProvider.Api -StartUpProjectName CaradaAuthProvider.Api -ContextTypeName CaradaAuthProviderDbContext -MigrationsDirectory Migrations -ConnectionStringName CaradaAuthProviderDbContext -Verbose
    ```

1. Migrationsディレクトリが生成され、Configuration.csファイルが生成される。

1. 初期処理で基本テーブルを生成するマイグレーションファイルを生成する。<br>
Nugetパッケージマネージャーコンソールを起動し、以下のコマンドを実行する。

    ```
    add-migration '作成するマイグレーションファイル名' -ProjectName '適用するプロジェクト名' -StartUpProjectName '適用するプロジェクト名' -ConnectionStringName 'ConnectionString名' -ConfigurationTypeName 'Configrationファイル名' -Verbose
    ```

    ex)CaradaAuthProvider.Apiプロジェクトに適用する場合。

    ```
    add-migration Initial -ProjectName CaradaAuthProvider.Api -StartUpProjectName CaradaAuthProvider.Api -ConnectionStringName CaradaAuthProviderDbContext -ConfigurationTypeName Configuration -Verbose
    ```

1. MigrationsディレクトリにyyyyMMddhhmmssS_Initial.csファイルが生成される。

1. マイグレーションに追加された情報を元に、実際のDBにテーブルの生成・変更を行う。<br>
Nugetパッケージマネージャーコンソールを起動し、以下のコマンドを実行する。

    ```
    update-database -ProjectName 'プロジェクト名' -StartUpProjectName 'プロジェクト名' -ConfigurationTypeName '対象のCongigrationファイル' -ConnectionStringName 'ConnectionString名' -Verbose
    ```

    ex)CaradaAuthProvider.Apiプロジェクトに適用する場合。

    ```
    update-database -ProjectName CaradaAuthProvider.Api -StartUpProjectName CaradaAuthProvider.Api -ConnectionStringName CaradaAuthProviderDbContext -ConfigurationTypeName Configuration -Verbose
    ```

1. DefaultConnectionというDBが作成される。

### Enable-Migrationsコマンドオプション詳細

|オプション|説明|
|:-----|:---|
|-EnableAutomaticMigrations|自動マイグレーションをするかどうか。<br>省略した場合、無効となる。|
|-ContextTypeName [ContextTypeName]|使用するコンテキストタイプを指定する。<br>省略した場合、対象のプロジェクトの単一のコンテキストタイプが使用される。|
|-MigrationsDirectory [MigrationsDirectory]|マイグレーションファイルを格納するディレクトリ名を指定する。<br>省略した場合、【Migrations】ディレクトリが作成される。|
|-ProjectName [ProjectName]|マイグレーションのConfigurationファイルが作成されるプロジェクトを指定する。<br>省略した場合、Nugetパッケージマネージャーコンソールで選択されたデフォルトのプロジェクトが使用される。|
|-StartUpProjectName [ProjectName]|ConnectionStringsが設定されたConfigrationファイルを含むプロジェクトを指定する。<br>省略した場合、指定されたプロジェクトのConfigrationファイルが使用される。|
|-ContextProjectName [ProjectName]|使用するDbContextクラスを含むプロジェクトを指定する。<br>省略した場合、マイグレーションのために使用したのと同じプロジェクトを検索する。|
|-ConnectionStringName [ConnectionString]|アプリケーションのConfigrationファイルからConnectionStringを指定する。|
|-ConnectionString [ConnectionString]|使用するConnectionStringを指定する。<br>省略した場合、コンテキストのデフォルトの接続が使用される。|
|-ConnectionProviderName [ProviderName]|ConnectionStringのProvider不変(invariant)名を指定する。|
|-Force|特定のプロジェクトに対して複数回実行する時に、マイグレーション設定が上書きされることを指定する。|
|-ContextAssemblyName [Assemblyname]|DbContextクラスを使用することを含むアセンブリ名を指定する。<br>コンテキストが参照されたアセンブリではなく、ソリューションのプロジェクトに含まれる場合、ContextprojectNameの代わりにこのパラメータを使用する。|
|-Verbose|詳細を表示したい場合に指定する。|

### Add-Migrationコマンドオプション

|オプション|説明|
|:-----|:---|
|-Name [Scriptname]|カスタムスクリプト名を指定する。|
|-ProjectName [ProjectName]|使用するマイグレーションのConfigrationを含むプロジェクトを指定する。<br>省略した場合、Nugetパッケージマネージャーコンソールで選択されたデフォルトのプロジェクトが使用される。。|
|-StartUpProjectName [ProjectName]|ConnectionStringが使用されたConfigurationファイルを含むプロジェクト指定する。<br>省略した場合、指定されたプロジェクトの設定ファイルが使用される。|
|-ConfigurationTypeName [ConfigrationFile]|マイグレーションのConfigrationファイルを指定する。<br>省略した場合、ターゲットプロジェクト内の単一のマイグレーションのConfigrationタイプを検索する。|
|-ConnectionStringName [ConnectionString]|アプリケーションのConfigrationファイルからConnectionStringを指定する。|
|-ConnectionString [ConnectionString]|使用するConnectionStringを指定する。<br>省略した場合、コンテキストのデフォルトの接続が使用される。|
|-ConnectionProviderName [ProviderName]|ConnectionStringのProvider不変(invariant)名を指定する。|
|-IgnoreChanges|現在のモデルで検出された保留中の変更を無視し、空のマイグレーションを設定。<br>これは既存のデータベースのマイグレーションを可能にするため、初期の空のマイグレーションを作成するために使用する。<br>**※対象のデータベーススキーマが現在のモデルと互換性があることが前提。**|
|-Verbose|詳細を表示した場合に指定する。|

### Update-Databaseコマンドオプション

|オプション|説明|
|:-----|:---|
|-SourceMigration [Migration]|-Scriptでのみ有効。<br>アップデートのスタート地点として使用する特定のマイグレーション名を指定する。<br>省略した場合、データベースのマイグレーションの最後に適用される。|
|-TargetMigration [Migration]|データベースを更新するために特定のマイグレーション名を指定する。<br>省略した場合、現在のモデルが使用される。|
|-Script [Script]|直接保留中の変更を実行せず、SQLスクリプトを生成する。|
|-Force|データベースの自動マイグレーション中のデータの損失を許容する。|
|-ProjectName [ProjectName]|使用するマイグレーションのConfigrationを含むプロジェクトを指定する。<br>省略した場合、Nugetパッケージマネージャーコンソールで選択されたデフォルトのプロジェクトが使用される。|
|-StartUpProjectName [ProjectName]|ConnectionStringが使用されたConfigurationファイルを指定する。<br>省略した場合、指定されたプロジェクトの設定ファイルが使用される。|
|-ConfigurationTypeName [ConfigrationFile]|マイグレーションのConfigrationファイルを指定する。<br>省略した場合、ターゲットプロジェクト内の単一のマイグレーションのConfigrationタイプを検索する。|
|-ConnectionStringName [ConnectionString]|アプリケーションのConfigrationファイルからConnectionStringを指定する。|
|-ConnectionString [ConnectionString]|使用するConnectionStringを指定する。<br>省略した場合、コンテキストのデフォルトの接続が使用される。|
|-ConnectionProviderName [ProviderName]|ConnectionStringのProvider不変(invariant)名を指定する。|
|-Verbose|実行されるクエリの詳細を表示したい場合に指定する。|

## 追加モジュール

**※Gitに上がっているものは設定済みのため、今回は実施の必要はない。参考までにモジュールの追加手順を載せておく。**

### CaradaAuthProvider.Api用

- [MTI.HC.AuthleteLib](https://redmine.hc-mti.com/projects/phr-fw-project/wiki/CARADA%E3%83%AA%E3%83%8B%E3%83%A5%E3%83%BC%E3%82%A2%E3%83%AB#AuthleteライブラリC)
- [NLog](http://nlog.codeplex.com/)
- [Sendgrid](https://github.com/sendgrid/sendgrid-csharp)
- [SendGrid.SmtpApi](https://github.com/sendgrid/smtpapi-csharp)

インストールコマンド
```
PM>Install-Package NLog
```

|モジュール名|バージョン|
|:--------|:------|
|MTI.HC.AuthleteLib|0.1.1|
|NLog|4.3.3|
|Sendgrid|6.3.4|
|SendGrid.SmtpApi|1.3.1|

### CaradaAuthProvider.Api.Test用

- [NUnit](http://www.nunit.org/)
- [NDbUnit.Core](https://www.nuget.org/packages/NDbUnit.Core/)
- [NDbUnit.SqlClient](https://www.nuget.org/packages/NDbUnit.SqlClient/)

インストールコマンド
```
PM> Install-Package NUnit
PM> Install-Package NDbUnit.Core
PM> Install-Package NDbUnit.SqlClient
```

|モジュール名|バージョン|
|:--------|:------|
|NUnit|3.2.1|
|NDbUnit.Core|1.6.8|
|NDbUnit.SqlClient|1.6.8|

### CaradaAuthProvider.Api.Test.Integration用

- [DynamicJson](http://dynamicjson.codeplex.com/)
- [NUnit](http://www.nunit.org/)
- [NDbUnit.Core](https://www.nuget.org/packages/NDbUnit.Core/)
- [NDbUnit.SqlClient](https://www.nuget.org/packages/NDbUnit.SqlClient/)
- [SpecFlow](http://www.specflow.org/)
- [SpecFlow - wiki](https://github.com/techtalk/SpecFlow/wiki)
- [SpecRun](https://www.nuget.org/packages/SpecRun.Runner/)

インストールコマンド
```
PM> Install-Package DynamicJson
PM> Install-Package NUnit
PM> Install-Package NDbUnit.Core
PM> Install-Package NDbUnit.SqlClient
```

|モジュール名|バージョン|
|:--------|:------|
|DynamicJson|1.2.0|
|NUnit|3.2.1|
|NDbUnit.Core|1.6.8|
|NDbUnit.SqlClient|1.6.8|
|SpecFlow|2.0.0|
|SpecRun.SpecFlow|1.3.0|
|SpecRun.Runner|1.3.0|

## 参考

- [EF Migrations Command Reference](https://coding.abel.nu/2012/03/ef-migrations-command-reference/)
- [SendGrid - for SubAccount](https://app.sendgrid.com/)
