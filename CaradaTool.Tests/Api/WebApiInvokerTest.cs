﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaTool.Api;
using MTI.CaradaTool.Tests.TestHelpers;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace MTI.CaradaTool.Tests.Api
{
    /// <summary>
    /// WebApiInvokerテストクラス
    /// </summary>
    [TestClass]
    public class WebApiInvokerTest
    {
        class MockedWebApiInvoker : WebApiInvoker
        {
            private class FakeResponseHandler : DelegatingHandler
            {
                protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
                {
                    return await Task.FromResult<HttpResponseMessage>(new HttpResponseMessage(HttpStatusCode.OK));
                }
            }

            protected override HttpClient CreateHttpClient()
            {
                return new HttpClient(new FakeResponseHandler());
            }
        }

        [TestMethod]
        public void PostJson_正常系_APIをコールできる()
        {
            CreateApiMockSetting();
            var target = new MockedWebApiInvoker();

            var header = new Dictionary<string, List<string>>
            {
                {"headerKey", new List<string>{"headerValue"}}
            };
            var result = target.PostJson("http://test.com/", header, new JObject());
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        /// <summary>
        /// APIモックにテストデータを投入します。
        /// </summary>
        private void CreateApiMockSetting()
        {
            var context = new ApiMockStorageContext();
            var tokenTable = context.MockSettingTable;
            tokenTable.InsertOrReplaceEntity(new MockSetting()
            {
                PartitionKey = "ClientTemplateWebApiInvokerTest",
                RowKey = "1",
                ContentType = "application/json",
                ResponseBody = "{\"result\":\"OK\"}",
                HeadersJson = "{\"headerKey\":[\"headerValue\"]}"
            });
        }
    }
}
