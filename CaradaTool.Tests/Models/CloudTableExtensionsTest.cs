﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage.Table;
using Mti.ClientTemplate.Web.Models;
using Microsoft.WindowsAzure.Storage;

namespace Mti.ClientTemplate.Web.Tests.Models
{
    [TestClass]
    public class CloudTableExtensionsTest
    {
        private CloudTable SomeTable;

        public class SomeTableEntity : TableEntity
        {
            public int No { get; set; }
        }

        [TestInitialize]
        public void TestInitialize()
        {
            // エミュレータから取得
            SomeTable = CloudStorageAccount.DevelopmentStorageAccount.CreateCloudTableClient().GetTableReference("SomeTable");
            SomeTable.CreateIfNotExists();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            SomeTable.DeleteIfExists();
        }

        [TestMethod]
        public void InsertEntity_異常系_引数のentityがnullの場合例外が発生する()
        {
            try
            {
                TableResult result = CloudTableExtensions.InsertEntity(SomeTable, null);
            }
            catch (ArgumentNullException)
            {
                // ArgumentNullException
                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        public void InsertEntity_正常系_標準テスト()
        {
            ITableEntity insertEntity = new SomeTableEntity()
            {
                PartitionKey = "pkey",
                RowKey = "rkey"
            };

            // Insert
            TableResult result = CloudTableExtensions.InsertEntity(SomeTable, insertEntity);

            // 検索して確認
            SomeTableEntity getEntity = CloudTableExtensions.RetrieveEntity<SomeTableEntity>(SomeTable, "pkey", "rkey");

            Assert.AreEqual(getEntity.PartitionKey, "pkey");
            Assert.AreEqual(getEntity.RowKey, "rkey");
        }

        [TestMethod]
        public void DeleteEntity_異常系_引数のentityがnullの場合例外が発生する()
        {
            try
            {
                TableResult result = CloudTableExtensions.DeleteEntity(SomeTable, null);
            }
            catch (ArgumentNullException)
            {
                // ArgumentNullException
                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        public void DeleteEntity_正常系_標準テスト()
        {
            ITableEntity entity = new SomeTableEntity()
            {
                PartitionKey = "pkey",
                RowKey = "rkey"
            };

            // Insert
            TableResult result = CloudTableExtensions.InsertEntity(SomeTable, entity);

            // Delete
            TableResult result2 = CloudTableExtensions.DeleteEntity(SomeTable, entity);

            // 検索されないこと
            SomeTableEntity getEntity = CloudTableExtensions.RetrieveEntity<SomeTableEntity>(SomeTable, "pkey", "rkey");

            Assert.IsNull(getEntity);
        }

        [TestMethod]
        public void DeleteEntity_異常系_削除対象がない場合例外が発生する()
        {
            ITableEntity entity = new SomeTableEntity()
            {
                PartitionKey = "pkey",
                RowKey = "rkey"
            };

            try
            {
                TableResult result = CloudTableExtensions.DeleteEntity(SomeTable, entity);
            }
            catch (StorageException)
            {
                // StorageException
                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        public void ReplaceEntity_異常系_引数のentityがnullの場合例外が発生する()
        {
            try
            {
                TableResult result = CloudTableExtensions.ReplaceEntity(SomeTable, null);
            }
            catch (ArgumentNullException)
            {
                // ArgumentNullException
                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        public void ReplaceEntity_正常系_標準テスト()
        {
            SomeTableEntity entity = new SomeTableEntity()
            {
                PartitionKey = "pkey",
                RowKey = "rkey"
            };

            // Insert
            TableResult result = CloudTableExtensions.InsertEntity(SomeTable, entity);

            entity.No = 2;

            // Replace
            TableResult result2 = CloudTableExtensions.ReplaceEntity(SomeTable, entity);

            // 検索して確認
            SomeTableEntity getEntity = CloudTableExtensions.RetrieveEntity<SomeTableEntity>(SomeTable, "pkey", "rkey");

            Assert.AreEqual(getEntity.No, 2);
        }

        [TestMethod]
        public void ReplaceEntity_異常系_更新対象がない場合例外が発生する()
        {
            SomeTableEntity entity = new SomeTableEntity()
            {
                PartitionKey = "pkey",
                RowKey = "rkey"
            };
            try
            {
                // Replace
                TableResult result = CloudTableExtensions.ReplaceEntity(SomeTable, entity);
            }
            catch (ArgumentException)
            {
                // ArgumentException
                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        public void InsertOrReplaceEntity_異常系_引数のentityがnullの場合例外が発生する()
        {
            try
            {
                TableResult result = CloudTableExtensions.InsertOrReplaceEntity(SomeTable, null);
            }
            catch (ArgumentNullException)
            {
                // ArgumentNullException
                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        public void InsertOrReplaceEntity_異常系_対象がない場合例外が発生する()
        {
            SomeTableEntity entity = new SomeTableEntity()
            {
                PartitionKey = "pkey",
                RowKey = "rkey"
            };

            // InsertOrReplace
            TableResult result = CloudTableExtensions.InsertOrReplaceEntity(SomeTable, entity);

            // 検索して確認
            SomeTableEntity getEntity = CloudTableExtensions.RetrieveEntity<SomeTableEntity>(SomeTable, "pkey", "rkey");

            Assert.AreEqual(getEntity.PartitionKey, "pkey");
            Assert.AreEqual(getEntity.RowKey, "rkey");
        }

        [TestMethod]
        public void InsertOrReplaceEntity_正常系_標準テスト()
        {
            SomeTableEntity entity = new SomeTableEntity()
            {
                PartitionKey = "pkey",
                RowKey = "rkey"
            };

            // Insert
            TableResult result = CloudTableExtensions.InsertEntity(SomeTable, entity);

            entity.No = 2;

            // InsertOrReplace
            TableResult result2 = CloudTableExtensions.InsertOrReplaceEntity(SomeTable, entity);

            // 検索して確認
            SomeTableEntity getEntity = CloudTableExtensions.RetrieveEntity<SomeTableEntity>(SomeTable, "pkey", "rkey");

            Assert.AreEqual(getEntity.PartitionKey, "pkey");
            Assert.AreEqual(getEntity.RowKey, "rkey");
            Assert.AreEqual(getEntity.No, 2);
        }

        [TestMethod]
        public void RetrieveEntity_正常系_標準テスト_対象が存在る場合()
        {
            ITableEntity insertEntity = new SomeTableEntity()
            {
              PartitionKey = "pkey",
              RowKey = "rkey"
            };

            // Insert
            TableResult result = CloudTableExtensions.InsertEntity(SomeTable, insertEntity);


            SomeTableEntity getEntity = CloudTableExtensions.RetrieveEntity<SomeTableEntity>(SomeTable, "pkey", "rkey");

            Assert.AreEqual(getEntity.PartitionKey, "pkey");
            Assert.AreEqual(getEntity.RowKey, "rkey");

        }

        [TestMethod]
        public void RetrieveEntity_正常系_標準テスト_対象が存在しない場合()
        {

            SomeTableEntity getEntity = CloudTableExtensions.RetrieveEntity<SomeTableEntity>(SomeTable, "pkey", "rkey");
            Assert.IsNull(getEntity);

        }

    }
}
