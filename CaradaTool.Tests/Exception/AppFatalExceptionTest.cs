﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaTool.Exception;
using System;

namespace MTI.CaradaTool.Tests.Exception
{
    /// <summary>
    /// AppFatalException UTクラス.
    /// </summary>
    [TestClass]
    public class AppFatalExceptionTest
    {
        [TestMethod]
        public void AppFatalException_正常系_コンストラクタ()
        {
            var target = new AppFatalException("foo.txt", "msg");
            Assert.AreEqual("msg", target.Message);
            Assert.AreEqual("foo.txt", target.FileName);

            target = new AppFatalException("msg2", new ArgumentException("arg error."));
            Assert.AreEqual("msg2", target.Message);
            Assert.IsNull(target.FileName);
            Assert.IsInstanceOfType(target.GetBaseException(), typeof(ArgumentException));
            Assert.AreEqual("arg error.", target.GetBaseException().Message);
        }
    }
}
