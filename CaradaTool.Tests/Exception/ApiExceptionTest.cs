﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaTool.Exception;
using System.Net;

namespace MTI.CaradaTool.Tests.Exception
{
    /// <summary>
    /// ApiException UTクラス.
    /// </summary>
    [TestClass]
    public class ApiExceptionTest
    {
        [TestMethod]
        public void ApiException_正常系_コンストラクタ()
        {
            var target = new ApiException("msg", "carada_01", HttpStatusCode.Accepted);
            Assert.AreEqual("msg", target.Message);
            Assert.AreEqual("carada_01", target.CaradaId);
            Assert.AreEqual(HttpStatusCode.Accepted, target.StatusCode);
        }
    }
}
