﻿using Microsoft.WindowsAzure.Storage.Table;
using System;

namespace MTI.CaradaTool.Tests.TestHelpers
{
    /// <summary>
    /// Azure Table Storage を操作するための拡張メソッドのクラスです。
    /// </summary>
    public static class CloudTableExtensions
    {
        /// <summary>
        /// テーブルにエンティティを追加します。
        /// </summary>
        /// <param name="cloudTable">対象となるテーブル</param>
        /// <param name="entity">追加するエンティティ</param>
        /// <returns>TableResult</returns>
        public static TableResult InsertEntity(this CloudTable cloudTable, ITableEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            var insertOperation = TableOperation.Insert(entity);
            return ExecuteCloudTable(cloudTable, insertOperation);
        }

        /// <summary>
        /// テーブルのエンティティを削除します。
        /// </summary>
        /// <param name="cloudTable">対象となるテーブル</param>
        /// <param name="entity">削除するエンティティ</param>
        /// <returns>TableResult</returns>
        public static TableResult DeleteEntity(this CloudTable cloudTable, ITableEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entity.ETag = "*";
            var deleteOperation = TableOperation.Delete(entity);

            return ExecuteCloudTable(cloudTable, deleteOperation);
        }

        /// <summary>
        /// テーブルからエンティティを取得します。
        /// </summary>
        /// <typeparam name="T">対象となるテーブルの型</typeparam>
        /// <param name="cloudTable">対象となるテーブル</param>
        /// <param name="partitionKey">Partitionキー</param>
        /// <param name="rowKey">Rowキー</param>
        /// <returns>取得したエンティティ</returns>
        public static T RetrieveEntity<T>(this CloudTable cloudTable, string partitionKey, string rowKey) where T : ITableEntity
        {
            var retrieveOperation = TableOperation.Retrieve<T>(partitionKey, rowKey);
            var retrievedResult = ExecuteCloudTable(cloudTable, retrieveOperation);

            return (T)retrievedResult.Result;
        }

        /// <summary>
        /// エンティティを置換します。
        /// </summary>
        /// <param name="cloudTable">対象となるテーブル</param>
        /// <param name="entity">置き換えるエンティティ</param>
        /// <returns>TableResult</returns>
        public static TableResult ReplaceEntity(this CloudTable cloudTable, ITableEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            var updateOperation = TableOperation.Replace(entity);
            return ExecuteCloudTable(cloudTable, updateOperation);
        }

        /// <summary>
        /// エンティティが存在しなければ追加し存在すれば上書きします。
        /// </summary>
        /// <param name="cloudTable">対象となるテーブル</param>
        /// <param name="entity">追加または上書きするエンティティ</param>
        /// <returns>TableResult</returns>
        public static TableResult InsertOrReplaceEntity(this CloudTable cloudTable, ITableEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            var insertOperation = TableOperation.InsertOrReplace(entity);
            return ExecuteCloudTable(cloudTable, insertOperation);
        }

        private static TableResult ExecuteCloudTable(CloudTable cloudTable, TableOperation tableOperation)
        {
            var result = cloudTable.Execute(tableOperation);
            return result;
        }
    }
}