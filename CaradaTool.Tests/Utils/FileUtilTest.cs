﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaTool.Tests.TestHelpers;
using MTI.CaradaTool.Utils;
using System;
using System.IO;
using System.Linq;

namespace MTI.CaradaTool.Tests.Utils
{
    /// <summary>
    /// FileUtil UTクラス.
    /// </summary>
    [TestClass]
    public class FileUtilTest
    {
        private FileUtil target = new FileUtil();
        private const string parentDirPath = @".\utTmp\";
        private const string targetDirPath = parentDirPath + @"in\";
        private string[] fileNames = { "TEST1_20160120_CARADAID発行.tsv", "TEST2_20160120_CARADAID発行.tsv" };

        #region テスト前後処理

        [TestInitialize]
        public void TestInitialize()
        {
            target.CreateDirectoryIfNotExist(targetDirPath);
            foreach (string fileName in fileNames)
            {
                target.Touch(targetDirPath + fileName);
            }
        }

        [TestCleanup]
        public void TestCleanup()
        {
            FileHelper.DeleteDir(parentDirPath);
        }

        #endregion

        #region GetFileNames

        [TestMethod]
        public void GetFileNames_正常系_ファイルあり()
        {
            var results = target.GetFileNames(targetDirPath);
            Assert.AreEqual(2, results.Count);
            foreach (var item in results.Select((v, i) => new { v, i }))
            {
                Assert.AreEqual(fileNames[item.i], item.v);
            }
        }

        [TestMethod]
        public void GetFileNames_正常系_ファイルなし()
        {
            FileHelper.DeleteDir(targetDirPath);

            target.CreateDirectoryIfNotExist(targetDirPath);

            var results = target.GetFileNames(targetDirPath);
            Assert.AreEqual(0, results.Count);
        }

        [TestMethod]
        public void GetFileNames_正常系_ディレクトリなし()
        {
            FileHelper.DeleteDir(targetDirPath);

            var results = target.GetFileNames(targetDirPath);
            Assert.AreEqual(0, results.Count);
        }

        #endregion

        #region IsFileLocked

        [TestMethod]
        public void IsFileLocked_正常系_ロックされていない()
        {
            Assert.IsFalse(target.IsFileLocked(targetDirPath + "TEST1_20160120_CARADAID発行.tsv"));
        }

        [TestMethod]
        public void IsFileLocked_正常系_ロックされている()
        {
            var filePath = targetDirPath + "TEST1_20160120_CARADAID発行.tsv";
            using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
            {
                Assert.IsTrue(target.IsFileLocked(filePath));
            }
        }

        [TestMethod]
        public void IsFileLocked_異常系_引数のpathがnull()
        {
            Assert.IsTrue(target.IsFileLocked(null));
        }

        [TestMethod]
        public void IsFileLocked_異常系_引数のpathが空()
        {
            Assert.IsTrue(target.IsFileLocked(string.Empty));
        }

        #endregion

        #region CreateDirectoryIfNotExist

        [TestMethod]
        public void CreateDirectoryIfNotExist_正常系_指定ディレクトリが存在しないので作る()
        {
            FileHelper.DeleteDir(targetDirPath);

            Assert.IsFalse(Directory.Exists(targetDirPath));
            target.CreateDirectoryIfNotExist(targetDirPath);
            Assert.IsTrue(Directory.Exists(targetDirPath));
        }

        [TestMethod]
        public void CreateDirectoryIfNotExist_正常系_指定ディレクトリが存在するので作らない()
        {
            var filePath1 = targetDirPath + "TEST1_20160120_CARADAID発行.tsv";
            var filePath2 = targetDirPath + "TEST2_20160120_CARADAID発行.tsv";

            Assert.IsTrue(Directory.Exists(targetDirPath));
            // 中にファイルが存在していること
            Assert.IsTrue(File.Exists(filePath1));
            Assert.IsTrue(File.Exists(filePath2));

            target.CreateDirectoryIfNotExist(targetDirPath);
            Assert.IsTrue(Directory.Exists(targetDirPath));

            // 中にファイルが変わらず存在していること
            Assert.IsTrue(File.Exists(filePath1));
            Assert.IsTrue(File.Exists(filePath2));
        }

        [TestMethod]
        public void CreateDirectoryIfNotExist_異常系_引数のpathがnull空スペースでArgumentException()
        {
            // 全角スペースはDirectory.Existsでエラーとならない(trueになる)
            try
            {
                target.CreateDirectoryIfNotExist(null);
                Assert.Fail("例外が発生しない");
            }
            catch (System.Exception e)
            {
                Assert.IsInstanceOfType(e, typeof(ArgumentException));
            }
            try
            {
                target.CreateDirectoryIfNotExist(string.Empty);
                Assert.Fail("例外が発生しない");
            }
            catch (System.Exception e)
            {
                Assert.IsInstanceOfType(e, typeof(ArgumentException));
            }
            try
            {
                target.CreateDirectoryIfNotExist(" ");
                Assert.Fail("例外が発生しない");
            }
            catch (System.Exception e)
            {
                Assert.IsInstanceOfType(e, typeof(ArgumentException));
            }
        }

        #endregion

        #region Touch

        [TestMethod]
        public void Touch_正常系_指定ファイルが存在しないので作る()
        {
            var filePath = targetDirPath + "TEST1_20160120_CARADAID発行.tsv";
            File.Delete(filePath);
            Assert.IsFalse(File.Exists(filePath));
            target.Touch(filePath);
            Assert.IsTrue(File.Exists(filePath));
            var fInfo = new FileInfo(filePath);
            Assert.AreEqual(0, fInfo.Length);
        }

        [TestMethod]
        public void Touch_正常系_指定ファイルが存在するので作らない()
        {
            var filePath = targetDirPath + "TEST1_20160120_CARADAID発行.tsv";
            // 一旦削除
            File.Delete(filePath);
            // 中身のあるファイルを作る
            WriteTestTsv(filePath);
            // サイズを保持
            var fInfo = new FileInfo(filePath);
            var size = fInfo.Length;
            Assert.IsTrue(size > 0);

            target.Touch(filePath);
            Assert.IsTrue(File.Exists(filePath));
            var fInfoAfter = new FileInfo(filePath);
            Assert.AreEqual(size, fInfoAfter.Length);
        }

        #endregion

        #region private method

        /// <summary>
        /// テスト用TSVファイル出力.
        /// </summary>
        /// <remarks>
        /// 正常にTSVファイルが出力されていることをもってWriteLine関数のテストも兼ねる.
        /// </remarks>
        /// <param name="filePath"></param>
        private void WriteTestTsv(string filePath)
        {
            target.WriteLine(filePath, string.Format("{0}\t{1}", "key1", "value1"), "Shift_JIS");
            target.WriteLine(filePath, string.Format("{0}\t{1}", "key2", "value2"), "Shift_JIS");
        }

        #endregion
    }
}
