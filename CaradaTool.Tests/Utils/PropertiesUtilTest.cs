﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaTool.Tests.TestHelpers;
using MTI.CaradaTool.Utils;
using System.IO;
using System.Text;

namespace MTI.CaradaTool.Tests.Utils
{
    /// <summary>
    /// PropertiesUtil UTクラス.
    /// </summary>
    [TestClass]
    public class PropertiesUtilTest
    {
        private PropertiesUtil target = new PropertiesUtil();
        private FileUtil fileUtil = new FileUtil();
        private const string parentDirPath = @".\utTmp\";
        private const string targetDirPath = parentDirPath + @"in\";
        private string filePath = targetDirPath + "GetPropertiesTest_{0}_{1}.txt";

        [TestInitialize]
        public void TestInitialize()
        {
            string delimiter;
            string encode;

            fileUtil.CreateDirectoryIfNotExist(targetDirPath);

            delimiter = "\t";
            encode = "Shift_JIS";
            WriteTestTsv(string.Format(filePath, "tab", "Shift_JIS"), delimiter, encode);
            delimiter = ",";
            encode = "Shift_JIS";
            WriteTestTsv(string.Format(filePath, "comma", "Shift_JIS"), delimiter, encode);
            delimiter = "\t";
            encode = "UTF-8";
            WriteTestTsv(string.Format(filePath, "tab", "UTF-8 BOM付き"), delimiter, encode);
            delimiter = ",";
            encode = "UTF-8";
            WriteTestTsv(string.Format(filePath, "comma", "UTF-8 BOM付き"), delimiter, encode);
            delimiter = "\t";
            WriteTestTsv(string.Format(filePath, "tab", "UTF-8 BOMなし"), delimiter, null);
            delimiter = ",";
            WriteTestTsv(string.Format(filePath, "comma", "UTF-8 BOMなし"), delimiter, null);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            FileHelper.DeleteDir(parentDirPath);
        }

        [TestMethod]
        public void GetProperties_正常系_引数がfilePathのみ()
        {
            var props = target.GetProperties(string.Format(filePath, "tab", "Shift_JIS"));
            Assert.AreEqual(2, props.Count);
            Assert.IsTrue(props.ContainsKey("key1"));
            Assert.AreEqual("あいうえお", props["key1"]);
            Assert.IsTrue(props.ContainsKey("key2"));
            Assert.AreEqual("かきくけこ", props["key2"]);
        }

        [TestMethod]
        public void GetProperties_正常系_引数がfilePath_delimiter()
        {
            var props = target.GetProperties(string.Format(filePath, "tab", "Shift_JIS"), "\t");
            Assert.AreEqual(2, props.Count);
            Assert.IsTrue(props.ContainsKey("key1"));
            Assert.AreEqual("あいうえお", props["key1"]);
            Assert.IsTrue(props.ContainsKey("key2"));
            Assert.AreEqual("かきくけこ", props["key2"]);

            props = target.GetProperties(string.Format(filePath, "comma", "Shift_JIS"), ",");
            Assert.AreEqual(2, props.Count);
            Assert.IsTrue(props.ContainsKey("key1"));
            Assert.AreEqual("あいうえお", props["key1"]);
            Assert.IsTrue(props.ContainsKey("key2"));
            Assert.AreEqual("かきくけこ", props["key2"]);
        }

        [TestMethod]
        public void GetProperties_正常系_引数がfilePath_delimiter_encode()
        {
            var props = target.GetProperties(string.Format(filePath, "tab", "Shift_JIS"), "\t", "Shift_JIS");
            Assert.AreEqual(2, props.Count);
            Assert.IsTrue(props.ContainsKey("key1"));
            Assert.AreEqual("あいうえお", props["key1"]);
            Assert.IsTrue(props.ContainsKey("key2"));
            Assert.AreEqual("かきくけこ", props["key2"]);

            props = target.GetProperties(string.Format(filePath, "comma", "Shift_JIS"), ",", "Shift_JIS");
            Assert.AreEqual(2, props.Count);
            Assert.IsTrue(props.ContainsKey("key1"));
            Assert.AreEqual("あいうえお", props["key1"]);
            Assert.IsTrue(props.ContainsKey("key2"));
            Assert.AreEqual("かきくけこ", props["key2"]);

            props = target.GetProperties(string.Format(filePath, "tab", "UTF-8 BOM付き"), "\t", "UTF-8");
            Assert.AreEqual(2, props.Count);
            Assert.IsTrue(props.ContainsKey("key1"));
            Assert.AreEqual("あいうえお", props["key1"]);
            Assert.IsTrue(props.ContainsKey("key2"));
            Assert.AreEqual("かきくけこ", props["key2"]);

            props = target.GetProperties(string.Format(filePath, "comma", "UTF-8 BOM付き"), ",", "UTF-8");
            Assert.AreEqual(2, props.Count);
            Assert.IsTrue(props.ContainsKey("key1"));
            Assert.AreEqual("あいうえお", props["key1"]);
            Assert.IsTrue(props.ContainsKey("key2"));
            Assert.AreEqual("かきくけこ", props["key2"]);

            props = target.GetProperties(string.Format(filePath, "tab", "UTF-8 BOMなし"), "\t", "UTF-8");
            Assert.AreEqual(2, props.Count);
            Assert.IsTrue(props.ContainsKey("key1"));
            Assert.AreEqual("あいうえお", props["key1"]);
            Assert.IsTrue(props.ContainsKey("key2"));
            Assert.AreEqual("かきくけこ", props["key2"]);

            props = target.GetProperties(string.Format(filePath, "comma", "UTF-8 BOMなし"), ",", "UTF-8");
            Assert.AreEqual(2, props.Count);
            Assert.IsTrue(props.ContainsKey("key1"));
            Assert.AreEqual("あいうえお", props["key1"]);
            Assert.IsTrue(props.ContainsKey("key2"));
            Assert.AreEqual("かきくけこ", props["key2"]);
        }

        [TestMethod]
        public void GetProperties_異常系_ファイルのdelimiterと違うdelimiterを指定()
        {
            var props = target.GetProperties(string.Format(filePath, "tab", "Shift_JIS"), ",");
            Assert.AreEqual(0, props.Count);
        }

        private void WriteTestTsv(string filePath, string delimiter, string encode)
        {
            Encoding encoding;
            if (string.IsNullOrEmpty(encode))
            {
                // UTF-8, BOMなし
                encoding = new UTF8Encoding(false);
            }
            else
            {
                encoding = Encoding.GetEncoding(encode);
            }
            using (var sw = new StreamWriter(filePath, true, encoding))
            {
                sw.WriteLine(string.Format("{0}{1}{2}", "key1", delimiter, "あいうえお"));
                sw.WriteLine(string.Format("{0}{1}{2}", "key2", delimiter, "かきくけこ"));
            }
        }
    }
}
