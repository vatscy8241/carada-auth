﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaTool.Logics;
using System;

namespace MTI.CaradaTool.Tests.Logics
{
    /// <summary>
    /// DateTimeExtensions UTクラス.
    /// </summary>
    [TestClass]
    public class DateTimeExtensionsTest
    {
        [TestMethod]
        public void ToUnixTime_正常系_1970年1月1日0時0分0秒が0になること()
        {
            Assert.AreEqual((long)0, new DateTime(1970, 1, 1).ToUnixTime());
        }
    }
}
