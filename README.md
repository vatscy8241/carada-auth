# デプロイ手順等をここに記載すること。

# 概要

## プロジェクト名称

* Carada（CCP）認証認可プロバイダー

## 開発環境

* Visual Studio 2015
* Azure SDK 2.9

## 機能一覧

* .gitignoreによる管理不要ファイルの除隊
* 各環境（Local/Dev/STG/RC/Release）の設定切り替え
* IPアドレス制限（ホワイトリスト）
* Debugログ／Infoログ出力
* 監査ログ出力
* 各ログのストレージへの自動転送
* ABMによる監視のサポート


## デプロイ手順

* パッケージから、またはVSからデプロイする
* RDPで各インスタンスにログインし、下記ディレクトリを手動作成する。
C:\Resources\Directory\ApplicationLog
C:\Resources\Directory\AuditLog
* 作成したディレクトリのプロパティから、Security > EditでUserの権限をフルアクセスにする。


## マイグレーション用スクリプト

### 初回リリース(6/28 S-in)

#### 構築用

* RC

update-database-rc.sql

* 本番

update-database-release.sql

#### 切り戻し用

* 各環境共通

initial-database.sql


## 開発情報

[Development.md](./Development.md)参照。

