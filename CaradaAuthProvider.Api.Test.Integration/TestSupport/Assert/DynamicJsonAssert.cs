﻿using Codeplex.Data;

namespace CaradaAuthProvider.Api.Test.Integration.TestSupport.Assert
{
    /// <summary>
    /// DynamicJsonのアサート。
    /// </summary>
    public class DynamicJsonAssert
    {
        /// <summary>
        /// DynamicJsonの値を検証する。
        /// </summary>
        /// <param name="expected">検証値</param>
        /// <param name="actual">実行値</param>
        public static void AreEqual(string expected, dynamic actual)
        {
            AreDynamicEqual(DynamicJson.Parse(expected), actual, "ルート");
        }

        /// <summary>
        /// Dynamicな値を検証する。
        /// </summary>
        /// <param name="expected">検証値</param>
        /// <param name="actual">実行値</param>
        /// <param name="label">ラベル</param>
        private static void AreDynamicEqual(dynamic expected, dynamic actual, string label)
        {
            if (!expected is DynamicJson)
            {
                Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(expected, actual, string.Format("【{0}】の値が異なります。", label));
                return;
            }

            DynamicJson expectedJson = expected as DynamicJson;

            if (expectedJson.IsArray)
            {
                int position = 0;

                foreach (dynamic expectedMember in expected)
                {
                    if (!actual.IsDefined(position))
                    {
                        Microsoft.VisualStudio.TestTools.UnitTesting.Assert.Fail(string.Format("【{0}の{1}番目】のメンバ情報が存在しません。", label, position.ToString()));
                    }

                    AreDynamicEqual(expectedMember, actual[position], string.Format("{0}の{1}番目", label, position.ToString()));

                    position++;
                }
                return;
            }

            foreach (string memberName in expectedJson.GetDynamicMemberNames())
            {
                if (!actual.IsDefined(memberName))
                {
                    Microsoft.VisualStudio.TestTools.UnitTesting.Assert.Fail(string.Format("【{0}】のメンバ情報が存在しません。", memberName));
                }

                AreDynamicEqual(expected[memberName], actual[memberName], memberName);
            }
        }
    }
}
