﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Threading;

namespace CaradaAuthProvider.Api.Test.Integration.TestSupport.Server
{
    // <summary>
    /// IISExpressのプロセス管理クラス。
    /// </summary>
    public class IisExpress : IDisposable
    {
        /// <summary>
        /// 破棄するかどうか
        /// </summary>
        private Boolean _isDisposed;

        /// <summary>
        /// IISExpressのプロセス
        /// </summary>
        private Process _process;

        /// <summary>
        /// IISExpressの実行ファイル名
        /// </summary>
        private const string IIS_EXPRESS_EXE_NAME = "iisexpress.exe";

        /// <summary>
        /// IISExpressのホームディレクトリの環境変数名
        /// </summary>
        private const string MS_IIS_EXPRESS_HOME_ENVIRONMENT_VARIABLE_NAME = "MS_IIS_EXPRESS_HOME";

        /// <summary>
        /// 破棄する
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// IISExpressを起動する。
        /// </summary>
        /// <param name="directoryPath">ディレクトリパス</param>
        /// <param name="port">ポート番号</param>
        public void Start(String directoryPath, Int32 port)
        {
            string iisExpressHome = System.Environment.GetEnvironmentVariable(MS_IIS_EXPRESS_HOME_ENVIRONMENT_VARIABLE_NAME, EnvironmentVariableTarget.Machine);

            if (string.IsNullOrEmpty(iisExpressHome))
            {
                throw new System.Exception(string.Format("システム環境変数の「{0}」が指定されていません。", MS_IIS_EXPRESS_HOME_ENVIRONMENT_VARIABLE_NAME));
            }

            string iisExpressPath = Path.Combine(iisExpressHome, IIS_EXPRESS_EXE_NAME);

            String arguments = String.Format(CultureInfo.InvariantCulture, @"/path:""{0}"" /port:{1} /trace:i", directoryPath, port);
            Console.WriteLine(arguments);

            ProcessStartInfo info = new ProcessStartInfo(iisExpressPath)
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                ErrorDialog = true,
                LoadUserProfile = true,
                CreateNoWindow = false,
                UseShellExecute = false,
                Arguments = arguments
            };

            Thread startThread = new Thread(() => StartIisExpress(info))
            {
                IsBackground = true
            };
            startThread.Start();
        }

        /// <summary>
        /// 破棄する。
        /// </summary>
        /// <param name="disposing">破棄するかどうか</param>
        protected virtual void Dispose(Boolean disposing)
        {
            if (_isDisposed)
            {
                return;
            }

            if (disposing)
            {
                if (_process.HasExited == false)
                {
                    _process.CloseMainWindow();
                }

                _process.Dispose();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// IISExpressを起動する。
        /// </summary>
        /// <param name="info">プロセスの起動情報</param>
        private void StartIisExpress(ProcessStartInfo info)
        {
            try
            {
                _process = Process.Start(info);

                _process.WaitForExit();
            }
            catch (Exception)
            {
                Console.WriteLine("IISの初期化中にエラーが発生しました。");
                Dispose();
            }
        }
    }
}
