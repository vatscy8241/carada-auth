﻿using Codeplex.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web;

namespace CaradaAuthProvider.Api.Test.Integration.TestSupport.Http
{
    /// <summary>
    /// HTTP接続マネジャ
    /// </summary>
    public class HttpConnectionManager
    {
        /// <summary>
        /// パラメータとURLのセパレータ
        /// </summary>
        private const string PARAMETER_URL_SEPARATOR = "?";

        /// <summary>
        /// パラメータのセパレータ
        /// </summary>
        private const string PARAMETER_SEPARATOR = "&";

        /// <summary>
        /// パラメータのキーと値のセパレータ
        /// </summary>
        private const string PARAMETER_KEY_VALUE_SEPARATOR = "=";

        /// <summary>
        /// リクエストのコンテントタイプ(x-www-form-urlencoded用）
        /// </summary>
        private const string REQUEST_CONTENT_TYPE_URLENCODED = "application/x-www-form-urlencoded";

        /// <summary>
        /// コネクションタイムアウト値(ms)
        /// </summary>
        private int ConnectionTimeout = 60000;

        /// <summary>
        /// Read/Writeタイムアウト値(ms)
        /// </summary>
        private int ReadWriteTimeout = 60000;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HttpConnectionManager()
        {
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="connectionTimeout">コネクションタイムアウト</param>
        /// <param name="readWriteTimeout">Read/Writeタイムアウト</param>
        public HttpConnectionManager(int connectionTimeout, int readWriteTimeout)
        {
            ConnectionTimeout = connectionTimeout;
            ReadWriteTimeout = readWriteTimeout;
        }

        /// <summary>
        /// GETリクエストを送信する。
        /// </summary>
        /// <param name="url">リクエストURL</param>
        /// <param name="parameters">パラメータ</param>
        /// <returns>レスポンスの結果</returns>
        public dynamic DoGetRequest(string url, IDictionary<string, object> parameters, IDictionary<string, string> headers)
        {
            string requestUrl = MakeRequestUrl(url, parameters);

            return DoRequest(requestUrl, "GET", request =>
            {
                foreach (KeyValuePair<string, string> pair in headers)
                {
                    request.Headers.Add(pair.Key, pair.Value);
                }
            });
        }

        /// <summary>
        /// POSTリクエストを送信する。
        /// </summary>
        /// <param name="url">リクエストURL</param>
        /// <param name="parameters">パラメータ</param>
        /// <returns>レスポンスの結果</returns>
        public dynamic DoPostRequest(string url, dynamic parameters, IDictionary<string, string> headers)
        {
            byte[] postData = Encoding.UTF8.GetBytes(parameters.ToString().Replace("\\/", "/"));

            return DoRequest(url, "POST", request =>
            {
                request.ContentType = REQUEST_CONTENT_TYPE_URLENCODED;
                request.ContentLength = postData.Length;

                foreach (KeyValuePair<string, string> pair in headers)
                {
                    request.Headers.Add(pair.Key, pair.Value);
                }

                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(postData, 0, postData.Length);
                    requestStream.Flush();
                }
            });
        }

        /// <summary>
        /// DELETEリクエストを送信する。
        /// </summary>
        /// <param name="url">リクエストURL</param>
        /// <param name="parameters">パラメータ</param>
        /// <returns>レスポンスの結果</returns>
        public dynamic DoDeleteRequest(string url, dynamic parameters, IDictionary<string, string> headers)
        {
            byte[] deleteData = Encoding.UTF8.GetBytes(parameters.ToString().Replace("\\/", "/"));

            return DoRequest(url, "DELETE", request =>
            {
                request.ContentType = REQUEST_CONTENT_TYPE_URLENCODED;
                request.ContentLength = deleteData.Length;

                foreach (KeyValuePair<string, string> pair in headers)
                {
                    request.Headers.Add(pair.Key, pair.Value);
                }

                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(deleteData, 0, deleteData.Length);
                    requestStream.Flush();
                }
            });
        }

        /// <summary>
        /// POSTリクエストを送信する。
        /// </summary>
        /// <param name="url">リクエストURL</param>
        /// <param name="parameters">パラメータ</param>
        /// <returns>レスポンスの結果</returns>
        public dynamic DoPutRequest(string url, dynamic parameters, IDictionary<string, string> headers)
        {
            byte[] putData = Encoding.UTF8.GetBytes(parameters.ToString().Replace("\\/", "/"));

            return DoRequest(url, "PUT", request =>
            {
                request.ContentType = REQUEST_CONTENT_TYPE_URLENCODED;
                request.ContentLength = putData.Length;

                foreach (KeyValuePair<string, string> pair in headers)
                {
                    request.Headers.Add(pair.Key, pair.Value);
                }

                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(putData, 0, putData.Length);
                    requestStream.Flush();
                }
            });
        }

        /// <summary>
        /// リクエストを送信する。
        /// </summary>
        /// <param name="url">URL</param>
        /// <param name="method">HTTPのメソッド</param>
        /// <param name="additonalSetting">リクエストに追加の設定を行うメソッド</param>
        /// <returns>レスポンスの結果</returns>
        private dynamic DoRequest(string url, string method, Action<HttpWebRequest> additonalSetting)
        {
            HttpWebRequest request = null;

            try
            {
                request = WebRequest.Create(url) as HttpWebRequest;
                request.Method = method;
                request.KeepAlive = false;
                request.Timeout = ConnectionTimeout;
                request.ReadWriteTimeout = ReadWriteTimeout;

                additonalSetting(request);

                try
                {
                    using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                    {
                        using (Stream stream = response.GetResponseStream())
                        {
                            return new
                            {
                                StatusCode = response.StatusCode,
                                ResponseBody = DynamicJson.Parse(stream),
                                ResponseHeaders = response.Headers
                            };
                        }
                    }
                }
                catch (WebException e)
                {
                    var response = (HttpWebResponse)e.Response;
                    var stream = response.GetResponseStream();
                    var result = new
                    {
                        StatusCode = response.StatusCode,
                        ResponseBody = DynamicJson.Parse(stream),
                        ResponseHeaders = response.Headers
                    };
                    return result;
                }
            }
            finally
            {
                if (request != null)
                {
                    request.Abort();
                }
            }
        }

        /// <summary>
        /// リクエストURLを作成する。
        /// </summary>
        /// <param name="url">URL</param>
        /// <param name="parameters">パラメータ</param>
        /// <returns>リクエストURL</returns>
        private string MakeRequestUrl(string url, IDictionary<string, object> parameters)
        {
            if (parameters == null || parameters.Count == 0)
            {
                return url;
            }

            StringBuilder parameter = new StringBuilder();

            foreach (string key in parameters.Keys)
            {
                object value = parameters[key];

                if (value is List<string>)
                {
                    List<string> values = (List<string>)value;
                    foreach (string listValue in values)
                    {
                        parameter.Append(MakeOneParameterStr(key, listValue));
                    }
                }
                else if (value is string)
                {
                    parameter.Append(MakeOneParameterStr(key, (value as string)));
                }
            }

            // 不要な"&"の除去
            if (parameter.Length > 0)
            {
                parameter.Remove(parameter.Length - 1, 1);
            }

            return new StringBuilder().Append(url).Append(PARAMETER_URL_SEPARATOR).Append(parameter.ToString()).ToString();
        }

        /// <summary>
        /// 1つのパラメータ文字列を作成する。
        /// </summary>
        /// <param name="parameterKey">パラメータのキー</param>
        /// <param name="parameterValue">パラメータの値</param>
        /// <returns>1つのパラメータ文字列</returns>
        private string MakeOneParameterStr(string parameterKey, string parameterValue)
        {
            StringBuilder parameter = new StringBuilder();

            parameter.Append(parameterKey);
            parameter.Append(PARAMETER_KEY_VALUE_SEPARATOR);
            parameter.Append(HttpUtility.UrlEncode(parameterValue));
            parameter.Append(PARAMETER_SEPARATOR);

            return parameter.ToString();
        }
    }
}
