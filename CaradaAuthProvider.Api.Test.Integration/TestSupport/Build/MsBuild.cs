﻿using System;
using System.Diagnostics;
using System.IO;

namespace CaradaAuthProvider.Api.Test.Integration.TestSupport.Build
{
    /// <summary>
    /// MSBuild用のクラス。
    /// </summary>
    public static class MsBuild
    {
        /// <summary>
        /// DOTNETのホームディレクトリの環境設定パス
        /// </summary>
        private const string MS_DOTNET_HOME_ENVIRONMENT_VARIABLE_NAME = "MS_DOTNET_HOME";

        /// <summary>
        /// MSBuildの実行ファイル名
        /// </summary>
        private const string MS_BUILD_EXE_NAME = "MSBuild.exe";

        /// <summary>
        /// Azure用のMSBuildを行う。
        /// </summary>
        /// <param name="azureProjectPath">Azureプロジェクトのパス</param>
        public static void StartMsBuildForAzure(string azureProjectPath, string configuration)
        {
            string arguments = string.Format(@"{0} /p:Configuration={1} /p:PackageForComputeEmulator=true /p:PackageWebRole=True", azureProjectPath, configuration);
            StartMsBuild(arguments);
        }

        /// <summary>
        /// MSBuilを行う。
        /// </summary>
        /// <param name="projectPath">プロジェクトへのパス</param>
        /// <param name="configuration">ビルド構成</param>
        /// <param name="debugDefine">デバッグ定数を定義するかどうか。デフォルトはtrue。</param>
        public static void StartMsBuild(string projectPath, string configuration, bool debugDefine = true)
        {
            string arguments;

            if (debugDefine)
            {
                arguments = string.Format(@"""{0}"" /p:Configuration={1};OutputPath=bin\ /t:Clean;rebuild;TransformWebConfig /p:DefineConstants=DEBUG", projectPath, configuration);
            }
            else
            {
                arguments = string.Format(@"""{0}"" /p:Configuration={1};OutputPath=bin\ /t:Clean;rebuild;TransformWebConfig", projectPath, configuration);
            }

            StartMsBuild(arguments);
        }

        /// <summary>
        /// MSBuildを行う。
        /// </summary>
        /// <param name="arguments">実行時の引数</param>
        private static void StartMsBuild(string arguments)
        {
            string dotnetHome = System.Environment.GetEnvironmentVariable(MS_DOTNET_HOME_ENVIRONMENT_VARIABLE_NAME, EnvironmentVariableTarget.Machine);

            if (string.IsNullOrEmpty(dotnetHome))
            {
                throw new System.Exception(string.Format("システム環境変数の「{0}」が指定されていません。", MS_DOTNET_HOME_ENVIRONMENT_VARIABLE_NAME));
            }

            string msBuildRunExecutePath = Path.Combine(dotnetHome, MS_BUILD_EXE_NAME);

            ProcessStartInfo processStartInfo = new ProcessStartInfo(msBuildRunExecutePath, arguments)
            {
                RedirectStandardOutput = false,
                RedirectStandardError = true,
                UseShellExecute = false,
                CreateNoWindow = true,
                LoadUserProfile = true,
                WindowStyle = ProcessWindowStyle.Hidden
            };

            using (Process process = Process.Start(processStartInfo))
            {
                process.WaitForExit();

                using (StreamReader reader = process.StandardError)
                {
                    Console.WriteLine(reader.ReadToEnd());
                }
            }
        }
    }
}
