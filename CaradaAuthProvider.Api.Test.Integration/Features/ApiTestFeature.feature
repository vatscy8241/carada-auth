﻿@Api
Feature: ApiTest
 APIのテストを行う。
 正しくAPIリクエストを行うことができるかをテストする。

@Api
Scenario: POSTでAPIリクエストが送れる
    Given デフォルトヘッダを付与する
    Given POSTメソッドでアクセスする
    Given リクエストボディにパラメータを追加
        """
        {
            "key1":"value1",
            "key2":"value2"
        }
        """
    When /api/values/postにアクセスしたとき
    Then ステータスコードが401であること
    