﻿using TechTalk.SpecFlow;

namespace CaradaAuthProvider.Api.Test.Integration.Definitions
{
    /// <summary>
    /// APIの受け入れテスト定義クラス。
    /// </summary>
    [Binding, Scope(Tag = "Api")]
    public sealed class ApiTestDefinition : AbstractScenarioDefinition
    {
        #region Scenario SetUp/TearDown

        /// <summary>
        /// テストのシナリオ毎の初期化処理。
        /// </summary>
        [BeforeScenario]
        public override void SetUp()
        {
            base.SetUp();
        }

        /// <summary>
        /// テストのシナリオ毎の終了処理。
        /// </summary>
        [AfterScenario]
        public override void TearDown()
        {
            base.TearDown();
        }

        #endregion

    }
}
