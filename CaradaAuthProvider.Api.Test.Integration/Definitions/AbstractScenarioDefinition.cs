﻿using CaradaAuthProvider.Api.Test.Integration.TestSupport.Http;
using Codeplex.Data;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Net;
using TechTalk.SpecFlow;

namespace CaradaAuthProvider.Api.Test.Integration.Definitions
{
    /// <summary>
    /// シナリオ定義の抽象クラス。
    /// </summary>
    [Binding]
    public abstract class AbstractScenarioDefinition
    {
        /// <summary>
        /// APIに送信するヘッダのcharsetのデフォルト値
        /// </summary>
        private const string API_HTTP_HEADER_CHARSET_DEFAULT = "utf-8";

        /// <summary>
        /// Http通信マネジャ
        /// </summary>
        protected readonly HttpConnectionManager Http = new HttpConnectionManager();

        #region Scenario SetUp/TearDown

        /// <summary>
        /// テストのシナリオ毎の初期化処理。
        /// </summary>
        public virtual void SetUp()
        {
            ScenarioContext.Current["Parameter"] = new NameValueCollection();
            ScenarioContext.Current["Header"] = new Dictionary<string, string>();
        }

        /// <summary>
        /// テストのシナリオ毎の終了処理。
        /// </summary>
        public virtual void TearDown()
        {
        }

        #endregion

        #region Given Methods

        /// <summary>
        /// アクセスするHTTPメソッドを設定する。
        /// </summary>
        /// <example>
        /// Given (GET|POST|PUT|DELETE)メソッドでアクセスする
        /// </example>
        /// <param name="newHttpMethod">HTTPメソッド（GET,POST,PUT,DELETEのいずれか）</param>
        [Given(@"(.*)メソッドでアクセスする")]
        public void SetHttpMethod(string pHttpMethod)
        {
            ScenarioContext.Current["HttpMethod"] = pHttpMethod;
        }

        /// <summary>
        /// 共通で使用するヘッダを追加する。
        /// </summary>
        /// <example>
        /// Given デフォルトヘッダを付与する
        /// </example>
        [Given(@"デフォルトヘッダを付与する")]
        public void AddDefaultHeaders()
        {
            AddHeader("charset", API_HTTP_HEADER_CHARSET_DEFAULT);
        }

        /// <summary>
        /// JSON文字列からパラメータを追加する。
        /// ※POST・PUT時に使用する。
        /// </summary>
        /// <example>
        /// Given リクエストボディにパラメータを追加
        ///     """
        ///     {
        ///        Uid : "1234-5678-9012-3456",
        ///        Value : 
        ///        {   
        ///             
        ///           {   
        ///               Date : "2016/04/27",
        ///               Age : 28 
        ///           }   
        ///        }
        ///     }
        ///     """   
        /// </example>
        /// <param name="jsonString">JSON文字列</param>
        [Given(@"リクエストボディにパラメータを追加")]
        public void AddParameterForRequestBody(string jsonString)
        {
            ScenarioContext.Current["Parameter"] = DynamicJson.Parse(jsonString);
        }

        /// <summary>
        /// 同じURLに複数回連続アクセスする
        /// </summary>
        /// <param name="url">URL</param>
        /// <param name="count">アクセス回数</param>
        /// <example>
        /// Given /abc/xyzに9回アクセスする
        /// </example>
        [Given(@"(.*)に(\d+)回アクセスする")]
        public void AcessUrlRepeatly(string url, int count)
        {
            for (int i = 0; i < count; i++)
            {
                DoProcessHttpAccess(url);
            }

        }

        #endregion

        #region When Methods

        /// <summary>
        /// HTTPでアクセスを行う。
        /// </summary>
        /// <example>
        /// When /valuesにアクセスしたとき
        /// </example>
        /// <param name="url">URL</param>
        /// <exception cref="ArgumentNullException">Httpメソッドが指定されていない場合に例外を投げます</exception>
        [When(@"(.*)にアクセスしたとき")]
        public void DoProcessHttpAccess(string url)
        {
            var httpMethod = ScenarioContext.Current["HttpMethod"] as string;

            if (string.IsNullOrEmpty(httpMethod))
            {
                throw new MissingFieldException("HTTPメソッドの設定がされていません。HTTPメソッドの設定を行ってください。");
            }

            dynamic parameters = ScenarioContext.Current["Parameter"];
            dynamic headers = ScenarioContext.Current["Header"];

            if ("GET".Equals(httpMethod.ToUpper()))
            {
                dynamic requestQuery = ConvertToDictionary(parameters as NameValueCollection);
                ScenarioContext.Current["Response"] = Http.DoGetRequest(ConvertRelativeUrlToRequestUrl(url), requestQuery, headers);

                DumpResponse(ScenarioContext.Current["Response"]);

                return;
            }
            if ("POST".Equals(httpMethod.ToUpper()))
            {
                ScenarioContext.Current["Response"] = Http.DoPostRequest(ConvertRelativeUrlToRequestUrl(url), parameters, headers);

                DumpResponse(ScenarioContext.Current["Response"]);

                return;
            }
            if ("PUT".Equals(httpMethod.ToUpper()))
            {
                ScenarioContext.Current["Response"] = Http.DoPutRequest(ConvertRelativeUrlToRequestUrl(url), parameters, headers);

                DumpResponse(ScenarioContext.Current["Response"]);

                return;
            }
            if ("DELETE".Equals(httpMethod.ToUpper()))
            {
                dynamic requestQuery = ConvertToDictionary(parameters as NameValueCollection);
                ScenarioContext.Current["Response"] = Http.DoDeleteRequest(ConvertRelativeUrlToRequestUrl(url), parameters, headers);

                DumpResponse(ScenarioContext.Current["Response"]);

                return;
            }

            throw new NotSupportedException(string.Format("{0}のHTTPメソッドのアクセスはサポートしていません。", httpMethod));
        }

        #endregion

        #region Then Methods

        /// <summary>
        /// ステータスコードの検証を行う。
        /// </summary>
        /// <example>
        /// Then ステータスコードが200であること
        /// </example>
        /// <param name="expected">検証値</param>
        [Then(@"ステータスコードが(.*)であること")]
        public void AssertHttpStatusCode(int expected)
        {
            dynamic response = ScenarioContext.Current["Response"];
            int actual = (int)response.StatusCode;

            Assert.AreEqual(expected, actual);
        }

        #endregion

        #region protected Methods

        /// <summary>
        /// パラメータを追加する。
        /// </summary>
        /// <param name="key">パラメータのキー</param>
        /// <param name="value">パラメータの値</param>
        protected void AddParameter(string key, string value)
        {
            var parameter = ScenarioContext.Current["Parameter"] as NameValueCollection;

            if (parameter == null)
            {
                throw new InvalidOperationException("パラメータオブジェクトがシナリオコンテキストにありません。初期化をおこなってください。");
            }

            parameter[key] = value;
        }

        /// <summary>
        /// リクエストヘッダを追加する。
        /// </summary>
        /// <param name="name">ヘッダ名</param>
        /// <param name="value">ヘッダの値</param>
        protected void AddHeader(string name, string value)
        {
            var headers = ScenarioContext.Current["Header"] as IDictionary<string, string>;

            if (headers == null)
            {
                throw new InvalidOperationException("ヘッダオブジェクトがシナリオコンテキストにありません。初期化をおこなってください。");
            }

            headers[name] = value;
        }


        /// <summary>
        /// 実行された結果のレスポンスボディを取得する。
        /// </summary>
        /// <returns>レスポンスボディ</returns>
        protected dynamic GetActualResponseBody()
        {
            dynamic response = ScenarioContext.Current["Response"];

            return response.ResponseBody;
        }

        /// <summary>
        /// 実行されたAPIの結果オブジェクトを取得する。
        /// </summary>
        /// <returns>結果オブジェクト</returns>
        protected dynamic GetActualResponseResult()
        {
            var responseBody = GetActualResponseBody();

            return responseBody.result;
        }

        /// <summary>
        /// 実行されたAPIの結果メッセージオブジェクトを取得する。
        /// </summary>
        /// <returns>結果メッセージオブジェクト</returns>
        protected dynamic GetActualResponseResultMessages()
        {
            var responseBody = GetActualResponseBody();

            return responseBody.resultMessages;
        }

        /// <summary>
        /// 実行された結果のレスポンスコードを取得する。
        /// </summary>
        /// <returns>レスポンスコード</returns>
        protected int GetActualResponseCode()
        {
            dynamic response = ScenarioContext.Current["Response"];

            return response.ResponseCode;
        }

        /// <summary>
        /// 実行された結果のレスポンスヘッダを取得する。
        /// </summary>
        /// <returns>レスポンスヘッダ</returns>
        protected WebHeaderCollection GetActualResponseHeaders()
        {
            dynamic response = ScenarioContext.Current["Response"];

            return response.ResponseHeaders;
        }

        #endregion

        #region private Methods

        /// <summary>
        /// ディクショナリーに変換する。
        /// </summary>
        /// <param name="collection">コレクション</param>
        /// <returns>変換後のディクショナリー</returns>
        private IDictionary<string, object> ConvertToDictionary(NameValueCollection collection)
        {
            var dictionary = new Dictionary<string, object>();

            if (collection == null)
            {
                return dictionary;
            }

            foreach (var key in collection.AllKeys)
            {
                dictionary[key] = collection[key];
            }

            return dictionary;
        }

        /// <summary>
        /// 相対URLからリクエストURLに変換する。
        /// </summary>
        /// <param name="relativeUrl">相対URL</param>
        /// <returns>リクエストURL</returns>
        private string ConvertRelativeUrlToRequestUrl(string relativeUrl)
        {
            return ConfigurationManager.AppSettings["Test.Url.Base"] + relativeUrl;
        }

        /// <summary>
        /// レスポンスをコンソールにダンプする。
        /// </summary>
        /// <param name="response">レスポンス</param>
        private void DumpResponse(dynamic response)
        {
            Console.WriteLine("------------- start response dump ------------------");
            Console.WriteLine(response);
            Console.WriteLine("------------- end   response dump ------------------");
        }

        #endregion

    }
}
