﻿using CaradaAuthProvider.Api.Test.Integration.TestSupport.Build;
using CaradaAuthProvider.Api.Test.Integration.TestSupport.IO;
using CaradaAuthProvider.Api.Test.Integration.TestSupport.Server;
using System;
using System.IO;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace CaradaAuthProvider.Api.Test.Integration
{
    /// <summary>
    /// テスト全体の設定を行う。
    /// </summary>
    [Binding]
    public class TestConfiguration
    {
        /// <summary>
        /// IIS情報
        /// </summary>
        private static IisExpress iisExpress;

        /// <summary>
        /// テスト対象のアプリケーション名
        /// </summary>
        private const string TEST_APPLICATION_NAME = "CaradaAuthProvider.Api";

        /// <summary>
        /// テスト対象をデプロイするディレクトリ名
        /// </summary>
        private const string TEST_INTEGRATE_DEPLOY_DIRECTORY_NAME = ".Integrate.Tmp.CaradaAuthProvider.Api";

        /// <summary>
        /// IISExpressの起動設定ファイルのキー名。
        /// </summary>
        private const string IIS_EXPRESS_PORT_RUN_CONFIGURATION_KEY_NAME = "IIS.Express.Run.Port.Number";

        /// <summary>
        /// ビルド構成の設定ファイルのキー名。
        /// </summary>
        private const string BUILD_CONFIGURATION_KEY_NAME = "Build.Configuration.Name";

        /// <summary>
        /// ビルドのデバッグ定数を設定するかどうかのキー名。
        /// </summary>
        private const string BUILD_CONFIGUTATION_DEBUG_KEY_NAME = "Build.Configuration.Debug.Define";

        #region Initialize/CleanUp

        /// <summary>
        /// 初期化を行う
        /// </summary>
        [BeforeTestRun]
        public static void Initialize()
        {
            Console.WriteLine("テストの初期化処理を行います。");

            StartUpIisExpress();

            Console.WriteLine("テストの初期化処理が完了しました。");

        }

        /// <summary>
        /// クリーンアップを行う
        /// </summary>
        [AfterTestRun]
        public static void CleanUp()
        {
            Console.WriteLine("テストの終了処理を行います。");

            ShutdownIisExpress();

            Console.WriteLine("テストの終了処理が完了しました。");
        }

        #endregion

        #region private Methods

        /// <summary>
        /// IISExpressを起動する。
        /// </summary>
        private static void StartUpIisExpress()
        {
            Console.WriteLine("テスト対象プロジェクトのビルドを開始しました。");
            Task buildTask = Task.Factory.StartNew(() =>
            {
                var debugDefine = bool.Parse(System.Configuration.ConfigurationManager.AppSettings[BUILD_CONFIGUTATION_DEBUG_KEY_NAME]);
                MsBuild.StartMsBuild(Path.Combine(GetApplicationPath(TEST_APPLICATION_NAME), "CaradaAuthProvider.Api.csproj"), System.Configuration.ConfigurationManager.AppSettings[BUILD_CONFIGURATION_KEY_NAME], debugDefine);
            });
            Task.WaitAll(buildTask);

            DirectoryUtils.Delete(GetApplicationPath(TEST_INTEGRATE_DEPLOY_DIRECTORY_NAME));
            DirectoryUtils.Create(GetApplicationPath(TEST_INTEGRATE_DEPLOY_DIRECTORY_NAME), new FileAttributes[] { FileAttributes.Hidden });
            DirectoryUtils.Copy(GetApplicationPath(TEST_APPLICATION_NAME), GetApplicationPath(TEST_INTEGRATE_DEPLOY_DIRECTORY_NAME), true);
            File.Copy(Path.Combine(GetApplicationPath(TEST_APPLICATION_NAME), @"obj\" + System.Configuration.ConfigurationManager.AppSettings[BUILD_CONFIGURATION_KEY_NAME] + @"\TransformWebConfig\transformed\Web.config"), Path.Combine(GetApplicationPath(TEST_INTEGRATE_DEPLOY_DIRECTORY_NAME), "Web.config"), true);
            Console.WriteLine(string.Format("copy from {0} : to {1}", GetApplicationPath(TEST_APPLICATION_NAME), GetApplicationPath(TEST_INTEGRATE_DEPLOY_DIRECTORY_NAME)));
            Console.WriteLine(string.Format("copy from {0} : to {1}", Path.Combine(GetApplicationPath(TEST_APPLICATION_NAME), @"obj\" + System.Configuration.ConfigurationManager.AppSettings[BUILD_CONFIGURATION_KEY_NAME] + @"\TransformWebConfig\transformed\Web.config"), Path.Combine(GetApplicationPath(TEST_INTEGRATE_DEPLOY_DIRECTORY_NAME), "Web.config")));

            Console.WriteLine("テスト対象プロジェクトのビルドを終了しました。");

            Console.WriteLine("IISを起動します。");
            Task iisExpressTask = Task.Factory.StartNew(
                () =>
                {
                    string iisExpressPort = System.Configuration.ConfigurationManager.AppSettings[IIS_EXPRESS_PORT_RUN_CONFIGURATION_KEY_NAME];
                    iisExpress = new IisExpress();
                    iisExpress.Start(GetApplicationPath(TEST_INTEGRATE_DEPLOY_DIRECTORY_NAME), int.Parse(iisExpressPort));
                });

            Task.WaitAll(iisExpressTask);
            // FIXME: IISExpress起動までちょっと待つ。(IISの起動確認後処理を抜けるようにあとで修正する)
            System.Threading.Thread.Sleep(3000);
            Console.WriteLine("IISを起動しました。");
        }

        /// <summary>
        /// IISExpressをシャットダウンする。
        /// </summary>
        private static void ShutdownIisExpress()
        {
            Console.WriteLine("IISを停止します。");
            Task iisExpressTask = Task.Factory.StartNew(
                () =>
                {
                    iisExpress.Dispose();
                    iisExpress = null;
                });
            Task.WaitAll(iisExpressTask);

            try
            {
                DirectoryUtils.Delete(GetApplicationPath(TEST_INTEGRATE_DEPLOY_DIRECTORY_NAME));
            }
            catch (Exception e)
            {
                // 権限なくて削除できない場合がある。
                Console.WriteLine("テンポラリファイルの削除に失敗しましたが、無視します。");
                Console.WriteLine(e);
            }

            Console.WriteLine("IISを停止しました。");
        }

        /// <summary>
        /// アプリケーションのパスを取得する。
        /// </summary>
        /// <param name="applicationName">アプリケーション名</param>
        /// <returns>アプリケーションのパス</returns>
        private static string GetApplicationPath(string applicationName)
        {
            var solutionFolder = Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory)));
            return Path.Combine(solutionFolder, applicationName);
        }

        #endregion
    }
}
