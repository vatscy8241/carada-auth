﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using AzureStorageExtensions;

namespace NLog.Targets
{
    class LogEntry : ITableEntity
    {
        public string PartitionKey { get; set; }
        public string RowKey { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public string ETag { get; set; }
        public DateTime LogDateTime { get; set; }


        readonly Dictionary<string, string> _properties = new Dictionary<string, string>();
        public string this[string name]
        {
            get
            {
                string value;
                if (_properties.TryGetValue(name, out value))
                    return value;
                else
                    return null;
            }
            set { _properties[name] = value; }
        }

        public void ReadEntity(IDictionary<string, EntityProperty> properties, OperationContext operationContext)
        {
            ExpandableTableEntity.ShrinkDictionary(properties);
            foreach (var entityProperty in properties)
            {
                if (entityProperty.Key == "LogDateTime")
                {
                    this.LogDateTime = entityProperty.Value.DateTime.GetValueOrDefault();
                    continue;
                }
                if (entityProperty.Value.PropertyType != EdmType.String)
                    continue;
                switch (entityProperty.Key)
                {
                    case "PartitionKey":
                    case "RowKey":
                    case "ETag":
                        continue;
                }
                _properties.Add(entityProperty.Key, entityProperty.Value.StringValue);
            }
        }

        public IDictionary<string, EntityProperty> WriteEntity(OperationContext operationContext)
        {
            var dict = new Dictionary<string, EntityProperty>
            {
                {"LogDateTime", new EntityProperty(this.LogDateTime)}
            };
            foreach (var property in _properties)
                dict.Add(property.Key, new EntityProperty(property.Value));
            ExpandableTableEntity.ExpandDictionary(dict);
            return dict;
        }



        //public LogEntry()
        //{
        //    var now = DateTime.UtcNow;

        //    DateTime historicalDate = new DateTime(1970, 1, 1, 0, 0, 0);
        //    long ticksTillNow = now.Ticks - historicalDate.Ticks;
        //    string uniqId = ticksTillNow.ToString();

        //    PartitionKey = string.Format("{0:yyyy-MM-dd}", now);

        //    RowKey = string.Format("{0:HH:mm:ss.fff}_{1}", now, uniqId);
        //}

        //public string Message { get; set; }
        //public string Level { get; set; }
        //public string LoggerName { get; set; }
        //public string RoleInstance { get; set; }
        //public string DeploymentId { get; set; }
        //public string StackTrace { get; set; }
    }
}
