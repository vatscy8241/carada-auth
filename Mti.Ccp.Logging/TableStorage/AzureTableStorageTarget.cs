﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog.Common;
using NLog.Config;
using AzureStorageExtensions;


namespace NLog.Targets
{
    [Target("AzureTableStorage")]
    class AzureTableStorageTarget : NLog.Targets.Target
    {
        //private string _tableEndpoint;

        //[Required]
        //public string TableStorageConnectionStringName { get; set; }

        //[Required]
        //public string StorageAccountName { get; set; }

        //[Required]
        //public string StorageAccountKey { get; set; }

        //[Required]
        //public string StorageTableName { get; set; }





        [RequiredParameter]
        public string ConnectionName { get; set; }

        [RequiredParameter]
        public string TableName { get; set; }

        public Period Period { get; set; }
        public int RemoveAfter { get; set; }
        public bool SortAscending { get; set; }

        readonly List<AzureProperty> _properties = new List<AzureProperty>();
        [ArrayParameter(typeof(AzureProperty), "property")]
        public IList<AzureProperty> Properties
        {
            get { return _properties; }
        }


        SettingAttribute _setting;
        CloudClient _client;
        protected override void InitializeTarget()
        {
            InternalLogger.Debug("InitializeTarget called.");

            _setting = new SettingAttribute
            {
                Name = this.TableName,
                Period = this.Period,
                RemoveAfter = this.RemoveAfter,
            };
            _client = CloudClient.Get(this.ConnectionName);
            base.InitializeTarget();
        }


        protected override void Write(AsyncLogEventInfo[] logEvents)
        {
            try
            {
                var table = _client.GetGenericCloudTable<LogEntry>(_setting.Name, _setting);
                var key = CreateLogEntryPartitionKey(DateTime.UtcNow);
                var logEntries = logEvents.Select(log => CreateLogEntry(key, log.LogEvent));
                table.BulkInsert(logEntries, true);
                foreach (var info in logEvents)
                {
                    info.Continuation(null);
                }
            }
            catch (Exception ex)
            {
                foreach (var info in logEvents)
                {
                    info.Continuation(ex);
                }
            }
        }
        protected override void Write(LogEventInfo logEvent)
        {
            var table = _client.GetGenericCloudTable<LogEntry>(_setting.Name, _setting);
            var key = CreateLogEntryPartitionKey(DateTime.UtcNow);
            var logEntry = CreateLogEntry(key, logEvent);
            table.Insert(logEntry, true);
        }



        LogEntry CreateLogEntry(string key, LogEventInfo logEventInfo)
        {
            var logEntry = new LogEntry
            {
                PartitionKey = key,
                RowKey = CreateLogEntryRowKey(logEventInfo.TimeStamp),
                LogDateTime = logEventInfo.TimeStamp,
            };
            foreach (var property in _properties)
                logEntry[property.Name] = property.Value.Render(logEventInfo);
            return logEntry;
        }

        string CreateLogEntryPartitionKey(DateTime timestamp)
        {
            var now = timestamp;

            DateTime historicalDate = new DateTime(1970, 1, 1, 0, 0, 0);
            long ticksTillNow = now.Ticks - historicalDate.Ticks;

            return string.Format("{0:yyyy-MM-dd}", now);
        }

        string CreateLogEntryRowKey(DateTime timestamp)
        {
            var now = timestamp;

            DateTime historicalDate = new DateTime(1970, 1, 1, 0, 0, 0);
            long ticksTillNow = now.Ticks - historicalDate.Ticks;
            string uniqId = ticksTillNow.ToString();

            return string.Format("{0:HH:mm:ss.fff}_{1}", now, uniqId);
        }
    }
}
