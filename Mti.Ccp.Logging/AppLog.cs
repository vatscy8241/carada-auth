﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.IO;


namespace Mti.Cpp.Logging
{
    public class AppLog
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Debugレベルのログを出力します
        /// </summary>
        /// <param name="message">出力するメッセージです。</param>
        /// <param name="line">必ず省略して下さい。</param>
        /// <param name="path">必ず省略して下さい。</param>
        /// <param name="memberName">必ず省略して下さい。</param>
        public static void Debug(
            string message, 
            [CallerLineNumber] int line = 0, 
            [CallerFilePath]   string path = "", 
            [CallerMemberName] string memberName = "")
        {
            string output = string.Format("[{0}#{1}({2})] {3}", Path.GetFileName(path), memberName, line, message);

            logger.Debug(output);
        }

        /// <summary>
        /// Infoレベルのログを出力します。
        /// </summary>
        /// <param name="message"></param>
        /// <param name="line">省略すること</param>
        /// <param name="path">省略すること</param>
        /// <param name="memberName">省略すること</param>
        public static void Info(
            string message, 
            [CallerLineNumber] int line = 0,
            [CallerFilePath]   string path = "",
            [CallerMemberName] string memberName = "")
        {
            string output = string.Format("[{0}#{1}({2})] {3}", Path.GetFileName(path), memberName, line, message);

            logger.Info(output);
        }


        /// <summary>
        /// Errorレベルのログを出力します。
        /// </summary>
        /// <param name="ex">Exceptionを渡します。</param>
        /// <param name="message"></param>
        /// <param name="line"></param>
        /// <param name="path"></param>
        /// <param name="memberName"></param>
        public static void Error(
            Exception ex, 
            string message, 
            [CallerLineNumber] int line = 0,
            [CallerFilePath]   string path = "",
            [CallerMemberName] string memberName = "")
        {
            string output = string.Format("[{0}#{1}({2})] {3}", Path.GetFileName(path), memberName, line, message);

            logger.Error(ex, output);
        }

        /// <summary>
        /// Errorレベルのログを出力します。Exceptionの情報が無い場合はこちらを使用して下さい。
        /// </summary>
        /// <param name="message"></param>
        /// <param name="line"></param>
        /// <param name="path"></param>
        /// <param name="memberName"></param>
        public static void Error(
            string message, 
            [CallerLineNumber] int line = 0,
            [CallerFilePath]   string path = "",
            [CallerMemberName] string memberName = "")
        {
            string output = string.Format("[{0}#{1}({2})] {3}", Path.GetFileName(path), memberName, line, message);

            logger.Error(output);
        }

        /// <summary>
        /// 監査ログを出力します。
        /// </summary>
        /// <param name="userId">UIDまたは管理者・栄養士のID、バッチ等でIDが存在しない場合はnull.</param>
        /// <param name="operation">操作内容</param>
        /// <param name="detail">操作内容の詳細</param>
        /// <param name="line"></param>
        /// <param name="path"></param>
        /// <param name="memberName"></param>
        public static void Audit(
            string userId, 
            string operation, 
            string detail, 
            [CallerLineNumber] int line = 0,
            [CallerFilePath]   string path = "",
            [CallerMemberName] string memberName = "")
        {
            //NLog.Common.InternalLogger.Debug("internal> Audit() called.");
            //logger.Trace("*** AUDIT/SAMPLE ***");

            string output = string.Format("[{0}#{1}({2})] WHO:[{3}], OPE:[{4}], DETAIL:[{5}]", 
                Path.GetFileName(path), memberName, line, 
                userId, operation, detail);

            logger.Trace(output);
        }
    }
}
