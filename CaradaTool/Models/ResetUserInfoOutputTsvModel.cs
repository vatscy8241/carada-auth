﻿namespace MTI.CaradaTool.Models
{
    /// <summary>
    /// ユーザー情報リセット結果TSVファイルモデル.
    /// </summary>
    /// <remarks>UTクラスなし</remarks>
    public class ResetUserInfoOutputTsvModel
    {
        /// <summary>
        /// CARADA ID.
        /// </summary>
        public string CaradaId { get; set; } = string.Empty;

        /// <summary>
        /// パスワード.
        /// </summary>
        public string Password { get; set; } = string.Empty;

        /// <summary>
        /// 初期化フラグ.
        /// </summary>
        public string Initialization { get; set; } = string.Empty;

        /// <summary>
        /// エラーコード.
        /// </summary>
        public string ErrCd { get; set; } = string.Empty;

        /// <summary>
        /// エラー詳細.
        /// </summary>
        public string ErrDetail { get; set; } = string.Empty;
    }
}
