﻿using MTI.CaradaTool.Exception;
using MTI.CaradaTool.Logics;
using MTI.CaradaTool.Models;
using MTI.CaradaTool.Properties;
using MTI.CaradaTool.Resource;
using MTI.CaradaTool.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;

namespace MTI.CaradaTool.Job
{
    public class ResetUserInfoApiJob : IJob
    {
        /// <summary>
        /// コンソール・ログ出力ユーティリティ.
        /// </summary>
        private OutputUtil outputUtil = new OutputUtil();

        /// <summary>
        /// ファイルユーティリティ
        /// </summary>
        private FileUtil fileUtil = new FileUtil();

        /// <summary>
        /// JWT用シークレット情報.
        /// </summary>
        private JwtSecretInfo secretInfo;

        /// <summary>
        /// ツール用プロパティ.
        /// </summary>
        private ToolProperties propInfo;

        /// <summary>
        /// JWT使用API呼び出しクラス.
        /// </summary>
        private JwtApiRequest jwtRequest;

        /// <summary>
        /// ユーザー情報リセット結果TSVファイルパス.
        /// </summary>
        private string outputTsvPath;

        /// <summary>
        /// 処理結果.
        /// </summary>
        private bool executeResult;

        /// <summary>
        /// ユーザー情報リセット結果TSVファイルのヘッダ.
        /// </summary>
        private static string[] outputTsvHeader = { "CARADA ID", "パスワード", "リセット種別", "エラーコード", "エラー詳細" };

        /// <summary>
        /// コンストラクタ.
        /// </summary>
        /// <param name="secretInfo">JWT用シークレット情報</param>
        /// <param name="propInfo">ツール用プロパティ</param>
        public ResetUserInfoApiJob(JwtSecretInfo secretInfo, ToolProperties propInfo)
        {
            this.secretInfo = secretInfo;
            this.propInfo = propInfo;
            this.jwtRequest = new JwtApiRequest(secretInfo, propInfo);
        }

        /// <summary>
        /// 本処理前に実行する処理.
        /// </summary>
        public void BeforeExecute()
        {
            // 処理開始コンソール表示とログ出力
            outputUtil.OutputConsoleAndLog(LogLevelEnum.Info, string.Format(Message.MSG_JOB_START, secretInfo.Issuer));
        }

        /// <summary>
        /// 本処理後に実行する処理.
        /// </summary>
        public void AfterExecute()
        {
            if (executeResult)
            {
                // 正常ログ
                Console.ForegroundColor = ConsoleColor.Green;
                outputUtil.OutputConsoleAndLog(LogLevelEnum.Info, Message.MSG_JOB_RESET_SUCCESS);
                Console.ResetColor();
                return;
            }
            // 失敗ログはAPIエラーをハンドリングしている箇所に任せる
        }

        /// <summary>
        /// 処理実行.
        /// </summary>
        public void Execute()
        {
            bool isStart = true;
            string caradaId = null;
            while (isStart || !IsValidCaradaId(caradaId))
            {
                isStart = false;
                Console.Write("リセットする CARADA ID を入力してください。：");
                caradaId = Console.ReadLine();
#if CI
                // ITの標準入力は画面に表示されないため、値検証用として標準出力へ書き込む
                Console.WriteLine(caradaId);
#endif

            }

            Console.WriteLine("ユーザー情報を利用開始前状態へリセットしますか？");
            Console.WriteLine("1を入力：利用開始前状態へリセットする。");
            Console.WriteLine("2を入力：パスワードのみリセットする。");
            Console.WriteLine("未入力、その他の値を入力：キャンセル。");

            var initialization = Console.ReadLine();
#if CI
            // ITの標準入力は画面に表示されないため、値検証用として標準出力へ書き込む
            Console.WriteLine(initialization);
#endif
            if ("1".Equals(initialization, StringComparison.OrdinalIgnoreCase))
            {
                initialization = "1";
                Console.WriteLine(String.Format("CARADA ID：{0}、利用開始前リセット", caradaId));
            }
            else if ("2".Equals(initialization, StringComparison.OrdinalIgnoreCase))
            {
                initialization = "0";
                Console.WriteLine(String.Format("CARADA ID：{0}、パスワードリセット", caradaId));
            }
            else
            {
                // キャンセルログ
                outputUtil.OutputConsoleAndLog(LogLevelEnum.Info, Message.MSG_JOB_CANCEL);
                // アプリケーションを終了。コマンドプロンプトは閉じない。
                return;
            }

            Console.WriteLine("本当にリセットしてよろしいですか？");
            Console.Write("(Yを入力：OK。未入力、Y以外を入力：キャンセル。)：");

            var confirm = Console.ReadLine();

#if CI
            // ITの標準入力は画面に表示されないため、値検証用として標準出力へ書き込む
            Console.WriteLine(confirm);
#endif

            if (!"Y".Equals(confirm, StringComparison.OrdinalIgnoreCase))
            {
                // キャンセルログ
                outputUtil.OutputConsoleAndLog(LogLevelEnum.Info, Message.MSG_JOB_CANCEL);
                // アプリケーションを終了。コマンドプロンプトは閉じない。
                return;
            }

            Console.WriteLine("リセットしています．．．");

            try
            {
                // ユーザー情報リセット結果TSVファイルパス生成
                outputTsvPath = Path.Combine(Settings.Default.ResultTsvPath, CreateOutputTsvFileName());

                // API実行
                executeResult = ExecuteApi(caradaId, initialization);
            }
            catch (ApiException apie)
            {
                // エラー行出力
                OutputTsv(new ResetUserInfoOutputTsvModel
                {
                    CaradaId = apie.CaradaId,
                    ErrCd = ((int)apie.StatusCode).ToString(),
                    ErrDetail = apie.Message
                }, true);

                // 失敗ログ
                OutputApiFailConsoleAndLog(apie.Message);
            }
        }

        /// <summary>
        /// CARADA ID検証.
        /// </summary>
        /// <param name="caradaId">インプットのCARADA ID</param>
        /// <returns>正しいCARADA IDの場合、true. 正しくない値・null・空の場合、false.</returns>
        private bool IsValidCaradaId(string caradaId)
        {
            var msg = "";
            // CARADA IDが空(または空行)
            if (string.IsNullOrEmpty(caradaId))
            {
                msg = ErrorMessage.ERR_DETAIL_CARADA_ID_REQUIRED;
            }
            // 形式が合致しない
            else if ((caradaId.Length > Settings.Default.CaradaIdMaxLength)
                || !Regex.IsMatch(caradaId, Settings.Default.CaradaIdReg))
            {
                msg = ErrorMessage.ERR_DETAIL_CARADA_ID_FORMAT;
            }
            if (!string.IsNullOrEmpty(msg))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(msg);
                Console.ResetColor();
                return false;
            }
            return true;
        }

        /// <summary>
        /// ユーザー情報リセット結果TSVファイル名生成.
        /// </summary>
        /// <returns>ユーザー情報リセット結果TSVファイル名</returns>
        private string CreateOutputTsvFileName()
        {
            var sb = new StringBuilder();
            sb.Append("ユーザー情報リセット結果_");
            sb.Append(OutputUtil.ExecDateTime);
            sb.Append(".tsv");
            return sb.ToString();
        }

        /// <summary>
        /// API実行.
        /// </summary>
        /// <param name="caradaId">インプットのCARADA ID</param>
        /// <param name="initialization">インプットの初期化フラグ</param>
        /// <returns>次の処理に移る場合、true.処理をやめる場合、false.</returns>
        private bool ExecuteApi(string caradaId, string initialization)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("carada_id", caradaId);
            parameters.Add("initialization", initialization);
            var tryCount = 1;

            outputUtil.OutputLog(LogLevelEnum.Info, "Execute API : {0}", propInfo.Uri);

            while (true)
            {
                outputUtil.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}",
                    JsonConvert.SerializeObject(parameters), tryCount.ToString());

                // API実行
                var response = jwtRequest.PostJson(parameters);

                // ステータスコード200(OK)の場合
                if (response.IsSuccessStatusCode)
                {
                    SuccessProc(response, caradaId, tryCount);
                    return true;
                }
                // ステータスコード400(Bad Request)の場合
                if (HttpStatusCode.BadRequest.Equals(response.StatusCode))
                {
                    ApiErrorProc(response, caradaId, tryCount);
                    return false;
                }
                // ステータスコード503(Service Unavailable)の場合
                if (HttpStatusCode.ServiceUnavailable.Equals(response.StatusCode))
                {
                    // 処理続行不能
                    // エラー行出力
                    ApiErrorProc(response, caradaId, tryCount);
                    return false;
                }

                // 上記以外は想定したJSONが取得出来ないので、Responseをそのままログ出力
                outputUtil.OutputLog(LogLevelEnum.Info, string.Format("API Response : {0} TryCount:{1}", response, tryCount));

                // ステータスコード504(Gateway Timeout)の場合
                if (HttpStatusCode.GatewayTimeout.Equals(response.StatusCode))
                {
                    // API実行リトライ回数だけリトライする
                    if (Settings.Default.ApiRetryTimes > tryCount)
                    {
                        tryCount++;
                        continue;
                    }
                    // 処理続行不能
                    // エラー行出力
                    throw new ApiException(ErrorMessage.ERR_DETAIL_SERVER_ERR, caradaId, response.StatusCode);
                }
                // 上記以外のステータスコードの場合(処理続行不能)
                // エラー行出力
                throw new ApiException(ErrorMessage.ERR_DETAIL_SERVER_ERR, caradaId, response.StatusCode);

            }
        }
        /// <summary>
        /// API成功時処理.
        /// </summary>
        /// <param name="response">レスポンス</param>
        /// <param name="caradaId">インプットのCARADA ID</param>
        /// <param name="tryCount">API実行回数</param>
        private void SuccessProc(HttpResponseMessage response, string caradaId, int tryCount)
        {
            var apiResult = JToken.Parse(response.Content.ReadAsStringAsync().Result);
            outputUtil.OutputApiResultWithMask(LogLevelEnum.Info, apiResult,
                "API Response : {0} TryCount:" + tryCount);

            // インプットとアウトプットのCARADA IDが合致するかチェック
            if (caradaId.Equals((string)apiResult["carada_id"]))
            {
                // 正常行出力
                OutputTsv(new ResetUserInfoOutputTsvModel
                {
                    CaradaId = caradaId,
                    Password = (string)apiResult["password"],
                    Initialization = (string)apiResult["initialization"]
                });
            }
            else
            {
                // エラー行出力(例外的)
                OutputTsv(new ResetUserInfoOutputTsvModel
                {
                    CaradaId = caradaId,
                    Password = (string)apiResult["password"],
                    Initialization = (string)apiResult["initialization"],
                    ErrDetail = string.Format(ErrorMessage.ERR_DETAIL_CARADA_ID_MISMATCH, caradaId, (string)apiResult["carada_id"])
                });
            }
        }

        /// <summary>
        /// APIエラー時処理.
        /// </summary>
        /// <param name="response">レスポンス</param>
        /// <param name="caradaId">インプットのCARADA ID</param>
        /// <param name="tryCount">API実行回数</param>
        private void ApiErrorProc(HttpResponseMessage response, string caradaId, int tryCount)
        {
            var apiResult = JToken.Parse(response.Content.ReadAsStringAsync().Result);
            outputUtil.OutputApiResultWithMask(LogLevelEnum.Info, apiResult,
                "API Response : {0} TryCount:" + tryCount);

            var errDetail = ((string)apiResult["error_description"]).Trim();
            // エラー行出力
            OutputTsv(new ResetUserInfoOutputTsvModel
            {
                CaradaId = caradaId,
                ErrCd = (string)apiResult["error"],
                ErrDetail = errDetail,
            }, true);

            // 失敗ログ
            OutputApiFailConsoleAndLog(errDetail);
        }

        /// <summary>
        /// ユーザー情報リセット結果TSVファイル出力.
        /// </summary>
        /// <param name="model">ユーザー情報リセット結果TSVファイルモデル</param>
        /// <param name="isError">エラー行出力の時、true.</param>
        /// <param name="isBlank">空行出力の時、true.</param>
        private void OutputTsv(ResetUserInfoOutputTsvModel model, bool isError = false, bool isBlank = false)
        {
            // 出力先フォルダが存在しなければ作る
            fileUtil.CreateDirectoryIfNotExist(Settings.Default.ResultTsvPath);

            var header = new ResetUserInfoOutputTsvModel();
            header.CaradaId = outputTsvHeader[0];
            header.Password = outputTsvHeader[1];
            header.Initialization = outputTsvHeader[2];
            header.ErrCd = outputTsvHeader[3];
            header.ErrDetail = outputTsvHeader[4];
            WriteTsv(header);

            // データ行出力
            WriteTsv(model);
        }

        /// <summary>
        /// TSVファイル出力.
        /// </summary>
        /// <param name="model">ユーザー情報リセット結果TSVファイルモデル</param>
        private void WriteTsv(ResetUserInfoOutputTsvModel model)
        {
            try
            {
                var initialization = model.Initialization;
                if (!outputTsvHeader[2].Equals(initialization))
                {
                    if ("1".Equals(initialization))
                    {
                        initialization = "利用開始前リセット";
                    }
                    else if ("0".Equals(initialization))
                    {
                        initialization = "パスワードリセット";
                    }
                }
                fileUtil.WriteLine(outputTsvPath,
                    string.Format("{0}\t{1}\t{2}\t{3}\t{4}", model.CaradaId, model.Password, initialization, model.ErrCd, model.ErrDetail),
                    Settings.Default.TsvEncode);
            }
            catch (System.Exception e)
            {
                throw new AppFatalException(ErrorMessage.ERR_TSV_OUTPUT, e);
            }
        }

        /// <summary>
        /// APIでエラーが発生した場合にコンソールとログに出力する.
        /// </summary>
        /// <param name="errDetail">エラー詳細</param>
        private void OutputApiFailConsoleAndLog(string errDetail)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            outputUtil.OutputConsoleAndLog(LogLevelEnum.Error, ErrorMessage.ERR_RESET_FAIL);
            outputUtil.OutputConsoleAndLog(LogLevelEnum.Error, errDetail);
            Console.ResetColor();
        }
    }
}
