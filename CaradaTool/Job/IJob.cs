﻿namespace MTI.CaradaTool.Job
{
    /// <summary>
    /// Jobインターフェース
    /// このインターフェースを 各Job が継承します。
    /// 自動単体テストでモックを利用するためにインターフェースを継承しています。
    /// </summary>
    /// <remarks>UTクラスなし</remarks>
    public interface IJob
    {
        /// <summary>
        /// 本処理前に実行する処理.
        /// </summary>
        void BeforeExecute();

        /// <summary>
        /// 本処理実行.
        /// </summary>
        void Execute();

        /// <summary>
        /// 本処理後に実行する処理.
        /// </summary>
        void AfterExecute();
    }
}
