﻿using MTI.CaradaTool.Properties;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using NLog.Targets;
using System;
using System.Text.RegularExpressions;

namespace MTI.CaradaTool.Utils
{
    /// <summary>
    /// コンソール・ログ出力ユーティリティクラス.
    /// </summary>
    public class OutputUtil
    {
        /// <summary>
        /// ロガー.
        /// </summary>
        private static ILogger logger;

        /// <summary>
        /// 実行時間.
        /// </summary>
        public static string ExecDateTime { get; set; }

        /// <summary>
        /// コンストラクタ.
        /// </summary>
        public OutputUtil()
        {
            if (logger == null)
            {
                logger = LogManager.GetCurrentClassLogger();
                ExecDateTime = DateTime.Now.ToString("yyyyMMddHHmmssfff");

                // UTなどConfigurationが無い状態では設定をスキップする
                if (LogManager.Configuration != null)
                {
                    // ログファイル名設定
                    var target = LogManager.Configuration.FindTargetByName("logfile") as FileTarget;
                    target.FileName = string.Format(Settings.Default.LogFileNameFormat, ExecDateTime);
                    LogManager.ReconfigExistingLoggers();
                }
            }
        }

        /// <summary>
        /// コンソールとログ両方に出力する.
        /// </summary>
        /// <param name="loglevel">ログレベル</param>
        /// <param name="message">メッセージ</param>
        /// <param name="e">Exception(オプション)</param>
        public virtual void OutputConsoleAndLog(LogLevelEnum loglevel, string message, System.Exception e = null)
        {
            Console.WriteLine(message);
            OutputLog(loglevel, message);
            if (e != null)
            {
                var msg = GetAllExceptionMessage(e);
                Console.WriteLine(msg);
                OutputLog(loglevel, msg);
            }
        }

        /// <summary>
        /// ログレベルに応じてログ出力する.
        /// </summary>
        /// <param name="loglevel">ログレベル</param>
        /// <param name="message">メッセージ</param>
        public virtual void OutputLog(LogLevelEnum loglevel, string message)
        {
            switch (loglevel)
            {
                case LogLevelEnum.Fatal:
                    logger.Fatal(message);
                    return;
                case LogLevelEnum.Error:
                    logger.Error(message);
                    return;
                case LogLevelEnum.Warn:
                    logger.Warn(message);
                    return;
                case LogLevelEnum.Debug:
                    logger.Debug(message);
                    return;
                case LogLevelEnum.Trace:
                    logger.Trace(message);
                    return;
                case LogLevelEnum.Info:
                default:
                    logger.Info(message);
                    return;
            }
        }

        /// <summary>
        /// コンソールとログ両方に出力する.
        /// </summary>
        /// <param name="loglevel">ログレベル</param>
        /// <param name="format">メッセージのフォーマット</param>
        /// <param name="args">メッセージ引数</param>
        public virtual void OutputConsoleAndLog(LogLevelEnum loglevel, string format, params string[] args)
        {
            OutputConsoleAndLog(loglevel, string.Format(format, args));
        }

        /// <summary>
        /// ログ出力する.
        /// </summary>
        /// <param name="loglevel">ログレベル</param>
        /// <param name="format">メッセージのフォーマット</param>
        /// <param name="args">メッセージ引数</param>
        public virtual void OutputLog(LogLevelEnum loglevel, string format, params string[] args)
        {
            OutputLog(loglevel, string.Format(format, args));
        }

        /// <summary>
        /// ExceptionのメッセージをInnerExceptionがnullになるまで繰り返し改行しながら出力する.
        /// </summary>
        /// <param name="e">Exception</param>
        /// <returns>全ExceptionのMessage</returns>
        public virtual string GetAllExceptionMessage(System.Exception e)
        {
            if (e == null)
            {
                return string.Empty;
            }
            string ret = e.Message;
            var ex = e.InnerException;
            while (ex != null)
            {
                ret += "\r\n";
                ret += ex.Message;
                ex = ex.InnerException;
            }
            return ret;
        }

        /// <summary>
        /// APIの結果をログ出力する.
        /// </summary>
        /// <param name="loglevel">ログレベル</param>
        /// <param name="apiResult">Responseから取得したJToken</param>
        /// <param name="format">ログのフォーマット「API Response : {0} TryCount:{1}」を想定</param>
        public virtual void OutputApiResultWithMask(LogLevelEnum loglevel, JToken apiResult, string format)
        {
            if (apiResult != null)
            {
                format = string.IsNullOrEmpty(format) ? "{0}" : format;
                string jsonStr = JsonConvert.SerializeObject(apiResult);
                var regexes = Properties.Settings.Default.MaskKeyNameRegexes;
                foreach (var s in regexes.Split(','))
                {
                    var regex = new Regex("^" + s + "$", RegexOptions.IgnoreCase);

                    foreach (JProperty child in apiResult.Children())
                    {
                        if (regex.IsMatch(child.Name))
                        {
                            jsonStr = jsonStr.Replace(child.Value.ToString(), Settings.Default.MaskString);
                        }
                    }
                }
                OutputLog(loglevel, string.Format(format, jsonStr));
            }
        }
    }

    /// <summary>
    /// ログレベルのEnum.
    /// </summary>
    public enum LogLevelEnum
    {
        Fatal,
        Error,
        Warn,
        Info,
        Debug,
        Trace,
    }
}
