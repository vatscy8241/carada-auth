IF object_id(N'[dbo].[FK_dbo.SecurityQuestionAnswers_dbo.SecurityQuestionMasters_SecurityQuestionId]', N'F') IS NOT NULL
    ALTER TABLE [dbo].[SecurityQuestionAnswers] DROP CONSTRAINT [FK_dbo.SecurityQuestionAnswers_dbo.SecurityQuestionMasters_SecurityQuestionId]
IF object_id(N'[dbo].[FK_dbo.SecurityQuestionAnswers_dbo.AspNetUsers_UserId]', N'F') IS NOT NULL
    ALTER TABLE [dbo].[SecurityQuestionAnswers] DROP CONSTRAINT [FK_dbo.SecurityQuestionAnswers_dbo.AspNetUsers_UserId]
IF object_id(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]', N'F') IS NOT NULL
    ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
IF object_id(N'[dbo].[FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]', N'F') IS NOT NULL
    ALTER TABLE [dbo].[AspNetUserLogins] DROP CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
IF object_id(N'[dbo].[FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]', N'F') IS NOT NULL
    ALTER TABLE [dbo].[AspNetUserClaims] DROP CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
IF object_id(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]', N'F') IS NOT NULL
    ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
IF EXISTS (SELECT name FROM sys.indexes WHERE name = N'IX_DisplayOrder' AND object_id = object_id(N'[dbo].[SecurityQuestionMasters]', N'U'))
    DROP INDEX [IX_DisplayOrder] ON [dbo].[SecurityQuestionMasters]
IF EXISTS (SELECT name FROM sys.indexes WHERE name = N'IX_UserId' AND object_id = object_id(N'[dbo].[AspNetUserLogins]', N'U'))
    DROP INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]
IF EXISTS (SELECT name FROM sys.indexes WHERE name = N'IX_UserId' AND object_id = object_id(N'[dbo].[AspNetUserClaims]', N'U'))
    DROP INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]
IF EXISTS (SELECT name FROM sys.indexes WHERE name = N'UserNameIndex' AND object_id = object_id(N'[dbo].[AspNetUsers]', N'U'))
    DROP INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
IF EXISTS (SELECT name FROM sys.indexes WHERE name = N'IX_SecurityQuestionId' AND object_id = object_id(N'[dbo].[SecurityQuestionAnswers]', N'U'))
    DROP INDEX [IX_SecurityQuestionId] ON [dbo].[SecurityQuestionAnswers]
IF EXISTS (SELECT name FROM sys.indexes WHERE name = N'IX_UserId' AND object_id = object_id(N'[dbo].[SecurityQuestionAnswers]', N'U'))
    DROP INDEX [IX_UserId] ON [dbo].[SecurityQuestionAnswers]
IF EXISTS (SELECT name FROM sys.indexes WHERE name = N'IX_RoleId' AND object_id = object_id(N'[dbo].[AspNetUserRoles]', N'U'))
    DROP INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]
IF EXISTS (SELECT name FROM sys.indexes WHERE name = N'IX_UserId' AND object_id = object_id(N'[dbo].[AspNetUserRoles]', N'U'))
    DROP INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]
IF EXISTS (SELECT name FROM sys.indexes WHERE name = N'RoleNameIndex' AND object_id = object_id(N'[dbo].[AspNetRoles]', N'U'))
    DROP INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
DROP TABLE [dbo].[SecurityQuestionMasters]
DROP TABLE [dbo].[AspNetUserLogins]
DROP TABLE [dbo].[AspNetUserClaims]
DROP TABLE [dbo].[AspNetUsers]
DROP TABLE [dbo].[SecurityQuestionAnswers]
DROP TABLE [dbo].[AspNetUserRoles]
DROP TABLE [dbo].[AspNetRoles]
DROP TABLE [dbo].[Issuers]
DROP TABLE [dbo].[__MigrationHistory]
